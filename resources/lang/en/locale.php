<?php

return [
    "Dashboard" => "Dashboard",
    "Quotes" => "Quotes",
    "Orders" => "Orders",
    "Clients" => "Clients",
    "Employees" => "Employees",
    "Services" => "Services",
    "Suppliers" => "Suppliers"
];