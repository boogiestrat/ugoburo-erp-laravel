<?php

return [
    "Dashboard" => "Tableau de bord",
    "Quotes" => "Soumissions",
    "Orders" => "Commandes",
    "Clients" => "Clients",
    "Employees" => "Employés",
    "Services" => "Services",
    "Suppliers" => "Fournisseurs"
];