@extends('layouts/contentLayoutMaster', ['sectionIcon' => 'feather icon-briefcase'])

@section('title', 'Edit ' . $employe->first_name . ' ' . $employe->last_name)

@section('vendor-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/employes.css')) }}">
        
@endsection

@section('content')
<!-- Employee edit start -->
<section class="users-edit">
  <div class="card">
    <div class="card-content">
      <div class="card-body">
          <!-- Employee edit account form start -->
          <form novalidate action="{{ route('employes.update', $employe) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <div class="controls">
                    <label>Prénom</label>
                    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Prénom" value="{{ $employe->first_name }}" required
                      data-validation-required-message="This first name field is required">
                  </div>
                </div>
              </div>
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <div class="controls">
                    <label>Nom</label>
                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Nom" value="{{ $employe->last_name }}" required
                      data-validation-required-message="This last name field is required">
                  </div>
                </div>
              </div>
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <div class="controls">
                    <label>Courriel</label>
                    <input type="text" class="form-control" name="email" id="email" placeholder="Courriel" value="{{ $employe->email }}" required
                      data-validation-required-message="This email field is required">
                  </div>
                </div>
              </div>
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <div class="controls">
                    <label>Adresse</label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="Adresse" value="{{ $employe->address }}">
                  </div>
                </div>
              </div>
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <div class="controls">
                    <label>Ville</label>
                    <input type="text" class="form-control" name="city" id="city" placeholder="Ville" value="{{ $employe->city }}">
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <fieldset class="form-group">
                    <label>Province</label>
                    <select class="form-control" name="province">
                        <option>Province</option>
                        <option value="AB" {{ $employe->province == 'AB' ? ' selected' : '' }}>Alberta</option>
                        <option value="BC" {{ $employe->province == 'BC' ? ' selected' : '' }}>British Columbia</option>
                        <option value="MB" {{ $employe->province == 'MB' ? ' selected' : '' }}>Manitoba</option>
                        <option value="NB" {{ $employe->province == 'NB' ? ' selected' : '' }}>New Brunswick</option>
                        <option value="NL" {{ $employe->province == 'NL' ? ' selected' : '' }}>Newfoundland and Labrador</option>
                        <option value="NS" {{ $employe->province == 'NS' ? ' selected' : '' }}>Nova Scotia</option>
                        <option value="ON" {{ $employe->province == 'ON' ? ' selected' : '' }}>Ontario</option>
                        <option value="PE" {{ $employe->province == 'PE' ? ' selected' : '' }}>Prince Edward Island</option>
                        <option value="QC" {{ $employe->province == 'QC' ? ' selected' : '' }}>Quebec</option>
                        <option value="SK" {{ $employe->province == 'SK' ? ' selected' : '' }}>Saskatchewan</option>
                        <option value="NT" {{ $employe->province == 'NT' ? ' selected' : '' }}>Northwest Territories</option>
                        <option value="NU" {{ $employe->province == 'NU' ? ' selected' : '' }}>Nunavut</option>
                        <option value="YT" {{ $employe->province == 'YT' ? ' selected' : '' }}>Yukon</option>
                    </select>
                </fieldset>
            </div>
            <div class="col-md-6 col-12">
                <fieldset class="form-group">
                  <label>Pays</label>
                    <select class="form-control" name="country" value="{{ $employe->country }}">
                        <option>Pays</option>
                        <option value="Canada">Canada</option>
                    </select>
                </fieldset>
            </div>
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <div class="controls">
                    <label>Code postal</label>
                    <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="Code postal" value="{{ $employe->postal_code }}">
                  </div>
                </div>
              </div>
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <div class="controls">
                    <label>Cellulaire</label>
                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Cellulaire" value="{{ $employe->mobile }}">
                  </div>
                </div>
              </div>
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <label>Type d'employé</label>
                  <select class="form-control" name="level_id">
                    <option value="1" {{ $employe->level_id == '1' ? ' checked' : ''  }}>Niveau 1 - Accès spécifique</option>
                    <option value="2"{{ $employe->level_id == '2' ? ' checked' : ''  }}>Niveau 2 - Accès standard</option>
                    <option value="3"{{ $employe->level_id == '3' ? ' checked' : ''  }}>Niveau 3 - Accès administrateur</option>
                  </select>
                </div>
              </div>
              <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Sauvegarder</button>
              </div>
            </div>
          </form>
          <!-- Clients edit account form ends -->
      </div>
    </div>
  </div>
</section>
<!-- Clients edit ends -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/employes.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/navs/navs.js')) }}"></script>
@endsection

