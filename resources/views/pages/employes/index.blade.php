
@extends('layouts/contentLayoutMaster', ['sectionIcon' => 'feather icon-briefcase'])

@section('title', 'Employes')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        @toastr_css
@endsection

@section('content')
<div class="row mb-1">
    <div class="col-12">
        Liste d'employés <a href="{{ route('employes.create') }}" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1 ml-2">Créer un employé</a>
    </div>
  </div>
  <!-- table -->
  <section id="basic-datatable">
      <div class="row">
          <div class="col-12">
              <div class="card">
                  <div class="card-content">
                      <div class="card-body card-dashboard">
                          <div class="table-responsive">
                              <table class="table zero-configuration">
                                  <thead>
                                      <tr>
                                          <th>Employé</th>
                                          <th>Courriel</th>
                                          <th>Cellulaire</th>
                                          <th>Modifié le</th>
                                          <th>Options</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  	@foreach($employes as $employe)
                                      <tr>
                                          <td>
                                              <span class="avatar"><img src="images/portrait/small/{{ $employe->avatar ?? 'avatar.jpg' }}" height="32" width="32"></span>
                                            {{ $employe->first_name }} {{ $employe->last_name }}
                                        </td>
                                          <td>{{ $employe->email }}</td>
                                          <td>{{ $employe->mobile }}</td>
                                          <td>{{ $employe->created }}</td>
                                          <td>
                                              <a href="{{ route('employes.edit', $employe) }}"><i class="users-edit-icon feather icon-edit-1 mr-50"></i></a>
                                              <a href="{{ route('employes.destroy', $employe) }}"><i class="users-delete-icon feather icon-trash-2"></i></a>
                                          </td>
                                      </tr>
                                     @endforeach
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!--/ table -->
  
@endsection
@section('vendor-script')
{{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
    @toastr_js
    @toastr_render
@endsection

