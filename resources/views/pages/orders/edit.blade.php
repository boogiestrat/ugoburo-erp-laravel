@extends('layouts/contentLayoutMaster', ['sectionIcon' => 'feather icon-briefcase'])

@section('title', 'Edit ')

@section('vendor-style')
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/bootstrap-datetimepicker.min.css')) }}">
@endsection

@section('page-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/employes.css')) }}">
        
@endsection

@section('content')

<div class="ibox">
    <div class="ibox-body">
        <div class="row">
          <div class="col-6">
            <h5 class="mb-3">
              <a class="font-20 mx-2" href="#" id="open_pdf_version" target="_blank"><i class="la la-file-pdf-o"></i> # {{ $order->order_number ?? $order->magento2_order_id }}</a> <br/> <h6 id="order-email"></h6>
            </h4>
          </div>
          <div class="col-6 text-right">
            <h6 class="mb-3" id="quote-date"></h6> <span id="created_by"></span> <br/>
          </div>
        </div>
        <form id="history-add" action="/webservices/orders/histories/add.php" method="POST">
        <div class="row my-3">
          <div class="col-6">
            <h5 class="font-strong mb-1">FACTURATION</h5>
            <textarea class="form-control" name="order_bill_to" id="order_bill_to" style="resize:none;" rows="7">{{ $order->gt_base }}</textarea>
            <br/><button class="btn btn-success pull-right btn-sm " id="save_bill_to_address">Enregistrer</button>
          </div>
          <div class="col-6">
            <h5 class="font-strong mb-1">LIVRAISON</h5>
            <textarea class="form-control" name="order_ship_to" id="order_ship_to" style="resize:none;" rows="7"></textarea>
            <br/><button class="btn btn-success pull-right btn-sm " id="save_ship_to_address">Enregistrer</button>
          </div>
        </div>


        <input type="hidden" id="order_id" name="order_id"/>

        <hr>
        <br>

        <div id="order_items"></div>

        <hr>
        <br>

      
        <div class="col-12 col-xl-10">

        <div class="history container">
          <div class="row">
            <div class="col-12">
              <h3>Livraison</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-3 form-group" id="date_1">
              <label class="font-normal" for="delivery_date">Date de livraison prévue</label>
              <div class="input-group-icon input-group-icon-left">
                <span class="input-icon input-icon-left"><i class="fa fa-calendar"></i></span>
                <input class="form-control form-control-line" id="delivery_date" name="delivery_date" type="text" value="">
              </div>
            </div>
            <div class="col-9">
              <div class="d-flex  h-100 align-items-center">
                <input type="hidden" name="action" id="action" value="">
                <input type="hidden" name="lang" id="lang" value="">
                <button class="btn btn-success btn-sm " id="estimated-change-without-notif-js">Enregistrer</button> &nbsp;
                <button class="btn btn-success btn-sm " id="estimated-change-without-apmt-js">Enregistrer & notifier sans rendez-vous</button> &nbsp;
                <button class="btn btn-success btn-sm " id="estimated-change-js">Enregistrer et notifier avec rendez-vous</button>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="order_shipping_service">
                  Compagnie de livraison associée
                </label>
                <select name="carrier_assigned" id="carrier_assigned" class="selectpicker show-tick form-control" placeholder="Compagnie de livraison">
                    @foreach ($shippingSuppliers as $carrier)
                    <option value="{{ $carrier->id }}">{{ $carrier->name }}</option>
                    @endforeach
                </select>
              </div>
            </div>
            <div class="col-6">
              <div class="d-flex h-100 align-items-center">
                <!-- <button class="btn btn-success btn-sm " id="assigned-carrier-js">Assigner</button> -->
                <div class="form-group mx-3">
                  <label class="checkbox checkbox-primary" for="installation">
                    <input type="checkbox" id="installation" name="installation">
                    <span class="input-span"></span>Installation
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-5">
              <div class="form-group">
                <label for="acknow_no">Numéro de suivi</label>
                <input class="form-control" type="text" id="acknow_no" name="acknow_no" placeholder="Ackno."/>
              </div>
            </div>
            <div class="col-5">
              <div class="form-group">
                <label for="tracking_no">URL de suivi</label>
                <input class="form-control" type="text" id="tracking_no" name="tracking_no" placeholder="Tracking no."/>
              </div>
              <div class="form-group">
                <label class="checkbox checkbox-primary" for="notify_tracking">
                  <input type="checkbox" id="notify_tracking" name="notify_tracking">
                  <span class="input-span"></span>Notifier le client par courriel
                </label>
              </div>
            </div>
            <div class="col-2">

              <div class="d-flex h-100 align-items-center">
                <button class="btn btn-success btn-sm " id="save-shipping-js">Enregistrer</button>
              </div>
            </div>
          </div>

          <br>
          <hr>
          <br>

          <div class="row">
            <div class="col-12 my-2">
              <h3>Ajouter un commentaire</h3>
            </div>
          </div>
          <div class="row">
            <div class="col col-6">
              <div class="form-group">
                <label for="order_status_comment">Statut</label><br/>
                <select class="selectpicker form-control" name="order_status_comment" id="order_status_comment" title="Sélection" data-style="btn-solid" >
                    @foreach ($orderStatuses as $status)
                    <option value="{{ $status->id }}">{{ $status->desc_fr }}</option>
                    @endforeach
                </select>
              </div>
            </div>
            <div class="col col-6">
              <div class="form-group">
                <label for="comment_file">
                  Ajouter un fichier
                </label>
                <input type="file" class="form-control" id="comment_file" name="comment_file" />
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="form-group">
                <label for="comment">Commentaire</label>
                <textarea name="comment" id="comment" class="form-control"></textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="form-group">
                <label class="checkbox checkbox-primary" for="notify">
                  <input type="checkbox" id="notify" name="notify">
                  <span class="input-span"></span>Notifier le client par courriel
                </label>
              </div>
              <div class="form-group">
                <label class="checkbox checkbox-primary" for="front">
                  <input type="checkbox" id="front" name="front">
                  <span class="input-span"></span>Visible sur le site
                </label>
              </div>
              <!-- <div class="form-group">
                <label class="checkbox checkbox-primary" for="quickbooks">
                  <input type="checkbox" id="quickbooks" name="quickbooks">
                  <span class="input-span"></span>Exportée vers Quickbooks
                </label>
              </div> -->
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <button class="btn btn-success pull-right" id="add-new-history" type="submit">Ajouter</button>
            </div>
          </div>

          <div class="row">
            <div class="col-12 my-2">
              <h3>Historique</h3>
            </div>
          </div>
          <div id="histories">
          </div>
        </div>

      </div>
      <div class="col-12 col-xl-2 offset-xl-2 mt-4 mt-xl-0" style="border-left: 1px solid #ccc;padding-left: 0;margin-left: 7rem;">
        <div class="container">
        <div class="row" style="background:#fafccb;padding-top:20px;padding-bottom:20px;">
          <div class="col-12">

            <div class="row" >
              <div class="col-6 text-right">
                Soustotal
              </div>
              <div class="col-6 text-right" id="rightside_subtotal">
                0.00$
              </div>
            </div>

            <div class="row" >
              <div class="col-6 text-right">
                Livraison
              </div>
              <div class="col-6 text-right" id="rightside_shipping">
                0.00$
              </div>
            </div>

            <div class="row" >
              <div class="col-6 text-right">
                Taxes
              </div>
              <div class="col-6 text-right" id="rightside_tax">
                0.00$
              </div>
            </div>

            <div class="row" >
              <div class="col-6 text-right">
                <strong>Total</strong>
              </div>
              <div class="col-6 text-right" id="rightside_total">
                0.00$
              </div>
            </div>


          </div>
        </div>
        </div>
      </div>
      <input type="hidden" name="id" id="quote-id" disabled>
        </form>
    </div>
</div>
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/bootstrap-sweetalert/dist/sweetalert.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/orders.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/navs/navs.js')) }}"></script>
@endsection