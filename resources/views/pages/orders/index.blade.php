
@extends('layouts/contentLayoutMaster', ['sectionIcon' => 'feather icon-shopping-cart'])

@section('title', 'Commandes')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <p>Liste de commandes</p>
    </div>
  </div>
  <!-- table -->
  <section id="basic-datatable">
      <div class="row">
          <div class="col-12">
              <div class="card">
                  <div class="card-content">
                      <div class="card-body card-dashboard">
                          <div class="table-responsive">
                              <table class="table zero-configuration">
                                  <thead>
                                      <tr>
                                        <th>Commande #</th>
                                        <th>Source</th>
                                        <th>Date</th>
                                        <th>Facturé à</th>
                                        <th>Livré à</th>
                                        <th>Total ($)</th>
                                        <th>Statut</th>
                                        <th class="no-sort no-search text-center">Action</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                        
                                      @foreach($orders as $order)
                                      @php
                                          //dd($order)
                                      @endphp
                                      <tr>
                                          <td>{{ $order->magento2_order_id }}{{ $order->order_number }}</td>
                                          <td>{{ $order->order_source }}</td>
                                          <td>{{ $order->updated_at }}</td>
                                          <td>{{ $order->billingAddress->entreprise }}</td>
                                          <td>{{ $order->shippingAddress->entreprise }}</td>
                                          <td>{{ $order->gt_base }}</td>
                                          <td>{{ $order->status->desc_fr }}</td>
                                          <td>
                                              <a href="{{ route('orders.edit', $order) }}/?id={{ $order->id }}"><i class="users-edit-icon feather icon-edit-1 mr-50"></i></a>
                                              <a href="{{ route('orders.destroy', $order) }}"><i class="users-delete-icon feather icon-trash-2"></i></a>
                                          </td>
                                      </tr>
                                     @endforeach
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!--/ table -->
  
@endsection
@section('vendor-script')
{{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
@endsection
@section('page-script')
        {{-- Page js files --}}
        <script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
@endsection

