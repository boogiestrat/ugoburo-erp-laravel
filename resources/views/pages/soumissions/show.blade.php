@extends('layouts/contentLayoutMaster', ['sectionIcon' => 'feather icon-file-text'])
    
@section('title', 'Soumissions')
    
@section('vendor-style')
        {{-- vendor css files --}}
        <style>
    #quote-app {
      transition: transform 0.15s ease-in-out;
      padding-top:90px!important;
    }
    #quote-app #date-validUntil {
        padding: 10px !important;
    }
    
    #quote-app #date-date {
        padding: 10px !important;
      }
    
    #quote-app #quote-top-title {
        padding: 10px !important;
      }
    
    #quote-app .form-control-line {
        // border: none;
        border-width: 1px!important;
      }
    
    #quote-app .btn-float-right {
        float: right;
      }
    
    #quote-app .custom-display-hidden {
        display: none !important;
      }
    
    #quote-app .custom-full-width {
        width: 100%;
        padding: 0px;
        margin-left: 0px;
        margin-right: 0px;
      }
    
    #quote-app .custom-td-no-padding {
        padding: 0px;
      }
    
    #quote-app .custom-width-tenpc {
        width: 10%;
      }
    
    #quote-app .custom-width-tenpc.scale-back {
        transform: scale(.5);
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
    
    #quote-app .mb-2 {
      margin-bottom: 4px;
    }
    
    #quote-app .address-bill .ibox {
      height: 100%;
    }
    
    #quote-app .ibox {
      margin-bottom: 10px;
    }
    #quote-app .ibox.selected {
        border: 2px solid green;
        border-radius: 5px;
      }
    
    #quote-app .ibox.cursored {
        cursor: pointer;
    }
    
    #quote-app .btn {
      margin-right: 5px;
      margin-left: 5px;
    }
    
    #quote-app .btn-icon {
      font-weight: normal!important;
    }
    
    #quote-app .quote-paper-container {
      display: flex;
      justify-content: center;
    }
    #quote-app .quote-paper-container .from-position {
        border: 5px dashed $brand-secondary;
        background-color: lighten($color: $brand-secondary, $amount: 40);
        border-radius: 15px;
      }
    
    #quote-app .quote-paper-container .title span, #quote-app .quote-paper-container .bold span {
          font-weight: 700;
        }
    
    
    #quote-app .inline-form-line, select {
        margin: 5px 10px 2px 0px;
      }
    
    #quote-app .file {
        background-color: #ffffff;
        padding: 60px 60px 0 60px!important;
    }
    #quote-app .file .header-container {
          display: flex;
          flex-direction: row;
          justify-content: space-between;
          margin-bottom:40px;
    }
    #quote-app .file .header-container.right-align {
            display: flex;
            flex-direction: column;
            align-items: flex-end;
            width: 50%;
          }
    
    #quote-app .file .header-container.left-align {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
          }
    
    #quote-app .addresses-container {
          display: flex;
          flex-direction: row;
          justify-content: space-between;
          padding: 10px 0px;
    }
    #quote-app .addresses-container .address-bill {
            width: 100%;
    }
    #quote-app .addresses-container .ibox-body {
              height: 100%;
            }
    
    
    #quote-app .address-bill:first-child {
            margin-right: 10px;
          }
    #quote-app .address-ship {
            margin-left: 10px;
            width: 100%;
          }
    
    #quote-app .overview-container {
          margin-top: 20px;
        }
    
    #quote-app .actions-container {
          margin: 25px 0;
          display: flex;
          flex-direction: row;
          justify-content: space-around;
    }
    #quote-app .actions-container .btn {
            width: 16.6%;
            line-height: 1;
          }
    
    #quote-app .section-container {
          transition: height 0.15s ease-in-out, transform 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
          overflow: hidden;
          margin: 0;
          margin-bottom:20px;
    }
    #quote-app .section-container .form-control {
            padding: 5px;
    
    }
    #quote-app .section-container  h3, .can-click-on-listener {
            cursor: pointer;
    }
    #quote-app .section-container textarea {
            width: 100%;
            // height: 72px;
    }
    #quote-app .section-container .total-detail {
            /*display: flex;*/
            flex-direction: column;
    }
    #quote-app .section-container div, span {
              display: flex;
              flex-direction: row;
              justify-content: space-between;
            }
    
    }
    #quote-app .section-container div {
              height: 40px;
              align-items: baseline;
            }
    
    }
    #quote-app .section-container .form-control {
              text-align: right;
            }
    
    #quote-app .btn-add-section {
            margin-bottom: 10px;
          }
    
    #quote-app .section-header {
    
          }
    
    #quote-app .section-row {
            display: flex;
            flex-direction: row;
            align-items: center;
    }
    #quote-app .section-row.no-padding {
              padding: 0;
            }
    
    #quote-app .section-row a {
              margin-left: 10px;
              margin-right: 10px;
            }
    
    #quote-app .section-row table {
              width: 100%;
    }
    #quote-app .section-row table td input {
                  width: 100%;
                  padding-right: 0;
                }
    
    
    #quote-app .section-row .section-row-container {
              display: flex;
              flex-direction: row;
              align-items: center;
            }
    
    #quote-app .section-row span {
              font-weight: 700;
              margin-left: 10px;
              margin-right: 10px;
    }
    #quote-app .section-row span span {
                font-weight: 300;
              }
    
    #quote-app .section-row .section-description {
              width: 100%;
    }
    #quote-app .section-row .section-description span {
                display: flex;
                flex-direction: row;
    }
    #quote-app .section-row .section-description span strong {
                  display: none;
                }
    
    #quote-app .section-row .section-description span strong.invalid {
                border: 1px solid red;
    }
    #quote-app .section-row .section-description span strong.invalid span strong {
                    font-weight: 700;
                    color: red;
                    display: inherit;
            }
    
    #quote-app .section-row .section-description span .section-cost {
              width: 66%;
            }
    
    #quote-app .section-row .bottom-section .column {
              width: 25%;
              align-items: flex-end;
      }
    
    #quote-app .item .ibox-body {
          padding: 0;
    
        }
    #quote-app .table {
          margin-bottom: 0;
    }
    #quote-app .table td {
            vertical-align: inherit!important;
          }
    
    #quote-app .profit {
          justify-content: flex-end;
          padding-top: 10px!important;
          padding-right: 15px!important;
          font-size:0.9em;
        }
    #quote-app .item-picture-column {
          width: 95px;
        }
    #quote-app .item-picture {
          margin: 0;
        }
    #quote-app .item-tools p {
            text-align: center;
            margin-bottom: 0;
    }
    #quote-app .item-tools p i {
              margin:7px;
        }
    
        /* modifs demander par le client 31 janv 2020 */
        /* pour la section item seulement */
        /* 401-modification-de-la-ligne-item */
    
    #quote-app .ibox-body {
          border: 1px solid #999;
        }
    
    #quote-app .ibox-body input, #quote-app .ibox-body textarea, #quote-app .ibox-body select {
          border: none;
      }
    
    #quote-app .bordered {
        border: 1px solid $silver;
        padding: 30px;
      }
    
    #quote-app .ntb {
        border-top: 0px;
      }
    
    #quote-app .abs {
        background-color: #fff;
      }
    
    #quote-app .section-container .bordered {
          border-top-width: 0px;
    }
    #quote-app .section-container .bordered:first-of-type {
            border-top-width: 1px;
        }
    #quote-app .section-container .bordered th {
          padding: 5px;
      }
    
    #quote-app .section-container .top-bordered {
        border-top: 1px solid $silver;
        padding-top: 1.5em;
        display: flex;
        justify-content: flex-end;
      }
    
    #quote-app .section-container .inline-form-container {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
    }
    #quote-app .section-container .inline-form-container .inline-form-line {
          display: flex;
          flex-direction: row;
          align-items: center;
          width: 100%;
          margin: 5px 0px;
    }
    #quote-app .section-container .inline-form-container .inline-form-line input {
        width: 100%;
    }
    
    .flex-table {
      display: flex;
      flex-direction: column;
    }
      .flex-table .ft-row {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    }
    .flex-table .ft-row .col {
          width: 25%;
          padding:5px;
        }
    .flex-table .ft-row .col.title {
          background-color: #ebedee;
    }
    
    address {
      margin-bottom: 2px;
    
      p {
        margin-bottom: 2px;
        margin-top: 2px;
      }
    }
    
    .sb {
      justify-content: space-between;
    }
    
    .column {
      display: flex;
      flex-direction: column;
      padding: 5px;
      padding: 5px;
    }
    .column.fs {
        align-self: flex-start;
      }
    
      span {
        margin-left: 0!important;
      }
    
      input {
        padding-left: 10px!important;
      }
    
      .column-discount {
        width: 73%;
      }
    
      .discount-config {
        display: flex;
        flex-direction: row;
    
        .input-group {
          width: 70px;
        }
    
        input {
          padding-left: 3px!important;
          padding-right: 0px !important;
        }
    
    textarea {
      resize: none;
    }
    .grey-bg {
      border: none!important;
      background-color: rgba(73,147,171,.05);
    
    }
    
    .greyer-bg {
      background-color: #d3d3d3DD;
    }
    
    .grabber {
      padding: 10px;
    }
    .input-group {
      display: flex;
      flex-direction: row;
      align-items: center;
    
    }
    
    .checkbox span {
        font-style: italic;
        font-size: 12px;
      }
    
    
    .editing-bg {
      position: fixed;
      top: 0;
      left: 0;
      height: 100%;
      width: 100%;
      z-index: 10;
      background-color: rgba(0,0,0,.45)
    }
    
    .flex-center {
      display: flex;
      align-items: center;
      justify-content: center;
    }
    
    .inline {
      display: flex;
      flex-direction: row;
      justify-content: space-evenly;
    }
    .quote-double-size-textarea {
      height: 144px !important;
    }
    
    
        </style>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <p>Soumission</p>
    </div>
    </div>
    <!-- table -->
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Type</th>
                                            <th>Modifié le</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($clients as $client)
                                        <tr>
                                            <td>{{ $client->entreprise }}</td>
                                            <td>{{ $client->client_type_id }}</td>
                                            <td>{{ $client->created }}</td>
                                            <td>
                                                <a href="{{ route('clients.edit', $client) }}"><i class="users-edit-icon feather icon-edit-1 mr-50"></i></a>
                                                <a href="{{ route('clients.destroy', $client) }}"><i class="users-delete-icon feather icon-trash-2"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ table -->
    
@endsection
@section('vendor-script')
    {{-- vendor files --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.1/metisMenu.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.3/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/localization/messages_fr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/keypress/2.1.5/keypress.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.fr.min.js"></script>
    <script src="/js/datatables.min.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script src="/js/select2.full.min.js"></script>
    <script src="/js/fr.js"></script>
    <script src="/js/bootbox.js"></script>
    
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="/js/quote/2.76326d47.chunk.js"></script>
    <script src="/js/quote/main.149e8bbe.chunk.js"></script>
    <script src="/js/quote/runtime~main.a8a9905a.js"></script>
@endsection

