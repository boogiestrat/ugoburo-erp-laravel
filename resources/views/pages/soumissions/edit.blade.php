@extends('layouts/contentLayoutMaster', ['sectionIcon' => 'feather icon-file-text'])
    
@section('title', 'Soumissions')
    
@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="/vendors/css/line-awesome/css/line-awesome.min.css" />
        <style>
   /*! CSS Used from: https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css */
*,::after,::before{box-sizing:border-box;}
hr{box-sizing:content-box;height:0;overflow:visible;}
h1,h3,h4,h5,h6{margin-top:0;margin-bottom:.5rem;}
p{margin-top:0;margin-bottom:1rem;}
address{margin-bottom:1rem;font-style:normal;line-height:inherit;}
ul{margin-top:0;margin-bottom:1rem;}
strong{font-weight:bolder;}
a{color:#007bff;text-decoration:none;background-color:transparent;-webkit-text-decoration-skip:objects;}
a:hover{color:#0056b3;text-decoration:underline;}
img{vertical-align:middle;border-style:none;}
svg:not(:root){overflow:hidden;}
table{border-collapse:collapse;}
th{text-align:inherit;}
label{display:inline-block;margin-bottom:.5rem;}
button{border-radius:0;}
button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color;}
button,input,optgroup,select,textarea{margin:0;font-family:inherit;font-size:inherit;line-height:inherit;}
button,input{overflow:visible;}
button,select{text-transform:none;}
button,html [type=button]{-webkit-appearance:button;}
[type=button]::-moz-focus-inner,button::-moz-focus-inner{padding:0;border-style:none;}
textarea{overflow:auto;resize:vertical;}
h1,h3,h4,h5,h6{margin-bottom:.5rem;font-family:inherit;font-weight:500;line-height:1.2;color:inherit;}
h1{font-size:2.5rem;}
h3{font-size:1.75rem;}
h4{font-size:1.5rem;}
h5{font-size:1.25rem;}
h6{font-size:1rem;}
hr{margin-top:1rem;margin-bottom:1rem;border:0;border-top:1px solid rgba(0,0,0,.1);}
.quote-logo {background-color: #e84933;padding: 2rem 1rem;margin-bottom: 1rem;}
.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px;}
.col-md-12{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px;}
@media (min-width:768px){
.col-md-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%;}
}
.table{width:100%;max-width:100%;margin-bottom:1rem;background-color:transparent;}
.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;}
.table thead th{vertical-align:bottom;border-bottom:2px solid #dee2e6;}
.table-bordered{border:1px solid #dee2e6;}
.table-bordered td,.table-bordered th{border:1px solid #dee2e6;}
.table-bordered thead th{border-bottom-width:2px;}
.table-striped tbody tr:nth-of-type(odd){background-color:rgba(0,0,0,.05);}
.form-control{display:block;width:100%;padding:.375rem .75rem;font-size:1rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;}
@media screen and (prefers-reduced-motion:reduce){
.form-control{transition:none;}
}
.form-control::-ms-expand{background-color:transparent;border:0;}
.form-control:focus{color:#495057;background-color:#fff;border-color:#80bdff;outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,.25);}
.form-control::-webkit-input-placeholder{color:#6c757d;opacity:1;}
.form-control::-moz-placeholder{color:#6c757d;opacity:1;}
.form-control:-ms-input-placeholder{color:#6c757d;opacity:1;}
.form-control::-ms-input-placeholder{color:#6c757d;opacity:1;}
.form-control::placeholder{color:#6c757d;opacity:1;}
.form-control:disabled{background-color:#e9ecef;opacity:1;}
select.form-control:not([size]):not([multiple]){height:calc(2.25rem + 2px);}
.btn{display:inline-block;font-weight:400;text-align:center;white-space:nowrap;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;}
@media screen and (prefers-reduced-motion:reduce){
.btn{transition:none;}
}
.btn:focus,.btn:hover{text-decoration:none;}
.btn:focus{outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,.25);}
.btn:disabled{opacity:.65;}
.btn-primary{color:#fff;background-color:#007bff;border-color:#007bff;}
.btn-primary:hover{color:#fff;background-color:#0069d9;border-color:#0062cc;}
.btn-primary:focus{box-shadow:0 0 0 .2rem rgba(0,123,255,.5);}
.btn-primary:disabled{color:#fff;background-color:#007bff;border-color:#007bff;}
.btn-success{color:#fff;background-color:#28a745;border-color:#28a745;}
.btn-success:hover{color:#fff;background-color:#218838;border-color:#1e7e34;}
.btn-success:focus{box-shadow:0 0 0 .2rem rgba(40,167,69,.5);}
.btn-success:disabled{color:#fff;background-color:#28a745;border-color:#28a745;}
.btn-outline-success{color:#28a745;background-color:transparent;background-image:none;border-color:#28a745;}
.btn-outline-success:hover{color:#fff;background-color:#28a745;border-color:#28a745;}
.btn-outline-success:focus{box-shadow:0 0 0 .2rem rgba(40,167,69,.5);}
.btn-outline-success:disabled{color:#28a745;background-color:transparent;}
.btn-outline-danger{color:#dc3545;background-color:transparent;background-image:none;border-color:#dc3545;}
.btn-outline-danger:hover{color:#fff;background-color:#dc3545;border-color:#dc3545;}
.btn-outline-danger:focus{box-shadow:0 0 0 .2rem rgba(220,53,69,.5);}
.btn-outline-danger:disabled{color:#dc3545;background-color:transparent;}
.btn-sm{padding:.25rem .5rem;font-size:.875rem;line-height:1.5;border-radius:.2rem;}
.input-group{position:relative;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;-ms-flex-align:stretch;align-items:stretch;width:100%;}
.input-group>.form-control{position:relative;-ms-flex:1 1 auto;flex:1 1 auto;width:1%;margin-bottom:0;}
.input-group>.form-control:focus{z-index:3;}
.input-group>.form-control:not(:last-child){border-top-right-radius:0;border-bottom-right-radius:0;}
.close{float:right;font-size:1.5rem;font-weight:700;line-height:1;color:#000;text-shadow:0 1px 0 #fff;opacity:.5;}
.close:focus,.close:hover{color:#000;text-decoration:none;opacity:.75;}
button.close{padding:0;background-color:transparent;border:0;-webkit-appearance:none;}
.mx-2{margin-right:.5rem!important;}
.mb-2{margin-bottom:.5rem!important;}
.mx-2{margin-left:.5rem!important;}
.mt-3{margin-top:1rem!important;}
.mt-4{margin-top:1.5rem!important;}
.text-left{text-align:left!important;}
.text-right{text-align:right!important;}
.text-center{text-align:center!important;}
@media print{
*,::after,::before{text-shadow:none!important;box-shadow:none!important;}
a:not(.btn){text-decoration:underline;}
thead{display:table-header-group;}
img,tr{page-break-inside:avoid;}
h3,p{orphans:3;widows:3;}
h3{page-break-after:avoid;}
.table{border-collapse:collapse!important;}
.table td,.table th{background-color:#fff!important;}
.table-bordered td,.table-bordered th{border:1px solid #dee2e6!important;}
}
/*! CSS Used from: https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css */
.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
.fa-chevron-up:before{content:"\f077";}
.fa-arrows-alt:before{content:"\f0b2";}
.fa-ellipsis-v:before{content:"\f142";}
/*! CSS Used from: /vendors/css/line-awesome/css/line-awesome.min.css */
.la{display:inline-block;}
.la{font:normal normal normal 16px/1 LineAwesome;font-size:inherit;text-decoration:inherit;text-rendering:optimizeLegibility;text-transform:none;-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;font-smoothing:antialiased;}
.la-chevron-down:before{content:"\f184";}
.la-chevron-up:before{content:"\f187";}
.la-cloud-upload:before{content:"\f194";}
.la-file-pdf-o:before{content:"\f1e7";}
.la-files-o:before{content:"\f1f1";}
.la-plus-circle:before{content:"\f2c3";}
.la-save:before{content:"\f2e9";}
.la-search:before{content:"\f2eb";}
.la-trash-o:before{content:"\f34d";}
/*! CSS Used from: /vendors/css/themify-icons/css/themify-icons.css */
[class^="ti-"]{font-family:'themify';speak:none;font-style:normal;font-weight:normal;font-variant:normal;text-transform:none;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
.ti-trash:before{content:"\e605";}
/*! CSS Used from: /css/main.min.css?v=20200430 */
img,input,textarea{max-width:100%;}
a:focus,button:focus,input:focus,select:focus{outline:0;}
.btn,a,button{cursor:pointer;}
a{color:inherit;}
a:focus,a:hover{text-decoration:none;color:#2b95b6;}
h1,h3,h4,h5,h6{font-weight:400;}
h6,strong{font-weight:500;}
.btn{outline:0!important;}
.btn{overflow:hidden;}
.btn{border-radius:2px;-webkit-transition:all ease-in-out .15s;transition:all ease-in-out .15s;position:relative;padding:.65rem 1.25rem;}
.btn:active,.btn:focus{-webkit-box-shadow:none;box-shadow:none;}
.btn-air{-webkit-box-shadow:0 5px 20px #d6dee4;}
.btn-sm{padding:.35rem .75rem;border-radius:2px;}
.btn-air{box-shadow:0 5px 20px #d6dee4;}
.btn-air:hover{-webkit-box-shadow:none;box-shadow:none;}
.btn-rounded{border-radius:50px!important;}
.btn-primary:active,.btn-primary:disabled,.btn-primary:focus,.btn-primary:hover{color:#fff;background-color:#eb3a10;border-color:#eb3a10;-webkit-box-shadow:none;box-shadow:none;background-image:none;}
.btn-primary:active{background-color:#d3340f!important;border-color:#a4290b!important;}
.btn-success{color:#fff;background-color:#2b95b6;border-color:#2b95b6;}
.btn-success:active,.btn-success:disabled,.btn-success:focus,.btn-success:hover{color:#fff;background-color:#2684a1;border-color:#2684a1;-webkit-box-shadow:none;box-shadow:none;background-image:none;}
.btn-success:active{background-color:#21738d!important;border-color:#185163!important;}
.btn-outline-success{color:#2b95b6;background-image:none;background-color:transparent;border-color:#2b95b6;}
.btn-outline-success:active,.btn-outline-success:focus,.btn-outline-success:hover{color:#fff;background-color:#2b95b6;border-color:#2b95b6;-webkit-box-shadow:none;box-shadow:none;background-image:none;}
.btn-outline-danger{color:#f04c25;background-image:none;background-color:transparent;border-color:#f04c25;}
.btn-outline-danger:active,.btn-outline-danger:focus,.btn-outline-danger:hover{color:#fff;background-color:#f04c25;border-color:#f04c25;-webkit-box-shadow:none;box-shadow:none;background-image:none;}
.btn-icon:not(.btn-labeled){display:table;margin:0 auto;}
.btn-icon:not(.btn-labeled)>i{display:table-cell;vertical-align:middle;line-height:0;padding-right:.5rem;}
.btn-icon [class*=" la-"]{font-size:1.35rem;}
.btn-sm .btn-icon [class*=" la-"]{font-size:1.1rem;}
.ibox{position:relative;}
.ibox{margin-bottom:25px;background-color:#fff;-webkit-box-shadow:0 2px 4px rgba(0,0,0,.08);box-shadow:0 2px 4px rgba(0,0,0,.08);}
.ibox .ibox-head{padding:0 20px;border-bottom:1px solid #eee;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center;min-height:60px;}
.ibox .ibox-head .ibox-title{font-size:16px;font-weight:500;}
.ibox .ibox-body{padding:25px;}
.form-control{border-radius:0;border-color:rgba(0,0,0,.1);padding:.65rem 1.25rem;font-family:sans-serif,Arial;}
.form-control:focus,input:focus{border-color:#2b95b6;}
.form-control::-webkit-input-placeholder{color:#919ca8;}
.form-control::-moz-placeholder{color:#919ca8;}
.form-control:-ms-input-placeholder{color:#919ca8;}
.form-control::-ms-input-placeholder{color:#919ca8;}
.form-control::placeholder{color:#919ca8;}
.form-control-line{border-width:0 0 1px;padding-left:0;}
select.form-control:not([size]):not([multiple]){height:calc(2.55rem + 2px);}
.input-group-addon{border-color:#e9ecef;}
.input-group-addon{padding:0 1rem;color:#71808f;border-radius:2px!important;}
#quote-app .quote-paper-container .file .section-container .can-click-on-listener,#quote-app .quote-paper-container .file .section-container h3{cursor:pointer;}
.table>tbody>tr>td{border-top:1px solid #e8e8e8;vertical-align:middle;}
.table thead th{border-bottom-width:1px;border-top:0;font-weight:600;vertical-align:middle;}
.table thead.thead-lg th{padding:16px 10.5px;}
.table-bordered{border:1px solid #e8e8e8;}
.table-striped tbody tr:nth-of-type(odd){background-color:rgba(0,0,0,.04);}
.thead-default th{background:#ebedee;}
table td{padding:4px!important;}
#quote-app .address-bill .ibox,#quote-app .quote-paper-container .file .addresses-container .address-bill .ibox-body{height:100%;}
.fade-in-up{-webkit-animation:fadeInUp .5s;animation:fadeInUp .5s;}
#quote-app{-webkit-transition:-webkit-transform .15s ease-in-out;transition:-webkit-transform .15s ease-in-out;transition:transform .15s ease-in-out;transition:transform .15s ease-in-out,-webkit-transform .15s ease-in-out;padding-top:90px!important;}
#quote-app #date-date,#quote-app #date-validUntil,#quote-app #quote-top-title{padding:10px!important;}
#quote-app .form-control-line{border-width:1px!important;}
#quote-app .btn-float-right{float:right;}
#quote-app .custom-display-hidden{display:none!important;}
#quote-app .custom-full-width{width:100%;padding:0;margin-left:0;margin-right:0;}
#quote-app .mb-2{margin-bottom:4px;}
#quote-app .ibox{margin-bottom:10px;}
#quote-app .btn{margin-right:5px;margin-left:5px;}
#quote-app .btn-icon{font-weight:400!important;}
#quote-app .quote-paper-container{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;}
#quote-app .quote-paper-container .bold span{font-weight:700;}
#quote-app .quote-paper-container .inline-form-line,#quote-app .quote-paper-container select{margin:5px 10px 2px 0;}
#quote-app .quote-paper-container .file{background-color:#fff;padding:60px 60px 0!important;}
#quote-app .quote-paper-container .file .header-container{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;margin-bottom:40px;}
#quote-app .quote-paper-container .file .header-container .right-align{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-webkit-box-align:end;-ms-flex-align:end;align-items:flex-end;width:50%;}
#quote-app .quote-paper-container .file .header-container .left-align{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-webkit-box-align:start;-ms-flex-align:start;align-items:flex-start;}
#quote-app .quote-paper-container .file .addresses-container{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;padding:10px 0;}
#quote-app .quote-paper-container .file .addresses-container .address-bill{width:100%;}
#quote-app .quote-paper-container .file .addresses-container .address-bill:first-child{margin-right:10px;}
#quote-app .quote-paper-container .file .overview-container{margin-top:20px;}
#quote-app .quote-paper-container .file .section-container textarea{width:100%;}
#quote-app .quote-paper-container .file .section-container{-webkit-transition:height .15s ease-in-out,-webkit-transform .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;transition:height .15s ease-in-out,-webkit-transform .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;transition:height .15s ease-in-out,transform .15s ease-in-out,box-shadow .15s ease-in-out;transition:height .15s ease-in-out,transform .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-transform .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;overflow:hidden;margin:0 0 20px;}
.nav-will-be{-webkit-transition:all .2s ease-in-out;}
#quote-app .quote-paper-container .file .section-container .form-control{padding:5px;}
#quote-app .quote-paper-container .file .section-container .total-detail{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;}
#quote-app .quote-paper-container .file .section-container .total-detail div,#quote-app .quote-paper-container .file .section-container .total-detail span{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;}
#quote-app .quote-paper-container .file .section-container .total-detail div{height:40px;-webkit-box-align:baseline;-ms-flex-align:baseline;align-items:baseline;}
#quote-app .quote-paper-container .file .section-container .total-detail .form-control{text-align:right;}
#quote-app .quote-paper-container .file .section-container .section-row{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-webkit-box-align:center;-ms-flex-align:center;align-items:center;}
#quote-app .quote-paper-container .file .section-container .section-row .section-row-container{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-webkit-box-align:center;-ms-flex-align:center;align-items:center;}
#quote-app .quote-paper-container .file .section-container .section-row span{font-weight:700;margin-left:10px;margin-right:10px;}
#quote-app .quote-paper-container .file .section-container .section-row span span{font-weight:300;}
#quote-app .quote-paper-container .item .ibox-body{padding:0;border:1px solid #999;}
#quote-app .quote-paper-container .item .table{margin-bottom:0;}
#quote-app .quote-paper-container .item .table td{vertical-align:inherit!important;}
#quote-app .quote-paper-container .item .item-tools p{text-align:center;margin-bottom:0;}
#quote-app .quote-paper-container .item .item-tools p i{margin:7px;}
#quote-app .quote-paper-container .item input,#quote-app .quote-paper-container .item select,#quote-app .quote-paper-container .item textarea{border:none;}
#quote-app .quote-paper-container .section-container th{padding:5px;}
#quote-app .quote-paper-container .top-bordered{border-top:1px solid #bfbdbd;padding-top:1.5em;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end;}
#quote-app .quote-paper-container .inline-form-container{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-webkit-box-align:start;-ms-flex-align:start;align-items:flex-start;}
#quote-app .quote-paper-container .inline-form-container .inline-form-line{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-webkit-box-align:center;-ms-flex-align:center;align-items:center;width:100%;margin:5px 0;}
#quote-app .quote-paper-container .inline-form-container .inline-form-line input{width:100%;}
#quote-app address{margin-bottom:2px;}
#quote-app address p{margin-bottom:2px;margin-top:2px;}
#quote-app .sb{-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;}
#quote-app .input-group{-ms-flex-direction:row;-webkit-box-orient:horizontal;-webkit-box-direction:normal;}
#quote-app textarea{resize:none;}
#quote-app .grey-bg{border:none!important;background-color:rgba(73,147,171,.05);}
#quote-app .grabber{padding:10px;}
#quote-app .input-group{display:-webkit-box;display:-ms-flexbox;display:flex;flex-direction:row;-webkit-box-align:center;-ms-flex-align:center;align-items:center;}
#quote-app .input-group input{padding-left:10px;}
#quote-app .input-group .input-group-addon{padding:.5rem .75rem;margin-bottom:0;font-size:1rem;font-weight:400;line-height:1.25;color:#495057;text-align:center;border-radius:.25rem;margin-right:0!important;}
#quote-app .quote-double-size-textarea{height:144px!important;}
.nav-will-be{transition:all .2s ease-in-out;}
.nav-will-be.fixed{position:fixed;top: 81px;margin-left: 260px;background-color:#fff;z-index:2000;-webkit-box-shadow:0 2px 40px rgba(0,0,0,.1);box-shadow:0 2px 40px rgba(0,0,0,.1);font-size:.85em;}
.nav-will-be.fixed .btn{font-size:.9em;line-height:1;}
.nav-will-be.fixed .overview-wrapper{max-width:1278px;margin:0 auto;}
.nav-will-be.fixed .overview-wrapper ul{list-style:none;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;width:100%;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:space-evenly;-ms-flex-pack:space-evenly;justify-content:space-evenly;margin:0;height:60px;padding:0;}
.nav-will-be.fixed .overview-wrapper ul li span strong{font-weight:700;}
.nav-will-be.fixed div:first-child,.nav-will-be.fixed div:last-child{margin-top:0;border:0;}
@media only screen and (max-width:1366px){
#quote-app,#quote-app .form-control{font-size:9px;}
#quote-app .quote-paper-container .file .section-container .section-row span{margin-left:0;}
.btn-icon [class*=" la-"]{font-size:1em;}
.btn-icon:not(.btn-labeled)>i{padding-right:none;}
#quote-app .quote-paper-container .file .section-container .section-row .section-row-container{font-size:9px;}
#quote-app h1{font-size:2em;}
}
.font-20{font-size:20px!important;}
.page-content{padding-top:20px;margin-top:5px;}
tbody tr{cursor:pointer;}
.btn-primary{color:#fff;background-color:#666;border-color:#666;}
/*! CSS Used from: Embedded */
.cnODkf{display:flex;flex-direction:row;-webkit-box-align:center;align-items:center;}
.cnODkf span{margin-right:5px;}
.ZdcHa .ibox-title{display:flex;flex-direction:row;-webkit-box-pack:justify;justify-content:space-between;-webkit-box-align:center;align-items:center;width:100%;}
.ZdcHa .ibox-title .la{font-size:20pt;}
.bdMaNh{transition:all 0.3s ease-in-out 0s;height:100%;}
.jDwhkl{display:flex;width:100%;flex-direction:row;-webkit-box-align:center;align-items:center;-webkit-box-pack:justify;justify-content:space-between;}
.jzXEDn{display:flex;flex-direction:column;margin:0px 10px;text-align:right;}
.jzXEDn span:first-child{font-weight:700;}
.fnkoVv .ibox-title{display:flex;flex-direction:row;-webkit-box-pack:justify;justify-content:space-between;-webkit-box-align:center;align-items:center;width:100%;}
.fnkoVv .ibox-title .la{font-size:20pt;}
.iQACky{transition:all 0.3s ease-in-out 0s;height:100%;}
.iQACky.closed{height:0px;overflow:hidden;padding-top:0px;padding-bottom:0px;}
.fqtwkp{display:flex;width:100%;flex-direction:row;-webkit-box-align:center;align-items:center;-webkit-box-pack:justify;justify-content:space-between;}
.dwjffe{position:absolute;opacity:0;transform:scale(0);transition:all 0.3s ease-in-out 0s;background:rgb(255, 255, 255);top:70px;right:40px;padding:10px;box-shadow:rgba(0, 0, 0, 0) 0px 1px 0px 0px;z-index:999;}
.dwjffe .form-control{display:inline;width:auto;}
.jPbzsv{display:flex;flex-direction:row;border-top:none;border-right:none;border-left:none;border-image:initial;border-bottom:1px solid rgb(195, 195, 195);padding:10.5px;-webkit-box-pack:justify;justify-content:space-between;}
.ifEVnC{display:flex;flex-direction:row;}
.dVDPKF{display:flex;-webkit-box-align:center;align-items:center;}
.dVDPKF i{font-size:16pt;padding:0px 5px;}
.dVDPKF i.fa{font-size:13pt;}
.guOpDn{margin:0px 10px;display:flex;flex-direction:column;text-align:right;}
.guOpDn span:first-child{font-weight:700;}
.eCiPAS{transition:all 0.3s ease-in-out 0s;-webkit-box-pack:start;justify-content:flex-start;display:flex;flex-direction:column;}
.bzMSHp .ibox-title{display:flex;flex-direction:row;-webkit-box-pack:justify;justify-content:space-between;-webkit-box-align:center;align-items:center;width:100%;}
.bzMSHp .ibox-title .la{font-size:20pt;}
.dvKTfg{transition:all 0.3s ease-in-out 0s;height:100%;}
/*! CSS Used from: Embedded */
.css-2b097c-container{position:relative;box-sizing:border-box;}
.css-yk16xz-control{-webkit-box-align:center;align-items:center;background-color:rgb(255, 255, 255);border-color:rgb(204, 204, 204);border-radius:4px;border-style:solid;border-width:1px;cursor:default;display:flex;flex-wrap:wrap;-webkit-box-pack:justify;justify-content:space-between;min-height:38px;position:relative;transition:all 100ms ease 0s;box-sizing:border-box;outline:0px!important;}
.css-yk16xz-control:hover{border-color:rgb(179, 179, 179);}
.css-1hwfws3{-webkit-box-align:center;align-items:center;display:flex;flex:1 1 0%;flex-wrap:wrap;padding:2px 8px;position:relative;overflow:hidden;box-sizing:border-box;}
.css-1g6gooi{margin:2px;padding-bottom:2px;padding-top:2px;visibility:visible;color:rgb(51, 51, 51);box-sizing:border-box;}
.css-1wy0on6{-webkit-box-align:center;align-items:center;align-self:stretch;display:flex;flex-shrink:0;box-sizing:border-box;}
.css-1okebmr-indicatorSeparator{align-self:stretch;background-color:rgb(204, 204, 204);margin-bottom:8px;margin-top:8px;width:1px;box-sizing:border-box;}
.css-tlfecz-indicatorContainer{color:rgb(204, 204, 204);display:flex;padding:8px;transition:color 150ms ease 0s;box-sizing:border-box;}
.css-tlfecz-indicatorContainer:hover{color:rgb(153, 153, 153);}
.css-19bqh2r{display:inline-block;fill:currentcolor;line-height:1;stroke:currentcolor;stroke-width:0;}
.css-1uccc91-singleValue{color:rgb(51, 51, 51);margin-left:2px;margin-right:2px;max-width:calc(100% - 8px);overflow:hidden;position:absolute;text-overflow:ellipsis;white-space:nowrap;top:50%;transform:translateY(-50%);box-sizing:border-box;}
/*! CSS Used from: Embedded */
[data-react-beautiful-dnd-drag-handle="0"]{-webkit-touch-callout:none;-webkit-tap-highlight-color:rgba(0,0,0,0);touch-action:manipulation;}
[data-react-beautiful-dnd-droppable="0"]{overflow-anchor:none;}
/*! CSS Used from: Embedded */
[data-react-beautiful-dnd-drag-handle="0"]{cursor:-webkit-grab;cursor:grab;}
/*! CSS Used keyframes */
@-webkit-keyframes fadeInUp{0%{opacity:0;-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);}to{opacity:1;-webkit-transform:none;transform:none;}}
@keyframes fadeInUp{0%{opacity:0;-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);}to{opacity:1;-webkit-transform:none;transform:none;}}
@-webkit-keyframes fadeInUp{0%{opacity:0;-webkit-transform:translateY(20px);transform:translateY(20px);}100%{opacity:1;-webkit-transform:translateY(0);transform:translateY(0);}}
/*! CSS Used fontfaces */
@font-face{font-family:'FontAwesome';src:url('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.eot?v=4.7.0');src:url('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.eot#iefix&v=4.7.0') format('embedded-opentype'),url('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2?v=4.7.0') format('woff2'),url('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.woff?v=4.7.0') format('woff'),url('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.ttf?v=4.7.0') format('truetype'),url('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular') format('svg');font-weight:normal;font-style:normal;}
@font-face{font-family:LineAwesome;src:url(/vendors/css/line-awesome/fonts/line-awesome.eot?v=1.1.);src:url(/vendors/css/line-awesome/fonts/line-awesome.eot?v=1.1.#iefix) format("embedded-opentype"),url(/vendors/css/line-awesome/fonts/line-awesome.woff2?v=1.1.) format("woff2"),url(/vendors/css/line-awesome/fonts/line-awesome.woff?v=1.1.) format("woff"),url(/vendors/css/line-awesome/fonts/line-awesome.ttf?v=1.1.) format("truetype"),url(/vendors/css/line-awesome/fonts/line-awesome.svg?v=1.1.#fa) format("svg");font-weight:400;font-style:normal;}
@font-face{font-family:LineAwesome;src:url(/vendors/css/line-awesome/fonts/line-awesome.svg?v=1.1.#fa) format("svg");}
@font-face{font-family:'themify';src:url('/vendors/css/themify-icons/fonts/themify.eot');src:url('/vendors/css/themify-icons/fonts/themify.eot#iefix') format('embedded-opentype'), 		url('/vendors/css/themify-icons/fonts/themify.woff') format('woff'), 		url('/vendors/css/themify-icons/fonts/themify.ttf') format('truetype'), 		url('/vendors/css/themify-icons/fonts/themify.svg') format('svg');font-weight:normal;font-style:normal;}
    
        </style>
@endsection

@section('content')

    <div id="quote-app"></div>

@endsection
@section('vendor-script')
    {{-- vendor files --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.1/metisMenu.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.3/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/localization/messages_fr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/keypress/2.1.5/keypress.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.fr.min.js"></script>
    <script src="/js/datatables.min.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script src="/js/select2.full.min.js"></script>
    <script src="/js/fr.js"></script>
    <script src="/js/bootbox.js"></script>
    
@endsection
@section('page-script')
    {{-- Page js files --}}
    @php 
    foreach(scandir(public_path() . '/js/quote/') as $file) {
        if (explode('.', $file)[count(explode('.', $file)) - 1] == 'js'){
            echo '<script src="/js/quote/' . $file . '"></script>';
        }
    }
    @endphp
@endsection

