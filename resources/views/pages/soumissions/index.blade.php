
@extends('layouts/contentLayoutMaster', ['sectionIcon' => 'feather icon-file-text'])

@section('title', 'Soumissions')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        Liste de soumissions <a href="{{ route('quotes.create') }}" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1 ml-2">Créer une soumission</a>
    </div>
  </div>
  <!-- table -->
  <section id="basic-datatable">
      <div class="row">
          <div class="col-12">
              <div class="card">
                  <div class="card-content">
                      <div class="card-body card-dashboard">
                          <div class="table-responsive">
                              <table class="table zero-configuration">
                                  <thead>
                                      <tr>
                                          <th>Numéro</th>
                                          <th>Source</th>
                                          <th>Modifié le</th>
                                          <th>Options</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                        
                                      @foreach($quotes as $quote)
                                     @php
                                         $data = json_decode($quote->data, true);
                                     @endphp
                                      <tr>
                                          <td>{{ $data['number'] }}</td>
                                          <td>{{ $quote->sub_total }}</td>
                                          <td>{{ $quote->id}}</td>
                                          <td>
                                              <a href="/ventes/soumission/load.php?id={{ $data['number'] }}"><i class="users-edit-icon feather icon-edit-1 mr-50"></i></a>
                                              <a href="{{ route('quotes.destroy', $quote) }}"><i class="users-delete-icon feather icon-trash-2"></i></a>
                                          </td>
                                      </tr>
                                     @endforeach
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!--/ table -->
  
@endsection
@section('vendor-script')
{{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
@endsection
@section('page-script')
        {{-- Page js files --}}
        <script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
@endsection

