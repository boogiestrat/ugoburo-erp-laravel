
@extends('layouts/contentLayoutMaster', ['sectionIcon' => 'feather icon-users'])

@section('title', 'Clients')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection

@section('content')
<div class="row mb-1">
    <div class="col-12">
        Liste de clients <a href="{{ route('clients.create') }}" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1 ml-2">Créer un client</a>
    </div>
  </div>
  <!-- table -->
  <section id="basic-datatable">
      <div class="row">
          <div class="col-12">
              <div class="card">
                  <div class="card-content">
                      <div class="card-body card-dashboard">
                          <div class="table-responsive">
                              <table class="table zero-configuration">
                                  <thead>
                                      <tr>
                                          <th>Client</th>
                                          <th>Type</th>
                                          <td>Contact (courriel)</td>
                                          <th>Contact (tel)</th>
                                          <th>Code postal</th>
                                          <th>Province</th>
                                          <th>Représentant</th>
                                          <th>Crée le</th>
                                          <th>Modifié le</th>
                                          <th>Options</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      @foreach($clients as $client)
                                      <tr>
                                          <td>{{ $client->entreprise }}</td>
                                          <td>{{ $client->client_type_id }}</td>
                                          <td>{{ $client->defaultContact()['email'] }}</td>
                                          <td>{{ $client->defaultContact()['phone'] }}</td>
                                          <td>{{ $client->defaultAddress()['postal_code'] }}</td>
                                          <td>{{ $client->defaultAddress()['province'] }}</td>
                                          <td>{{ $client->employee_id }}</td>
                                          <td>{{ $client->created_at }}</td>
                                          <td>{{ $client->updated_at }}</td>
                                          <td>
                                              <a href="{{ route('clients.edit', $client) }}"><i class="users-edit-icon feather icon-edit-1 mr-50"></i></a>
                                              <a href="{{ route('clients.destroy', $client) }}"><i class="users-delete-icon feather icon-trash-2"></i></a>
                                          </td>
                                      </tr>
                                     @endforeach
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!--/ table -->
  
@endsection
@section('vendor-script')
{{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
@endsection
@section('page-script')
        {{-- Page js files --}}
        <script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
@endsection

