@extends('layouts/contentLayoutMaster', ['sectionIcon' => 'feather icon-users'])

@section('title', 'Edit ' . $client->entreprise)

@section('vendor-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/clients.css')) }}">
        
@endsection

@section('content')
<!-- Client edit start -->
<section class="users-edit">
  <div class="card">
    <div class="card-content">
      <div class="card-body">
          <!-- Client edit account form start -->
          <form novalidate action="{{ route('clients.update', $client) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <div class="controls">
                    <label>Nom de l'entreprise</label>
                    <input type="text" class="form-control" name="entreprise" id="entreprise" placeholder="Nom de l'entreprise" value="{{ $client->entreprise }}" required
                      data-validation-required-message="This username field is required">
                  </div>
                </div>
              </div>
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <label>Type d'entreprise</label>
                  <select class="form-control" name="client_type_id">
                    <option value="1" {{ $client->client_type_id == '1' ? ' checked' : ''  }}>Particulier</option>
                    <option value="2"{{ $client->client_type_id == '2' ? ' checked' : ''  }}>Entreprise</option>
                    <option value="3"{{ $client->client_type_id == '3' ? ' checked' : ''  }}>Gouvernement</option>
                  </select>
                </div>
              </div>
              <div class="col-12">
                <label>Notes</label>
                <fieldset class="form-group">
                    <textarea class="form-control" id="notes"name="notes" rows="3" placeholder="Notes">{{ $client->notes }}</textarea>
                </fieldset>
              </div>
            </div>        
            <div class="row">
              <div class="col-12">
                  <h6 class="py-1 mx-1 mb-0 font-medium-2">
                    <i class="feather icon-home mr-50"></i>Adresses
                  </h6>
                  <div id="addresses">
                    @foreach($client->addresses as $key => $address)
                      <div class="address-entry inline-form row border rounded px-1 py-1 mb-1">
                          <div class="col-12 col-md-6">
                              <input type="hidden" name="addresses[{{$key}}][id]" value="{{ $address->id }}">
                              <input class="form-control mb-1" name="addresses[{{$key}}][address]" type="text" placeholder="Adresse" value="{{ $address->address }}" />
                          </div>
                          <div class="col-md-6 col-12">
                              <input class="form-control mb-1" name="addresses[{{$key}}][city]" type="text" placeholder="Ville" value="{{ $address->city }}" />
                          </div>
                          <div class="col-md-6 col-12">
                              <fieldset class="form-group">
                                  <select class="form-control" name="addresses[{{$key}}][province]">
                                      <option>Province</option>
                                      <option value="AB" {{ $address->province == 'AB' ? ' selected' : '' }}>Alberta</option>
                                      <option value="BC" {{ $address->province == 'BC' ? ' selected' : '' }}>British Columbia</option>
                                      <option value="MB" {{ $address->province == 'MB' ? ' selected' : '' }}>Manitoba</option>
                                      <option value="NB" {{ $address->province == 'NB' ? ' selected' : '' }}>New Brunswick</option>
                                      <option value="NL" {{ $address->province == 'NL' ? ' selected' : '' }}>Newfoundland and Labrador</option>
                                      <option value="NS" {{ $address->province == 'NS' ? ' selected' : '' }}>Nova Scotia</option>
                                      <option value="ON" {{ $address->province == 'ON' ? ' selected' : '' }}>Ontario</option>
                                      <option value="PE" {{ $address->province == 'PE' ? ' selected' : '' }}>Prince Edward Island</option>
                                      <option value="QC" {{ $address->province == 'QC' ? ' selected' : '' }}>Quebec</option>
                                      <option value="SK" {{ $address->province == 'SK' ? ' selected' : '' }}>Saskatchewan</option>
                                      <option value="NT" {{ $address->province == 'NT' ? ' selected' : '' }}>Northwest Territories</option>
                                      <option value="NU" {{ $address->province == 'NU' ? ' selected' : '' }}>Nunavut</option>
                                      <option value="YT" {{ $address->province == 'YT' ? ' selected' : '' }}>Yukon</option>
                                  </select>
                              </fieldset>
                          </div>
                          <div class="col-md-6 col-12">
                              <fieldset class="form-group">
                                  <select class="form-control" name="addresses[{{$key}}][country]" value="{{ $address->country }}">
                                      <option>Pays</option>
                                      <option value="Canada">Canada</option>
                                  </select>
                              </fieldset>
                          </div>
                          <div class="col-md-6 col-12">
                              <input class="form-control mb-1" name="addresses[{{$key}}][postal_code]" type="text" placeholder="Code postal" value="{{ $address->postal_code }}" />
                          </div>
                          <div class="col-md-6 col-12">
                            <ul class="list-unstyled mb-0">
                              <li class="d-inline-block mr-2">
                              <fieldset >
                                <label>
                                  <input type="checkbox" name="addresses[{{$key}}][is_default_facturation]" value="1"{{ $address->is_default_facturation == '1' ? ' checked' : '' }}>
                                  Facturation
                                </label>
                              </fieldset>
                            </li>
                            <li class="d-inline-block mr-2">
                              <fieldset>
                                <label>
                                  <input type="checkbox" name="addresses[{{$key}}][is_default_shipping]" value="1"{{ $address->is_default_shipping == '1' ? ' checked' : '' }}>
                                  Livraison
                                </label>
                              </fieldset>
                            </li>
                          </ul>
                          </div>
                          <div class="col-12 text-right">
                              <button type="button" class="btn btn-success btn-sm btn-add-address">
                                  <i class="feather icon-plus"></i>
                              </button>
                          </div>
                      </div>
                      @endforeach
                  </div>
              </div>
              <div class="col-12">
                  <h6 class="py-1 mx-1 mb-0 font-medium-2">
                    <i class="feather icon-user mr-50"></i>Contacts
                  </h6>
                  <div id="contacts">
                    @foreach($client->contacts as $key => $contact)
                      <div class="contact-entry inline-form row border rounded px-1 py-1 mb-1">
                          <div class="col-12 col-md-6">
                              <input type="hidden" name="contacts[{{$key}}][id]" value="{{ $contact->id }}">
                              <input class="form-control mb-1" name="contacts[{{$key}}][first_name]" type="text" placeholder="Prénom" value="{{ $contact->first_name }}" />
                          </div>
                          <div class="col-md-6 col-12">
                              <input class="form-control mb-1" name="contacts[{{$key}}][last_name]" type="text" placeholder="Nom" value="{{ $contact->last_name }}" />
                          </div>
                          <div class="col-md-6 col-12">
                              <input class="form-control mb-1" name="contacts[{{$key}}][email]" type="text" placeholder="Courriel" value="{{ $contact->email }}" />
                          </div>
                          <div class="col-md-6 col-12">
                              <input class="form-control mb-1" name="contacts[{{$key}}][phone]" type="text" placeholder="Téléphone" value="{{ $contact->phone }}" />
                          </div>
                          <div class="col-md-6 col-12">
                              <input class="form-control mb-1" name="contacts[{{$key}}][cell]" type="text" placeholder="Cellulaire" value="{{ $contact->cell }}" />
                          </div>
                          <div class="col-12 text-right">
                              <button type="button" class="btn btn-success btn-sm btn-add-contact">
                                  <i class="feather icon-plus"></i>
                              </button>
                          </div>
                      </div>
                      @endforeach
                  </div>
              </div>
              <div class="col-12">
                  <h6 class="py-1 mx-1 mb-0 font-medium-2">
                    <i class="feather icon-lock mr-50"></i>Permissions
                  </h6>
                  Ce client peut être vu par les administrateurs et par: 
                  
                  <div class="form-group">
                    <select class="select2 form-control" multiple="multiple" name="permissions[]" id="permissions-select2">
                      @foreach($employes as $employe)
                      <option value="{{ $employe->id }}"{{ $employe->hasPermission($client->id) ? ' selected' : '' }}>{{ $employe->first_name }} {{ $employe->last_name }}</option>
                      @endforeach
                    </select>
                  </div>
              </div>
              <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Sauvegarder</button>
              </div>
            </div>
          </form>
          <!-- Clients edit account form ends -->
      </div>
    </div>
  </div>
</section>
<!-- Clients edit ends -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/clients.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/navs/navs.js')) }}"></script>
@endsection

