@extends('layouts/contentLayoutMaster', ['sectionIcon' => 'feather icon-users'])

@section('title', 'New client')

@section('vendor-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/clients.css')) }}">
        
@endsection

@section('content')
<!-- Client edit start -->
<section class="users-edit">
  <div class="card">
    <div class="card-content">
      <div class="card-body">
          <!-- Client edit account form start -->
          <form novalidate action="{{ route('clients.store') }}" method="POST">
            @csrf
            @method('POST')
            <div class="row">
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <div class="controls">
                    <label>Nom de l'entreprise</label>
                    <input type="text" class="form-control" name="entreprise" id="entreprise" placeholder="Nom de l'entreprise" required
                      data-validation-required-message="This username field is required">
                  </div>
                </div>
              </div>
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <label>Type d'entreprise</label>
                  <select class="form-control" name="client_type_id">
                    <option value="1">Particulier</option>
                    <option value="2">Entreprise</option>
                    <option value="3">Gouvernement</option>
                  </select>
                </div>
              </div>
              <div class="col-12">
                <label>Notes</label>
                <fieldset class="form-group">
                    <textarea class="form-control" id="notes"name="notes" rows="3" placeholder="Notes"></textarea>
                </fieldset>
              </div>
            </div>        
            <div class="row">
              <div class="col-12">
                  <h6 class="py-1 mx-1 mb-0 font-medium-2">
                    <i class="feather icon-home mr-50"></i>Adresses
                  </h6>
                  <div id="addresses">
                      <div class="address-entry inline-form row border rounded px-1 py-1 mb-1">
                          <div class="col-12 col-md-6">
                              <input class="form-control mb-1" name="addresses[0][address]" type="text" placeholder="Adresse" />
                          </div>
                          <div class="col-md-6 col-12">
                              <input class="form-control mb-1" name="addresses[0][city]" type="text" placeholder="Ville" />
                          </div>
                          <div class="col-md-6 col-12">
                              <fieldset class="form-group">
                                  <select class="form-control" name="addresses[0][province]">
                                      <option>Province</option>
                                      <option value="AB">Alberta</option>
                                      <option value="BC">British Columbia</option>
                                      <option value="MB">Manitoba</option>
                                      <option value="NB">New Brunswick</option>
                                      <option value="NL">Newfoundland and Labrador</option>
                                      <option value="NS">Nova Scotia</option>
                                      <option value="ON">Ontario</option>
                                      <option value="PE">Prince Edward Island</option>
                                      <option value="QC">Quebec</option>
                                      <option value="SK">Saskatchewan</option>
                                      <option value="NT">Northwest Territories</option>
                                      <option value="NU">Nunavut</option>
                                      <option value="YT">Yukon</option>
                                  </select>
                              </fieldset>
                          </div>
                          <div class="col-md-6 col-12">
                              <fieldset class="form-group">
                                  <select class="form-control" name="addresses[0][country]">
                                      <option>Pays</option>
                                      <option value="Canada">Canada</option>
                                  </select>
                              </fieldset>
                          </div>
                          <div class="col-md-6 col-12">
                              <input class="form-control mb-1" name="addresses[0][postal_code]" type="text" placeholder="Code postal" />
                          </div>
                          <div class="col-md-6 col-12">
                            <ul class="list-unstyled mb-0">
                              <li class="d-inline-block mr-2">
                              <fieldset >
                                <label>
                                  <input type="checkbox" name="addresses[0][is_default_facturation]" value="1">
                                  Facturation
                                </label>
                              </fieldset>
                            </li>
                            <li class="d-inline-block mr-2">
                              <fieldset>
                                <label>
                                  <input type="checkbox" name="addresses[0][is_default_shipping]" value="1">
                                  Livraison
                                </label>
                              </fieldset>
                            </li>
                          </ul>
                          </div>
                          <div class="col-12 text-right">
                              <button type="button" class="btn btn-success btn-sm btn-add-address">
                                  <i class="feather icon-plus"></i>
                              </button>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-12">
                  <h6 class="py-1 mx-1 mb-0 font-medium-2">
                    <i class="feather icon-user mr-50"></i>Contacts
                  </h6>
                  <div id="contacts">
                      <div class="contact-entry inline-form row border rounded px-1 py-1 mb-1">
                          <div class="col-12 col-md-6">
                              <input class="form-control mb-1" name="contacts[0][first_name]" type="text" placeholder="Prénom" />
                          </div>
                          <div class="col-md-6 col-12">
                              <input class="form-control mb-1" name="contacts[0][last_name]" type="text" placeholder="Nom" />
                          </div>
                          <div class="col-md-6 col-12">
                              <input class="form-control mb-1" name="contacts[0][email]" type="text" placeholder="Courriel" />
                          </div>
                          <div class="col-md-6 col-12">
                              <input class="form-control mb-1" name="contacts[0][phone]" type="text" placeholder="Téléphone" />
                          </div>
                          <div class="col-md-6 col-12">
                              <input class="form-control mb-1" name="contacts[0][cell]" type="text" placeholder="Cellulaire" />
                          </div>
                          <div class="col-12 text-right">
                              <button type="button" class="btn btn-success btn-sm btn-add-contact">
                                  <i class="feather icon-plus"></i>
                              </button>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-12">
                  <h6 class="py-1 mx-1 mb-0 font-medium-2">
                    <i class="feather icon-lock mr-50"></i>Permissions
                  </h6>
                  Ce client peut être vu par les administrateurs et par: 
                  
                  <div class="form-group">
                    <select class="select2 form-control" multiple="multiple" name="permissions[]" id="permissions-select2">
                      @foreach($employes as $employe)
                      <option value="{{ $employe->id }}">{{ $employe->first_name }} {{ $employe->last_name }}</option>
                      @endforeach
                    </select>
                  </div>
              </div>
              <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Sauvegarder</button>
              </div>
            </div>
          </form>
          <!-- Clients edit account form ends -->
      </div>
    </div>
  </div>
</section>
<!-- Clients edit ends -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/clients.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/navs/navs.js')) }}"></script>
@endsection

