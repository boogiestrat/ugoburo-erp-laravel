@extends('layouts/fullLayoutMaster')

@section('title', 'Login Page')

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/pages/authentication.css')) }}">
@endsection

@section('content')
<section class="row flexbox-container">
  <div class="col-xl-8 col-11 d-flex justify-content-center">
    <div class="card bg-authentication rounded-0 mb-0">
      <div class="row m-0">
        <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
          <img src="{{ asset('images/pages/login.png') }}" alt="branding logo">
        </div>
        <div class="col-lg-6 col-12 p-0" style="background: #e84933;">
          <div class="card rounded-0 mb-0 px-2" style="background:none;color:#fff;">
            <div class="card-header pb-1">
              <div class="card-title">
                <h1 style="color:#fff;"><img src="/images/logo/logo.png" alt="UGOBURO" class="image" style="display: inline-block;vertical-align: middle;"> ERP</h1>
              </div>
            </div>
            <p class="px-2">{{ __('Connexion sécurisée avec') }}.</p>
            <div class="card-content">
              <div class="card-body pt-1">
                        <a href="{{ url('auth/google') }}" class="btn button is-primary is-large" style="background-color:#fff;">
                            <img src="/images/logo/google.svg"> <strong>GOOGLE</strong>
                        </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<style>
    body{background-image: url(images/bg4.jpg);background-repeat: no-repeat;background-size: cover;}
</style>
@endsection

