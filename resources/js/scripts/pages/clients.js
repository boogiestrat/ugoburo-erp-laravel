$(document).ready(function () {
    $(document).on('click', '.btn-add-address', function(e) {

        e.preventDefault();

        var controlForm  = $('#addresses:first'),
            currentEntry = $(this).parents('.address-entry:first'),
            newEntry     = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').each(function() {
        this.value= "";
        let name_number = this.name.match(/\d+/);
        name_number++;
        this.name = this.name.replace(/\[[0-9]\]+/, '['+name_number+']')
        });

        controlForm.find('.address-entry:not(:last) .btn-add-address')
            .removeClass('btn-add-address').addClass('btn-remove-address')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<i class="feather icon-minus"></i>');

    }).on('click', '.btn-remove-address', function(e) {

        e.preventDefault();

        $(this).parents('.address-entry:first').remove();

        return false;
    });

    $(document).on('click', '.btn-add-contact', function(e) {

    e.preventDefault();

    var controlForm  = $('#contacts:first'),
        currentEntry = $(this).parents('.contact-entry:first'),
        newEntry     = $(currentEntry.clone()).appendTo(controlForm);

    newEntry.find('input').each(function() {
        this.value= "";
        let name_number = this.name.match(/\d+/);
        name_number++;
        this.name = this.name.replace(/\[[0-9]\]+/, '['+name_number+']')
    });

    controlForm.find('.contact-entry:not(:last) .btn-add-contact')
        .removeClass('btn-add-contact').addClass('btn-remove-contact')
        .removeClass('btn-success').addClass('btn-danger')
        .html('<i class="feather icon-minus"></i>');

    }).on('click', '.btn-remove-contact', function(e) {

        e.preventDefault();

        $(this).parents('.contact-entry:first').remove();

        return false;
    });

    if ($("#permissions-select2").length > 0) {
        $("#permissions-select2").select2({
            dropdownAutoWidth: false,
            width: '100%'
        });
    }
});