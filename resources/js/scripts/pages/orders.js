function initButtons() {
    /** Options Configuration **/
}

// Boutton Livraison "Enregistrer"
$(document).on('click', '#open_pdf_version', function(e) {
  e.preventDefault();
  window.open("/webservices/orders/pdf/generate.php?id=" + this_id, '_blank');
});

$(document).on('click', '#save_ship_to_address', function(e) {
  e.preventDefault();
  $('#action').val('save_ship_to_address');
  $('#history-add').submit();
});

$(document).on('click', '#save_bill_to_address', function(e) {
  e.preventDefault();
  $('#action').val('save_bill_to_address');
  $('#history-add').submit();
});

$(document).on('click', '#add-new-history', function(e) {
  // console.log("save estimated-change-without-notif-js");
  e.preventDefault();
  $('#action').val('new_comment_form_save');
  $('#history-add').submit();
});

// Boutton Livraison "Enregistrer"
$(document).on('click', '#save-shipping-js', function(e) {
  // console.log("save estimated-change-without-notif-js");
  e.preventDefault();
  $('#action').val('shipping_form_save');
  $('#history-add').submit();
});

// Boutton Livraison "Enregistrer"
$(document).on('click', '#estimated-change-without-notif-js', function(e) {
  // console.log("save estimated-change-without-notif-js");
  e.preventDefault();
  $('#action').val('eta_form_save');
  $('#history-add').submit();
});

// Boutton Livraison "Enregistrer & notifier sans rendez-vous"
$(document).on('click', '#estimated-change-without-apmt-js', function(e) {
  // console.log("save estimated-change-without-apmt-js");
  e.preventDefault();
  $('#action').val('eta_form_save_and_notify_without_meeting');
  $('#history-add').submit();
});

// Boutton Livraison "Enregistrer et notifier avec rendez-vous"
$(document).on('click', '#estimated-change-js', function(e) {
  // console.log("save estimated-change-js");
  e.preventDefault();
  $('#action').val('eta_form_save_and_notify_with_meeting');
  $('#history-add').submit();
});
var this_id = 0;

function getQueryVariable(variable)
{ 
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
           var pair = vars[i].split("=");
           if(pair[0] == variable){return pair[1];}
   }
   return(false);
}

$( document ).ready(function() {
  var this_id = getQueryVariable("id");
  fetch("/webservices/orders/get.php?id=" + this_id, {
    method: 'GET',
  })
    .then(res => res.json())
    .then(json => {
      console.log(json.data);
      if (!json.done) {
        swal("Erreur!", 'Erreur : ' + json.msg, "error");
        return;
      }

      if (!json.data.bill_to.formatted) {
        var order_bill_to = json.data.bill_to.entreprise + "\n";
        if (json.data.bill_to.firstname && json.data.bill_to.lastname) {
          order_bill_to += json.data.bill_to.firstname + ' ' + json.data.bill_to.lastname + "\n";
        }

        if (json.data.bill_to.address) {
          var address = json.data.bill_to.address.replace(/\, /g,"\n");
          order_bill_to += address + "\n";
        }

        json.data.bill_to.formatted = order_bill_to;
      }

      $(document.getElementById('order_bill_to')).val(json.data.bill_to.formatted);

      if (!json.data.ship_to.formatted) {
        var order_ship_to = json.data.ship_to.entreprise + "\n";
        if (json.data.ship_to.firstname && json.data.ship_to.lastname) {
          order_ship_to += json.data.ship_to.firstname + ' ' + json.data.ship_to.lastname + "\n";
        }
        if (json.data.ship_to.address) {
          var address = json.data.ship_to.address.replace(/\, /g,"\n");
          order_ship_to += address + "\n";
        }

        json.data.ship_to.formatted = order_ship_to;
      }

      $(document.getElementById('order_ship_to')).val(json.data.ship_to.formatted);

      $(document.getElementById('order_specialist')).text(json.data.created_by);
      $(document.getElementById('order_id')).val(json.data.id);
      $(document.getElementById('order-id')).text(json.data.number);
      $(document.getElementById('order-email')).text(json.data.bill_to.firstname+' '+json.data.bill_to.lastname+' ( '+json.data.bill_to.email + ' ) ');
      populateHistories(json.data.histories);

      $(document.getElementById('order_status_comment')).val(json.data.fk_status_id).change();
      $(document.getElementById('carrier_assigned')).val(json.data.carrier_assigned).change();

      $(document.getElementById('acknow_no')).val(json.data.acknow_no).change();
      $(document.getElementById('tracking_no')).val(json.data.tracking_no).change();

      if (json.data.installation == "on") {
        $(document.getElementById('installation')).prop('checked', true).change();
      }

      if (json.data.estimated_delivery_date) {
        $(document.getElementById('delivery_date')).val(json.data.estimated_delivery_date).change();
      }

      $("#quote-date").text(json.data.created_at);
      $("#order-id").text(json.data.order_number);
      if (json.data.created_by) {
        json.data.created_by = json.data.created_by.toLowerCase()
        .split(' ')
        .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
        .join(' ');
        json.data.created_by = 'Représentant : ' + json.data.created_by;
      } else {
        if (json.data.order_source == "website") {
          json.data.created_by = 'Source: site web';
        }
      }
      $("#created_by").text(json.data.created_by);
      build_order_items(json.data.items, json.data.taxes);
      $(document.getElementById('history-add')).on('submit', function(e) {
        e.preventDefault();
        const formData = new FormData(this);
        const form = $(e.target);
        addHistory(form, formData);
      });

      $(document.getElementById('delivery_date')).datetimepicker({
        "todayHighlight": true,
        minView: 2,
        format: 'yyyy-mm-dd',
        autoclose: true
      });
      
      
    })
    .catch(e => {
      swal("Erreur!", 'Erreur : ' + e, "error");
    });
});

$('#datatable').on('click', 'tr', e => {
  const row = $(e.currentTarget);
  const btn = row.find('.modify-btn')[0];

  if (e.target.tagName !== 'I' && e.target.tagName !== 'A') {
    btn.click();
  }
});


build_order_items = (items, taxes) => {
  var html = '';

  html += `<table style="width:100%;">`;

  html += `<thead style="background:#ccc;">`;
  html += `<tr>
    <td><strong>SKU</strong></td>
    <td><strong>Description</strong></td>
    <td align="right"><strong>Prix</strong></td>
    <td align="right"><strong>Qté.</strong></td>
    <td align="right"><strong>Total</strong></td>
  </tr>`;
  html += `</thead>`;

  var rightside_subtotal = 0.0;
  var rightside_shipping = 0.0;
  var rightside_tax = 0.0;

  html += `<tbody style="vertical-align: baseline;">`;
  console.log(items);
  for( var idx in items ) {
    var item = items[idx];

    /*
    global = $ global
    unit = $ par unit
    cost = % coutant
    list = % list
    */

    if (item.expeditionPriceMethod == 'global') {
      rightside_shipping = parseFloat(rightside_shipping) + parseFloat(item.expeditionPrice);
    }

    if (item.expeditionPriceMethod == 'unit') {
      rightside_shipping = parseFloat(rightside_shipping) + ( parseFloat(item.expeditionPrice) * item.quantity );
    }

    html += `<tr>`;
    html += `<td>`+item.SKU+`</td>`;
    html += `<td>`+item.title+' '+item.description.replace(/\n/g,"<br/>")+`</td>`;
    html += `<td align="right">`+( item.price * 1 ).toFixed(2)+` $</td>`;
    html += `<td align="right">`+item.quantity+`</td>`;
    html += `<td align="right">`+( item.price * item.quantity ).toFixed(2)+` $</td>`;
    html += `</tr>`;

    rightside_subtotal = parseFloat(rightside_subtotal) + parseFloat( parseFloat(item.price) * parseFloat(item.quantity) );
  }
  html += `</tbody>`;

  html += `</table>`;
  $('#order_items').html(html);



  for( var idx in taxes ) {
    rightside_tax = parseFloat(rightside_tax) + parseFloat( ( parseFloat(rightside_subtotal) * parseFloat(taxes[idx].rate) ).toFixed(2) );
  }


  $('#rightside_subtotal').html(parseFloat(rightside_subtotal).toFixed(2)+'$');
  $('#rightside_shipping').html(parseFloat(rightside_shipping).toFixed(2)+'$');
  $('#rightside_tax').html(parseFloat(rightside_tax).toFixed(2)+'$');
  $('#rightside_total').html((parseFloat(rightside_subtotal) + parseFloat(rightside_tax) + parseFloat(rightside_shipping)).toFixed(2)+'$');

}

populateHistories = (histories) => {

  $(document.getElementById('histories')).html(histories.map(history => {
    if (!history.status_desc_fr) { history.status_desc_fr = 'Aucun statut'; }
    console.log(history);
    if (history.estimated_delivery_date == "1969-12-31") {
      history.estimated_delivery_date = '';
    }
    history.file_details = '';
    if (history.filepath) {
      if (history.filesize) {
        history.filesize = ( ( parseFloat(history.filesize) / 1024 ) / 1024 ).toFixed(2);
      }
      history.file_details = '<a href="'+history.filepath+'" target="_blank">'+history.filename+' ('+ history.filesize  +' MB)</a>';
    }
    if (history.emp_first_name) {
      history.emp_first_name = history.emp_first_name.charAt(0).toUpperCase() + history.emp_first_name.substring(1);
    } else {
      history.emp_first_name = '';
    }
    if (history.emp_last_name) {
      history.emp_last_name = history.emp_last_name.charAt(0).toUpperCase() + history.emp_last_name.substring(1);
    } else {
      history.emp_last_name = '';
    }
    return (`
      <hr/>
      <div class="h-history">
        <div class="h-icon">
          <i class="fa fa-file"></i>
        </div>
        <div class="h-content">
          <div class="h-header">
            <span>
              ${history.created_at}
            </span> |
            ${history.emp_first_name != '' && history.emp_last_name != '' && history.comment ? `
            <span>
              ${history.emp_first_name} ${history.emp_last_name}
            </span> |
            ` : ''}
            <span>
              ${history.status_desc_fr}
            </span>
          </div>

          <div class="h-comment">
            <p>
              ${history.comment}

              ${history.comment_fr != '' && $('#lang').val() == "fr" ? `
                ${history.comment_fr}
              ` : ''}
              ${history.comment_en != '' && $('#lang').val() == "en"  ? `
                ${history.comment_en}
              ` : ''}
            </p>
          </div>
          <div class="h-info">

            ${history.is_customer_notified == '1' ? `<p><span>Client notifié</span></p>`: '' }

            ${history.acknow_no != '' ? `
              <p><span>Acknow No: </span>${history.acknow_no}</p>
            ` : ''}
            ${history.file_details != '' ? `
              <p><span><i class="fa fa-file"></i></span> ${history.file_details}</p>
            ` : ''}
            ${history.tracking_no != '' ? `
              <p><span>Tracking No: </span>${history.tracking_no}</p><p><span>Expédié par: </span> ${history.supplier}</p>
            ` : ''}

          </div>
        </div>
      </div>
    `);
  }));
}

addHistory = (form, formData) => {
  const data = {
    method: form.attr('method'),
    url: form.attr('action'),
  }

  fetch(data.url, {
    method: 'POST',
    body: formData,
  })
    .then(res => res.json())
    .then(json => {
      populateHistories(json.data.histories);

      $('#comment').val('');
      $('#comment_file').val('');
      $('#notify').prop("checked", false);
      $('#front').prop("checked", false);
      swal({
          title: "Réussi!",
          text: "L'enregistrement a bien été effectué.",
          type: 'success',
          showConfirmButton: false,
          timer: 1000,
          showCancelButton: false
      },function(){
          swal.close();
      });
      // swal("Effacé!", "La modification a été effectué.", "success");
    })
    .catch(err => console.log(err));
}

$('#order-modal').on('hidden.bs.modal', function () {
  reloadDatatable();
});

reloadDatatable = () => {
  $(document.getElementById('datatable')).DataTable().ajax.reload();
}
