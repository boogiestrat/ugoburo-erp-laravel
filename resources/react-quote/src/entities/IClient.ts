import { IContact } from './IContact';
import { IAddress } from './IAddress';

export interface IClient {
  readonly id: number;
  readonly title: string;
  readonly entreprise?: string;
  readonly active: boolean;
  readonly created_at: string;
  readonly updated_at: string;
  readonly client_type_enum: ClientType;
  readonly contacts: Array<IContact>;
  readonly addresses: Array<IAddress>;
}

export enum ClientType {
  PARTICULIER = 1,
  ENTREPRISE,
  GOUVERNEMENT
}
