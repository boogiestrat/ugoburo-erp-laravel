export interface ISelectValue {
  readonly value: any;
  readonly label: string;
  readonly data: any;
}
