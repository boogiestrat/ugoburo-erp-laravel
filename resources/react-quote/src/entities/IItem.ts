import { ProfitMethod } from "../constants/enums";
import { IDiscount } from "./";

export interface IItem {
  readonly id: number;
  readonly SKU: string;
  readonly title: string;
  readonly price: number;
  readonly quantity: number;
  readonly description: string;
  readonly profitMethod: ProfitMethod;
  readonly priceParameterValue: number;
  readonly discount: IDiscount;
  readonly expeditionPrice: number;
  readonly expeditionPriceMethod: 'global' | 'unit' | 'cost' | 'list' | null;
  readonly expeditionPercent: number;
  readonly isFocused: boolean;
  readonly description_open: boolean;
  readonly pictureURL?: string;
  readonly calculateUnitPrice: (this: IItem) => number;
  readonly calculateCostPrice: (this: IItem) => number;
  readonly calculateLineTotal: (this: IItem) => number;
  readonly calculateItemProfit: (this: IItem) => number;
  readonly calculateLineShipping: (this: IItem) => number;
  readonly calculateGrossProfit: (this: IItem) => number;
}
