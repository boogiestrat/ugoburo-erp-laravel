export interface IDiscount {
  readonly code: string;
  readonly name: string;
  readonly parts: Array<number | string>;
}
