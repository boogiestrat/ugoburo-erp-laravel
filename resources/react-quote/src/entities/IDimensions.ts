export interface IDimensions {
  readonly height: number;
  readonly width: number;
}
