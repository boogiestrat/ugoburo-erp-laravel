export interface IClause {
  readonly clause: string;
  readonly value: boolean;
  readonly description?: string;
  readonly priceValue: number;
  readonly percentValue: number;
}
