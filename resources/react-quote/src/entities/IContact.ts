export interface IContact {
  readonly id: number;
  readonly is_default_contact: number;
  readonly first_name: string;
  readonly last_name: string;
  readonly email: string;
  readonly phone: string;
  readonly cell: string;
}
