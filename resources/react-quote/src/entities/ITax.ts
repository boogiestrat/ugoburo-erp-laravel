export interface ITax {
  readonly id: number;
  readonly fk_province_id: number;
  readonly valueDescription: string;
  readonly value: number
}
