import { IItem } from './';

export interface ISection {
  readonly id: number;
  readonly title: string;
  readonly items: Array<IItem>;
  readonly folded: boolean;
  readonly calculateGrossProfit: (this: ISection) => number;
  readonly calculateCost: (this: ISection) => number;
  readonly calculateProfit: (this: ISection) => number;
  readonly calculateTotal: (this: ISection) => number;
}
