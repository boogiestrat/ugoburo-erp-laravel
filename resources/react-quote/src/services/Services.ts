export const getBaseUrl = () => process.env.NODE_ENV === 'production' ? '../../' : 'http://ugoburoerp.local/';
