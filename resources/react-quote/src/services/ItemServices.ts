import { IItem } from "../entities";

import _ from "lodash";
import { getBaseUrl } from "./Services";

export const fetchItem = (keyword: string): Promise<Array<IItem>> => {
  return fetch(getBaseUrl() + "/webservices/item/search?q=" + keyword)
    .then(body => body.json())
    .then(json => {
      return _.map(
        json,
        item =>
          ({
            ...item,
            price: parseFloat(item.price)
          } as IItem)
      );
    });
};
