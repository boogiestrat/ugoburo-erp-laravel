import { IDimensions } from '../entities';

export const getHetmlElementDimsension = (element: HTMLElement): IDimensions => ({
  height: element.clientHeight,
  width: element.clientWidth,
});
