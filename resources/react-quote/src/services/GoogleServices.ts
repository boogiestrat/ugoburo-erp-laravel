import { IAddress } from '../entities';

import _ from 'lodash';

export const decomposeAddressFromGoogle = (googleAddres: Array<{readonly long_name: string, readonly short_name: string, readonly types: Array<string>}>): IAddress => ({
  id: -1,
  is_default_facturation: 0,
  is_default_shipping: 0,
  address: getValueFromGoogleArray(googleAddres, 'street_number') + ' ' + getValueFromGoogleArray(googleAddres, 'route'),
  city: getValueFromGoogleArray(googleAddres, 'locality'),
  postal_code: getValueFromGoogleArray(googleAddres, 'postal_code'),
  province: getValueFromGoogleArray(googleAddres, 'administrative_area_level_1'),
  country: getValueFromGoogleArray(googleAddres, 'country'),
});

export const getValueFromGoogleArray = (googleArray: Array<{readonly long_name: string, readonly short_name: string, readonly types: Array<string>}>, paramWanted: string) =>
  _.filter(googleArray, values => values.types.indexOf(paramWanted) > -1)[0].long_name ? _.filter(googleArray, values => values.types.indexOf(paramWanted) > -1)[0].long_name : '';
