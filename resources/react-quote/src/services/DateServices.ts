export const getNow = () => new Date();

export const getNowYear = () => new Date().getFullYear();

export const getMonth = () => ('0' + (new Date().getMonth() + 1)).slice(-2);

export const getDay = () => ('0' + new Date().getDate()).slice(-2);

export const getFullDate = () => `${getNowYear()}${getMonth()}${getDay()}`;
