import { IContact } from '../entities';

export default class UBContact {
  public static createEmptyContact(): IContact {
    return {
      cell: '',
      email: '',
      first_name: '',
      id: -1,
      is_default_contact: 0,
      last_name: '',
      phone: '',
    }
  }
}
