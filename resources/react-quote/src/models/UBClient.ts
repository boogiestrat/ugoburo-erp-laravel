import { IClient, ClientType } from '../entities';
import moment from 'moment';
import { getBaseUrl } from '../services';

export default class UBClient {
  public static createEmptyClient(): IClient {
    return {
      active: true,
      addresses: [],
      client_type_enum: ClientType.ENTREPRISE,
      contacts: [],
      created_at: moment().format('DD-MM-YYYY hh:mm:ss'),
      id: -1,
      title: '',
      updated_at: moment().format('DD-MM-YYYY hh:mm:ss'),
    }
  }

  public static async getClientById(id: number): Promise<IClient | null> {
    try {
      const res = await fetch(`${getBaseUrl()}webservices/client/get.php`, { method: 'POST', body: JSON.stringify({ id }) });
      const json = await res.json();
      return json as IClient;
    }
    catch (e) {
      return null;
    }
  }

  public static async searchClient(terms: string): Promise<Array<IClient>> {
    try {
      const res = await fetch(`${getBaseUrl()}webservices/client/search.php`, { method: 'POST', body: JSON.stringify({ q: terms }) });
      const json = await res.json();
      return json as Array<IClient>;
    }
    catch (e) {
      return [];
    }
  }
}
