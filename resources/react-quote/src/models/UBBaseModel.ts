export class UBBaseModel {
  private _entity: any;
  private _serviceURL: string;

  public constructor(entity: any, serviceURL: string) {
    this._entity = entity;
    this._serviceURL = serviceURL;
  }

  public entity(): any {
    return this._entity;
  }

  public save(): Promise<any> {
    return new Promise((resolve) => {
      resolve(0);
    });
  }
}
