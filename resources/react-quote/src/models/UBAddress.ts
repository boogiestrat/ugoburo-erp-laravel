import { IAddress } from '../entities';

export default class UBAddress {
  public static createEmptyAddress(): IAddress {
    return {
      address: '',
      city: '',
      country: '',
      id: -1,
      is_default_facturation: 0,
      is_default_shipping: 0,
      postal_code: '',
      province: '',
    }
  }
}
