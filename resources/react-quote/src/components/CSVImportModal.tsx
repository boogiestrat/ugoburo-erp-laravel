import React, { useState } from 'react';

import _, { Dictionary } from 'lodash';

import SelectAsync from './SelectAsync';

import Modal from './Modal';
import { ISelectValue } from '../entities';

interface ICSVImportModalProps {
  readonly close: Function;
  readonly getValuesFromCSV: () => Promise<any>;
  readonly insertDataIntoSection: (data: Array<{}>, section: number | string) => void;
  readonly loadSectionsForSelect: (keywords: string, callback: Function) => void;
  readonly sections: Array<{readonly sectionId: number, readonly sectionTitle: string}>;
}

export const CSVImportModal: React.FC<ICSVImportModalProps> = (props) => {
  const [ isLoading, setIsLoading ] = useState(false);
  const [ isImporting, setIsImporting] = useState(false);
  const [ data, setData ] = useState([{}]);
  const [ error, setError ] = useState('');
  const [ sectionIndex, setSectionIndex ] = useState(-1);
  const [ newSectionName, setNewSectionName ] = useState('');

  const loadData = () => {
    setIsLoading(true);
    props.getValuesFromCSV()
      .then(res => {
        setIsLoading(false);

        if (!res || !res.done) {
          setData([]);
          setError('Une erreur s\'est prodite. Veuillez réessayer.');
          return;
        }

        res.csv[0] = _.filter(res.csv[0], h => h !== '');
        res.csv[0] = _.filter(res.csv[0], h => ['Part Number', 'Qty', 'Part Description', 'List'].indexOf(h) > -1);
        res.csv = _.filter(res.csv, rowForFilter => rowForFilter['Part Number'] !== '...' && rowForFilter['Part Number'] !== '' && rowForFilter['Qty'] !== '');

        setData(res.csv);
      }).catch(() => {
        setIsLoading(false);
        setError('Une erreur s\'est prodite. Veuillez réessayer.');
        setData([]);
      });
  }

  const handleCSVItemChange = (newValue: string, rowIndex: number, cellHeader: string) => {
    setData(_.map(data, (statedDataRow, index) => {
      if (index !== rowIndex) {
        return statedDataRow;
      }

      return { ...statedDataRow, [cellHeader]: newValue };
    }));
  }

  const loadItemIntoChosenSection = () => {
    setIsImporting(true);
    setTimeout(() => {
      props.insertDataIntoSection(data.slice(1), sectionIndex === -1 ? newSectionName : sectionIndex);
    }, 100);
    setTimeout(() => {
      setIsImporting(false);
      props.close();
    }, 1000);
    return {};
  }

  const addLineAfter = (after: number) => {
    setData([ ...data.slice(0, after + 1), { 'Part Number': 'Nouvel item', 'Qty': 1, 'Part Description': '', Tag: '', List: 0 }, ...data.slice(after + 1) ])
  }

  const deleteLine = (at: number) => {
    setData([ ...data.slice(0, at), ...data.slice(at + 1) ]);
  }

  const getTitle = (value: string) => {
    switch (value) {
      case 'Part Number':
        return 'SKU';
      case 'Qty':
        return 'Quantité';
      case 'Part Description':
        return 'Nom';
      case 'List':
        return 'Prix de liste';
      default:
        return 'Titre';
    }
  }

  return (
    <Modal
      title="Importation CSV"
      doneBtnAction={() => loadItemIntoChosenSection()}
      doneBtnText={''}
      close={() => props.close()}
    >
      <div>
        <div>
          <div>
            {error !== '' &&
            <div className="alert alert-warning alert-dismissable fade show has-icon">
              <i className="la la-warning alert-icon"></i>
              <button className="close" data-dismiss="alert" aria-label="Fermer" onClick={() => setError('')}></button><strong>Attention!</strong><br />{error} </div>
            }
            <div className="mb-3">
              {!isLoading &&
                <label className="btn btn-info file-input mr-2">
                  <span className="btn-icon"><i className="la la-upload"></i>Choisir un fichier CSV</span>
                  <input onChange={loadData} type="file" id="file_to_import" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                </label>
              }
            </div>
            {(data.length > 0 && _.toArray(data[0]).length > 0) &&
              <button disabled={isImporting} className="btn btn-primary btn-rounded submit-btn" onClick={() => loadItemIntoChosenSection()}>{isImporting ? 'Importation en cours...' : 'Importer'}</button>
            }
            <div className="mb-4">
              {(data.length > 0 && _.toArray(data[0]).length > 0) &&
                <div className="form-group">

                </div>
              }
            </div>
            {(data.length > 0 && _.toArray(data[0]).length > 0) &&
              <table className="table table-bordered" style={{ width: '90%', margin: 'auto' }}>
                <thead className="thead-default">
                  <tr>
                    {_.map(data[0], (header: string, thIndex: number) => (
                      <th key={`th-${thIndex}`}>{getTitle(header)}</th>
                    ))}
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody style={{ height: 300, overflow: 'scroll' }}>

                  {_.map(_.filter(data, (rowForFilter, index: number) =>
                    //@ts-ignore
                    rowForFilter['Part Number'] !== '...' && rowForFilter['Part Number'] !== '' && index !== 0), (row: Dictionary<string>, trIndex: number) => (
                    <tr key={`tr-${trIndex}`}>
                      {_.map(data[0], (cHeader: string, dIndex: number) => {
                        return <td key={`td-${trIndex}-${dIndex}`}>
                          <input className="form-control form-control-sm" type="text" value={row[cHeader]} onChange={e => handleCSVItemChange(e.currentTarget.value, trIndex + 1, cHeader)}/>
                        </td>
                      })}
                      <td>
                        {/* <button className="btn btn-success btn-rounded btn-sm submit-btn" onClick={() => addLineAfter(trIndex + 1)}>Ajouter une ligne</button> */}
                        <button className="btn btn-danger btn-rounded btn-sm submit-btn" onClick={() => deleteLine(trIndex + 1)}>Supprimer</button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            }
          </div>
        </div>
      </div>
      {(data.length > 0 && _.toArray(data[0]).length > 0) &&
        <button disabled={isImporting} className="btn btn-primary btn-rounded submit-btn" onClick={() => loadItemIntoChosenSection()}>{isImporting ? 'Importation en cours...' : 'Importer'}</button>
      }
    </Modal>
  )
}
