import React from 'react';
import { ISelectValue, IItem } from '../../entities';
import _ from 'lodash';
import moment from 'moment';
import { createEmptyItem } from '../../services';

interface IInlineTextInputProps {
  readonly value: string;
  readonly title: string;
  readonly placeholder: string;
  readonly onChange: (value: any, isFocused?: boolean) => void;
  readonly overrideType?: string;
  readonly noLabel?: boolean;
  readonly onBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
  readonly groupWith?: any;
  readonly inputWidth?: number;
  readonly noPadding?: boolean;
  readonly fullWidth?: boolean;
  readonly inputClassName?: string;
  readonly style?: {
    readonly container?: React.CSSProperties,
    readonly input?: React.CSSProperties,
  };
  readonly loadOptions?: Function;
  readonly autosuggestCallback?: Function;
  readonly inputRef?: string;
  readonly id?: string;
  readonly isItem?: boolean;
  readonly autofocus?: boolean;
  readonly forceValue?: boolean;
  readonly max?: number;
  readonly min?: number;
  readonly noDefault?: boolean;
  readonly leaveOnEnter?: boolean;
};

interface IInlineTextInputState {
  readonly value: string;
  readonly options: Array<ISelectValue>;
  readonly reloadData: boolean;
  readonly editing: boolean;
}

export default class InlineTextInput extends React.Component<IInlineTextInputProps> {
  public state: IInlineTextInputState = {
    value: '',
    options: [],
    reloadData: false,
    editing: false,
  }

  public componentDidMount() {
    this.setState({ value: this.props.value });

    if (this.props.overrideType && this.props.id) {
      if (this.props.overrideType === 'date') {
        if (process.env.NODE_ENV !== 'development') {
          // @ts-ignore
          window.jQuery(`#${this.props.id}`).datepicker({
            autoclose: true,
            orientation: 'bottom',
            dateFormat: 'DD/MM/YYYY',
            format: 'dd/mm/yyyy',
            language: 'fr'
          });
          // @ts-ignore
          window.jQuery(`#${this.props.id}`).datepicker('setDate', this.props.value);
          // @ts-ignore
          window.jQuery(`#${this.props.id}`).datepicker().on('changeDate', e => {
            this.setState({ value: moment(e.date).format('DD/MM/YYYY') });
            return this.props.onChange(e.date);
          });
        }
      }
    }
  }

  public update = () => {
    this.setState({ value: this.props.value });
  }

  private _getSuggestions = (callback: Function | undefined) => {
    if (callback === undefined) {
      return;
    }

    return _.map(this.state.options, (option, suggIndex) => {
      return (
        <div key={`suggestion-${suggIndex}`} className="tt-suggestion tt-selectable" onClick={() => {
          this.setState({ reloadData: true, options: [] }, () => {
            callback({ ...(this.props.isItem ? createEmptyItem() : {}), ...option.data } as any);
          })
        }}>
          {_.map(option.label, (letter, letterIndex) => {
            if (this.props.onBlur) {
              if (this.state.value.toLowerCase().indexOf(letter.toLowerCase()) > -1) {
                return (
                  <strong key={`suggestion-${suggIndex}-letter-${letterIndex}`} className="tt-highlight" style={{ fontWeight: 700 }}>{letter}</strong>
                )
              } else {
                return letter;
              }
            } else {
                if (this.props.value && this.props.value.toLowerCase().indexOf(letter.toLowerCase()) > -1) {
                  return (
                    <strong key={`suggestion-${suggIndex}-letter-${letterIndex}`} className="tt-highlight" style={{ fontWeight: 700 }}>{letter}</strong>
                  )
                } else {
                  return letter;
                }
            }
          })}
        </div>
      );
    });
  }

  public render() {
    const width = this.props.inputWidth ? { width: `${this.props.inputWidth}px` } : this.props.fullWidth && this.props.fullWidth === true ? { width: '100%' } : {};
    const padding = this.props.noPadding ? { padding: '3px 0px' } : {};
    const inputStyle: React.CSSProperties = this.props.style ? { ...width, ...this.props.style.input } : { ...width, ...padding };

    const input = (
      <input
        onKeyDown={(event: React.KeyboardEvent<HTMLInputElement>) => {
          if (this.props.leaveOnEnter === undefined || !this.props.leaveOnEnter) {
            return;
          }

          if (event.keyCode === 13) {
            event.currentTarget.blur();
          }
        }}
        onFocus={e => e.target.select()}
        autoFocus={this.props.autofocus !== undefined ? this.props.autofocus : false}
        id={this.props.id ? this.props.id : ''}
        ref={this.props.inputRef ? this.props.inputRef : "input"}
        style={inputStyle}
        className={"form-control form-control-line" + (this.props.inputClassName ? ' ' + this.props.inputClassName : '')}
        type="text"
        onBlur={e => {
          if (this.props.onBlur) {
            this.props.onBlur(e);
          }
        }}
        onChange={(e) => {
          if (this.props.overrideType === 'date') {
            this.setState({ value: e.currentTarget.value });

            if (moment(e.currentTarget.value).isValid()) {
              this.props.onChange(e.currentTarget.value);
            }
            return;
          }

          const value = e.currentTarget.value;

          if (this.props.max !== undefined && value.replace(',', '').length > this.props.max) {
            return;
          }

          this.setState({ editing: true });

          if (this.props.onBlur) {
            this.setState({ value }, () => {
              if (this.props.loadOptions !== undefined) {
                if (this.state.value !== '') {
                  this.props.loadOptions(value, (res: Array<ISelectValue>) => {
                    this.setState({ options: res })
                  });
                } else {
                  this.setState({ options: [] });
                }
              }
            });
          } else {
            this.props.onChange(value);
            this.setState({ value });

            if (this.props.loadOptions) {
              if (value !== '') {
                this.props.loadOptions(value, (res: Array<ISelectValue>) => {
                  this.setState({ options: res })
                });
              } else {
                this.setState({ options: [] });
              }
            }
          }
        }}
        {...(this.props.max !== undefined) &&
          {
            maxLength: this.props.max,
          }
        }
        {...(this.props.min !== undefined) &&
          {
            minLength: this.props.min,
          }
        }
        {...(this.props.forceValue !== undefined && this.props.forceValue) ?
          {
            value: this.props.onBlur || this.props.overrideType === 'date' ? this.state.value : this.props.value
          }
        :
          {
            defaultValue: this.props.onBlur || this.props.overrideType === 'date' ? this.state.value : this.props.value
          }
        }
        placeholder={this.props.placeholder ? this.props.placeholder : ''}
      />
    );
    return (
      <div className="inline-form-line" style={this.props.style && this.props.style.container ? this.props.style.container : {}}>
        {(this.props.noLabel === undefined || this.props.noLabel === false) &&
          <label>{this.props.title}:</label>
        }
        {this.props.groupWith ?
          <div className="input-group">
            {input}
            {this.props.groupWith}
            {this.state.options.length > 0 &&
              <div className="tt-menu tt-open" style={{ position: 'absolute', top: '100%', zIndex: 100, display: 'block', maxHeight: 150, overflowY: 'scroll' }}>
                <div className="tt-dataset tt-dataset-states">
                  {this._getSuggestions(this.props.autosuggestCallback)}
                </div>
              </div>
            }
          </div>
        :
          <div>
            {input}
            {this.state.options.length > 0 &&
              <div className="tt-menu" style={{ position: 'absolute', zIndex: 100, display: 'block', maxHeight: 150, overflowY: 'scroll' }}>
                <div className="tt-dataset tt-dataset-states">
                  {this._getSuggestions(this.props.autosuggestCallback)}
                </div>
              </div>
            }
          </div>
        }
      </div>
    );
  }
}
