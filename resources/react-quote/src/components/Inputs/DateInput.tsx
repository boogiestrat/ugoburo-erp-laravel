import React, { useEffect, useState } from 'react';
import moment from 'moment';

export const DateInput = ({ date, id, min, max }: any) => {
  const [value, setValue] = useState('');

  useEffect(() => {
    setValue(moment(date).format('DD/MM/YYYY'));

    // @ts-ignore
    window.jQuery(`#${id}`).datepicker({
      autoclose: true,
      orientation: 'bottom',
      defaultViewDate: date,
      startDate: min,
      format: 'dd/mm/yyyy'
    });
    // @ts-ignore
    window.jQuery(`#${id}`).datepicker('setDate', date);
    // @ts-ignore
    window.jQuery(`#${id}`).datepicker().on('changeDate', e => setValue(moment(e.date).format('DD/MM/YYYY')));
  });

  return (
    <input
      id={id}
      value={value}
      onChange={e => setValue(e.currentTarget.value)}
      onBlur={e => setValue(moment(e.currentTarget.value).format('DD/MM/YYYY'))}
      type="text"
      className="form-control form-control-line"
    />
  )
}
