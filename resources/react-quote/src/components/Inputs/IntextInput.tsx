import React from 'react';

interface IIntextInputProps {
  readonly value: string;
  readonly onTextChange: (value: string) => void;
  readonly id: string;
  readonly custom?: Function;
  readonly style?: React.CSSProperties;
  readonly onBlur?: (value: string) => void;
  readonly alwaysOn?: boolean;
  readonly maxLength?: number;
}

interface IIntextInputState {
  readonly editing: boolean;
  readonly value?: string;
}

export default class IIntextInput extends React.Component<IIntextInputProps> {
  public state: IIntextInputState = {
    editing: false,
  }

  public componentDidMount() {
    window.addEventListener('click', this._clickListener);

    if (this.props.alwaysOn !== undefined && this.props.alwaysOn) {
      this.setState({ editing: true });
    }

    if (this.props.onBlur !== undefined) {
      this.setState({ value: this.props.value });
    }
  }

  public componentWillUnmount() {
    window.removeEventListener('click', this._clickListener);
  }

  private _clickListener = (e: MouseEvent) => {
    const target = e.target as HTMLElement;

    if (!target.classList.contains('can-click-on-listener') && (this.props.alwaysOn === undefined || !this.props.alwaysOn)) {
      this.setState({ editing: false });
    }
  }

  private _keyPressed = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      if (this.props.onBlur !== undefined) {
        this.props.onBlur(this.state.value || '');
      }

      if (this.props.alwaysOn === undefined || !this.props.alwaysOn) {
        this.setState({ editing: false });
      }
    }
  }

  public render() {
    if (!this.state.editing) {
      return (
        <span style={{ marginLeft: '0px', marginRight: '0px'}} onClick={() => {
          this.setState({ editing: true }, () => {
            if (!this.props.custom) {
              const input = document.getElementById(this.props.id) as HTMLInputElement;
              if (input && input.select) {
                input.select();
              }
            }
          });
        }}>
          {this.props.children}
        </span>
      );
    } else {
      return !this.props.custom ? (
        <input {...( this.props.maxLength !== undefined ? { maxLength: this.props.maxLength } : {})} type="text" id={this.props.id} style={{ minWidth: 50, width: 'auto', ...this.props.style }} onKeyPress={this._keyPressed} className="form-control form-control-line can-click-on-listener" onBlur={e => this.props.onBlur !== undefined ? this.props.onBlur(e.currentTarget.value.replace(',', '.')) : {}} defaultValue={this.props.onBlur !== undefined ? this.state.value || '' : this.props.value} onChange={(e) => this.props.onBlur !== undefined ? this.setState({ value: e.currentTarget.value.replace(',', '.') }) : this.props.onTextChange(e.currentTarget.value)}/>
      ) :
      this.props.custom();
    }
  }
}
