import React from 'react';

import { IDimensions } from '../entities';

import { getHetmlElementDimsension } from '../services';

const PAPER_HEIGHT: number = 11;
const PAPER_WIDTH: number = 8.5;
const PADDING_VERTICAL: number = 60 * 2;
const PADDING_HORIZONTAL: number = 60;

interface IPaperLikeProps {}

interface IPaperLikeState {
  readonly dimensions: IDimensions;
  readonly paperHeight: number;
  readonly paperWidth: number;
  readonly loading: boolean;
}

export default class PaperLiker extends React.Component<IPaperLikeProps> {
  public state: IPaperLikeState = {
    dimensions: getHetmlElementDimsension(document.getElementById('quote-app') as HTMLElement),
    paperHeight: 0,
    paperWidth: 0,
    loading: true,
  };

  public componentDidMount() {
    this._setPaperDimensions();
  }

  private _setPaperDimensions = () => {
    const paperWidth = this.state.dimensions.width * 0.9;
    this.setState({ paperWidth, paperHeight: (PAPER_HEIGHT * paperWidth) / PAPER_WIDTH, loading: false });
  }

  public render() {
    if (this.state.loading) {
      return (
        <div>Chargement</div>
      );
    } else {
      return (
        <div className="quote-paper-container">
          <div className="file" style={{
            width: this.state.paperWidth,
            // height: this.state.paperHeight,
            paddingTop: PADDING_VERTICAL,
            paddingBottom: PADDING_VERTICAL,
            paddingLeft: PADDING_HORIZONTAL,
            paddingRight: PADDING_HORIZONTAL,
          }}>
            {this.props.children}
          </div>
        </div>
      );
    }
  }
}
