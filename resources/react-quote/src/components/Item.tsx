import React from 'react';

import _ from 'lodash';

import InlineTextInput from './Inputs/InlineTextInput';

import { IItem, IDiscount, ISelectValue, ISection } from '../entities';
import { ProfitMethod } from '../constants/enums';
import { getPriceMethodStringBasedOnValue, moneyFormat, getBaseUrl, percentFormat } from '../services';
import { DraggableProvided } from 'react-beautiful-dnd';
import ShippingCost from '../components/Inputs/ShippingCost';

import styled from 'styled-components';

import TextareaAutosize from 'react-autosize-textarea';

interface IItemProps {
  readonly index: number;
  readonly item: IItem;
  readonly itemIndex: number;
  readonly addItemToSection: (index: number) => void;
  readonly duplicateItemToSection: (index: number, itemIndex: number) => void;
  readonly deleteItemOfSection: (index: number, itemIndex: number) => void;
  readonly updateQuoteItem: (index: number, itemIndex: number, param: string, value: any) => Promise<any>;
  readonly loadOptions: Function;
  readonly availableDiscounts: Array<IDiscount>;
  readonly style?: React.CSSProperties;
  readonly isSelectingEnabled: boolean;
  readonly onSelectItemChange: (id: number, value: boolean) => void;
  readonly isChecked: (id: number) => boolean;
  readonly itemsCount: number;
  readonly draggableProvided: DraggableProvided;
  readonly foldAllOtherItem: (itemToKeepOpen: number, inSection: number) => void;
  readonly handleSlider: (images: Array<string>) => void;
  readonly deletePicture: (callback: Function, msg: string) => void;
  readonly sections: Array<ISection>;
  readonly moveItemToSectionWithPosition: (prevItemIndex: number, prevSectionIndex: number, nextSectionIndex: number, position: 'top' | 'bottom') => void;
  readonly globalIndex: number;
  readonly confirmationMethod: (callback: Function, msg: string) => void;
  readonly defaultLogisticCostMethod: 'global' | 'unit' | 'cost' | 'list';
}

interface IItemState {
  readonly setBySKU: boolean;
  readonly setByName: boolean;
  readonly subMenuIsOpen: boolean;
  readonly moveToSectionIndex: number;
  readonly moveToIndex: 'top' | 'bottom';
}

const RowTh = styled.th`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;

  .item-menu {
    cursor: pointer;
    width: 10px;
    text-align: center;
  }
`

const SubMenu = styled.div`
  position: absolute;
  opacity: 0;
  transform: scale(0);
  transition: 0.3s all ease-in-out;
  background: #fff;
  top: 70px;
  right: 40px;
  padding: 10px;
  box-shadow: 0px 1px 0px 0px #00000000;
  z-index: 999;

  .form-control {
    display: inline;
    width: auto;
  }

  &.open {
    opacity: 1;
    transform: inherit;
    box-shadow: 0px 1px 10px 3px #00000020;

    &:after {
      content: ' ';
      top: -10px;
      right: 20px;
      position: absolute;
      height: 20px;
      width: 20px;
      background: #fff;
      transform: rotate(45deg);
    }
  }
`

const HeadRow = styled.div`
  display: flex;
  flex-direction: row;
  border: none;
  border-bottom: 1px solid #c3c3c3;
  padding: 10.5px;
  justify-content: space-between;
`

const SecondRow = styled.div`
  display: flex;
  flex-direction: row;
  padding: 10.5px;
  border: 1px solid #e8e8e8;
  width: 100%;
  border-top: none;

  .section {
    display: flex;
    flex-direction: column;
    flex: 1;
    padding: 0 10px;
    height: 100%;
    justify-content: space-between;

    span {
      &:first-child {
        font-weight: 700;
      }
    }
  }
`

const DiscountConfig = styled.div`
  display: flex;
  flex-direction: row;

  .inline-form-line {
    display: flex;
    flex-direction: row;

    .input-group {
      width: 65px;

      span.input-group-addon {
        padding: 4px 6px!important;
      }

      input {
        padding: 0 0 0 3px;
      }
    }
  }
`

const Container = styled.div`
  display: flex;
  flex-direction: row;

`

const RightRowSide = styled.div`
  display: flex;
  align-items: center;

  i {
    font-size: 16pt;
    padding: 0 5px;

    &.fa {
      font-size: 13pt
    }
  }
`

const RowSection = styled.div`
  margin: 0 20px;
  display: flex;
  align-items: center;
  font-size: 18pt;
  font-weight: 700;
`

const ColumnInSection = styled.div`
  margin: 0 10px;
  display: flex;
  flex-direction: column;
  text-align: right;

  span:first-child {
    font-weight: 700;
  }
`

const IBox = styled.div`
  transition: all 0.3s ease-in-out;
  justify-content: flex-start;
  display: flex;
  flex-direction: column;
`

export default class Item extends React.Component<IItemProps> {
  public state: IItemState = {
    setByName: false,
    setBySKU: false,
    subMenuIsOpen: false,
    moveToSectionIndex: 0,
    moveToIndex: 'top',
  };

  public componentDidMount() {
    window.addEventListener('click', this._handleMoveHandler);
  }

  private _handleMoveHandler = (ev: MouseEvent) => {
    const elem = ev.target as HTMLElement;

    if (!elem.classList.contains('open') && !elem.classList.contains('item-menu') && !elem.classList.contains('do-not-close')) {
      this.setState({ subMenuIsOpen: false });
    }
  }

  private _updateItem = (item: any, itemIndex: number) => {
    if (isNaN(item.price)) {
      item.price = 0;
    }

    if (item.discount_title !== null) {
      const parts = JSON.parse(item.discount_values);
      const discount = {
        code: item.discount_code,
        name: item.discount_title,
        parts
      } as IDiscount;
      this.props.updateQuoteItem(this.props.index, itemIndex, '', { ...item, discount: { ...item.discount, ...discount }} as IItem);
    } else {
      this.props.updateQuoteItem(this.props.index, itemIndex, '', item);
    }
  }

  private _numberUpdate = (value: string, param: string) => {
    return !isNaN(parseFloat(value)) ? this.props.updateQuoteItem(this.props.index, this.props.itemIndex, param, value[value.length - 1] === ',' || value[value.length - 1] === '.' ? value : parseFloat(value.replace(',', '.'))) : this.props.updateQuoteItem(this.props.index, this.props.itemIndex, param, 0);
  }

  private _toggleSubMenu = () => {
    this.setState({ subMenuIsOpen: !this.state.subMenuIsOpen }, () => {
    });
  }

  private _moveItemToSection = (prevSectionIndex: number, prevItemIndex: number) => {
    this.props.moveItemToSectionWithPosition(prevItemIndex, prevSectionIndex, this.state.moveToSectionIndex, this.state.moveToIndex);
    this.setState({ subMenuIsOpen: false });
  }

  public render() {
    const { item, itemIndex, index, foldAllOtherItem } = this.props;
    const profitMethodStrings = _.filter(ProfitMethod, methodString => typeof methodString === 'string');
    const profitMethodindexes = _.filter(ProfitMethod, methodIndex=> typeof methodIndex === 'number');

    return (
      <div
        className="item ibox mb-2"
        style={{
          ...this.props.style,
          backgroundColor: '#ffffff',
          boxShadow: 'none',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'flex-start',
          overflow: 'hidden',
          transition: 'all 0.3s ease-in-out',
        }}
      >
        <IBox onFocus={() => foldAllOtherItem(itemIndex, index)} className={`ibox-body ${item.isFocused ? '' : 'closed'}`}>
          <SubMenu className={this.state.subMenuIsOpen ? 'open do-not-close' : 'do-not-close'}>
            <div className="submenu do-not-close">
              <div className="submenu__col do-not-close">
                <div className="submenu__item do-not-close">
                  <span className="do-not-close">Déplacer vers la section </span>
                  <select className="form-control form-control-line do-not-close" onChange={event => this.setState({ moveToSectionIndex: Number(event.currentTarget.value) })}>
                    {_.map(this.props.sections, (section, sectionIndex) => (
                      <option className="do-not-close" key={`item-${this.props.index}-move-to-section-${sectionIndex}`} value={sectionIndex}>{section.title}</option>
                    ))}
                  </select>
                  <span className="do-not-close">et mettre en </span>
                  <select className="form-control form-control-line do-not-close" onChange={event => this.setState({ moveToIndex: event.currentTarget.value })}>
                    <option className="do-not-close" value="top">haut</option>
                    <option className="do-not-close" value="bottom">bas</option>
                  </select>
                  <span className="do-not-close">de la liste. </span>
                  <button className="btn btn-success" onClick={() => this._moveItemToSection(this.props.index, this.props.itemIndex)}>Exécuter</button>
                </div>
              </div>
            </div>
          </SubMenu>

          <table className="table table-bordered table-head-silver">
            <tr>
              <th style={{ width: '25%' }}>
                <span style={{ marginRight: 10 }}>{this.props.globalIndex + 1}. </span>
                SKU
              </th>
              <th>Nom</th>
              <th style={{textAlign: 'center', width: '125px'}}>Qté</th>
              <th className="text-center" style={{textAlign: 'center', width: '125px'}}>Prix unitaire</th>
              <th className="text-center" style={{textAlign: 'center', width: '200px'}}>Total</th>
              <th className="item-tools" style={{width: '100px'}} rowSpan={2}>
              <Container>
                <RightRowSide>
                  <p>
                    <i className="fa fa-ellipsis-v do-not-close" onClick={() => this._toggleSubMenu()}>
                      {process.env.NODE_ENV === 'development' &&
                        '+'
                      }
                    </i>
                    <i className="la la-files-o" onClick={() => this.props.duplicateItemToSection(this.props.index, itemIndex)}></i>
                  </p>
                  <p>
                    <i className="fa fa-arrows-alt" {...this.props.draggableProvided.dragHandleProps}>
                      {process.env.NODE_ENV === 'development' &&
                        '+'
                      }
                    </i>
                    <i className="la la-trash-o" onClick={() => this.props.confirmationMethod(() => this.props.deleteItemOfSection(this.props.index, itemIndex), 'Voulez-vous supprimer cette ligne?') }></i>
                  </p>
                </RightRowSide>
              </Container>
              </th>
            </tr>
            <tr onFocus={() => foldAllOtherItem(itemIndex, index)}>
              <td style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', border: 0 }}>
                <div style={{ width: '100%' }}>
                  <InlineTextInput
                    style={{
                      input: {
                        lineHeight: '20px',
                        fontSize: item.SKU.length < 32 ? '14px' : `${(item.SKU.length * -0.102) + 17.264 >= 4 ? (item.SKU.length * -0.102) + 17.264 : 4}px`
                      }
                    }}
                    forceValue
                    isItem={true}
                    fullWidth={true}
                    loadOptions={this.props.loadOptions}
                    value={item.SKU}
                    title=""
                    placeholder=""
                    noLabel={true}
                    onChange={(value: string) => {
                      this.props.updateQuoteItem(this.props.index, itemIndex, 'SKU', value);
                    }}
                    autosuggestCallback={(item: any) => {
                      this.props.updateQuoteItem(this.props.index, itemIndex, 'SKU', item.SKU).then(() => {
                        if (item.SKU) {
                          this._updateItem(item, itemIndex);

                          const descTextarea = document.getElementById(`textarea-desc-${index}-${itemIndex}`) as HTMLTextAreaElement;

                          if (!descTextarea) {
                            return;
                          }

                          descTextarea.value = item.description;
                        }
                      })
                    }}
                  />
                </div>
              </td>
              <td>
                <InlineTextInput
                  forceValue
                  isItem={true}
                  fullWidth={true}
                  loadOptions={this.props.loadOptions}
                  value={item.title}
                  title=""
                  placeholder=""
                  noLabel={true}
                  onChange={(value: string) => {
                    this.props.updateQuoteItem(this.props.index, itemIndex, 'title', value);
                  }}
                  autosuggestCallback={(item: any) => {
                    this._updateItem(item, itemIndex);

                    const descTextarea = document.getElementById(`textarea-desc-${index}-${itemIndex}`) as HTMLTextAreaElement;

                    if (!descTextarea) {
                      return;
                    }

                    descTextarea.value = item.description;
                  }}
                />
              </td>
              <td>
                <InlineTextInput
                  inputClassName="text-right"
                  value={item.quantity.toString()}
                  title=""
                  placeholder=""
                  noLabel={true}
                  onChange={(value: string) => this._numberUpdate(value, 'quantity')}
                />
              </td>
              <td className="text-right">
                {moneyFormat(item.calculateUnitPrice())}
              </td>
              <td className="text-right">
                {moneyFormat(item.calculateLineTotal())}
              </td>
            </tr>
          </table>

          <table className="table table-bordered table-head-silver">

              <tr>
                <th style={{ fontWeight: 700 }}>Fournisseur</th>
                <th style={{ textAlign: 'center', fontWeight: 700 }}>Prix liste</th>
                <th style={{ width: '25%', textAlign: 'center', fontWeight: 700 }}>Coût logistique</th>
                <th style={{ width: '25%', textAlign: 'center', fontWeight: 700 }} colSpan={2}>Calcul du prix</th>
              </tr>

            <tbody>
              <tr>
                <td style={{ width: '25%' }}>
                  <div style={{ width: '100%', display: 'flex', flexDirection: 'row' }}>
                    <select
                      className="form-control"
                      value={item.discount.name}
                      onChange={e => {
                        const name = e.currentTarget.value;

                        const discount = _.find(this.props.availableDiscounts, dis => dis.name === name);

                        if (!discount) {
                          return;
                        }

                        this.props.updateQuoteItem(this.props.index, itemIndex, 'discount', discount);
                      }}
                      name="discounts"
                      id="discounts"
                      style={{ width: '100%' }}
                    >
                      {item.discount.name === '' &&
                        <option value="-1">Choisir un fournisseur</option>
                      }
                      {_.map(_.filter(this.props.availableDiscounts, dis => dis.name !== ''), discount => (
                        <option key={`available-discount-${discount.name}`} value={discount.name}>{discount.name}</option>
                      ))}
                    </select>
                  </div>
                </td>
                <td  style={{ width: '15%' }}>
                  <InlineTextInput
                    inputClassName="text-right"
                    value={item.price ? item.price.toString() : '0'}
                    title=""
                    placeholder=""
                    noLabel={true}
                    max={7}
                    onChange={(value: string) => {
                      this._numberUpdate(value, 'price');
                    }}
                  />
                </td>

                <ShippingCost
                  method={item.expeditionPriceMethod || 'global'}
                  onChange={(value, method) => {
                    this._numberUpdate(value.toString(), 'expeditionPrice');
                    this.props.updateQuoteItem(this.props.index, itemIndex, 'expeditionPriceMethod', method);
                  }}
                  value={item.expeditionPrice}
                  basedOn={item}
                />

                <td >

                    <select className="form-control form-control-line" value={item.profitMethod} onChange={e => {
                      const profitMethod = e.currentTarget.value;

                      this.props.updateQuoteItem(this.props.index, itemIndex, 'profitMethod', profitMethod);
                    }}>
                    {_.map(profitMethodStrings, (method, index: number) => (
                      <option key={`option-${index}`} value={profitMethodindexes[index]}>{getPriceMethodStringBasedOnValue(index)}</option>
                    ))}
                    </select>

                </td>
                <td>

                    <InlineTextInput
                      value={item.priceParameterValue.toString()}
                      title=""
                      placeholder=""
                      noLabel={true}
                      onChange={(value: string) => {
                        this._numberUpdate(value, 'priceParameterValue');
                      }}
                    />

                </td>



              </tr>
            </tbody>
          </table>

          <table className="table table-bordered">
            <tr>
              <td style={{width: '100px'}}>
                <strong>Description</strong>
              </td>
              <td>
                <TextareaAutosize
                  id={`textarea-desc-${index}-${itemIndex}`}
                  defaultValue={item.description}
                  onChange={e => {
                      this.props.updateQuoteItem(this.props.index, itemIndex, 'description', e.currentTarget.value);
                    }
                  }
                  style={{ borderRadius: 0, borderColor: 'rgba(0,0,0,.1)', padding: '.65rem 1.25rem', fontFamily: 'sans-serif, Arial', fontSize: '1rem', lineHeight: 1.5, color: '#495057' }}
                  onBlur={e => {
                    if (e.currentTarget.value.length < 35) {
                      e.currentTarget.parentElement && e.currentTarget.parentElement.classList.add('invalid');
                    } else {
                      e.currentTarget.parentElement && e.currentTarget.parentElement.classList.remove('invalid');
                    }
                  }}
                />
              </td>
            </tr>
          </table>

          <HeadRow className="grey-bg">
            <Container>
              <ColumnInSection style={{marginLeft: '25px'}}>
                <span>QTE</span>
                <span>{item.quantity}</span>
              </ColumnInSection>
              <ColumnInSection style={{marginLeft: '25px'}}>
                <span>Prix U</span>
                <span>{moneyFormat(item.calculateUnitPrice())}</span>
              </ColumnInSection>
              <ColumnInSection style={{marginLeft: '25px'}}>
                <span>Total vendant</span>
                <span>{moneyFormat(item.calculateUnitPrice() * item.quantity)}</span>
              </ColumnInSection>
              <ColumnInSection style={{marginLeft: '25px'}}>
                <span>Coût/U</span>
                <span>{moneyFormat(item.calculateCostPrice())}</span>
              </ColumnInSection>
              <ColumnInSection style={{marginLeft: '25px'}}>
                <span>Coût/Log</span>
                <span>{moneyFormat(item.calculateLineShipping())}</span>
              </ColumnInSection>
              <ColumnInSection style={{marginLeft: '25px'}}>
                <span>Coût tot/U</span>
                <span>{moneyFormat(item.calculateCostPrice() + (item.calculateLineShipping() / item.quantity))}</span>
              </ColumnInSection>
              <ColumnInSection style={{marginLeft: '25px'}}>
                <span>Coût total</span>
                <span>{moneyFormat((item.calculateCostPrice() * item.quantity) + item.calculateLineShipping())}</span>
              </ColumnInSection>
              <ColumnInSection style={{marginLeft: '25px'}}>
                <span>Profit $</span>
                <span>{moneyFormat(item.calculateItemProfit())}</span>
              </ColumnInSection>
              <ColumnInSection style={{marginLeft: '25px'}}>
                <span>GP%</span>
                <span>{percentFormat(item.calculateGrossProfit())}</span>
              </ColumnInSection>
            </Container>
          </HeadRow>
        </IBox>
      </div>
    );
  }
}
