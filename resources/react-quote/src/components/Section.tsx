import React from "react";

import { Draggable, Droppable, DraggableProvided, DroppableProvided } from "react-beautiful-dnd";
import IntextInput from "./Inputs/IntextInput";
import Item from "./Item";

import _ from "lodash";

import { ISection, IItem, IDiscount } from "../entities";
import { moneyFormat } from "../services";

interface ISectionProps {
  readonly index: number;
  readonly section: ISection;
  readonly addSection: () => void;
  readonly duplicateSection: (index: number) => void;
  readonly deleteSection: (index: number) => void;
  readonly addItemToSection: (index: number) => void;
  readonly duplicateItemToSection: (index: number, itemIndex: number) => void;
  readonly deleteItemOfSection: (index: number, itemIndex: number) => void;
  readonly toggleFolded: (index: number) => void;
  readonly updateQuoteItem: (
    index: number,
    itemIndex: number,
    param: string,
    value: any
  ) => Promise<any>;
  readonly updateQuoteSection: (param: string, value: any) => void;
  readonly updateQuoteItems: (items: Array<IItem>) => void;
  readonly loadOptions: Function;
  readonly availableDiscounts: Array<IDiscount>;
  readonly style?: React.CSSProperties;
  readonly isSelectingEnabled: boolean;
  readonly onSelectSectionChange: (id: number, value: boolean) => void;
  readonly onSelectItemChange: (id: number, value: boolean) => void;
  readonly isItemChecked: (id: number) => boolean;
  readonly isSectionChecked: (id: number) => boolean;
  readonly draggableProvided: DraggableProvided;
  readonly foldAllOtherItem: (itemToKeepOpen: number, inSection: number) => void;
  readonly handleSlider: (images: Array<string>) => void;
  readonly confirmationWithCallback: (callback: Function, msg: string) => void;
  readonly sections: Array<ISection>;
  readonly moveItemToSectionWithPosition: (prevItemIndex: number, prevSectionIndex: number, nextSectionIndex: number, position: 'top' | 'bottom') => void;
  readonly startIndex: number;
  readonly defaultLogisticCostMethod: 'global' | 'unit' | 'cost' | 'list';
}

interface ISectionState {
  readonly height: number | string;
  readonly width: number | string;
  readonly shouldUpdateHeight: boolean;
  readonly isFloating: boolean;
  readonly position: {
    readonly x: number;
    readonly y: number;
  };
}

export default class Section extends React.Component<ISectionProps> {
  public state: ISectionState = {
    height: "auto",
    width: 0,
    shouldUpdateHeight: true,
    isFloating: false,
    position: {
      x: 0,
      y: 0
    }
  };

  private interval: any;

  public componentDidMount() {
    const height = document.getElementsByClassName("section-container")[
      this.props.index
    ].clientHeight;

    this.setState({ height });
  }

  public componentWillUpdate(
    nextProps: ISectionProps,
    nextState: ISectionState
  ) {
    if (!nextProps.section.folded && this.state.height !== "auto") {
      this.setState({ height: "auto" });
    }

    if (
      this.state.position.y !== nextState.position.y &&
      this.state.isFloating
    ) {
      clearInterval(this.interval);
    }
  }

  public componentDidUpdate() {
    const height = document.getElementsByClassName("section-container")[
      this.props.index
    ].clientHeight;

    if (this.state.height === "auto") {
      this.setState({ height });
    }

    if (height !== this.state.height && this.state.shouldUpdateHeight) {
      this.setState({ height });
    }
  }

  public render() {
    const sectionTitle = this.props.section.title;

    return (
      <div
        className="section-container"
        id={`section-${this.props.index}`}
        style={{
          backgroundColor: '#ffffff',
          ...(this.props.section.folded ? { height: 57 } : {})
        }}
      >
        <div className="section-row">
          <div className="row custom-full-width">
            <div className="col-md-12 custom-full-width">

            </div>
          </div>
        </div>
        <div
          className="section-row sb section-header"
          id={`section-header-${this.props.index}`}
          {...this.props.draggableProvided.dragHandleProps}
        >
          <div className="section-row-container">

            <IntextInput
              maxLength={35}
              id={`section-${this.props.index}-input`}
              value={sectionTitle}
              onTextChange={(value: string) =>
                this.props.updateQuoteSection("title", value)
              }
            >
              <h3 style={{ minWidth: 50, minHeight: 29, border: '1px', borderStyle: 'solid', borderColor: '#dddddd', padding: '5px' }} className="can-click-on-listener">{sectionTitle}</h3>
            </IntextInput>
            <div className="fold">
              <p
                className="btn btn-outline-default"
                onClick={() =>
                  this.setState({ shouldUpdateHeight: false }, () =>
                    this.props.toggleFolded(this.props.index)
                  )
                }
              >
                {this.props.section.folded ? (
                  <i className="fa fa-chevron-down">
                    {process.env.NODE_ENV === "development" && "\\/"}
                  </i>
                ) : (
                  <i className="fa fa-chevron-up">
                    {process.env.NODE_ENV === "development" && "/\\"}
                  </i>
                )}
              </p>
            </div>
            <button
              className="btn btn-outline-success btn-air btn-rounded btn-sm"
              onClick={() => this.props.duplicateSection(this.props.index)}
            >
              <i className="la la-files-o"></i>
            </button>
            <button
              className="btn btn-outline-danger btn-air btn-rounded btn-sm"
              onClick={() => this.props.deleteSection(this.props.index)}
            >
              <i className="la la-trash-o"></i>
            </button>
            <div className="grabber">
              <i
                className="fa fa-arrows-alt"
              >
                {process.env.NODE_ENV === "development" && "+"}
              </i>
            </div>
          </div>
          <div className="section-row-container">
            <span>
              Bénéfice brut{" "}
              <span>
                {this.props.section.calculateGrossProfit().toFixed(2)} %
              </span>
            </span>
            <span>
              Coût{" "}
              <span>{moneyFormat(this.props.section.calculateCost())}</span>
            </span>
            <span>
              Profit{" "}
              <span>{moneyFormat(this.props.section.calculateProfit())}</span>
            </span>
          </div>
        </div>
        <Droppable
          droppableId={`section-${this.props.index}`}
          type="item"
          direction="vertical"
        >
          {(droppableProvided: DroppableProvided, snapshot) => (
            <div
              {...droppableProvided.droppableProps}
              ref={droppableProvided.innerRef}
              style={{ backgroundColor: snapshot.isDraggingOver ? '#2b95b638' : '', minHeight: snapshot.isDraggingOver ? '100px' : '0px', width: '100%' }}
            >
              {(this.props.section.items.length === 0) &&
                <div style={{ height: 50, width: '100%' }} ></div>
              }
              {_.map(
                this.props.section.items,
                (item, itemIndex: number) => {
                  return (
                    <Draggable draggableId={`section-${this.props.index}-item-${itemIndex}`} key={`section-${this.props.index}-item-${itemIndex}`} index={itemIndex}>
                      {(draggableProvided, snapshot) => (
                        <div
                          ref={draggableProvided.innerRef}
                          {...draggableProvided.draggableProps}
                        >
                          <Item
                            availableDiscounts={this.props.availableDiscounts}
                            key={`item-${this.props.index}-${itemIndex}`}
                            draggableProvided={draggableProvided}
                            itemsCount={this.props.section.items.length}
                            addItemToSection={this.props.addItemToSection}
                            deleteItemOfSection={
                              this.props.deleteItemOfSection
                            }
                            confirmationMethod={this.props.confirmationWithCallback}
                            duplicateItemToSection={
                              this.props.duplicateItemToSection
                            }
                            updateQuoteItem={this.props.updateQuoteItem}
                            index={this.props.index}
                            itemIndex={itemIndex}
                            isSelectingEnabled={
                              this.props.isSelectingEnabled
                            }
                            onSelectItemChange={
                              this.props.onSelectItemChange
                            }
                            item={item}
                            loadOptions={this.props.loadOptions}
                            isChecked={this.props.isItemChecked}
                            foldAllOtherItem={this.props.foldAllOtherItem}
                            handleSlider={this.props.handleSlider}
                            deletePicture={this.props.confirmationWithCallback}
                            sections={this.props.sections}
                            moveItemToSectionWithPosition={this.props.moveItemToSectionWithPosition}
                            globalIndex={this.props.startIndex + itemIndex}
                            style={{
                              ...(snapshot.isDragging ? {
                                boxShadow: '1px 1px 10px 3px #00000022',
                                transform: 'scale(1.05)'
                              } : {})
                            }}
                            defaultLogisticCostMethod={this.props.defaultLogisticCostMethod}
                          />
                          {draggableProvided.placeholder}
                        </div>
                      )}
                    </Draggable>
                  );
                }
              )}
              <div style={{ minHeight: snapshot.isDraggingOver ? '100px' : '0px', width: '100%' }}>
                {droppableProvided.placeholder}
              </div>
            </div>
          )}
        </Droppable>
        <div className="section-row mt-3">
          <div className="row custom-full-width">
            <div className="col-md-12 custom-full-width">
              <p
                className="btn btn-outline-success btn-air btn-rounded btn-sm btn-float-right"
                onClick={() => this.props.addItemToSection(this.props.index)}
              >
                <span className="btn-icon btn-float-right"><i className="la la-plus-circle"></i>Ajouter un produit</span>
              </p>
            </div>
          </div>
        </div>
        <div className="section-row top-bordered">
          <span>
            Sous-total de la section
            <span>{moneyFormat(this.props.section.calculateTotal())}</span>
          </span>
        </div>
      </div>
    );
  }
}
