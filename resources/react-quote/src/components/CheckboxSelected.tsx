import React from 'react';

interface ICheckboxSelectedProps {
  readonly visible: boolean;
  onSelectStateChange: (state: boolean) => void;
  isChecked: boolean;
};

interface ICheckboxSelectedState {};

export default class CheckboxSelected extends React.Component<ICheckboxSelectedProps> {
  public state: ICheckboxSelectedState = {};

  public render() {
    return (
      <div className="flex-center" style={{ height: 40, width: 40 }}>
        {this.props.visible &&
          // <input type="checkbox" onChange={e => this.props.onSelectStateChange(e.currentTarget.checked)}/>
          <div className="form-group">
            <label className="checkbox">
              <input type="checkbox" checked={this.props.isChecked} onChange={e => this.props.onSelectStateChange(e.currentTarget.checked)} />
              <span className="input-span"></span>
            </label>
          </div>
        }
      </div>
    );
  }
}
