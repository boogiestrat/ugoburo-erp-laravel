import React from 'react';
import InlineTextInput from './Inputs/InlineTextInput';

import _ from 'lodash';

import UBQuote from '../models/UBQuote';
import { moneyFormat } from '../services';
import { MoneyMethodEnum, IConfirmation } from '../entities';

interface ITotalProps {
  readonly quote: UBQuote;
  readonly updateQuote: (param: string, value: any) => Promise<any>;
  readonly onModalNeeded: (confirmation: IConfirmation) => void;
}

interface ITotalState {
  readonly shippingPercentValue: number;
}

export default class Total extends React.Component<ITotalProps> {
  public state: ITotalState = {
    shippingPercentValue: 0,
  }

  private _numberUpdate = (value: string, param: string) => {
    return !isNaN(parseFloat(value)) ? this.props.updateQuote(param, value[value.length - 1] === ',' || value[value.length - 1] === '.' ? value : parseFloat(value.replace(',', '.'))) : this.props.updateQuote(param, 0);
  }

  public render() {
    return (
      <div id="totals" className="section-container top-bordered" style={{ paddingBottom: 20 }}>
        <div style={{ width: '40%' }}>
          <div>
            <h4>Informations additionnelles</h4>
            <textarea className="quote-double-size-textarea" rows={5} defaultValue={this.props.quote.get().additionalNote} onChange={e => this.props.updateQuote('additionalNote', e.currentTarget.value)}></textarea>
          </div>
          <div className="mt-3">
            <h4>Informations additionnelles - <strong style={{fontWeight:900}}>INTERNE</strong></h4>
            <textarea className="quote-double-size-textarea" rows={5} defaultValue={this.props.quote.get().additionalNoteInterne} onChange={e => this.props.updateQuote('additionalNoteInterne', e.currentTarget.value)}></textarea>
          </div>
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            {_.map(this.props.quote.get().clauses, clause => (
              <div key={`clause-${clause.clause}`} className="form-group" style={{ marginRight: 10 }}>
                <label className="checkbox">
                  <input type="checkbox" checked={clause.value} onChange={e => this.props.updateQuote('clauses', _.map(this.props.quote.get().clauses, clauseInState => {
                    if (clauseInState.clause !== clause.clause) {
                      return clauseInState;
                    }

                    return { ...clauseInState, value: e.currentTarget.checked };
                  }))} />
                  <span className="input-span"></span>Clause {clause.clause}{clause.description && <span> - {clause.description}</span>}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="separator" style={{
          width: '5%',
          display: 'flex',
          justifyContent: 'center',
        }}>
          <span style={{
            width: 1,
            height: '100%',
            backgroundColor: '#bfbdbd',
          }}></span>
        </div>
        <div style={{ width: '55%' }}>
          <div className="total-detail">
            <div><h5>Sous-total</h5><span>{moneyFormat(this.props.quote.calculateOverallTotal())}</span></div>
            <hr />
            <div style={{ display: 'initial' }}>
              <div>
                <span style={{ width: '10%' }}>
                  <h5>Escompte</h5>
                </span>
                <select className="form-control form-control-line" style={{ width: 130 }} name="escompte-type-selector" id="escompte-type-selector" onChange={(e) => this.props.updateQuote('discountMethod', e.currentTarget.value)} value={this.props.quote.get().discountMethod}>
                  <option value="percent">%</option>
                  <option value="amount">$</option>
                </select>
                <InlineTextInput
                  leaveOnEnter
                  groupWith={<span className="input-group-addon">{this.props.quote.get().discountMethod === MoneyMethodEnum.PERCENT ? '%' : '$'}</span>}
                  title=""
                  noLabel={true}
                  placeholder=""
                  value={this.props.quote.get().discountParam.toString()}
                  onChange={(val) => {
                    this._numberUpdate(val, 'discountParam');
                  }}
                />
                <span>
                  {moneyFormat(this.props.quote.calculateDiscountTotal())}
                </span>
              </div>
            </div>
            <hr />
            <div style={{ display: 'initial' }}>
              <div>
                <span style={{ width: '10%' }}>
                  <h5>Frais</h5>
                </span>
                <select className="form-control form-control-line" style={{ width: 130 }} name="shipping-type-selector" id="shipping-type-selector" onChange={(e) => this.props.updateQuote('installationAndShippingMethod', e.currentTarget.value)} value={this.props.quote.get().installationAndShippingMethod}>
                  <option value="percent">%</option>
                  <option value="amount">$</option>
                </select>
                <InlineTextInput
                  leaveOnEnter
                  title=""
                  groupWith={<span className="input-group-addon">{this.props.quote.get().installationAndShippingMethod === MoneyMethodEnum.PERCENT ? '%' : '$'}</span>}
                  noLabel={true}
                  placeholder=""
                  value={this.props.quote.get().installationAndShippingParam.toString()}
                  onChange={(val) => {
                    this._numberUpdate(val, 'installationAndShippingParam');
                  }}
                />
                <span>
                  {moneyFormat(this.props.quote.calculateShippingAndInstallationTotal())}
                </span>
              </div>
            </div>
            <hr />
            {this.props.quote.get().billTo !== null ?
              _.map(this.props.quote.getTaxes(), (tax, taxIndex) => (
                <div key={`tax-${taxIndex}`}>{tax.valueDescription}<span>{moneyFormat(this.props.quote.calculateTax(tax.value))}</span></div>
              ))
            :
              <div></div>
            }
            <div><h5>TOTAL</h5><span>{moneyFormat(this.props.quote.calculateQuoteTotal())}</span></div>
            <hr/>
            <div className="custom-display-hidden">Dépôt<span>
              <InlineTextInput
                title=""
                groupWith={<span className="input-group-addon">$</span>}
                noLabel={true}
                placeholder=""
                value={this.props.quote.get().depot.toString()}
                onChange={(val) => {
                  this._numberUpdate(val, 'depot');
                }}
              />
            </span></div>
            {/* <div>Dépôt (%)<span>
              <InlineTextInput
                title=""
                groupWith={<span className="input-group-addon">%</span>}
                noLabel={true}
                placeholder=""
                value={this.props.quote.get().depotPercent ? this.props.quote.get().depotPercent.toFixed(0) : '0'}
                onChange={(val) => {
                  const value = Number(val);

                  if (isNaN(value)) {
                    return;
                  }

                  this.props.updateQuote('depotPercent', value);
                  this.props.updateQuote('depot', (Number(value) / 100) * this.props.quote.calculateQuoteTotal());
                }}
              />
            </span></div> */}
            <div className="mt-4">Solde<span>{moneyFormat(this.props.quote.calculateSolde())}</span></div>
          </div>
          <hr/>
          {this.props.quote.get().billTo.contact &&
            <button className="btn btn-primary btn-air btn-rounded btn-float-right" onClick={() => {
              if (!this.props.quote.get().billTo.client || !this.props.quote.get().shipTo.client) {
                this.props.onModalNeeded({
                  message: 'Les champs "Adresse de facturation" et "Adresse de livraison" doivent avoir une valeur.',
                  onCancel: () => {},
                  onOk: () => {},
                  title: 'Attention'
                } as IConfirmation)
                return;
              }

              this.setState({ saving: true });
              this.props.quote
                .saveQuote()
                .then(response => {
                  if (response.valid) {
                    if (response.id) {
                      this.props.updateQuote('id', Number(response.id));
                    }
                    this.setState({ saving: false });
                  } else {
                    this.setState({ saving: false });
                  }
                })
                .catch(error =>
                  this.setState({ saving: false }, () =>
                    alert(
                      'Attention, erreur lors de la sauvegarde. Veuillez réessayer.'
                    )
                  )
                );
              this.props.quote.emailModal();
            }}>
              <span className="btn-icon">
                <i className="la la-paper-plane"></i>Envoyer au client
              </span>
            </button>
          }
        </div>
      </div>
    );
  }
}
