import React from 'react';

import _ from 'lodash';

import UBQuote from '../models/UBQuote';
import { moneyFormat, getBaseUrl } from '../services';

import * as Scroll from 'react-scroll';
import { Link, Element , Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll';
import { IConfirmation, IDiscount } from '../entities';

interface IOverviewProps {
  readonly quote: UBQuote;
  readonly closeAllSections: (actual: boolean, callback: (value: boolean) => void) => void;
  readonly saveQuote: () => Promise<any>;
  readonly updateQuote: (param: string, value: any) => void;
  readonly closeImportModal: () => void;
  readonly openImportModal: () => void;
  readonly onInLotStateChange: (isInLotUpdating: boolean) => void;
  readonly isInLotUpdating: boolean;
  readonly onModalNeeded: (confirmation: IConfirmation) => void;
  readonly addNewSection: () => Promise<void>;
  readonly availableDiscounts: Array<IDiscount>;
  readonly updateDiscounts: (discounts: Array<IDiscount>) => void;
  readonly loadPossibleDiscount: (inputValue: string, callback: Function) => void;
  readonly isCSVImportOpen: boolean;
  readonly deleteDiscount: (discountIndex: number, discountName: string, callback: Function) => void
}

interface IOverviewState {
  readonly saving: boolean;
  readonly discounts: Array<IDiscount>;
  readonly discountNameToModidy: string;
  readonly discountToModify: IDiscount;
  readonly discountAfter: IDiscount;
  readonly isModifying: boolean;
  readonly isGenPDF: boolean;
  readonly isAdmin: boolean;
  readonly isClosed: boolean;
  readonly availableDiscounts: Array<IDiscount>;
  readonly applyTo: { readonly index: number; readonly value: string; };
}

export default class Overview extends React.Component<IOverviewProps> {
  public state: IOverviewState = {
    saving: false,
    discountAfter: {
      code: "",
      name: "",
      parts: [0, 0, 0, 0, 0]
    },
    discountNameToModidy: "",
    isModifying: false,
    isGenPDF: false,
    isAdmin: false,
    isClosed: false,
    discountToModify: {
      code: "",
      name: "",
      parts: [0, 0, 0, 0, 0]
    },
    discounts: [],
    availableDiscounts: [],
    applyTo: { index: -1, value: '-1' }
  };

  public render() {
    return (
      <div className="overview-wrapper">
        <ul>
          <li>
            <span><strong>Vendant </strong> {this.props.quote.calculateOverallTotal().toFixed(2)} $</span>
          </li>
          <li>
            <span><strong>Coût </strong> {this.props.quote.calculateOverallCost().toFixed(2)} $</span>
          </li>
          <li>
            <span><strong>Profit </strong> {this.props.quote.calculateOverallProfit().toFixed(2)} $</span>
          </li>
          <li>
            <span><strong>GP </strong> {this.props.quote.calculateOverallGrossProfit().toFixed(2)} %</span>
          </li>
          <li>
            <a target="_blank" rel="noopener noreferrer" style={{ textDecoration: 'none', display: 'flex', alignItems: 'center', justifyContent: 'center' }} onClick={() => {this.props.quote.saveQuote()}} href={getBaseUrl() + 'webservices/quote/pdf/generate.php?id=' + this.props.quote.get().number} className="btn btn-success btn-air btn-rounded"><span className="btn-icon" style={{ fontSize: '11px' }}><i className="la la-file-pdf-o"></i> Client</span></a>
          </li>
          <li>
            <a target="_blank" rel="noopener noreferrer" style={{ textDecoration: 'none', display: 'flex', alignItems: 'center', justifyContent: 'center' }} onClick={() => {this.props.quote.saveQuote()}} href={getBaseUrl() + 'webservices/quote/pdf/generate.php?admin=1&id=' + this.props.quote.get().number} className="btn btn-success btn-air btn-rounded"><span className="btn-icon" style={{ fontSize: '11px' }}><i className="la la-file-pdf-o"></i> Admin</span></a>
          </li>
          <li>
            <button
              className="btn btn-success btn-air btn-rounded"
              onClick={() => this.props.openImportModal()}
            >
              <span className="btn-icon"><i className="la la-cloud-upload"></i> CSV</span>
            </button>
          </li>
          <li>
            <button
              className="btn btn-success btn-air btn-rounded"
              onClick={() => this.props.closeAllSections(this.state.isClosed, (isClosed) => this.setState({ isClosed }))}
            >
              <span className="btn-icon"><i className={`la la-chevron-${this.state.isClosed ? 'down' : 'up'}`}></i> {this.state.isClosed ? 'Ouvrir' : 'Fermer'}</span>
            </button>
          </li>
          <li>
            <button
              disabled={this.state.saving}
              className="btn btn-primary btn-air btn-rounded"
              onClick={() => {
                if (!this.props.quote.get().billTo.client || !this.props.quote.get().shipTo.client) {
                  this.props.onModalNeeded({
                    message: 'Les champs "Adresse de facturation" et "Adresse de livraison" doivent avoir une valeur.',
                    onCancel: () => {},
                    onOk: () => {},
                    title: 'Attention'
                  } as IConfirmation)
                  return;
                }

                this.setState({ saving: true });
                this.props.quote
                  .saveQuote()
                  .then(response => {
                    if (response.valid) {
                      if (response.id) {
                        this.props.updateQuote('id', Number(response.id));
                      }
                      this.setState({ saving: false });
                    } else {
                      this.setState({ saving: false });
                    }
                  })
                  .catch(error =>
                    this.setState({ saving: false }, () =>
                      alert(
                        'Attention, erreur lors de la sauvegarde. Veuillez réessayer.'
                      )
                    )
                  );
              }}
            >
              <span className="btn-icon"><i className="la la-save"></i>{this.state.saving ? 'Sauvegarde...' : 'Sauvegarder'}</span>
            </button>
          </li>
        </ul>
      </div>
    );
  }
}
