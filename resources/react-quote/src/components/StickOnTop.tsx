import React from 'react';

const HEADER_CLASS: string = 'header';
const SIDEBAR_CLASS: string = 'page-sidebar';
const CONTENT_PADDING: number = 30;

interface IStickOnTopProps {}
interface IStickOnTopState {
  readonly isFixOnTop: boolean;
  readonly headerHeight: number;
  readonly sideBarWidth: number;
  readonly yPosition: number;
  readonly childrenHeight: number;
  readonly initiate: boolean;
}

export default class StickOnTop extends React.Component<IStickOnTopProps> {
  public state: IStickOnTopState = {
    isFixOnTop: true,
    headerHeight: 0,
    yPosition: 9999,
    childrenHeight: 0,
    sideBarWidth: 0,
    initiate: false,
  }

  public componentDidMount() {
    window.addEventListener('scroll', this._onScroll);
    window.addEventListener('resize', this._reInit);
  }

  private _getChildrenPosition = () => {
    const sticky = this.refs.sticky as HTMLElement;
    const rect = sticky.getBoundingClientRect();
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    return scrollTop + rect.top;
  }

  private _init = () => {
    setTimeout(() => {
      this.setState({ headerHeight: this._getHeaderHeight(), sideBarWidth: this._getSideBarWidth(), childrenHeight: this._getChildrenHeight(), yPosition: this._getChildrenPosition(), initiate: true });
    }, 250);
  };

  private _reInit = () => this.setState({ headerHeight: this._getHeaderHeight(), sideBarWidth: this._getSideBarWidth(), childrenHeight: this._getChildrenHeight(), initiate: true });

  public componentWillUnmount() {
    window.removeEventListener('scroll', this._onScroll);
    window.addEventListener('resize', this._reInit);
  }

  private _onScroll = () => {
    if (!this.state.initiate) {
      this._init();
    }

    const yPosition = window.scrollY + this.state.headerHeight;

    this.setState({ isFixOnTop: yPosition >= this.state.yPosition + this.state.headerHeight });
  }

  private _getHeaderHeight = () => {
    const header = document.getElementsByClassName(HEADER_CLASS)[0] as HTMLElement;

    if (!header) {
      return 0;
    }

    return header.clientHeight;
  }

  private _getSideBarWidth = () => {
    const sidebar = document.getElementsByClassName(SIDEBAR_CLASS)[0] as HTMLElement;

    if (!sidebar) {
      return 0;
    }

    return sidebar.clientWidth;
  }

  private _getChildrenHeight = () => {
    const header = this.refs.sticky as HTMLElement;

    if (!header) {
      return 0;
    }

    return header.clientHeight;
  }

  public render() {
    const clientElement = document.getElementById('quote-app') as HTMLElement;

    return (
      <div>
        <div ref="sticky" className={`nav-will-be ${this.state.isFixOnTop ? 'fixed' : 'free'}`} style={this.state.isFixOnTop ? {
          top: 0,
          left: this.state.sideBarWidth,
          width: clientElement.clientWidth + CONTENT_PADDING
        } : {} as React.CSSProperties}>
          {this.props.children}
        </div>
          <div className="holder" style={{
            height: (this.state.isFixOnTop ? 1 : 0) * (this.state.childrenHeight + 20)
          }}>
          </div>
      </div>
    )
  }
}
