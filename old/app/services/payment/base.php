<?php

namespace Ekosys\Payment;

class Base{

    function getTemplatePart($data,$part){
        $html = get_html_template_part($data,"payment/$part.php");
        return $html;
    }

}