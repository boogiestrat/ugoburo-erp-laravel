<?php

namespace Ekosys\Payment;

use Ekosys\Payment\Base;
use GlobalPayments\Api\ServicesConfig;
use GlobalPayments\Api\HostedPaymentConfig;
use GlobalPayments\Api\Entities\Enums\HppVersion;
use GlobalPayments\Api\Services\HostedService;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\HostedPaymentData;
use GlobalPayments\Api\Entities\Enums\AddressType;
use GlobalPayments\Api\Entities\Exceptions\ApiException;

class GlobalPaymentHosted extends Base{

    const PAYMENT_TYPE = 'global_payment';
    const SANDBOX_URL = "https://pay.sandbox.realexpayments.com/pay";
    const LIVE_URL = "https://pay.realexpayments.com/pay";
    const RESPONSE_CODE_SUCCESS = '00';

    public function isEnabled(){
        return getenv('GLOBAL_PAYMENT_ENABLED');
    }

    public function isSandbox(){
        return getenv('GLOBAL_PAYMENT_SANDBOX_MODE');
    }

    function getRequestEndPoint(){
        return getenv('BASE_URL').'webservices/payment/global-payment-gateway.php';
    }

    function getResponseEndPoint(){
        return getenv('BASE_URL').'webservices/payment/global-payment-gateway.php';
    }

    public function getApiUrl(){
        if($this->isSandbox()){
            return self::SANDBOX_URL;
        }else{
            return self::LIVE_URL;
        }
    }

    private function getMerchantId(){
        return getenv('GLOBAL_PAYMENT_MERCHANT_ID');
    }
    private function getAccountId(){
        return getenv('GLOBAL_PAYMENT_ACCOUNT_ID');
    }
    private function getSharedSecret(){
        return getenv('GLOBAL_PAYMENT_SHARED_SECRET');
    }
    private function getRebatePassword(){
        return getenv('GLOBAL_PAYMENT_REBATE');
    }

    private function getConfig($lang='en'){

        $config = new ServicesConfig();
        $config->merchantId = $this->getMerchantId();
        $config->accountId = $this->getAccountId();
        $config->sharedSecret = $this->getSharedSecret();
        $config->serviceUrl = $this->getApiUrl();
        $config->rebatePassword = $this->getRebatePassword();

        $config->hostedPaymentConfig = new HostedPaymentConfig();
        $config->hostedPaymentConfig->version = HppVersion::VERSION_2;
        $config->hostedPaymentConfig->language = $lang;


        return $config;
    }

    private function getService($lang='en'){
        return new HostedService($this->getConfig($lang));
    }

    public function mapCountry($country){
        $data = (new \League\ISO3166\ISO3166)->name($country);
        return $data['numeric'];
    }

    public function formatNumber($number){
        return  preg_replace('/[^0-9.]+/', '', $number);
    }

    public function setRegistry($amount,$quote,$language,$emails,$isDeposit){
        $_SESSION['registry'] = json_encode(
            [
                'amount'=>$amount,
                'quote_id'=>$quote['id'],
                'language'=>$language,
                'emails'=>$emails,
                'is_deposit'=>$isDeposit,
                'referer'=>(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"
            ]
        );
    }

    public function getRegistry(){
        return json_decode($_SESSION['registry']);
    }

    public function getHPP($amount,$quote,$language,$emails,$isDeposit){

        $quoteJsonData = json_decode($quote['data']);
        $service = $this->getService($language);

        // Add 3D Secure 2 Mandatory and Recommended Fields
        $hostedPaymentData = new HostedPaymentData();
        $hostedPaymentData->customerEmail =$quoteJsonData->billTo->contact->email;
        $hostedPaymentData->customerPhoneMobile = $this->formatNumber($quoteJsonData->billTo->contact->phone);
        $hostedPaymentData->addressesMatch = false;
        $hostedPaymentData->supplementaryData = ['quote_id'=>$quote['id'],'emails'=>$emails,'isDeposit'=>$isDeposit];


        $billingAddress = new Address();
        $billingAddress->streetAddress1 = $quoteJsonData->billTo->address->address;
        $billingAddress->city = $quoteJsonData->billTo->address->city;
        $billingAddress->postalCode = $quoteJsonData->billTo->address->postal_code;
        $billingAddress->country = $this->mapCountry($quoteJsonData->billTo->address->country);
        $billingAddress->state = $quoteJsonData->billTo->address->province;

        $shippingAddress = new Address();
        $billingAddress->streetAddress1 = $quoteJsonData->shipTo->address->address;
        $billingAddress->city = $quoteJsonData->shipTo->address->city;
        $billingAddress->postalCode = $quoteJsonData->shipTo->address->postal_code;
        $billingAddress->country = $this->mapCountry($quoteJsonData->shipTo->address->country);
        $billingAddress->state = $quoteJsonData->shipTo->address->province;

        try {
            $hppJson = $service->charge($amount)
                ->withCurrency("CAD")
                ->withHostedPaymentData($hostedPaymentData)
                ->withAddress($billingAddress, AddressType::BILLING)
                ->withAddress($shippingAddress, AddressType::SHIPPING)
                ->serialize();


            $this->setRegistry($amount,$quote,$language,$emails,$isDeposit);

            return $hppJson;
            // TODO: pass the HPP request JSON to the JavaScript, iOS or Android Library

        } catch (ApiException $e) {
            return $e;
        }
    }

    public function getForm($amount,$isDeposit,$quote,$emails,$language){
        return $this->getTemplatePart([
            'apiURL'=>$this->getApiUrl(),
            'jsonResponse'=>$this->getHPP($amount,$quote,$language,$emails,$isDeposit),
            //'requestEndpoint'=>$this->getRequestEndPoint(),
            'responseEndPoint'=>$this->getResponseEndPoint(),

        ], 'global-payment/form');
    }

    public function processResponse($responseJson){
        $msg = new \Plasticbrain\FlashMessages\FlashMessages();

        $service = $this->getService();
        try {

            // create the response object from the response JSON
            $parsedResponse = $service->parseResponse($responseJson, true);
            $registry = $this->getRegistry();

            $responseCode = $parsedResponse->responseCode; // 00
            $responseMessage = $parsedResponse->responseMessage; // [ test system ] Authorised
            $responseValues = $parsedResponse->responseValues; // get values accessible by key


            $orderId = $responseValues['ORDER_ID'];
            $quoteId = $registry->quote_id;
            $isDeposit = $registry->is_deposit;
            $recipients = $registry->emails;
            $language = $registry->language;
            $referer = $registry->referer;

            if($responseCode==self::RESPONSE_CODE_SUCCESS){

                $amount = number_format(($responseValues["AMOUNT"] / 100),2,'.','');

                $quotePayment = new \Ekosys\Entities\QuotePayment();
                $quotePayment->setAmount($amount);
                $quotePayment->setCardNumber( $parsedResponse->cardLast4?$parsedResponse->cardLast4:'xxxx');
                $quotePayment->setDateCreated(new \DateTime());
                $quotePayment->setClientFirstName($responseValues['HPP_CUSTOMER_EMAIL']);
                $quotePayment->setClientLastName('');
                $quotePayment->setPaymentMethod(self::PAYMENT_TYPE);
                $quotePayment->setTransactionId($orderId);
                $quotePayment->setQuoteId($quoteId);
                $quotePayment->setIsDeposit($isDeposit);
                $quotePayment->save();

                //update quote if deposit

                $quote = get_quote($quoteId);
                $quote_data = json_decode($quote['data'],JSON_OBJECT_AS_ARRAY);

                if($isDeposit){
                    $quote_data['depot'] = $amount;
                    $quote_data['status'] = \Quote::STATUS_DEPOSIT;
                }else{
                    $quote_data['status'] = \Quote::STATUS_PAID;
                }

                $quote = new \Quote($quote_data);
                $quote->update();

                update_quote_status($quoteId,$quote_data['status']);

                send_quote_payment_confirmation_email(get_quote($quoteId),$isDeposit,$amount,$recipients);

                log_data('client', '/' . \LoggerSection::payment . '/' . \LoggerType::add . '/{' . $quotePayment->getId() . '}', '{"data": ' . $quotePayment->toJson() . '}', \LoggerSection::payment, \LoggerType::add);

                $msg->success(__('payment_success'),$referer,true);
                return true;
            }


        } catch (ApiException $e) {
            // For example if the SHA1HASH doesn't match what is expected
            // TODO: add your error handling here
            $msg->error($e->getMessage(),$referer,true);
        }

    }

}