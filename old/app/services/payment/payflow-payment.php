<?php

namespace Ekosys\Payment;

include "payflow/sdk.php";

class PayflowPayment extends Base {

    const PAYMENT_TYPE = 'paypal_payflow';

    const ENV_TEST='test';
    const ENV_LIVE = 'live';

    public function getEnvironment(){
        return getenv('PAYPAL_PAYFLOW_MODE');
        //return 'live';
    }

    public function isSandbox(){
        return $this->getEnvironment()==self::ENV_TEST;
    }

    function getSdk(){
        return new \PayFlowSDk(
            getenv('PAYPAL_PAYFLOW_VENDOR'),
            getenv('PAYPAL_PAYFLOW_PARTNER'),
            getenv('PAYPAL_PAYFLOW_USER'),
            getenv('PAYPAL_PAYFLOW_PASSWORD'),
            getenv('PAYPAL_PAYFLOW_BILLING_TYPE')
        );
    }

    public function charge($ccNumber,$cvv,$expiration,$cardName,$quote,$amount,$firstname,$lastname){

        $amount = (float)str_replace(',','.',$amount);

        if($this->isSandbox()) {
            $amount = 100.50;
        }

        $sdk = $this->getSdk();

        $sdk->setEnvironment('test');                           // test or live
        $sdk->setTransactionType('S');                          // S = Sale transaction, R = Recurring, C = Credit, A = Authorization, D = Delayed Capture, V = Void, F = Voice Authorization, I = Inquiry, N = Duplicate transaction
        $sdk->setPaymentMethod('C');                            // A = Automated clearinghouse, C = Credit card, D = Pinless debit, K = Telecheck, P = PayPal.
        $sdk->setPaymentCurrency('CAD');                        // 'USD', 'EUR', 'GBP', 'CAD', 'JPY', 'AUD'.
        $sdk->setAmount($amount, FALSE);
        $sdk->setCCNumber($ccNumber);
        $sdk->setCVV($cvv);
        $sdk->setExpiration($expiration);
        $sdk->setCreditCardName($cardName);


        $sdk->setCustomerFirstName($firstname);
        $sdk->setCustomerLastName($lastname);
        /*$sdk->setCustomerAddress('589 8th Ave Suite 10');
        $sdk->setCustomerCity('New York');
        $sdk->setCustomerState('NY');
        $sdk->setCustomerZip('10018');
        $sdk->setCustomerCountry('US');
        $sdk->setCustomerPhone('212-123-1234');
        $sdk->setCustomerEmail($email);*/
        $sdk->setPaymentComment('Payment of '.$quote['number']);

        return ['result'=>$sdk->processTransaction(),'response'=>$sdk->getResponse()];

        /*if($sdk->processTransaction()):
            echo('Transaction Processed Successfully!');
        else:
            echo('Transaction could not be processed at this time.');
        endif;

        echo('<h2>Name Value Pair String:</h2>');
        echo('<pre>');
        print_r($sdk->debugNVP('array'));
        echo('</pre>');

        echo('<h2>Response From Paypal:</h2>');
        echo('<pre>');
        print_r($sdk->getResponse());
        echo('</pre>');

        exit;*/

    }

    function getFormUrl(){
        return BASE_URL.'webservices/payment/payflow-gateway.php';
    }

    public function getForm($amount,$isDeposit,$quote,$emails,$language){

        $quoteJsonData = json_decode($quote['data']);

        return $this->getTemplatePart(
            [
                'url'=>$this->getFormUrl(),
                'amount'=>$amount,
                'quoteId'=>$quote['id'],
                'firstName'=>$quoteJsonData->billTo->contact->first_name,
                'lastName'=>$quoteJsonData->billTo->contact->last_name,
                'isDeposit'=>$isDeposit,
                'emails'=>$emails,
                'language'=>$language
            ],
            'payflow-payment/form');
    }

    public function validate($poNumber){

        if(!$poNumber){
            return __('po-payment_error-po-number');
        }

        return true;
    }

}
