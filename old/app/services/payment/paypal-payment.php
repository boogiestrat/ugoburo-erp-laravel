<?php

namespace Ekosys\Payment;



class PaypalPayment{

    const PAYMENT_TYPE = 'paypal_braintree';
    const MODE_SANDBOX = 'sandbox';
    const MODE_LIVE = 'live';

    public function isEnabled(){
        return getenv('PAYPAL_BRAINTREE_ENABLED');
    }

    public function isSandbox(){
        return getenv('PAYPAL_BRAINTREE_SANDBOX_MODE');
    }

    public function getEnvironment(){
        if($this->isSandbox()){
           return self::MODE_SANDBOX;
        }else{
            return self::MODE_LIVE;
        }
    }

    public function getMerchantId(){
        if($this->isSandbox()){
            return getenv('PAYPAL_BRAINTREE_SANDBOX_MERCHANT_ID');
        }else{
            return getenv('PAYPAL_BRAINTREE_LIVE_MERCHANT_ID');
        }
    }

    public function getPublicKey(){
        if($this->isSandbox()){
            return getenv('PAYPAL_BRAINTREE_SANDBOX_PUBLIC_KEY');
        }else{
            return getenv('PAYPAL_BRAINTREE_LIVE_PUBLIC_KEY');
        }
    }

    public function getPrivateKey(){
        if($this->isSandbox()){
            return getenv('PAYPAL_BRAINTREE_SANDBOX_PRIVATE_KEY');
        }else{
            return getenv('PAYPAL_BRAINTREE_LIVE_PRIVATE_KEY');
        }
    }

    public function checkConfiguration(){
        $configs = [
            $this->getEnvironment(),
            $this->getMerchantId(),
            $this->getPublicKey(),
            $this->getPrivateKey()
        ];

        foreach ($configs as $config){
            if(!$config) throw new \Exception("Vous devez configurer Paypal Braintree pour pouvoir utiliser cette méthode de paiement");
        }
    }

    public function getGateway(){

        $this->checkConfiguration();

        $gateway = new \Braintree\Gateway([
            'environment' => $this->getEnvironment(),
            'merchantId' => $this->getMerchantId(),
            'publicKey' => $this->getPublicKey(),
            'privateKey' => $this->getPrivateKey()
        ]);

        return $gateway;
    }

    function getFormUrl(){
        return PATH.'webservices/payment/paypal-gateway.php';
    }

    function getForm($amount,$isDeposit,$quoteId,$firstName,$lastName,$emails,$language){

        try {
            $gateway = $this->getGateway();
            return $this->getTemplatePart([
                'gateway'=>$gateway,
                'url'=>$this->getFormUrl(),
                'amount'=>$amount,
                'quoteId'=>$quoteId,
                'firstName'=>$firstName,
                'lastName'=>$lastName,
                'isDeposit'=>$isDeposit,
                'emails'=>$emails,
                'language'=>$language

            ],
                'paypal/form');

        }catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    function charge($amount,$nonce){
        $amount =$this->formatAmount($amount);
        $gateway =$this->getGateway();
        $result = $gateway->transaction()->sale([
            'amount' => $amount,
            'paymentMethodNonce' => $nonce,
            'options' => [
                'submitForSettlement' => true
            ]
        ]);

        return $result;
    }

    function formatAmount($amount){
        $amount = str_replace(',','.',$amount);
        return number_format($amount,2,'.','');
    }

}