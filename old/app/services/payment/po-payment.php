<?php

namespace Ekosys\Payment;

class PoPayment extends Base {

    const PAYMENT_TYPE = 'purchase_order';

    function getFormUrl(){
        return BASE_URL.'webservices/payment/po-gateway.php';
    }

    public function getForm($amount,$isDeposit,$quote,$emails,$language){

        $quoteJsonData = json_decode($quote['data']);

        return $this->getTemplatePart(
            [
                'url'=>$this->getFormUrl(),
                'amount'=>$amount,
                'quoteId'=>$quote['id'],
                'firstName'=>$quoteJsonData->billTo->contact->first_name,
                'lastName'=>$quoteJsonData->billTo->contact->last_name,
                'isDeposit'=>$isDeposit,
                'emails'=>$emails,
                'language'=>$language
            ],
            'po-payment/form');
    }

    public function validate($poNumber){

        if(!$poNumber){
            return __('po-payment_error-po-number');
        }

        return true;
    }

}
