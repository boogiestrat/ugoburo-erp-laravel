<?php

## Needs to be automated
## include_once(WEB_ROOT . 'services/serviceName.php');

include_once(APP_ROOT . 'services/template.php');
include_once(APP_ROOT . 'services/address.php');
include_once(APP_ROOT . 'services/contact.php');
include_once(APP_ROOT . 'services/client.php');
include_once(APP_ROOT . 'services/employee.php');
include_once(APP_ROOT . 'services/item.php');
include_once(APP_ROOT . 'services/manufacturier.php');
include_once(APP_ROOT . 'services/itemTask.php');
include_once(APP_ROOT . 'services/quote.php');
include_once(APP_ROOT . 'services/order.php');
include_once(APP_ROOT . 'services/logger.php');
include_once(APP_ROOT . 'services/authMiddleware.php');
include_once(APP_ROOT . 'services/ssp.class.php');
include_once(APP_ROOT . 'services/quote.class.php');
include_once(APP_ROOT . 'services/payment/base.php');
include_once(APP_ROOT . 'services/payment/paypal-payment.php');
include_once(APP_ROOT . 'services/payment/global-payment.php');
include_once(APP_ROOT . 'services/payment/po-payment.php');
include_once(APP_ROOT . 'services/payment/payflow-payment.php');
include_once(APP_ROOT . 'services/payment/pool.php');
include_once(APP_ROOT . 'services/client.class.php');
