<?php

/**
 * 1. Check if the user exists
 * 2. Validate the email with the Google Service
 */

class AuthMiddleware {
    const MAX_CONNECTION_TIME = 24 * 60 * 60 * 60; // 1 day in seconds

    private $_user;

    function __construct() {
        if (!$this->_checkIfSessionIsValid()) {
            $this->_logoutUser();
        }

        $this->_validateUserInDB();

        $this->_validateToken();
    }

    function _getGoogleToken() {

    }

    function _validateUserInDB() {
        $userFromSession = get_current_employee();

        if (!$userFromSession) {
            $this->_logoutUser();
        }

        if (!$userFromDB = get_employee_by_email($userFromSession['email'])) {
            $this->_logoutUser();
        }

        $this->_user = $userFromSession['email'];
    }

    function _validateToken() {
        $googleClient = new Google_Client();
        $googleClient->setApplicationName($_ENV['GOOGLE_APP_NAME']);
        $googleClient->setClientId($_ENV['GOOGLE_CLIENT_ID']);
        $googleClient->setClientSecret($_ENV['GOOGLE_CLIENT_SECRETE']);
        $googleClient->setRedirectUri('postmessage');

        // $googleClient->authenticate($code);
        // $token = json_decode($googleClient->getAccessToken());

        // keep the google token in session in case we need it
        $code = $_SESSION['token'];

        // You can read the Google user ID in the ID token.
        // "sub" represents the ID token subscriber which in our case
        // is the user ID. This sample does not use the user ID.
        $attributes = $googleClient->verifyIdToken($code, $_ENV['GOOGLE_CLIENT_ID']);

        return $attributes['email'] === $this->_user;
    }

    function _checkIfSessionIsValid() {
        if(!isset($_SESSION['connection_time']) || !$sessionTime = unserialize($_SESSION['connection_time'])) {
            $this->_createSessiontime();
            return $this->_checkIfSessionIsValid();
        }

        $connection = new DateTime(unserialize($_SESSION['connection_time'])->date);
        $now = new DateTime();

        return date_diff($now, $connection)->s < AuthMiddleware::MAX_CONNECTION_TIME;
    }

    function _logoutUser() {
        $_SESSION = array();

        // Si vous voulez détruire complètement la session, effacez également
        // le cookie de session.
        // Note : cela détruira la session et pas seulement les données de session !
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finalement, on détruit la session.
        session_destroy();
        header('Location: ' . $_ENV['PATH'] . 'login');
        exit();
    }

    function _createSessiontime() {
        $_SESSION['connection_time'] = serialize(new DateTime());
    }

    /**
     * Default 2 = Conseiller
     *
     * 0- Inactif
     * 1- Représentant externe
     * 2- Représentant interne
     * 3- Support
     * 4- Direction
     * 5- Administration
     */
    function validateAuthLevel($required = 1) {
        $userFromSession = get_current_employee();

        if ($userFromSession['fk_user_level_id'] < $required) {
            header('Location: ' . $_ENV['PATH']);
            exit();
        }
    }
}
