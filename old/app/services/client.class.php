<?php

  class Client {
    protected $id;
    protected $title;
    protected $active;
    protected $created_at;
    protected $updated_at;
    protected $client_type_enum;
    protected $contacts;
    protected $addresses;

    public function __construct($id = false, $obj = false) {
      if (!$id) {
        $this->title = $obj->title;
        $this->active = $obj->active;
        $this->created_at = $obj->created_at;
        $this->updated_at = $obj->updated_at;
        $this->client_type_enum = $obj->client_type_enum;
        $this->contacts = $obj->contacts;
        $this->addresses = $obj->addresses;
      } else {
        if (!$client = $this->fetchClientWithId($id)) {
          return;
        }

        $this->id = $client['id'];
        $this->title = $client['title'];
        $this->active = $client['active'];
        $this->created_at = $client['created_at'];
        $this->updated_at = $client['updated_at'];
        $this->client_type_enum = $client['client_type_enum'];
        $this->contacts = $client['contacts'];
        $this->addresses = $client['addresses'];
      }
    }

    private function fetchClientWithId($id) {
      global $db;
      $id = $db->safe($id);

      $query = "  SELECT
                    id, active, entreprise AS title, creation AS created_at, modified AS updated_at, fk_client_type_id AS client_type_enum
                  FROM `client`
                  WHERE id=$id;";


      if (!$client = $db->queryFirst($query)) {
        return false;
      }

      $client['addresses'] = $this->fetchClientAddressesWithClientId($id);
      $client['contacts'] = $this->fetchClientContactsWithClientId($id);
      return $client;
    }

    private function fetchClientAddressesWithClientId($id) {
      global $db;
      $id = $db->safe($id);

      $query = "  SELECT
                    id, `address`, city, province, postal_code, country, is_default_shipping, is_default_facturation
                  FROM `address`
                  WHERE fk_client_id=$id;";

      if (!$addresses = $db->queryArray($query)) {
        return [];
      }

      return $addresses;
    }

    private function fetchClientContactsWithClientId($id) {
      global $db;
      $id = $db->safe($id);

      $query = "  SELECT
                    first_name, last_name, email, phone, cell, id, is_default_contact
                  FROM contact
                  WHERE fk_client_id=$id;";

      if (!$contacts = $db->queryArray($query)) {
        return [];
      }

      return $contacts;
    }

    public function getAddresses() {
      return $this->addresses;
    }

    public function toJson() {
      return [
        'id' => $this->id,
        'title' => $this->title,
        'active' => $this->active,
        'created_at' => $this->created_at,
        'updated_at' => $this->updated_at,
        'client_type_enum' => $this->client_type_enum,
        'contacts' => $this->contacts,
        'addresses' => $this->addresses,
      ];
    }

    public static function search($terms, $limit = 25, $json_encoded = true) {
      global $db;

      $employeeLevel = get_current_employee()['fk_user_level_id'];
      $employeeId = get_current_employee()['id'];

      $keyword = $db->safe($terms);

      $query = "  SELECT c.id
                  FROM client AS c
                  LEFT JOIN employee_client_permissions AS ecp ON ecp.fk_client_id = c.id
                  WHERE ( c.entreprise LIKE '%" . $keyword . "%' ) ";
      if ($employeeLevel == 1) {
        $query .= " AND ( ( c.fk_created_by_id = '$employeeId' ) OR ( ecp.fk_employee_id = '$employeeId' ) ) ";
      }
                  $query .= "
                  LIMIT $limit
      ";

      if (!$ids = $db->queryArray($query)) {
        return [];
      }

      $res = [];

      foreach ($ids as $id) {
        $client = new Client((int)$id['id']);
        $res[] = $client->toJson();
      }

      if ($json_encoded) {
        return json_encode($res);
      }

      return $res;
    }
  }


  abstract class ClientTypeEnum {
    const PARTICULIER = 1;
    const ENTREPRISE = 2;
    const GOUVERNEMENT = 3;
  }
