<?php

function update_address($address) {
  global $db;

  $sql = "UPDATE address
  SET
    address='".$db->safe($address["address"])."',
    city='".$db->safe($address["city"])."',
    province='".$db->safe($address["province"])."',
    country='".$db->safe($address["country"])."',
    postal_code='".$db->safe($address["postal_code"])."',
    is_default_facturation='".$db->safe($address["is_default_facturation"])."',
    is_default_shipping='".$db->safe($address["is_default_shipping"])."'
  WHERE id='".$db->safe($address["id"])."'
  ";

  $db->query($sql);

  return true;
}
function search_addresses_dt($keyword) {
  global $db;

  $keyword = $db->safe($keyword);

  // $query = "  SELECT a.*, c.*
  //             FROM `address` AS a
  //             LEFT JOIN `client` AS c ON c.id = a.fk_client_id
  //             WHERE a.address LIKE '%" . $keyword . "%'
  //             OR a.city LIKE '%" . $keyword . "%'
  //             OR c.entreprise LIKE '%" . $keyword . "%'
  // ";

  // $organisationsAddresses = $db->queryArray($query);

  $query = "  SELECT
                c.*
              FROM `address` AS a
              WHERE (
                a.address LIKE '%" . $keyword . "%'
            )";

  $addresses = $db->queryArray($query);

  return $addresses;
}
function search_addresses($keyword) {
  global $db;

  $keyword = $db->safe($keyword);

  // $query = "  SELECT a.*, c.*
  //             FROM `address` AS a
  //             LEFT JOIN `client` AS c ON c.id = a.fk_client_id
  //             WHERE a.address LIKE '%" . $keyword . "%'
  //             OR a.city LIKE '%" . $keyword . "%'
  //             OR c.entreprise LIKE '%" . $keyword . "%'
  // ";

  // $organisationsAddresses = $db->queryArray($query);

  $query = "  SELECT
                c.*
              FROM `contact` AS c
              LEFT JOIN employee_contacts AS ec ON ec.fk_contact_id = c.id
              WHERE (
                c.first_name LIKE '%" . $keyword . "%'
                OR c.last_name LIKE '%" . $keyword . "%'
                OR c.email LIKE '%" . $keyword . "%'
                OR c.phone LIKE '%" . $keyword . "%'
            )";

  if (get_current_employee()['fk_user_level_id'] != 3) {
    $employee = get_current_employee()['id'];
    $query .= " AND (c.fk_created_by_id = $employee OR ec.fk_employee_id = $employee)";
  }

  $contactsAddresses = $db->queryArray($query);

  $i = 0;

  foreach ($contactsAddresses as $contact) {
    $contactsAddresses[$i]['organisations'] = get_contact_addresses($contact['id']);
    $i++;
  }

  return $contactsAddresses;
}

function fetchTaxByProvinceString($province) {
  global $db;

  $province = $db->safe($province);

  switch ($province) {
    case 'New Brunswick':
    case 'New-Brunswick':
    case 'new-brunswick':
    case 'new brunswick':
    case 'newbrunswick':
    case 'Nouveau Brunswick':
    case 'nouveau brunswick':
      $province = 'Nouveau-Brunswick';
      break;
    case 'Nova Scotia':
    case 'Nova-Scotia':
    case 'Novascotia':
    case 'novascotia':
    case 'nova-scotia':
    case 'Nouvelle Écosse':
    case 'nouvelle écosse':
    case 'nouvelle ecosse':
      $province = 'Nouvelle-Écosse';
      break;
    case 'Prince Edward Island':
      $province = 'Île-du-Prince-Édouard';
      break;
    case 'British Columbia':
      $province = 'Colombie-Britannique';
      break;
    case "Quebec";
        $province = "Québec";
    default:
      break;
  }

  $province = \Ekosys\Entities\Province::getByName($province);
  $taxes = $province->getTaxesByYear(date('Y'));
  $output = [];

  /** @var Ekosys\Entities\Tax $tax */
    foreach($taxes as $tax){
      $output[]= $tax->toArray();
  }

  return $output;

  /*$query = "SELECT tax.* FROM tax LEFT JOIN province AS p ON p.province_name LIKE '$province' WHERE tax.fk_province_id = p.id";

  $res = $db->queryArray($query);

  return $res;*/
}
