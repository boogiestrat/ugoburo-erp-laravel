<?php

    function get_field_types () {
        global $db;

        $query = "  SELECT f.*
                    FROM item_field_type AS f
                    ORDER BY f.list_order ASC
        ";

        return $db->queryArray($query);
    }

    function get_field_type ($id) {
        global $db;

        $query = "  SELECT f.*
                    FROM item_field_type AS f
                    WHERE id = " . $id;

        return $db->queryArray($query);
    }

    function get_item_types () {
        global $db;

        $query = "  SELECT t.*
                    FROM item_type AS t
                    ORDER BY t.list_order ASC
        ";

        return $db->queryArray($query);
    }

    function get_item_categories () {
        global $db;

        $query = "  SELECT c.*
                    FROM item_category AS c
                    ORDER BY c.list_order ASC
        ";

        return $db->queryArray($query);
    }

    function get_all_items($limit = 100) {
        global $db;

        $limit = (int) $limit;

        $query = "  SELECT
                        i.*,
                        t.name AS type
                    FROM
                        item AS i
                    INNER JOIN
                        item_type AS t  ON i.fk_type_id = t.id
                    ORDER BY
                        i.list_order ASC
                    LIMIT $limit
        ";

        $items = $db->queryArray($query);

        return $items;
    }

    function filter_items($key, $value) {
        global $db;

        $key = $db->safe($key);
        $value = $db->safe($value);

        $query = "  SELECT i.*, f.name AS field_type, t.name AS type, c.name AS category
                    FROM item AS i
                    INNER JOIN item_field_type AS f ON i.fk_field_type_id = f.id
                    INNER JOIN item_type AS t  ON i.fk_type_id = t.id
                    LEFT JOIN item_category AS c ON i.fk_category_id = c.id
                    WHERE i." . $key . " = '" . $value . "'
                    ORDER BY c.name ASC, i.list_order ASC
        ";

        $items = $db->queryArray($query);

        $item_variants = filter_item_variants($key, $value);
        $items = join_item_variants($items, $item_variants);

        return $items;
    }

    function get_item_child ($item_id) {
        global $db;

        $item_id = $db->safe($item_id);

        $query = "SELECT * FROM item_variant WHERE fk_item_id=$item_id;";

        return $db->queryArray($query);
    }

    function get_item_children ($variant_id) {
        global $db;

        $variant_id = $db->safe($variant_id);

        $query = "SELECT * FROM item_variant WHERE id=$variant_id LIMIT 1;";

        return $db->queryFirst($query);
    }

    function search_items($keyword) {
        global $db;

        $keyword = $db->safe($keyword);

        $query = "  SELECT i.*, d.title as discount_title, d.values as discount_values, d.code as discount_code
                    FROM item AS i
                    LEFT JOIN discount AS d ON d.id = i.fk_default_discount_id
                    WHERE i.title LIKE '%" . $keyword . "%'
                    OR i.SKU LIKE '%" . $keyword . "%'
                    LIMIT 10
        ";

        $results = $db->queryArray($query);
        foreach($results as $i=>$v) {
          $results[$i]["description"] = strip_tags(str_replace("<br>","\n", html_entity_decode($results[$i]["description"])));
        }
        return $results;

    }

    function get_item ($item_id) {
        global $db;

        $item_id = $db->safe($item_id);

        $query = "  SELECT i.*, t.name AS type
                    FROM item AS i
                    INNER JOIN item_type AS t  ON i.fk_type_id = t.id
                    WHERE i.id = '" . $item_id . "'
        ";

        $item = $db->queryFirst($query);


        $item_variants = filter_item_variants('id', $item_id);
        $item = join_item_variants($item, $item_variants);

        return $item;
    }

    function get_next_item_order() {
        global $db;

        $query = "  SELECT MAX(i.list_order) + 1 as next_order
                    FROM item AS i
        ";
        $rs = $db->queryFirst($query);

        return $rs['next_order'];
    }

    function delete_item ($item_id) {  // TODO Need global security validation, Delete variants
        global $db;

        $item_id = $db->safe($item_id);

        $query = "  DELETE FROM item
                    WHERE item.id = '". $item_id ."'
                ";

        if ($result = $db->query($query)) {
            log_data(get_current_employee()['id'], '/' . LoggerSection::item . '/' . LoggerType::delete . '/{' . $item_id . '}', '{"data": ' . json_encode($result) . '}', LoggerSection::item, LoggerType::delete);
            return $result;
        } else {
            return false;
        }
    }

    function reorder_item ($item_id, $order, $new_order) {  // TODO Need global security validation
        global $db;

        $item_id = $db->safe($item_id);
        $order = $db->safe($order);
        $new_order = $db->safe($new_order);

        if ($order == $new_order) // No change
            return true;

        // Move all other items
        if ($order < $new_order)
            $query = "  UPDATE item
                        SET item.list_order = item.list_order - 1
                        WHERE item.list_order > ". $order ."
                        AND item.list_order <= ". $new_order ."
                    ";
        else
            $query = "  UPDATE item
                        SET item.list_order = item.list_order + 1
                        WHERE item.list_order >= ". $new_order ."
                        AND item.list_order < ". $order ."
                     ";

        if(!$db->query($query))
            return false;

        // Move selected item
        $query = "  UPDATE item
                    SET item.list_order = '" . $new_order . "'
                    WHERE item.id = '". $item_id ."'
                ";

        return $db->query($query);
    }


    function create_item ($data) {  // TODO Need global security validation
        global $db;

        $name = $db->safe($data['name']);
        $sku = $db->safe($data['sku']);
        $description = strip_tags($db->safe($data['description']));
        $price = str_replace(',', '.', $db->safe($data['price'])); //Replace , by .
        $list_order = get_next_item_order();
        $fk_type_id = $db->safe($data['fk_type_id']);
        $fk_manufacturier_id = $db->safe($data['fk_manufacturier_id']);
        $default_discount_id = $db->safe($data['item_default_discount']);
        $picture = '';

        ### PICTURE ###
        if ($_FILES) {
            $image = new Bulletproof\Image($_FILES);
            $upload_folder = realpath(ROOT .'web/assets/upload/item/');
            $image->setLocation($upload_folder)
                ->setSize(10, 10000000);

            if($image['picture']){
                $upload = $image->upload();

                if($upload){
                    $cropped_image = new \Gumlet\ImageResize($image->getFullPath());
                    $cropped_image->crop(AVATAR_SIZE, AVATAR_SIZE);
                    $cropped_image->save($image->getFullPath());

                    $picture = $image->getName() . '.' . $image->getMime();
                }  else{
                    //throw new \Exception($image["error"]);
                    return false;
                }
            }
        }

        if (empty($fk_type_id)) {
          $fk_type_id = 0;
        }

        if (empty($fk_manufacturier_id)) {
          $fk_manufacturier_id = 0;
        }

        if (empty($default_discount_id)) {
          $default_discount_id = 0;
        }

        $query = "  INSERT INTO item (
                        title,
                        sku,
                        description,
                        price,
                        picture,
                        list_order,
                        fk_type_id,
                        fk_manufacturier_id,
                        fk_default_discount_id
                    ) VALUES (
                        '". $name ."',
                        '". $sku ."',
                        '". $description ."',
                        $price,
                        '". $picture ."',
                        '0',
                        '". $fk_type_id ."',
                        '". $fk_manufacturier_id ."',
                        '". $default_discount_id ."'
                    )
                ";

        if ($db->query($query)) {
            $lastInsertedId = $db->getLastInsertId();
            log_data(get_current_employee()['id'], '/' . LoggerSection::item . '/' . LoggerType::add . '/{' . $lastInsertedId . '}', '{"data": ' . json_encode($data) . '}', LoggerSection::item, LoggerType::add);

            return $lastInsertedId;
        }
        else
            return false;
    }

    function create_child_item ($parentId, $data, $isDataSafe = false) {
        if (!$isDataSafe) {
            return false;
        }

        global $db;

        $fk_item_id = $db->safe($parentId);
        $itemInserted = [];

        for ($i = 0; $i < count($data); $i++) {
            $query = "INSERT INTO item_variant (
                    name,
                    description,
                    price,
                    picture,
                    list_order,
                    fk_item_id
                ) VALUE (
                    '" . $data[$i][name] . "',
                    '" . $data[$i][description] . "',
                    '" . $data[$i][price] . "',
                    '" . $data[$i][image] . "',
                    '$i',
                    '$fk_item_id'
                )
            ";

            if ($db->query($query)) {
                $lastInsertedId = $db->getLastInsertId();

                log_data(get_current_employee()['id'], '/' . LoggerSection::item . '/' . LoggerType::add . '/{' . $lastInsertedId . '}', '{"data": ' . json_encode($data) . '}', LoggerSection::item, LoggerType::add);

                $itemInserted[] = $lastInsertedId;
            }
        }

        return $itemInserted;
    }

    function update_item ($data) {  // TODO Need global security validation
        global $db;

        $id = $db->safe($data['id']);
        $name = $db->safe($data['name']);
        $sku = $db->safe($data['sku']);
        $description = strip_tags($db->safe($data['description']));
        $price = $db->safe($data['price']); //Replace , by .
        $fk_type_id = $db->safe($data['fk_type_id']);
        $fk_manufacturier_id = $db->safe($data['fk_manufacturier_id']);
        $picture = $db->safe($data['picture']);
        $default_discount_id = $db->safe($data['item_default_discount']);

        if (!empty($_FILES)) {
            ### PICTURE ###
            $image = new Bulletproof\Image($_FILES);
            $upload_folder = realpath(ROOT .'web/assets/upload/item/');
            $result = $image->setLocation($upload_folder)
                ->setSize(10, 10000000);

            if($image['picture']){
                $upload = $image->upload();

                if($upload){
                    $cropped_image = new \Gumlet\ImageResize($image->getFullPath());
                    $cropped_image->crop(AVATAR_SIZE, AVATAR_SIZE);
                    $cropped_image->save($image->getFullPath());

                    $picture = $image->getName() . '.' . $image->getMime();
                }  else{
                    //throw new \Exception($image["error"]);
                    return false;
                }
            }
        }

        $query = "  UPDATE item
                    SET title = '". $name ."',
                    sku = '". $sku ."',
                    description = '". $description ."',
                    price =  $price,
                    fk_type_id = ". $fk_type_id .",
                    fk_manufacturier_id = ". $fk_manufacturier_id .",
                    picture = '". $picture ."',
                    fk_default_discount_id = $default_discount_id
                    WHERE id = '". $id ."'
                ";

        try {
            if ($db->query($query)) {
                log_data(get_current_employee()['id'], '/' . LoggerSection::item . '/' . LoggerType::update . '/{' . $id . '}', '{"data": ' . json_encode($data) . '}', LoggerSection::item, LoggerType::update);
                return true;
            }

            return false;
        } catch (Exception $e) {
            return $e;
        }
    }

    function delete_precedent_children($nextChildrenIds, $parentId) {
        global $db;

        $item_variant_id = $db->safe($item_variant_id);

        $query = "  DELETE FROM item_variant
                    WHERE item_variant.fk_item_id = '" . $parentId . "' AND item_variant.id NOT IN ('" . implode(', ', $nextChildrenIds) . "');
                ";

        return $db->query($query);
    }

    function delete_precedent_tasks($nextTaskIds, $parentId) {
        global $db;

        if ($nextTaskIds[0] == null) {
            return true;
        }

        $query = "  DELETE FROM item_tasks
                    WHERE item_tasks.fk_item_id = '" . $db->safe($parentId) . "' AND item_variant.id NOT IN ('" . implode(', ', $nextTaskIds) . "');
                ";

        return $db->query($query);
    }

    function update_child_item ($item, $isDataSafe = true) {
        if (!$isDataSafe) {
            return false;
        }

        global $db;

        for ($i = 0; $i < count($item); $i++) {
            $actualChild = get_item_child($item[$i]["parentId"]);
            $exists = false;

            foreach ($actualChild as $child) {
                if ($child["id"] == $item[$i]["id"]) {
                    $exists = true;
                }
            }

            if ($exists) {
                $query = "UPDATE item_variant SET
                        name = '" . $item[$i]["name"] .
                        "', description = '" . $item[$i]["description"] .
                        "', price = " . $item[$i]["price"] .
                        ", picture = '" . $item[$i]["image"] .
                        "', list_order = " . $i .
                        ", fk_item_id = " . $item[$i]["parentId"] .
                        " WHERE id =" . $item[$i]["id"] . ";";

                if (!$db->query($query)) {
                    return false;
                }
            } else {
                create_child_item($item[$i]["parentId"], [$item[$i]], true);
            }


        }

        return true;
    }

    ### Item Category ###

    function get_all_items_categories() {
        global $db;

        $query = "  SELECT * FROM item_category;";

        return $db->queryArray($query);
    }

    function get_next_item_category_order() {
        global $db;

        $query = "  SELECT MAX(c.list_order) + 1 as next_order
                    FROM item_category AS c
        ";
        $rs = $db->queryFirst($query);

        return $rs['next_order'];
    }

    function update_item_category ($data) {
        global $db;

        $id = $db->safe($data['id']);
        $name = $db->safe($data['name']);
        $description = $db->safe($data['description']);

        $query = "  UPDATE item_category SET name='$name', description='$description' WHERE id=$id;";

        return $db->query($query);
    }

    function get_item_category ($id) {
        global $db;

        $id = $db->safe($id);

        $query = "  SELECT * FROM item_category WHERE id=$id;";

        return $db->queryFirst($query);
    }

    function delete_item_category ($id) {
        global $db;

        $id = $db->safe($id);

        $query = "  DELETE FROM item_category WHERE id=$id;";

        return $db->query($query);
    }

    function create_item_category ($data) {  // TODO Need global security validation
        global $db;

        $name = $db->safe($data['name']);
        $description = $db->safe($data['description']);
        $list_order = get_next_item_category_order();

        $query = "  INSERT INTO item_category (
                        name,
                        description,
                        list_order
                    ) VALUE (
                        '". $name ."',
                        '". $description ."',
                        '". $list_order ."'
                    )
                ";

        $db->query($query);
        return $db->getLastInsertId();
    }

    ## Item variants ##

    function delete_all_item_variant_from_parent($parentId) {
        global $db;

        $parentId = $db->safe($parentId);

        $query = "  DELETE FROM item_variant
                    WHERE item_variant.fk_item_id = '". $parentId ."'
                ";

        return $db->query($query);
    }

    function delete_all_item_variant($item_variant_id) {
        global $db;

        $item_variant_id = $db->safe($item_variant_id);

        $query = "  DELETE FROM item_variant
                    WHERE item_variant.id = '". $item_variant_id ."'
                ";

        return $db->query($query);
    }

    function create_item_variant($data) {
        global $db;

        $item_variant_name = $db->safe($data['item_variant_name']);
        $item_variant_cost = $db->safe($data['item_variant_cost']);
        $item_id = $db->safe($data['item_id']);

        $query = "  INSERT INTO item_category (
                        name,
                        cost,
                        fk_item_id
                    ) VALUE (
                        '". $item_variant_name ."',
                        '". $item_variant_cost ."',
                        '". $item_id ."'
                    )
                ";

        $db->query($query);
        return $db->getLastInsertId();
    }

    function get_all_item_variants() {
        global $db;

        $query = "  SELECT o.*
                    FROM item_variant AS o
                    INNER JOIN item AS i ON i.id = o.fk_item_id
                    ORDER BY o.price ASC
        ";

        return $db->queryArray($query);
    }

    function filter_item_variants($key, $value) {
        global $db;

        $key = $db->safe($key);
        $value = $db->safe($value);

        $query = "  SELECT o.*
                    FROM item_variant AS o
                    INNER JOIN item AS i ON i.id = o.fk_item_id
                    WHERE i." . $key . " = '" . $value . "'
                    ORDER BY o.price ASC
        ";

        return $db->queryArray($query);
    }

    function join_item_variants($items, $item_variants) {

        foreach ($items as $key => $item) {
            foreach ($item_variants as $key_variant => $variant) {
                if ($variant['fk_item_id'] == $item['id'])
                    $items[$key]['variants'][] = $variant;
            }
        }

        return $items;
    }

    function join_item_tasks($items, $item_tasks) {

        foreach ($items as $key => $item) {
            foreach ($item_tasks as $key_task => $task) {
                if ($task['fk_item_id'] == $item['id'])
                    $items[$key]['tasks'][] = $task;
            }
        }

        return $items;
    }

    function get_item_type_badge($type_id) {
        switch ($type_id) {
            case 1:
                return 'primary';
            case 2:
                return 'success';
            case 3:
                return 'default';
            default:
                return 'primary';
        }
    }

    function get_item_type_name($type_id) {
        global $db;

        $type_id = $db->safe($type_id);

        $query = "  SELECT name FROM item_type
                    WHERE id = $type_id;";

        return $db->queryFirst($query)['name'];
    }
