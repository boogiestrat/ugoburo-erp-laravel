<?php

class Logger {
    private $_by;
    private $_path;
    private $_data;
    private $_section;
    private $_operation;

    function __construct($by, $path, $data, $section = LoggerSection::client, $operation = LoggerType::add) {
        $this->_by = $by;
        $this->_path = $path;
        $this->_data = $data;
        $this->_section = $section;
        $this->_operation = $operation;
    }

    function getBy() {
        return $this->_by;
    }

    function getPath() {
        return $this->_path;
    }

    function getSection() {
        return $this->_section;
    }

    function getOperation() {
        return $this->_operation;
    }

    function getData() {
        return $this->_data;
    }

    function getEmployee() {
        return get_employee($this->_by);
    }
}

abstract class LoggerSection
{
    const client = 'client';
    const quote = 'quote';
    const contact = 'contact';
    const manufacturier = 'manufacturier';
    const order = 'order';
    const item = 'item';
    const budget = 'budget';
    const progress = 'progress';
    const task = 'task';
    const document = 'document';
    const employee = 'employee';
    const payment = 'payment';
}

abstract class LoggerType
{
    const add = 'add';
    const delete = 'delete';
    const update = 'update';
    const copy = 'copy';
}

function log_data($by, $path, $data, $section = LoggerSection::client, $operation = LoggerType::add) {
    global $skip_logger;

    if($skip_logger)return;

    $logger = new Logger($by, $path, $data);

    global $db;

    $query = "INSERT INTO `log`
                (fk_employee_id, path, data, section, operation)
              VALUE
                (" . (int)$logger->getBy() . ", '" . $logger->getPath() . "', '" . $logger->getData() . "', '" . $section . "', '" . $operation . "');";

    $db->query($query);
}

function read_log_by_employee(int $employee_id) {

}

function read_log_by_section(LoggerSection $section) {

}

function read_log_by_type(LoggerType $type) {

}

function read_all_log() {

}

function object_has_been_updated(LoggerSection $section, $id) {

}
