<?php

class Quote {

    //todo get from db
    const STATUS_SENT = 2;
    const STATUS_DEPOSIT = 3;
    const STATUS_PAID = 4;

    private $id;
  private $language;
  private $client_id;
  private $sections;
  private $title;
  private $additionalNote;
  private $additionalNoteInterne;
  private $date;
  private $validUntil;
  private $clauses;
  private $depot;
  private $discountMethod;
  private $discountParam;
  private $installationAndShippingMethod;
  private $installationAndShippingParam;
  private $address;
  private $specialist;
  private $billTo;
  private $shipTo;
  private $additionnalFees;
  private $total;
  private $data;
  private $number;

  public function __construct($json_quote) {
    $this->id = $json_quote['id'];
    $this->language = $json_quote['language'];
    $this->sections = $json_quote['sections'];
    $this->title = $json_quote['title'];
    $this->additionalNote = $json_quote['additionalNote'];
    $this->additionalNoteInterne = $json_quote['additionalNoteInterne'];
    $this->date = $json_quote['date'];
    $this->validUntil = $json_quote['validUntil'];
    $this->clauses = $json_quote['clauses'];
    $this->depot = $json_quote['depot'];
    $this->total = $json_quote['total'];
    $this->discountMethod = $json_quote['discountMethod'];
    $this->discountParam = $json_quote['discountParam'];
    $this->installationAndShippingMethod = $json_quote['installationAndShippingMethod'];
    $this->installationAndShippingParam = $json_quote['installationAndShippingParam'];
    $this->address = $json_quote['address'];
    $this->specialist = $json_quote['specialist'];
    $this->billTo = $json_quote['billTo'];
    $this->shipTo = $json_quote['shipTo'];
    $this->client_id = $json_quote['billTo']['client']['id'];
    $this->additionnalFees = $json_quote['additionnalFees'];
    $this->data = $json_quote;
    $this->number = $json_quote['number'];
  }

  public function send_to_quickbooks($payment_method) {
    require dirname(dirname(dirname(__DIR__))) . '/web/webservices/qbwc/api.php';

    // $payment_method == credit_card | purchase_order

    $api = new QBWCApi();
    echo $api->send_to_quickbooks_from_erp($payment_method, $this->data);
  }

  public function item_calculateLineShipping($section_index, $item_index) {
    $result = 0;

    if ($this->sections[$section_index]["items"][$item_index]["expeditionPriceMethod"] == 'global') {
      $result = $this->sections[$section_index]["items"][$item_index]["expeditionPrice"];
    }

    if ($this->sections[$section_index]["items"][$item_index]["expeditionPriceMethod"] == 'unit') {
      $result = $this->sections[$section_index]["items"][$item_index]["expeditionPrice"] * $this->sections[$section_index]["items"][$item_index]["quantity"];
    }

    if ($this->sections[$section_index]["items"][$item_index]["expeditionPriceMethod"] == 'cost') {
      $result = ( $this->sections[$section_index]["items"][$item_index]["expeditionPrice"] / 100 ) * $this->item_calculateCostPrice($section_index, $item_index);
    }

    if ($this->sections[$section_index]["items"][$item_index]["expeditionPriceMethod"] == 'list') {
      $result = ( $this->sections[$section_index]["items"][$item_index]["expeditionPrice"] / 100 ) * $this->sections[$section_index]["items"][$item_index]["price"];
    }

    return $result;
  }

  public function item_calculateCostPrice($section_index, $item_index) {

    $price = $this->sections[$section_index]["items"][$item_index]["price"];
    $quantity = $this->sections[$section_index]["items"][$item_index]["quantity"];

    if (empty($this->sections[$section_index]["items"][$item_index]["discount"]["name"])) {
      return $price;
    }

    foreach($this->sections[$section_index]["items"][$item_index]["discount"]["parts"] as $discount) {
      if (!empty($discount)) {
        if (is_numeric($discount)) {
          $price = $price - ($price * ( $discount / 100.0 ) ) ;
        }
      }
    }

    if ($quantity === 0) return 0;

    return $price;
  }

  public function item_calculateUnitPrice($section_index, $item_index) {
    $price = 0;

    if ($this->sections[$section_index]["items"][$item_index]["profitMethod"] == 1) { // COST_PLUS_PERCENT
      $price = $this->item_calculateCostPrice($section_index, $item_index) * (1 + ($this->sections[$section_index]["items"][$item_index]["priceParameterValue"] / 100));
    } elseif ($this->sections[$section_index]["items"][$item_index]["profitMethod"] == 2) { // GROSS_PROFIT
      $price = $this->item_calculateCostPrice($section_index, $item_index)/ (1 - ($this->sections[$section_index]["items"][$item_index]["priceParameterValue"] / 100) + $this->item_calculateLineShipping($section_index, $item_index));
    } elseif ($this->sections[$section_index]["items"][$item_index]["profitMethod"] == 3) { // NET_PROFIT
      $price = $this->item_calculateCostPrice($section_index, $item_index) + $this->sections[$section_index]["items"][$item_index]["priceParameterValue"];
    } elseif ($this->sections[$section_index]["items"][$item_index]["profitMethod"] == 4) { // LIST_MINUS
      $price = $price - (($this->sections[$section_index]["items"][$item_index]["priceParameterValue"] / 100) * $price);
    } else { // FIXED_PRICE
      $price = $this->sections[$section_index]["items"][$item_index]["priceParameterValue"];
    }

    return $price;
  }

  public function item_calculateLineTotal($section_index, $item_index) {
    $quantity = $this->sections[$section_index]["items"][$item_index]["quantity"];
    return $this->item_calculateUnitPrice($section_index, $item_index) * $quantity;
  }

  public function item_calculateItemProfit($section_index, $item_index) {
    $quantity = $this->sections[$section_index]["items"][$item_index]["quantity"];
    return (

      ($this->item_calculateUnitPrice($section_index, $item_index) * $quantity)
      -
      (
       ($this->item_calculateCostPrice($section_index, $item_index) * $quantity)
       +
       $this->item_calculateLineShipping($section_index, $item_index)
      )

    );
  }

  public function item_calculateGrossProfit($section_index, $item_index) {
    $total = $this->item_calculateLineTotal($section_index, $item_index);
    $gross = 0;

    if ($total != 0) {
      $gross = ( $this->item_calculateItemProfit($section_index, $item_index) / $total ) * 100;
    }

    if ($gross < 0) { $gross = 0; }
    if ($gross > 100) { $gross = 100; }
    if (empty($gross)) { $gross = 0; }

    return $gross;
  }

  public function section_calculateTotal($section_index) {
    $result = 0;
    foreach($this->sections[$section_index]["items"] as $item_index => $item) {
      $result = $result + $this->item_calculateLineTotal($section_index, $item_index);
    }
    return $result;
  }

  public function section_calculateProfit($section_index) {
    $result = 0;
    foreach($this->sections[$section_index]["items"] as $item_index => $item) {
      $result = $result + $this->item_calculateItemProfit($section_index, $item_index);
    }
    return $result;
  }

  public function crunch_numbers() {

    foreach($this->sections as $section_index => $section) {
      foreach($section["items"] as $item_index => $item) {

        $_LineShipping = $this->item_calculateLineShipping($section_index, $item_index);
        $_CostPrice = $this->item_calculateCostPrice($section_index, $item_index);
        $_ItemProfit = $this->item_calculateItemProfit($section_index, $item_index);
        $_ItemPrice = $this->item_calculateUnitPrice($section_index, $item_index);
        $_GrossProfit = $this->item_calculateGrossProfit($section_index, $item_index);

        $this->sections[$section_index]["items"][$item_index]["_LineShipping"] = round($_LineShipping, 2);
        $this->sections[$section_index]["items"][$item_index]["_CostPrice"] = round($_CostPrice, 2);
        $this->sections[$section_index]["items"][$item_index]["_ItemProfit"] = round($_ItemProfit, 2);
        $this->sections[$section_index]["items"][$item_index]["_ItemPrice"] = round($_ItemPrice, 2);
        $this->sections[$section_index]["items"][$item_index]["_GrossProfit"] = round($_GrossProfit, 2);
      }
    }

    $this->data["sections"] = $this->sections;

  }

  public function create_order() {
    global $db;

    $quoteOverview = calculate_quote_overview($this->data['id']);
    $quotePayment = \Ekosys\Entities\QuotePayment::getPaymentByQuoteId($this->data['id']);
    $quoteDeposit = \Ekosys\Entities\QuotePayment::getDepositByQuoteId($this->data['id']);
    $quoteTotal = \Ekosys\Entities\Tax::calculate($this->data['id']);

    /*
    order status;
    - NEW
    - PROCESSING
    - MANUFACTURING
    - TRANSIT
    - COMPLETED
    */
    // echo '<pre>';
    // print_r($this->data);die();
    $order_number = $this->number;
    // $order_number = date("Ym")."0001";
    // $sql = "SELECT order_number FROM orders WHERE order_number LIKE '".date("Ym")."%' ORDER BY order_number DESC LIMIT 1";
    // $results = $db->queryArray($sql);
    // if (!empty($results)) {
    //   $new_order_number = $results[0]["order_number"];
    //   $new_order_number++;
    //   $order_number = $new_order_number;
    // }

    $discountMethod = $this->data['discountMethod'];
    $discountParam = $this->data['discountParam'];
    $feesMethod = $this->data['installationAndShippingMethod'];
    $feesParam = $this->data['installationAndShippingParam'];
    $total = $this->total;
    $subTotal = (float)$quoteTotal->getSubTotal();

    $discount = ($discountMethod == 'percent' ? $subTotal * ($discountParam / 100) : $discountParam);
    $fees = ($feesMethod == 'percent' ? $subTotal * ($feesParam / 100) : $feesParam);

    $sql = "INSERT INTO orders(
      `fk_status_id`,
      `fk_client_id`,
      `order_source`,
      `fk_order_id`,
      `order_number`,
      `created_fk_employee_id`,
      `purchased_on`,
      `updated_at`,
      `fees`,
      `discount`,
      `gt_base`
    ) VALUES (
      '1',
      '".$this->data['billTo']['client']['id']."',
      'erp',
      '".$this->data['id']."',
      '".$order_number."',
      '".$this->data['specialist']['id']."',
      '".date('Y-m-d H:i:s', strtotime($this->data['date']))."',
      '".date('Y-m-d H:i:s')."',
      '".number_format($fees, 2, '.', '')."',
      '".number_format($discount, 2, '.', '')."',
      '".number_format($total, 2, '.', '')."'
    )";

    $db->query($sql);
    $order_id = $db->getLastInsertId();

    // Addresses (ship_to)
    $firstname = $this->data['shipTo']['contact']['first_name'];
    $lastname = $this->data['shipTo']['contact']['last_name'];
    $formatted = $this->data['shipTo']['address']['address'];
    $output = $this->data['shipTo']['client']['title']."\n$firstname $lastname\n$formatted";
    $sql = "  INSERT INTO `order_addresses`
                (`type`, `formatted`, `firstname`, `email`, `lastname`, `address`, `city`, `province`, `postal_code`, `country`, `entreprise`, `fk_order_id`)
              VALUES (
                'ship_to',
                '$output',
                '" . $this->data['shipTo']['contact']['first_name'] . "',
                '" . $this->data['shipTo']['contact']['email'] . "',
                '" . $this->data['shipTo']['contact']['last_name'] . "',
                '" . $this->data['shipTo']['address']['address'] . "',
                '" . $this->data['shipTo']['address']['city'] . "',
                '" . $this->data['shipTo']['address']['province'] . "',
                '" . $this->data['shipTo']['address']['postal_code'] . "',
                '" . $this->data['shipTo']['address']['country'] . "',
                '" . $this->data['shipTo']['client']['title'] . "',
                $order_id
              );";

    $db->query($sql);

    // Addresses (bill_to)
    $firstname = $this->data['billTo']['contact']['first_name'];
    $lastname = $this->data['billTo']['contact']['last_name'];
    $formatted = $this->data['billTo']['address']['address'];
    $output = $this->data['billTo']['client']['title']."\n$firstname $lastname\n$formatted";
    $sql = "  INSERT INTO `order_addresses`
                (`type`, `formatted`, `firstname`, `email`, `lastname`, `address`, `city`, `province`, `postal_code`, `country`, `entreprise`, `fk_order_id`)
              VALUES (
                'bill_to',
                '$output',
                '" . $this->data['billTo']['contact']['first_name'] . "',
                '" . $this->data['billTo']['contact']['email'] . "',
                '" . $this->data['billTo']['contact']['last_name'] . "',
                '" . $this->data['billTo']['address']['address'] . "',
                '" . $this->data['billTo']['address']['city'] . "',
                '" . $this->data['billTo']['address']['province'] . "',
                '" . $this->data['billTo']['address']['postal_code'] . "',
                '" . $this->data['billTo']['address']['country'] . "',
                '" . $this->data['billTo']['client']['title'] . "',
                $order_id
              );";

    $db->query($sql);

    // Histories
    $sql = "  INSERT INTO `order_histories`
                (
                  `estimated_delivery_date`,
                  `acknow_no`,
                  `tracking_no`,
                  `fk_status_id`,
                  `fk_order_id`,
                  `fk_created_by_id`,
                  `fk_shipping_by_id`,
                  `comment`,
                  `comment_fr`,
                  `comment_en`,
                  `is_customer_notified`,
                  `is_visible_on_front`,
                  `is_exported_to_quickbooks`
                )
               VALUES (
                '', '', '', 3, $order_id, ".$this->data['specialist']['id'].", 0, '', '','', 1, 0, 0
              );";

    $db->query($sql);

    // Items
    foreach($this->data["sections"] as $section) {
      // $sql = "INSERT INTO orders_sections(
      //   `order_id`,
      //   `title`
      // ) VALUES(
      //   '".$order_id."',
      //   '".$section["title"]."'
      // )";
      // $db->query($sql);
      // $orders_sections_id = $db->getLastInsertId();

      foreach($section["items"] as $item) {
        $sql = "INSERT INTO `order_item` (
          `fk_order_id`,
          `SKU`,
          `title`,
          `price`,
          `quantity`,
          `description`,
          `priceParameterValue`,
          `expeditionPrice`,
          `expeditionPriceMethod`,
          `expeditionPercent`,
          `pictureURL`,
          `profitMethod`,
          `discountCode`,
          `discountName`,
          `discountParts`
        ) VALUES(
          $order_id,
          '".$item["SKU"]."',
          '".$item["title"]."',
          ".$item["_ItemPrice"].",
          ".$item["quantity"].",
          '".$item["description"]."',
          ".$item["priceParameterValue"].",
          ".$item["expeditionPrice"].",
          '".$item["expeditionPriceMethod"]."',
          ".$item["expeditionPercent"].",
          '".$item["pictureURL"]."',
          ".$item["profitMethod"].",
          '".$item["discountCode"]."',
          '".$item["discountName"]."',
          '".$item["discountParts"]."'
        )";

        $db->query($sql);
      }
    }

    // Taxes
    foreach ($this->data['billTo']['taxes'] as $tax) {
      $sql = "  INSERT INTO `order_taxes`
                  (`amount`, `rate`, `fk_tax_id`, `fk_order_id`)
                  VALUES (
                    '0',
                    '" . str_replace(',', '.', $tax['value']) . "',
                    '" . $tax['id'] . "',
                    '" . $order_id . "'
                  );";

      $db->query($sql);
    }

    return get_order($order_id);


    // $sql = "INSERT INTO orders_billto(
    //   `order_id`,
    //   `client_id`,
    //   `contact_id`,
    //   `address_id`
    // ) VALUES(
    //   '".$order_id."',
    //   '".$this->data['billTo']['client']['id']."',
    //   '".$this->data["billTo"]["address"]["id"]."',
    //   '".$this->data["billTo"]["contact"]["id"]."'
    // )";
    // $db->query($sql);

    // $sql = "INSERT INTO orders_shipto(
    //   `order_id`,
    //   `client_id`,
    //   `contact_id`,
    //   `address_id`
    // ) VALUES(
    //   '".$order_id."',
    //   '".$this->data['shipTo']['client']['id']."',
    //   '".$this->data["shipTo"]["address"]["id"]."',
    //   '".$this->data["shipTo"]["contact"]["id"]."'
    // )";
    // $db->query($sql);

    // foreach($this->data["availableDiscounts"] as $discount) {

    // }

    // foreach($this->data["sections"] as $section) {
    //   $sql = "INSERT INTO orders_sections(
    //     `order_id`,
    //     `title`
    //   ) VALUES(
    //     '".$order_id."',
    //     '".$section["title"]."'
    //   )";
    //   $db->query($sql);
    //   $orders_sections_id = $db->getLastInsertId();

    //   foreach($section["items"] as $item) {
    //     $sql = "INSERT INTO orders_sections_items(
    //       `order_id`,
    //       `orders_sections_id`,
    //       `sku`,
    //       `qty`
    //     ) VALUES(
    //       '".$order_id."',
    //       '".$orders_sections_id."',
    //       '".$item["SKU"]."',
    //       '".$item["quantity"]."'
    //     )";
    //     $db->query($sql);
    //   }
    // }
  }

  public function create() {
    global $db;

    $this->crunch_numbers();

    if ($this->billTo['contact']['id']=="-1" || !$this->billTo['contact']['id']){

        if ($contact = get_contact_by_email($this->billTo['contact']['email'])){
            $this->billTo['contact']['id'] = $contact['id'];
        } else {
            $this->billTo['contact']['id'] = create_contact($this->billTo['contact']);
        }

    }

    if ($this->shipTo['contact']['id']=="-1" || !$this->shipTo['contact']['id']){
        if ($contact = get_contact_by_email($this->shipTo['contact']['email'])){
            $this->shipTo['contact']['id'] = $contact['id'];
        } else {
            $this->shipTo['contact']['id'] = create_contact($this->shipTo['contact']);
        }
    }

    $this->date = date("Y-m-d H:i:s", strtotime($this->date));
    $this->validUntil = date("Y-m-d H:i:s", strtotime($this->validUntil));

    if ($this->billTo) {
      if ($this->billTo["client"]) {


        $title = trim($this->billTo["client"]["title"]);
        $query = "SELECT * FROM client WHERE entreprise = '$title';";
        $results = $db->queryArray($query);
        if (empty($results)) {
          $query = "INSERT INTO client(entreprise, active, fk_client_type_id, fk_created_by_id) VALUES(
            '".$db->safe($this->billTo["client"]["title"])."',
            '".$db->safe(1)."',
            '".$db->safe($this->billTo["client"]["client_type_enum"])."',
            '".$db->safe($this->specialist['id'])."'
          )";

          if ($db->query($query)) {
            $newId = $db->getLastInsertId();
            $this->billTo["client"]["id"] = $newId;

            if (!empty($this->billTo["address"])) {
              $query = "INSERT INTO address(fk_client_id,address,city,province,postal_code,country,is_default_facturation,is_default_shipping) VALUES(
                '".$db->safe($this->billTo["client"]["id"])."',
                '".$db->safe($this->billTo["address"]["address"])."',
                '".$db->safe($this->billTo["address"]["city"])."',
                '".$db->safe($this->billTo["address"]["province"])."',
                '".$db->safe($this->billTo["address"]["postal_code"])."',
                '".$db->safe($this->billTo["address"]["country"])."',
                '".$db->safe(0)."',
                '".$db->safe(0)."'
              )";
              $db->query($query);
            }
          }
        }

        $query = "SELECT * FROM contact WHERE
          first_name='".$db->safe($this->billTo["contact"]["first_name"])."'
          AND last_name='".$db->safe($this->billTo["contact"]["last_name"])."'
          AND email='".$db->safe($this->billTo["contact"]["email"])."'
          AND fk_client_id='".$db->safe($this->billTo["client"]["id"])."'
        ";
        $results = $db->queryArray($query);
        if (empty($results)) {
          $query = "INSERT INTO contact(first_name,last_name,email,fk_client_id) VALUES(
            '".$db->safe($this->billTo["contact"]["first_name"])."',
            '".$db->safe($this->billTo["contact"]["last_name"])."',
            '".$db->safe($this->billTo["contact"]["email"])."',
            '".$db->safe($this->billTo["client"]["id"])."'
          )";
          if ($db->query($query)) {
            $newId = $db->getLastInsertId();
            $this->billTo["contact"]["id"] = $newId;
          }
        }
      }
    }

    if ($this->shipTo) {
      if ($this->shipTo["client"]) {

        $title = $db->safe($this->shipTo["client"]["title"]);
        $query = "SELECT * FROM client WHERE entreprise = '$title' ";
        $results = $db->queryArray($query);
        if (empty($results)) {
          $query = "INSERT INTO client(entreprise, active, fk_client_type_id, fk_created_by_id) VALUES(
            '".$db->safe($this->shipTo["client"]["title"])."',
            '".$db->safe(1)."',
            '".$db->safe($this->shipTo["client"]["client_type_enum"])."',
            '".$db->safe($this->specialist['id'])."'
          )";

          if ($db->query($query)) {
            $newId = $db->getLastInsertId();
            $this->shipTo["client"]["id"] = $newId;

            if (!empty($this->shipTo["address"])) {
              $query = "INSERT INTO address(fk_client_id,address,city,province,postal_code,country,is_default_facturation,is_default_shipping) VALUES(
                '".$db->safe($this->shipTo["client"]["id"])."',
                '".$db->safe($this->shipTo["address"]["address"])."',
                '".$db->safe($this->shipTo["address"]["city"])."',
                '".$db->safe($this->shipTo["address"]["province"])."',
                '".$db->safe($this->shipTo["address"]["postal_code"])."',
                '".$db->safe($this->shipTo["address"]["country"])."',
                '".$db->safe(0)."',
                '".$db->safe(0)."'
              )";
              $db->query($query);
            }
          }
        }

        $query = "SELECT * FROM contact WHERE
          first_name='".$db->safe($this->shipTo["contact"]["first_name"])."'
          AND last_name='".$db->safe($this->shipTo["contact"]["last_name"])."'
          AND email='".$db->safe($this->shipTo["contact"]["email"])."'
          AND fk_client_id='".$db->safe($this->shipTo["client"]["id"])."'
        ";
        $results = $db->queryArray($query);
        if (empty($results)) {
          $query = "INSERT INTO contact(first_name,last_name,email,fk_client_id) VALUES(
            '".$db->safe($this->shipTo["contact"]["first_name"])."',
            '".$db->safe($this->shipTo["contact"]["last_name"])."',
            '".$db->safe($this->shipTo["contact"]["email"])."',
            '".$db->safe($this->shipTo["client"]["id"])."'
          )";
          if ($db->query($query)) {
            $newId = $db->getLastInsertId();
            $this->shipTo["contact"]["id"] = $newId;
          }
        }
      }
    }

    $query = "  INSERT INTO `quote`
                SET
                `language`='{$db->safe($this->language)}',
                `data_sections`='{$this->jsonEncode($this->sections)}',
                `title`='{$db->safe($this->title)}',
                `additionnal_note`='{$db->safe($this->additionalNote)}',
                `additionnal_note_interne`='{$db->safe($this->additionalNoteInterne)}',
                `creation`='{$db->safe($this->date)}',
                `valid_until`='{$db->safe($this->validUntil)}',
                `data_clauses`='{$this->jsonEncode($this->clauses)}',
                `deposit`='{$db->safe($this->depot)}',
                `total`='{$db->safe($this->total)}',
                `discount_method`='{$db->safe($this->discountMethod)}',
                `discount_param`='{$db->safe($this->discountParam)}',
                `expedition_and_installation_method`='{$db->safe($this->installationAndShippingMethod)}',
                `expedition_and_installation_param`='{$db->safe($this->installationAndShippingParam)}',
                `data_address`='{$this->jsonEncode($this->address)}',
                `data_specialist`='{$this->jsonEncode($this->specialist)}',
                `fk_bill_to_id`='{$db->safe($this->billTo['contact']['id'])}',
                `data_bill_to`='{$this->jsonEncode($this->billTo)}',
                `fk_ship_to_id`='{$db->safe($this->shipTo['contact']['id'])}',
                `fk_client_id`='{$db->safe($this->client_id)}',
                `number`='{$db->safe($this->number)}',
                `data_ship_to`='{$this->jsonEncode($this->shipTo)}',
                `data_additionnal_fees`='{$this->jsonEncode($this->additionnalFees)}',
                `created_fk_employee_id`='{$db->safe($this->specialist['id'])}';";

    if ($db->query($query)) {
      $newId = $db->getLastInsertId();

      $this->data['id'] = $newId;

      $newDataString = json_encode($this->data);
      $newDataString = $db->safe($newDataString);

      $updateQuery = "UPDATE `quote` SET `data`='$newDataString' WHERE id=$newId";
      // print_r($updateQuery);die();
      $db->query($updateQuery);

      return $newId;
    }

    return false;
  }

  public function update() {
    global $db;

    $this->crunch_numbers();

    $this->validUntil = date("Y-m-d H:i:s", strtotime($this->validUntil));

        if ($this->billTo) {
          if ($this->billTo["client"]) {

            $title = $this->billTo["client"]["title"];
            $query = "SELECT * FROM client WHERE entreprise = '$title' ";
            $results = $db->queryArray($query);
            if (empty($results)) {
              $query = "INSERT INTO client(entreprise, active, fk_client_type_id, fk_created_by_id) VALUES(
                '".$db->safe($this->billTo["client"]["title"])."',
                '".$db->safe(1)."',
                '".$db->safe($this->billTo["client"]["client_type_enum"])."',
                '".$db->safe($this->specialist['id'])."'
              )";

              if ($db->query($query)) {
                $newId = $db->getLastInsertId();
                $this->billTo["client"]["id"] = $newId;

                if (!empty($this->billTo["address"])) {
                  $query = "INSERT INTO address(fk_client_id,address,city,province,postal_code,country,is_default_facturation,is_default_shipping) VALUES(
                    '".$db->safe($this->billTo["client"]["id"])."',
                    '".$db->safe($this->billTo["address"]["address"])."',
                    '".$db->safe($this->billTo["address"]["city"])."',
                    '".$db->safe($this->billTo["address"]["province"])."',
                    '".$db->safe($this->billTo["address"]["postal_code"])."',
                    '".$db->safe($this->billTo["address"]["country"])."',
                    '".$db->safe(0)."',
                    '".$db->safe(0)."'
                  )";
                  $db->query($query);
                }
              }
            }

            $query = "SELECT * FROM contact WHERE
              first_name='".$db->safe($this->billTo["contact"]["first_name"])."'
              AND last_name='".$db->safe($this->billTo["contact"]["last_name"])."'
              AND email='".$db->safe($this->billTo["contact"]["email"])."'
              AND fk_client_id='".$db->safe($this->billTo["client"]["id"])."'
            ";
            $results = $db->queryArray($query);
            if (empty($results)) {
              $query = "INSERT INTO contact(first_name,last_name,email,fk_client_id) VALUES(
                '".$db->safe($this->billTo["contact"]["first_name"])."',
                '".$db->safe($this->billTo["contact"]["last_name"])."',
                '".$db->safe($this->billTo["contact"]["email"])."',
                '".$db->safe($this->billTo["client"]["id"])."'
              )";
              if ($db->query($query)) {
                $newId = $db->getLastInsertId();
                $this->billTo["contact"]["id"] = $newId;
              }
            }
          }
        }

        if ($this->shipTo) {
          if ($this->shipTo["client"]) {

            $title = $this->shipTo["client"]["title"];
            $query = "SELECT * FROM client WHERE entreprise = '$title' ";
            $results = $db->queryArray($query);
            if (empty($results)) {
              $query = "INSERT INTO client(entreprise, active, fk_client_type_id, fk_created_by_id) VALUES(
                '".$db->safe($this->shipTo["client"]["title"])."',
                '".$db->safe(1)."',
                '".$db->safe($this->shipTo["client"]["client_type_enum"])."',
                '".$db->safe($this->specialist['id'])."'
              )";

              if ($db->query($query)) {
                $newId = $db->getLastInsertId();
                $this->shipTo["client"]["id"] = $newId;

                if (!empty($this->shipTo["address"])) {
                  $query = "INSERT INTO address(fk_client_id,address,city,province,postal_code,country,is_default_facturation,is_default_shipping) VALUES(
                    '".$db->safe($this->shipTo["client"]["id"])."',
                    '".$db->safe($this->shipTo["address"]["address"])."',
                    '".$db->safe($this->shipTo["address"]["city"])."',
                    '".$db->safe($this->shipTo["address"]["province"])."',
                    '".$db->safe($this->shipTo["address"]["postal_code"])."',
                    '".$db->safe($this->shipTo["address"]["country"])."',
                    '".$db->safe(0)."',
                    '".$db->safe(0)."'
                  )";
                  $db->query($query);
                }
              }
            }

            $query = "SELECT * FROM contact WHERE
              first_name='".$db->safe($this->shipTo["contact"]["first_name"])."'
              AND last_name='".$db->safe($this->shipTo["contact"]["last_name"])."'
              AND email='".$db->safe($this->shipTo["contact"]["email"])."'
              AND fk_client_id='".$db->safe($this->shipTo["client"]["id"])."'
            ";
            $results = $db->queryArray($query);
            if (empty($results)) {
              $query = "INSERT INTO contact(first_name,last_name,email,fk_client_id) VALUES(
                '".$db->safe($this->shipTo["contact"]["first_name"])."',
                '".$db->safe($this->shipTo["contact"]["last_name"])."',
                '".$db->safe($this->shipTo["contact"]["email"])."',
                '".$db->safe($this->shipTo["client"]["id"])."'
              )";
              if ($db->query($query)) {
                $newId = $db->getLastInsertId();
                $this->shipTo["contact"]["id"] = $newId;
              }
            }
          }
        }


    if (isset($this->data["csv_import"])) {
      $items = $this->data["csv_import"];
      foreach($items as $item) {
        $codeimport_mfg = trim($item["Mfg"]); // vendor
        $codeimport_cat = trim($item["Cat"]); // discount

        if (!empty($codeimport_mfg) && !empty($codeimport_cat)) {


          $sql = "SELECT * FROM discount
          LEFT JOIN manufacturier ON manufacturier.id = discount.fk_manufacturier_id
          WHERE discount.codeimport LIKE '%$codeimport_cat%' AND manufacturier.code LIKE '%$codeimport_mfg%' ";

          // print_r($sql);
          $results = $db->queryArray($sql);
          // print_r($results);
          if (!empty($results)) {
            foreach($results as $row) {
              if (!isset($this->data["availableDiscounts"])) {
                $this->data["availableDiscounts"] = [];
              }

              $found = false;
              foreach($this->data["availableDiscounts"] as $discount) {
                if ($discount["code"] == $row["code"]) {
                  $found = true;
                  break;
                }
              }

              $discount = array(
                "code" => $row["code"],
                "name" => $row["title"] . "(".$row["name"].")",
                "parts" =>
                json_decode($row["values"], true)
              );
              foreach($this->data["sections"] as $k=>$v) {
                foreach($this->data["sections"][$k]["items"] as $ik=>$iv) {
                  if ($this->data["sections"][$k]["items"][$ik]["SKU"] == $item["Part Number"]) {
                    $this->data["sections"][$k]["items"][$ik]["discount"] = $discount;
                  }
                }
              }
              if (!$found) {
                $this->data["availableDiscounts"][] = $discount;

                  // print_r($this->data);
              }
            }
          } else {

            // insert
              // $sql = "INSERT INTO manufacturier(
              //   name,
              //   code
              // ) VALUES(
              //   '".$codeimport_mfg."-".$codeimport_cat."',
              //   '".$codeimport_mfg."'
              // ) ";
              // $db->query($sql);
              // $manufacturier_id = $db->getLastInsertId();
              // $sql = "INSERT INTO discount(
              //   fk_manufacturier_id	,
              //   code	,
              //   title
              // ) VALUES(
              //   '".$manufacturier_id."',
              //   '".$codeimport_cat."',
              //   '".$codeimport_cat."'
              // ) ";
              // $db->query($sql);
              $discount = array(
                "code" => $codeimport_cat,
                "name" => $codeimport_mfg."-".$codeimport_cat,
                "parts" =>
                  [0,0,0,0,0]

                );

                              $found = false;
                              foreach($this->data["availableDiscounts"] as $_discount) {
                                if ($_discount["code"] == $codeimport_cat) {
                                  $found = true;
                                  break;
                                }
                              }

                              if (!$found) {
                                $this->data["availableDiscounts"][] = $discount;
                              }
                foreach($this->data["sections"] as $k=>$v) {
                  foreach($this->data["sections"][$k]["items"] as $ik=>$iv) {
                    if ($this->data["sections"][$k]["items"][$ik]["SKU"] == $item["Part Number"]) {
                      $this->data["sections"][$k]["items"][$ik]["discount"] = $discount;
                    }
                  }
                }
          }
        }
      }
      unset($this->data["csv_import"]);
    }

    if (!$this->subTotal) {
      $this->subTotal = 0;
    }

    $query = "  UPDATE `quote`
                SET
                  `language`='{$db->safe($this->language)}',
                  `data_sections`='{$this->jsonEncode($this->sections)}',
                  `title`='{$db->safe($this->title)}',
                  `additionnal_note`='{$db->safe($this->additionalNote)}',
                  `additionnal_note_interne`='{$db->safe($this->additionalNoteInterne)}',
                  `valid_until`='{$db->safe($this->validUntil)}',
                  `data_clauses`='{$this->jsonEncode($this->clauses)}',
                  `deposit`='{$db->safe($this->depot)}',
                  `total`='{$db->safe($this->total)}',
                  `sub_total`='{$db->safe($this->subTotal)}',
                  `discount_method`='{$db->safe($this->discountMethod)}',
                  `discount_param`='{$db->safe($this->discountParam)}',
                  `expedition_and_installation_method`='{$db->safe($this->installationAndShippingMethod)}',
                  `expedition_and_installation_param`='{$db->safe($this->installationAndShippingParam)}',
                  `data_address`='{$this->jsonEncode($this->address)}',
                  `data_specialist`='{$this->jsonEncode($this->specialist)}',
                  `fk_bill_to_id`='{$db->safe($this->billTo['contact']['id'])}',
                  `data_bill_to`='{$this->jsonEncode($this->billTo)}',
                  `fk_ship_to_id`='{$db->safe($this->shipTo['contact']['id'])}',
                  `fk_client_id`='{$db->safe($this->client_id)}',
                  `data`='{$this->jsonEncode($this->data)}',
                  `data_ship_to`='{$this->jsonEncode($this->shipTo)}',
                  `data_additionnal_fees`='{$this->jsonEncode($this->additionnalFees)}',
                  `created_fk_employee_id`='{$db->safe($this->specialist['id'])}'
                WHERE `number`={$db->safe($this->number)}";

    $res = $db->query($query);

    return $this->data;
  }

  private function jsonEncode($value) {
    global $db;

    return $db->safe(json_encode($value));
  }
}
