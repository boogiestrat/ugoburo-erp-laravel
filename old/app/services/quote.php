<?php

const PROFIT_METHOD_FIXED_PRICE = 0;
const PROFIT_METHOD_COST_PLUS_PERCENT =1;
const PROFIT_METHOD_GROSS_PROFIT = 2;
const PROFIT_METHOD_NET_PROFIT = 3 ;
const PROFIT_METHOD_LIST_MINUS =  4;

const QUOTE_ADDRESS_BILLING='billTo';
const QUOTE_ADDRESS_SHIPPING='shipTo';

function search_quotes_datatables($datatable_query) {

  global $db;

  $employee_id = get_current_employee()['id'];
  $my_client_only = false;
  if (user_level(1)) {
    $my_client_only = true;
  }

  $search_query = $datatable_query["search"]["value"];

  $limit_start = $datatable_query["start"];
  $limit_length = $datatable_query["length"];
  $limit_stop = $limit_start + $limit_length;

  $order_by = $datatable_query["order"][0]["column"];
  $order_by_sort = strtoupper($datatable_query["order"][0]["dir"]);
  $nb_items_per_page = 25;

  if ($order_by == "0") {
    $order_by = "entreprise";
  }

  $recordsTotal = 0;
  $recordsFiltered = 0;

  $returned_results = [];

  /*
    sql pure
    limit, order by
  */
  $sql = " SELECT
    count(*) as TOTAL
    FROM quote
  ";

  if ($my_client_only) {
    $sql .= "
      WHERE q.created_fk_employee_id = '$employee_id'
    ";
  }

  $results_clients_total = $db->queryFirst($sql);
  $recordsTotal = $results_clients_total["TOTAL"];
  $recordsFiltered = $recordsTotal;

  $sql = " SELECT q.*, cl.entreprise as client_name, qs.name as quote_status, concat(ec.first_name, ' ', ec.last_name) as employee_name,
   ct.email as email_client
    FROM quote as q
      LEFT JOIN employee AS ec ON ec.id = q.created_fk_employee_id
      LEFT JOIN quote_status AS qs ON qs.id = q.fk_status_id
      LEFT JOIN client AS cl ON cl.id = q.fk_client_id
      LEFT JOIN contact AS ct ON ct.fk_client_id = q.fk_client_id AND ct.is_default_contact
  ";

  $sql .= " WHERE ( 1 = 1 ) ";

  if ($my_client_only) {
    $sql .= "
      AND ( q.created_fk_employee_id = '$employee_id' )
    ";
  }

  if (!empty($search_query) && strlen($search_query) >=2) {
    $sql .= "
      AND (
        qs.name LIKE '%$search_query%'
        OR cl.entreprise LIKE '%$search_query%'
        OR ec.first_name LIKE '%$search_query%'
        OR ec.last_name LIKE '%$search_query%'
        OR q.number LIKE '%$search_query%'
        OR q.title LIKE '%$search_query%'
      )
    ";
  }

  $sql .= "
    ORDER BY $order_by $order_by_sort
    LIMIT $limit_start, $limit_stop
  ";

  $results_clients = $db->queryArray($sql);

  $returned_results = $results_clients;

  $data = [
    "data" => $returned_results,
    "recordsTotal" => $recordsTotal,
    "recordsFiltered" => $recordsFiltered,
  ];

  return $data;

}

function get_all_quotes ($order_by = 'id DESC') {
    global $db;

    $order_by = $db->safe($order_by);
    $isAdmin = get_current_employee()['fk_user_level_id'] > 2 ? true : false;

    $query = "  SELECT  q.id, q.total, q.fk_status_id, q.fk_type_id, q.creation,
                        c.first_name, c.last_name, c.email,
                        b.first_name AS beneficiary_first_name, b.last_name AS beneficiary_last_name,
                        qs.name AS status,
                        p.name AS payment_type,
                        '$isAdmin' AS isAdmin,
                        CONCAT (ec.last_name, ',', ec.first_name) AS created_by,
                        CONCAT (em.last_name, ',', em.first_name) AS modified_by
                FROM quotes AS q
                INNER JOIN client AS c ON c.id = q.fk_client_id

                INNER JOIN quote_status AS qs ON qs.id = q.fk_status_id
                LEFT JOIN payment_mode AS p ON p.id = q.fk_payment_mode_id
                LEFT JOIN employee AS ec ON ec.id = q.created_fk_employee_id
                LEFT JOIN employee AS em ON em.id = q.modified_fk_employee_id
                ORDER BY ". $order_by ."
            ";

    return $db->queryArray($query);
}

function get_quote_json ($quote_id) {
  global $db;

  $quote_id = $db->safe($quote_id);

  $query = "  SELECT quotes.data FROM quotes WHERE quotes.number='$quote_id';";

  if ($res = $db->queryFirst($query)) {
    if ($res['data'] !== "") {
      return $res['data'];
    }
  }

  return false;
}


function get_quote_by_number ($quote_id) {
    global $db;

    $quote_id = $db->safe($quote_id);

    $query = "  SELECT  q.*,
                        CONCAT (ec.last_name, ',', ec.first_name) AS created_by,
                        CONCAT (em.last_name, ',', em.first_name) AS modified_by,
                        em.id AS modified_by_id
                FROM quote AS q
                LEFT JOIN employee AS ec ON ec.id = q.created_fk_employee_id
                LEFT JOIN employee AS em ON em.id = q.modified_fk_employee_id
                WHERE q.number = ". $quote_id ."
            ";

    return $db->queryFirst($query);
}
function get_quote ($quote_id) {
    global $db;

    $quote_id = $db->safe($quote_id);

    $query = "  SELECT  q.*,
                        CONCAT (ec.last_name, ',', ec.first_name) AS created_by,
                        CONCAT (em.last_name, ',', em.first_name) AS modified_by,
                        em.id AS modified_by_id
                FROM quote AS q
                LEFT JOIN employee AS ec ON ec.id = q.created_fk_employee_id
                LEFT JOIN employee AS em ON em.id = q.modified_fk_employee_id
                WHERE q.id = ". $quote_id ."
            ";

    return $db->queryFirst($query);
}
function get_quotes_by_email($email,$quoteNumber=null){
    /** @var $db SimpleMySQLi */
    global $db;
    $email = $db->safe($email);
    $query ="SELECT quote.*
            FROM quote
            INNER JOIN contact billTo ON quote.fk_bill_to_id = billTo.id
            INNER JOIN contact shipTo ON quote.fk_ship_to_id = shipTo.id
            WHERE billTo.email= '{$email}' OR shipTo.email='{$email}' ";

    if($quoteNumber){
        $quoteNumber = $db->safe($quoteNumber);
        $query.=" AND quote.number = $quoteNumber";
        return $db->queryFirst($query);
    }

    return $db->queryArray($query);
}

function ordercreate_quote($data) {
    global $db;

    $id = $db->safe($data['id']);
    $sql = "SELECT * FROM quote WHERE id='".$id."'";
    $quote_results = $db->queryFirst($sql);
    if (!empty($quote_results)) {
      $data = json_decode($quote_results["data"], true);
      // print_r($data);die();
      $quote_results = array_merge($quote_results,$data);
      $quote_results["id"] = $id;
      $quote_results["data"] = $data;
      $quote_results["data_sections"] = json_decode($quote_results["data_sections"], true);
      $quoteObj = new Quote($quote_results);
      return $quoteObj->create_order();
    }
    return false;
}

function copy_quote($data) {
    global $db;

    $id = $db->safe($data['id']);

    $selectQuery = "  SELECT `data` from `quote` WHERE id=$id";

    $quoteDataToCopy = $db->queryFirst($selectQuery);

    $newData = json_decode($quoteDataToCopy['data']);

    $newId = get_next_general_id();
    reserve_general_id($newId);

    $newData->number = $newId;

    $query = "  INSERT INTO quote (
                    sub_total,
                    tps,
                    tvq,
                    total,
                    deposit,
                    balance,
                    data_config,
                    data_items,
                    fk_client_id,
                    fk_type_id,
                    fk_payment_mode_id,
                    created_fk_employee_id,
                    modified_fk_employee_id,
                    `started`,
                    creation,
                    last_modification,
                    `language`,
                    data_sections,
                    title,
                    additionnal_note,
                    valid_until,

                    data_address,
                    data_specialist,
                    data_organisation,
                    fk_bill_to_id,
                    data_bill_to,
                    fk_ship_to_id,
                    data_ship_to,
                    data_additionnal_fees,
                    data_clauses,
                    `number`,
                    additionnal_note_interne
                ) SELECT
                    sub_total,
                    tps,
                    tvq,
                    total,
                    deposit,
                    balance,
                    data_config,
                    data_items,
                    fk_client_id,
                    fk_type_id,
                    fk_payment_mode_id,
                    created_fk_employee_id,
                    modified_fk_employee_id,
                    `started`,
                    NOW() as creation,
                    NOW() as last_modification,
                    `language`,
                    data_sections,
                    title,
                    additionnal_note,
                    valid_until,

                    data_address,
                    data_specialist,
                    data_organisation,
                    fk_bill_to_id,
                    data_bill_to,
                    fk_ship_to_id,
                    data_ship_to,
                    data_additionnal_fees,
                    data_clauses,
                    $newId AS `number`,
                    additionnal_note_interne
                FROM quote WHERE id = $id;";

    if ($db->query($query)) {
        $newId = $db->getLastInsertId();
        $newData->id = $newId;

        $newDataString = json_encode($newData);

        $updateQuery = "UPDATE `quote` SET `data`='$newDataString' WHERE id=$newId";
        $db->query($updateQuery);

        log_data(get_current_employee()['id'], '/' . LoggerSection::quote . '/' . LoggerType::copy . '/{' . $newId . '}', '{"data": ' . json_encode($data) . '}', LoggerSection::quote, LoggerType::copy);

        return $newId;
    }
    else
        return false;
}

function create_quote($data) {
    global $db;

    $data = $db->safe($data);
    $data['fk_payment_mode_id'] = ( isset($data['fk_payment_mode_id']) ? $data['fk_payment_mode_id'] : '');
    $data['fk_type_id'] = ( isset($data['fk_type_id']) ? $data['fk_type_id'] : '0');
    if (empty($data['fk_type_id'])) { $data['fk_type_id'] = 0 ; }
    $numeric_fields = ['sub_total', 'discount_rate', 'discount', 'tps', 'tvq', 'total', 'deposit', 'balance'];

    // Converts empty strings in zeros for field with numeric types
    foreach ($numeric_fields as $numeric_field) {
        if (empty($data[$numeric_field])) {
            $data[$numeric_field] = 0;
        }
    }

    $query = "  INSERT INTO quote ( sub_total,
                                    discount_rate,
                                    discount,
                                    tps,
                                    tvq,
                                    total,
                                    deposit,
                                    balance,
                                    data_config,
                                    data_items,
                                    `data`,
                                    fk_client_id,
                                    fk_type_id,
                                    fk_payment_mode_id,
                                    created_fk_employee_id,
                                    modified_fk_employee_id,
                                    `started` )
	            VALUES          (   '" . $data['sub_total'] . "',
                                    '" . $data['discount_rate'] . "',
                                    '" . $data['discount'] . "',
                                    '" . $data['tps'] . "',
                                    '" . $data['tvq'] . "',
                                    '" . $data['total'] . "',
                                    '" . $data['deposit'] . "',
                                    '" . $data['balance'] . "',
                                    '" . $data['data_config'] . "',
                                    '" . $data['data_items'] . "',
                                    '" . $data['data'] . "',
                                    '" . $data['fk_client_id'] . "',
                                    '" . $data['fk_type_id'] . "',
                                    '" . $data['fk_payment_mode_id'] . "',
                                    '" . $data['created_fk_employee_id'] . "',
                                    '" . $data['modified_fk_employee_id'] . "',
                                    '" . $data['started'] . "' )
            ";

    if ($db->query($query)) {
        $newId = $db->getLastInsertId();
        log_data(get_current_employee()['id'], '/' . LoggerSection::quote . '/' . LoggerType::add . '/{' . $newId . '}', '{"data": ' . $data['data'] . '}', LoggerSection::quote, LoggerType::add);

        return $newId;
    }
    else
        return false;
}

function update_quote_status($quoteId, $statusId) {
    global $db;

    $quoteId = $db->safe($quoteId);
    $statusId = $db->safe($statusId);

    // if ($statusId == 4) { // 4 == paid (Payé)
    //
    //   $id = $quoteId;
    //   $sql = "SELECT * FROM quote WHERE id='".$id."'";
    //   $quote_results = $db->queryFirst($sql);
    //   if (!empty($quote_results)) {
    //     $data = json_decode($quote_results["data"], true);
    //     // print_r($data);die();
    //     $quote_results = array_merge($quote_results,$data);
    //     $quote_results["data"] = $data;
    //     $quote_results["data_sections"] = json_decode($quote_results["data_sections"], true);
    //     $quoteObj = new Quote($quote_results);
    //     $quoteObj->create_order();
    //   }
    //
    // }

    $query = "UPDATE quote SET
                fk_status_id=" . $statusId . "
              WHERE id=" . $quoteId . ";";

    return $db->query($query);
}

function update_quote_modified_employee($quoteId, $modifiedEmployeeId) {
    global $db;

    $quoteId = $db->safe($quoteId);
    $modifiedEmployeeId = $db->safe($modifiedEmployeeId);

    $query = "UPDATE quote SET
                modified_fk_employee_id=" . $modifiedEmployeeId . "
              WHERE id=" . $quoteId . ";";

    return $db->query($query);
}

function update_quote($data) {
    global $db;

    $data = $db->safe($data);

    $data['fk_payment_mode_id'] = ( isset($data['fk_payment_mode_id']) ? $data['fk_payment_mode_id'] : '0');
    if (empty($data['fk_payment_mode_id'])) { $data['fk_payment_mode_id'] = 0 ; }

    $data['fk_type_id'] = ( isset($data['fk_type_id']) ? $data['fk_type_id'] : '0');
    if (empty($data['fk_type_id'])) { $data['fk_type_id'] = 0 ; }

    $data['modified_fk_employee_id'] = ( isset($data['modified_fk_employee_id']) ? $data['modified_fk_employee_id'] : '0');
    if (empty($data['modified_fk_employee_id'])) { $data['modified_fk_employee_id'] = 0 ; }

    if (empty($data['started'])) {
      $data['started'] = date("Y-m-d H:i:s");
    }

    $numeric_fields = ['sub_total', 'discount_rate', 'discount', 'tps', 'tvq', 'total', 'deposit', 'balance'];

    // Converts empty strings in zeros for field with numeric types
    foreach ($numeric_fields as $numeric_field) {
        if (empty($data[$numeric_field])) {
            $data[$numeric_field] = 0;
        }
    }

    $query = "  UPDATE quote SET
                    sub_total='" . $data['sub_total'] . "',
                    discount_method='" . $data['discount_rate'] . "',
                    discount_param='" . $data['discount'] . "',
                    tps='" . $data['tps'] . "',
                    tvq='" . $data['tvq'] . "',
                    total='" . $data['total'] . "',
                    deposit='" . $data['deposit'] . "',
                    balance='" . $data['balance'] . "',
                    data_config='" . $data['data_config'] . "',
                    data_items='" . $data['data_items'] . "',
                    `data`='" . $data['data'] . "',
                    fk_client_id='" . $data['fk_client_id'] . "',
                    fk_type_id='" . $data['fk_type_id'] . "',
                    fk_payment_mode_id='" . $data['fk_payment_mode_id'] . "',
                    created_fk_employee_id='" . $data['created_fk_employee_id'] . "',
                    modified_fk_employee_id='" . $data['modified_fk_employee_id'] . "',
                    `started`='" . $data['started'] . "'
                WHERE id='" . $data['id'] . "';";

    if ($result = $db->query($query)) {
        log_data(get_current_employee()['id'], '/' . LoggerSection::quote . '/' . LoggerType::update . '/{' . $data['id'] . '}', '{"data": ' . $data['data'] . '}', LoggerSection::quote, LoggerType::update);
        return $result;
    } else {
        return false;
    }
}

function delete_quote ($quote_id) {  // TODO : Need global security validation
    global $db;

    $quote_id = $db->safe($quote_id);

    $query = "  DELETE FROM quote
                WHERE quote.id = '". $quote_id ."'
            ";

    if ($result = $db->query($query)) {
        log_data(get_current_employee()['id'], '/' . LoggerSection::quote . '/' . LoggerType::delete . '/{' . $quote_id . '}', '{"data": ' . json_encode($result) . '}', LoggerSection::quote, LoggerType::delete);
        return $result;
    }
}

function get_active_quotes_count() {
    global $db;

    $query = "  SELECT COUNT(q.id) as active_quotes_count
                FROM quote as q
                WHERE q.fk_status_id = '1'
                AND q.creation > DATE_ADD(NOW(), INTERVAL -30 DAY)
    ";

    return $db->queryFirst($query);
}

function new_customer_quote($client_id) {

  global $db;

  $data_bill_to = [
    'client'=>[
      'contacts'=>[],
      'addresses'=>[]
    ],
    'address'=>[],
    'contact'=>[],
    'taxes'=>[]
  ];
  $sql = "SELECT * FROM address WHERE is_default_facturation='1' AND fk_client_id='$client_id'";
  $result = $db->queryFirst($sql);
  if (!empty($result)) {
    $data_bill_to['address'] = $result;
  }

  $data_ship_to = [
    'client'=>[
      'contacts'=>[],
      'addresses'=>[]
    ],
    'address'=>[],
    'contact'=>[],
    'taxes'=>[]
  ];
  $sql = "SELECT * FROM address WHERE is_default_shipping='1' AND fk_client_id='$client_id'";
  $result = $db->queryFirst($sql);
  if (!empty($result)) {
    $data_ship_to['address'] = $result;
  }

  $sql = "SELECT * FROM contact WHERE is_default_contact='1' AND fk_client_id='$client_id'";
  $result = $db->queryFirst($sql);
  if (!empty($result)) {
    $data_bill_to['contact'] = $result;
    $data_ship_to['contact'] = $result;
  }

  $sql = "SELECT *, client.entreprise as title FROM client WHERE id='$client_id'";
  $result = $db->queryFirst($sql);
  if (!empty($result)) {
    $data_bill_to['client'] = $result;
    $data_ship_to['client'] = $result;

    $sql = "SELECT * FROM address WHERE fk_client_id='$client_id'";
    $results = $db->queryArray($sql);
    if (!empty($results)) {

      $data_bill_to['client']['addresses'] = $results;
      foreach($results as $address) {
        $sql = "SELECT * FROM province WHERE province_name='".$address["province"]."' ";
        $result = $db->queryFirst($sql);
        if (!empty($result)) {
          $province_id = $result["id"];

          $sql = "SELECT * FROM tax WHERE fk_province_id='".$province_id."' ";
          $result = $db->queryFirst($sql);
          if (!empty($result)) {
            $data_bill_to["taxes"][] = $result;
          }
        }
      }

      $data_ship_to['client']['addresses'] = $results;
      foreach($results as $address) {
        $sql = "SELECT * FROM province WHERE province_name='".$address["province"]."' ";
        $result = $db->queryFirst($sql);
        if (!empty($result)) {
          $province_id = $result["id"];

          $sql = "SELECT * FROM tax WHERE fk_province_id='".$province_id."' ";
          $result = $db->queryFirst($sql);
          if (!empty($result)) {
            $data_ship_to["taxes"][] = $result;
          }
        }
      }

    }

    $sql = "SELECT * FROM contact WHERE fk_client_id='$client_id'";
    $results = $db->queryArray($sql);
    if (!empty($results)) {
      $data_bill_to['client']['contacts'] = $results;
      $data_ship_to['client']['contacts'] = $results;
    }

  }

  $employee_id = get_current_employee()['id'];
  $data_specialist = [];
  $sql = "SELECT * FROM employee WHERE id='$employee_id'";
  $result = $db->queryFirst($sql);
  if (!empty($result)) {
    $data_specialist = $result;
  }

  $sections = json_decode('[{"id":0,"title":"Nouvelle section","items":[],"folded":false}]', true);

  $data_default = json_decode('{"additionalNote":"","additionalNoteInterne":"","additionnalFees":[],"availableDiscounts":[],"clauses":[],"date":"12-02-2019 09:49:48","depot":0,"depotPercent":0,"discountMethod":"percent","discountParam":0,"installationAndShippingMethod":"percent","installationAndShippingParam":0,"id":5,"language":"fr","number":"2019120001","status":{"color":"primary","id":1,"name":"En soumission","list_order":1},"title":"","validUntil":"01-01-2020 09:49:48","total":0,"subTotal":0,"TPS":0,"TVQ":0}', true);

  $data = $data_default;
  $newId = get_next_general_id();
  $data["number"] = $newId;
  $data["date"] = date("Y-m-d H:i:s");
  unset($data["id"]);
  $data["validUntil"] = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", time()) . " + 365 day"));
  $data["billTo"] = $data_bill_to;
  $data["shipTo"] = $data_ship_to;
  $data["sections"] = $sections;
  $data["fk_client_id"] = $client_id;
  $data["specialist"] = $data_specialist;

  $quote_id = create_quote_json($data);
  return $quote_id;
}

function create_quote_json($quote) {
  global $db;
  // $quote_data = json_decode($quote, true);
  $sql = "SELECT * FROM quote WHERE number='".$quote["number"]."'";
  $quote_results = $db->queryFirst($sql);
  if (!empty($quote_results)) {
    update_quote_json($quote);
  } else {
    $quoteObj = new Quote($quote);
    return $quoteObj->create();
  }
}

function update_quote_json($quote) {
  $quoteObj = new Quote($quote);
  return $quoteObj->update();
}

function quote_exists_by_general_id($id) {
  global $db;

  $id = $db->safe($id);

  $query = "  SELECT COUNT(*) as exists_by_qty FROM quote WHERE id=$id;";

  $result = $db->queryFirst($query);

  if (!$result['exists_by_qty']) {
    return false;
  }

  return $result['exists_by_qty'] > 0;
}

function get_next_general_id() {
  global $db;

  $date = date("Ym");

  $query = " SELECT project_id FROM general_id order by project_id desc limit 1;";

  $result = $db->queryFirst($query);

  if (empty($result)) {
    $result['project_id']=0;
  } else {
    $result['project_id']=substr($result['project_id'], -4);
  }

  return $date . sprintf("%04d", ($result['project_id'] + 1));
}

function reserve_general_id($id) {
  global $db;

  $query = "  INSERT INTO general_id SET project_id='$id';";

  $result = $db->query($query);

  return $result;
}

function get_quote_customer_name($quote){
  $quoteJsonData = json_decode($quote['data']);

  return $quoteJsonData->billto->contact->first_name.' '.$quoteJsonData->billto->contact->last_name;
}

function get_quote_billing_html($quote){
    return get_quote_address_html($quote);
}

function get_quote_shipping_html($quote){
    return get_quote_address_html($quote,QUOTE_ADDRESS_SHIPPING);
}


function get_quote_address_html($quote,$type=QUOTE_ADDRESS_BILLING){
    $quoteJsonData = json_decode($quote['data']);
    $quoteTo = $quoteJsonData->{$type};

    return get_html_template_part([
        'type'=>$type,
        'first_name'=>$quoteTo->contact->first_name,
        'last_name'=>$quoteTo->contact->last_name,
        'address'=>$quoteTo->address->address,
        'city'=>$quoteTo->address->city,
        'province'=>$quoteTo->address->province,
        'country'=>$quoteTo->address->country,
        'postal_code'=>$quoteTo->address->postal_code,
        'phone'=>$quoteTo->contact->phone,
        'mobile'=>$quoteTo->contact->cell,
        'email'=>$quoteTo->contact->email,
    ],"quote/address.php");
}

function get_quote_payment_method_html($quote,$isDeposit=true){
    $quoteJsonData = json_decode($quote['data']);
    $quotePayment = null;

    if($isDeposit){
        $quotePayment =  \Ekosys\Entities\QuotePayment::getDepositByQuoteId($quote['id']);
    }else{
        $quotePayment =  \Ekosys\Entities\QuotePayment::getPaymentByQuoteId($quote['id']);
    }

    return get_html_template_part(
        $quotePayment->toArray(),
        'quote/payment_method.php');

}


function send_quote_payment_confirmation_email($quote,$isDeposit,$amount,$recipients){

    $quoteJsonData = json_decode($quote['data']);
// print_r($quoteJsonData);die();
    $mail = new \SendGrid\Mail\Mail();
    $from = getenv('DEFAULT_CONTACT_EMAIL');
    $fromName = getenv('CLIENT_NAME');
    $phone = getenv('CLIENT_PHONE');
    $subject = "Confirmation du paiement complet soumission #".$quoteJsonData->number;

    if($isDeposit)$subject = "Confirmation du paiement d’un dépôt soumission#".$quoteJsonData->number;
    $subject = getenv("SITE_NAME").' - '.$subject;

    $quotePayment = \Ekosys\Entities\QuotePayment::getPaymentByQuoteId($quote["id"]);
    $quoteDeposit = \Ekosys\Entities\QuotePayment::getDepositByQuoteId($quote["id"]);
    $quoteTotal = \Ekosys\Entities\Tax::calculate($quote);

    $template =  $html = get_html_template_part(
        [
            'store_url'=>getenv('BASE_URL'),
            'logo_url'=>getenv('BASE_URL').'assets/img/ugoburo-logo.png',
            'store_name'=>getenv('CLIENT_NAME'),
            'customer_name'=>$quoteJsonData->billTo->contact->first_name." ".$quoteJsonData->billTo->contact->last_name,
            'contact_email'=>getenv('DEFAULT_CONTACT_EMAIL'),
            'contact_phone'=>getenv('DEFAULT_CONTACT_PHONE'),
            'quote_id'=>$quoteJsonData->number,
            'billing'=>get_quote_billing_html($quote),
            'shipping'=>get_quote_shipping_html($quote),
            'is_deposit'=>$isDeposit,
            'quote_number'=>$quote['number'],
            'quote_date'=>date('Y-m-d',strtotime($quoteJsonData->date)),
            'quote_payment_method'=>get_quote_payment_method_html($quote,$isDeposit),
            'quote_items'=> get_html_template_part(['data'=>$quoteJsonData],'payment/sections.php'),
            'quote_totals'=>get_html_template_part(['payment'=>$quotePayment,'deposit'=>$quoteDeposit,'quoteTotal'=>$quoteTotal,'quoteJsonData'=>$quoteJsonData],'payment/totals.php'),


        ],
        "payment/confirmation-email.php");
// print_r($template);die();
    /*** TEST **/
    /*echo $template;
    exit;*/
    /** FIN TEST */


    $mail->setFrom($from, $fromName);
    $mail->setSubject($subject);

    foreach($recipients as $recipient){
        if($recipient!=$quoteJsonData->specialist->email) $mail->addTo($recipient);
    }

    $mail->addCc($quoteJsonData->specialist->email);

    $extra_recipients = getenv('PAYMENT_CONFIRMATION_EMAILS');
    if (!empty($extra_recipients)) {
      if (strpos($extra_recipients, ",")) {
        $extra_recipients = explode(",", $extra_recipients);
        // echo '<pre>';print_r($extra_recipients);
        foreach($extra_recipients as $extra_recipient) {$extra_recipient=trim($extra_recipient);
          if($extra_recipient!=$quoteJsonData->specialist->email) {
            $found=false;
            foreach($recipients as $recipient){
              if ($recipient==$extra_recipient){$found=true;break;}

            }
            if (!$found) {print_r($extra_recipient);
              $mail->addBcc(trim($extra_recipient));
            }
          }
        }
      } else {
        $mail->addBcc($extra_recipients);
      }
    }

    if(getenv('DEBUG') && $quoteJsonData->specialist->email!= getenv('DEBUG_CONTACT_EMAIL')) $mail->addBcc(getenv('DEBUG_CONTACT_EMAIL'));

    $mail->addContent("text/plain", strip_tags($template));
    $mail->addContent("text/html", $template);

    $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

    try {
        $response = $sendgrid->send($mail);
        $result = array('code' => $response->statusCode(), 'msg' => $response->body(). "\n");

    } catch (Exception $e) {
        $json = array('code' => 500, 'msg' => 'Caught exception: '.  $e->getMessage(). "\n");
        $result= json_encode($json);
    }
    // print_r($result);
    return $result;
}

function get_quote_sections($quote){
    if(is_int($quote)){
        $quote = get_quote($quote);
    }
    $data = json_decode($quote['data']);
    $sections = [];
    foreach($data->sections as $sectionKey => $section){
        $section_total = 0;
        foreach ($section->items as $item){
            $section_total+=calculate_quote_item_total($item);
        }
        $section_data =  new stdClass();
        $section_data->title = $section->title;
        $section_data->total = currency_format($section_total);
        $sections[] =$section_data;

    }
    return $sections;
}

function calculate_quote_item_total($quote_item,$format=false){
    $total = calculate_quote_item_unit_price($quote_item) * $quote_item->quantity;
    if($format) $total = currency_format($total);
    return $total;
}

function calculate_quote_item_unit_price($quote_item,$format=false) {
    switch ($quote_item->profitMethod) {
        case PROFIT_METHOD_COST_PLUS_PERCENT:
          $price = calculate_quote_item_cost_price($quote_item) * (1 + ($quote_item->priceParameterValue / 100));
          break;
      case PROFIT_METHOD_GROSS_PROFIT:
          $price = calculate_quote_item_cost_price($quote_item) / (1 - ($quote_item->priceParameterValue / 100));
          break;
      case PROFIT_METHOD_NET_PROFIT:
          $price = calculate_quote_item_cost_price($quote_item)  + $quote_item->priceParameterValue;
          break;
      case PROFIT_METHOD_LIST_MINUS:
          $price = $quote_item->price * (1 - ($quote_item->priceParameterValue / 100));
          break;
      case PROFIT_METHOD_FIXED_PRICE:
      default:
          $price = $quote_item->priceParameterValue;
          break;
  }

  if($format) $price = currency_format($price);

  return $price;
}

function calculate_quote_item_cost_price($quote_item) {
    $price = $quote_item->price;
    foreach($quote_item->discount->parts as $discount){
        $price = $price - ($price * ($discount/100));
    }
    // return $price + $quote_item->expeditionPrice + (($quote_item->expeditionPercent / 100) * $price);
    return $price + $quote_item->expeditionPrice;
}

function calculate_quote_item_expedition_price($quote_item){
    $price = $quote_item->price;
    // return $price + $quote_item->expeditionPrice + (($quote_item->expeditionPercent / 100) * $price);
    return $price + $quote_item->expeditionPrice;
}

function calculate_quote_overview($quote){
    if(!is_array($quote)){
        $quote = get_quote($quote);
    }
    $data = json_decode($quote['data']);

    $total_cost = 0;
    $total_price = 0;
    $total_profit = 0;
    $total_exp = 0;
    $total_gp = 0;

    if ($data->sections) {
      foreach($data->sections as $sectionKey => $section) {
        foreach ($section->items as $item) {
          $total_cost+= $item->_CostPrice * $item->quantity;
          $total_price+= $item->_ItemPrice * $item->quantity;
          $total_exp += $item->_LineShipping;
        }
      }
    }

    $total_profit = $total_price-$total_cost;
    $total_gp = 0;
    if ($total_price != 0) {
      $total_gp = number_format((($total_price - $total_cost - $total_exp) / $total_price) * 100,2);
    }

    if(is_numeric($total_gp))$total_gp.='%';
    else $total_gp='N/A';

    $totals = new stdClass();
    $totals->gp = $total_gp;
    $totals->cost = currency_format($total_cost);
    $totals->profit = currency_format($total_profit);

    return $totals;
}

function calculate_quote_discount_overview($quote){

    if(is_int($quote)){
        $quote = get_quote($quote);
    }
    $data = json_decode($quote['data']);

    $discount_items = [];

    foreach($data->sections as $sectionKey => $section) {
        foreach ($section->items as $item) {
            $discount_items[$item->discount->name][] = $item;
        }
    }

    $discount_overviews=[];

    foreach($discount_items as $discount_name=>$items){

        $discount_cost = 0;
        $discount_price = 0;
        $total_exp = 0;

        foreach($items as $item){
            $discount_cost+= $item->_CostPrice * $item->quantity;
            $discount_price+= $item->_ItemPrice * $item->quantity;
            $total_exp += $item->_LineShipping;
        }

        $discount_profit = $discount_price-$discount_cost;
        if ($discount_price==0) {
            $discount_gp = 'n/a';
        } else {
            $discount_gp = number_format((($discount_price - $discount_cost - $total_exp) / $discount_price) * 100,2);
            $discount_gp .= '%';
        }

        $discount_overview = new stdClass();
        $discount_overview->name = $discount_name;
        $discount_overview->gp = $discount_gp;
        $discount_overview->cost = currency_format($discount_cost);
        $discount_overview->profit = currency_format($discount_profit);

        $discount_overviews[] = $discount_overview;
    }

    return $discount_overviews;

}

function calculate_quote_section_overview($quote){

    if(is_int($quote)){
        $quote = get_quote($quote);
    }
    $data = json_decode($quote['data']);

    $section_overviews = [];

    foreach($data->sections as $sectionKey => $section) {

        $section_cost = 0;
        $section_price = 0;
        $section_profit = 0;
        $total_exp = 0;
        $section_gp = 0;

        foreach ($section->items as $item) {
            $section_cost+= $item->_CostPrice * $item->quantity;
            $section_price+= $item->_ItemPrice * $item->quantity;
            $total_exp += $item->_LineShipping;
        }

        $section_profit = $section_price-$section_cost;
        if ($section_price==0) {
            $section_gp = 'n/a';
        } else {
            $section_gp = number_format((($section_price - $section_cost - $total_exp) / $section_price) * 100,2);
            $section_gp .= '%';
        }


        $section_overview = new stdClass();
        $section_overview->title = $section->title;
        $section_overview->gp = $section_gp;
        $section_overview->cost = currency_format($section_cost);
        $section_overview->profit = currency_format($section_profit);

        $section_overviews[] = $section_overview;
    }

    return $section_overviews;

}


function get_payment_url($lang){

  if(getenv("SAME_DOMAIN_PAYMENT"))  return getenv("SAME_DOMAIN_PAYMENT");

  $url = getenv('PROTOCOL');
  if ($lang == "en") {
    $url .= getenv('DOMAIN_PAYMENTS_EN');
  } else {
    $url .= getenv('DOMAIN_PAYMENTS_FR');
  }
  $url .= getenv('PAYMENT_PATH');
  return $url;
}

function get_quote_payment_token($quote_id,$depositAmount,$emails,$lang=null){
    return  \Ekosys\Security::encrypt(['quote_id'=>$quote_id,'deposit'=>$depositAmount?$depositAmount:'0','emails'=>$emails,'lang'=>$lang],\Ekosys\Security::PAYMENT_URL_KEY,true);
}

function get_quote_payment_url($quote_id,$depositAmount,$emails,$lang=null){
    return get_payment_url($lang).urlencode(get_quote_payment_token($quote_id,$depositAmount,$emails,$lang));
}


/**
 * @param $encodedData
 * @return Exception|stdClass
 */
function get_quote_payment_data($encodedData){
    try{
        $decrypt = \Ekosys\Security::decrypt($encodedData,\Ekosys\Security::PAYMENT_URL_KEY,true);

        if(!$decrypt) return new \Exception('Invalid payment token');

        return $decrypt;
    }catch (\Exception $ex){
        return $ex;
    }
}

function get_quote_pdf_html($quote_id){
    $quote_html = null;
    if(!$quote = get_quote($quote_id)) {
         throw new \Exception("Erreur - Numéro de soumission inexistant.");
    }
    include( WEB_ROOT . 'webservices/quote/print/quote_2.tpl.pdf.php');
    return $quote_html;
}

function get_quote_pdf($quote_id,$output=\Mpdf\Output\Destination::STRING_RETURN,$filename='soumission.pdf'){
    $quote_html = get_quote_pdf_html($quote_id);

    $mpdf = new \Mpdf\Mpdf([
        'mode' => 'utf-8',
        'format' => 'Legal',
        'orientation' => 'P',
        'tempDir' => sys_get_temp_dir()
    ]);
    $mpdf->SetProtection(array('copy','print'), '', PDF_PASSWORD, 128);
    $mpdf->WriteHTML($quote_html);
    $mpdf->SetHTMLFooter('<div class="quote-footer"><b>'.__('quote-pdf_footer-customer-service').' :</b> '.__('quote-pdf_footer-business-hours').' | <b>'.__('quote-pdf_footer-email').'</b> : '.getenv('DEFAULT_CONTACT_EMAIL').' <b>{PAGENO}/{nb}</b></div>');

    $quote_pdf_data = $mpdf->Output($filename, $output);

    return $quote_pdf_data;
}

const QUOTE_HTML_PART_CLIENT = 'client';
const QUOTE_HTML_PART_SECTIONS = 'sections';
const QUOTE_HTML_PART_ADDITIONAL = 'additional';
const QUOTE_HTML_PART_TOTALS = 'totals';
const QUOTE_HTML_PART_SUMMARY= 'summary';
const QUOTE_HTML_PART_DEPOSIT= 'deposit';
const QUOTE_HTML_PART_DEPOSIT_INFO= 'deposit-info';
const QUOTE_HTML_PART_PAYMENT_INFO= 'payment-info';
/**
 * @param $data Quote json decoded data
 * @param $part Html output
 */
function get_quote_html_part($data,$part){
    $html = get_html_template_part($data,"quote/$part.php");
    return $html;
}

function get_quote_payment_html_part($data,$part){
    $html = get_html_template_part($data,"quote/payment/$part.php");
    return $html;
}


function get_quote_status_badge($id) {
  global $db;

  $id = $db->safe($id);

  $badge = 'secondary';

  if ($id == 0) {
      return [
          'id' => 0,
          'name' => 'Non défini',
          'color' => 'primary',
          'list_order' => 0,
      ];
  }

  $query = "  SELECT *
              FROM quote_status
              WHERE id = $id;
  ";

  $result = $db->queryFirst($query);

  return $result;
}

function get_all_quote_status() {
  global $db;

  $query = "  SELECT *
              FROM quote_status;
  ";

  $result = $db->queryArray($query);

  return $result;
}
