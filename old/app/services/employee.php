<?php

    abstract class EmployeeLevel
    {
        const RepresentantExterne = 'Représentant externe';
        const RepresentantInterne = 'Représentant interne';
        const Support = 'Support';
        const Gestionnaire = 'Gestionnaire';
        const Administrateur = 'Administration';
        const Inactif = 'Inactif';
    }

    function get_all_user_levels ($orderBy = "list_order") {
        global $db;

        $query = "  SELECT a.*
                    FROM user_level AS a
                    ORDER BY a.$orderBy DESC
        ";

        return $db->queryArray($query);
    }

    function get_all_employees() {

        global $db;
        $employeeLevel = get_current_employee()['fk_user_level_id'];
        $query = "  SELECT e.*, ul.name AS user_level
                    FROM employee AS e
                    INNER JOIN user_level AS ul ON e.fk_user_level_id = ul.id
                    WHERE e.fk_user_level_id <= '".$employeeLevel."'
                    ORDER BY ABS(e.fk_user_level_id) DESC
        ";

        return $db->queryArray($query);
    }

    function get_employee($id) {
        global $db;

        $query = "  SELECT e.*, ul.name AS user_level
                    FROM employee AS e
                    INNER JOIN user_level AS ul ON e.fk_user_level_id = ul.id
                    WHERE e.id = '" . $id . "'
        ";

        return $db->queryFirst($query);
    }

    function get_employee_by_email($email) {
        global $db;

        $query = "  SELECT e.*, ul.name AS user_level
                    FROM employee AS e
                    INNER JOIN user_level AS ul ON e.fk_user_level_id = ul.id
                    WHERE e.email = '" . $email . "'
        ";

        return $db->queryFirst($query);
    }

    function delete_employee ($employee_id) {  // TODO Need global security validation
        global $db;

        $quote_id = $db->safe($quote_id);

        $query = "  DELETE FROM employee
                    WHERE employee.id = '". $employee_id ."'
                ";

        if ($result = $db->query($query)) {
            log_data(get_current_employee()['id'], '/' . LoggerSection::employee . '/' . LoggerType::delete . '/{' . $employee_id . '}', '{"data": ' . json_encode($result) . '}', LoggerSection::employee, LoggerType::delete);
            return $result;
        } else {
            return false;
        }
    }

    function create_employee ($data, $log = true) {  // TODO Need global security validation
        global $db;

        $first_name = $db->safe($data['first_name']);
        $last_name = $db->safe($data['last_name']);
        $email = $db->safe($data['email']);
        $mobile = format_phone_number($db->safe($data['mobile']));
        $user_level = $db->safe($data['user_level']);
        $avatar = '';

        if (!empty($_FILES)) {
            ### AVATAR ###
            $image = new Bulletproof\Image($_FILES);
            $upload_folder = realpath(ROOT .'web/assets/upload/employee/');
            $image->setLocation($upload_folder)
                ->setSize(10, 10000000);

            if($image['avatar']){

                $upload = $image->upload();

                if($upload){
                    $cropped_image = new \Gumlet\ImageResize($image->getFullPath());
                    $cropped_image->crop(AVATAR_SIZE, AVATAR_SIZE);
                    $cropped_image->save($image->getFullPath());

                    $avatar = $image->getName() . '.' . $image->getMime();
                }  else{
                    //throw new \Exception($image["error"]);
                    return false;
                }
            }
        }

        $query = "  INSERT INTO employee (
                        first_name,
                        last_name,
                        email,
                        mobile,
                        avatar,
                        fk_user_level_id
                    ) VALUE (
                        '". $first_name ."',
                        '". $last_name ."',
                        '". $email ."',
                        '". $mobile ."',
                        '". $avatar ."',
                        '". $user_level ."'
                    )
                ";

        if ($db->query($query)) {
            $newId = $db->getLastInsertId();
            if ($log) {
                log_data(get_current_employee()['id'], '/' . LoggerSection::employee . '/' . LoggerType::add . '/{' . $newId . '}', '{"data": ' . json_encode($data) . '}', LoggerSection::employee, LoggerType::add);
            }

            return $newId;
        }
        else
            return false;
    }

    function update_employee ($data) {  // TODO Need global security validation
        global $db;

        $id = $db->safe($data['id']);
        $first_name = $db->safe($data['first_name']);
        $last_name = $db->safe($data['last_name']);
        $email = $db->safe($data['email']);
        $mobile = format_phone_number($db->safe($data['mobile']));
        $user_level = $db->safe($data['user_level']);
        if (isset($data['fk_user_level_id'])) { $user_level = $db->safe($data['fk_user_level_id']); }
        $avatar = $db->safe($data['avatar']);

        if (!empty($_FILES)) {
            ### AVATAR ###
            $image = new Bulletproof\Image($_FILES);
            $upload_folder = realpath(WEB_ROOT .'assets/upload/employee/');
            $image->setLocation($upload_folder)
                ->setSize(10, 10000000);

            if($image['avatar']){

                $upload = $image->upload();

                if($upload){
                    $cropped_image = new \Gumlet\ImageResize($image->getFullPath());
                    $cropped_image->crop(AVATAR_SIZE, AVATAR_SIZE);
                    $cropped_image->save($image->getFullPath());
                    $avatar = $image->getName() . '.' . $image->getMime();
                } else{
                    //throw new \Exception($image["error"]);
                    return false;
                }

            } elseif(strpos($data['avatar'], 'http') === 0) { //Image By URL
                $avatar = strtolower($first_name . '-' . $last_name . '.jpg');
                $temp_file = tempnam(sys_get_temp_dir(), 'avatar');
                copy($data['avatar'], $temp_file);
                $cropped_image = new \Gumlet\ImageResize($temp_file);
                $cropped_image->crop(AVATAR_SIZE, AVATAR_SIZE);
                $cropped_image->save($upload_folder . DIRECTORY_SEPARATOR . $avatar, IMAGETYPE_JPEG);
            }
        }

        $query = "  UPDATE employee
                    SET first_name = '". $first_name ."',
                    last_name = '". $last_name ."',
                    email = '". $email ."',
                    mobile = '". $mobile ."',
                    avatar = '". $avatar ."',
                    fk_user_level_id = '". $user_level ."'
                    WHERE id = '". $id ."'
                ";

        if ($result = $db->query($query)) {
            log_data(get_current_employee()['id'], '/' . LoggerSection::employee . '/' . LoggerType::update . '/{' . $id . '}', '{"data": ' . json_encode($data) . '}', LoggerSection::employee, LoggerType::update);
            return $result;
        } else {
            return false;
        }
    }

    function get_current_employee() { //Logged in user
        return unserialize($_SESSION['employee']);
    }

    function search_employees($keyword) {
        global $db;

        $user_level = get_current_employee()['fk_user_level_id'];
        $current_id = get_current_employee()['id'];

        $keyword = $db->safe($keyword);

        if ($user_level == 1 || $user_level == 2)  {
          $query = "  SELECT c.*
                      FROM employee AS c
                      WHERE c.id = $current_id
          ";
        } else {
          $query = "  SELECT c.*
                      FROM employee AS c
                      WHERE c.last_name LIKE '%" . $keyword . "%'
                      OR c.first_name LIKE '%" . $keyword . "%'
          ";
        }


        return $db->queryArray($query);
    }

    function create_client_relation($clientId, $employeeId) {
        global $db;

        $clientId = $db->safe($clientId);
        $employeeId = $db->safe($employeeId);

        $query =    "INSERT INTO `employee_clients`
                        (`fk_employee_id`, `fk_client_id`)
                    VALUES ('$employeeId', '$clientId');";

        return $db->query($query);
    }

    function delete_client_relation($clientId, $employeeId) {
        global $db;

        $clientId = $db->safe($clientId);
        $employeeId = $db->safe($employeeId);

        $query =    "DELETE FROM `employee_clients` WHERE
                        fk_employee_id = '$employeeId' AND fk_client_id = '$clientId';";

        return $db->query($query);
    }

    function add_client_permission($employeeId, $clientId, $read = true, $update = false, $delete = false) {
        if (get_current_employee()['fk_user_level_id'] != 3) {
            return false;
        }

        global $db;

        $employeeId = $db->safe($employeeId);
        $clientId = $db->safe($clientId);
        $read = $db->safe($read) ? 1 : 0;
        $update = $db->safe($update) ? 1 : 0;
        $delete = $db->safe($delete) ? 1 : 0;

        $query = "  INSERT INTO employee_client_permissions
                        (fk_employee_id, fk_client_id, read_data, update_data, delete_data)
                    VALUES
                        ($employeeId, $clientId, $read, $update, $delete)";

        return $db->query($query);
    }

    function add_contact_permission($employeeId, $contactId) {
        if (get_current_employee()['fk_user_level_id'] != 3) {
            return false;
        }

        global $db;

        $employeeId = $db->safe($employeeId);
        $contactId = $db->safe($contactId);
        $read = $db->safe($read) ? 1 : 0;
        $update = $db->safe($update) ? 1 : 0;
        $delete = $db->safe($delete) ? 1 : 0;

        $query = "  INSERT INTO employee_contacts
                        (fk_employee_id, fk_contact_id, permission)
                    VALUES
                        ($employeeId, $contactId, 755)";

        return $db->query($query);
    }

    function remove_client_permission($employeeId) {
        if (get_current_employee()['fk_user_level_id'] != 3) {
            return false;
        }

        global $db;

        $employeeId = $db->safe($employeeId);

        $query = "  DELETE FROM employee_client_permissions
                    WHERE fk_employee_id = $employeeId";

        return $db->query($query);
    }

    function remove_client_permission_by_client($clientId) {
        if (get_current_employee()['fk_user_level_id'] != 3) {
            return false;
        }

        global $db;

        $clientId = $db->safe($clientId);

        $query = "  DELETE FROM employee_client_permissions
                    WHERE fk_client_id = $clientId";

        return $db->query($query);
    }

    function remove_contact_permission_by_contact($contactId) {
        if (get_current_employee()['fk_user_level_id'] != 3) {
            return false;
        }

        global $db;

        $contactId = $db->safe($contactId);

        $query = "  DELETE FROM employee_contacts
                    WHERE fk_contact_id = $contactId";

        return $db->query($query);
    }

    function get_employees_by_client($clientId) {
        global $db;

        $clientId = $db->safe($clientId);

        $query = "  SELECT DISTINCT b.*
                    FROM employee_client_permissions AS cc
                    LEFT JOIN employee AS b ON b.id = cc.fk_employee_id
                    WHERE cc.fk_client_id = $clientId";

        return $db->queryArray($query);
    }
