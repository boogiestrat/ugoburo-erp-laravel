<?php

/**
 *  employeesOnly: Affichera seulement les clients de l'employé. Peut être bypasser si le niveau de l'employé est admin.
 */

function search_clients_contacts_addresses($datatable_query) {

  global $db;

  $employee_id = get_current_employee()['id'];
  $my_client_only = false;
  if (user_level(1)) {
    $my_client_only = true;
  }

  $search_query = $datatable_query["search"]["value"];

  $limit_start = $datatable_query["start"];
  $limit_length = $datatable_query["length"];
  $limit_stop = $limit_start + $limit_length;

  $order_by = $datatable_query["order"][0]["column"];
  $order_by_sort = strtoupper($datatable_query["order"][0]["dir"]);
  $nb_items_per_page = 25;

  if ($order_by == "0") {
    $order_by = "entreprise";
  }

  $recordsTotal = 0;
  $recordsFiltered = 0;

  $returned_results = [];

  if (empty($search_query) || strlen($search_query) < 2) {
    /*
      sql pure
      limit, order by
    */
    $sql = " SELECT
      count(*) as TOTAL
      FROM client
    ";

    if ($my_client_only) {
      $sql .= "
        LEFT JOIN
            employee_client_permissions ON ( employee_client_permissions.fk_client_id = client.id
                AND employee_client_permissions.fk_employee_id = $employee_id )
                WHERE employee_client_permissions.fk_employee_id IS NOT NULL
      ";
    }

    $results_clients_total = $db->queryFirst($sql);
    $recordsTotal = $results_clients_total["TOTAL"];
    $recordsFiltered = $recordsTotal;

    $sql = " SELECT client.*, client_type.name as client_type
    FROM client
    LEFT JOIN client_type ON client_type.id = client.fk_client_type_id
    ";
    if ($my_client_only) {
      $sql .= "
        LEFT JOIN
            employee_client_permissions ON ( employee_client_permissions.fk_client_id = client.id
                AND employee_client_permissions.fk_employee_id = $employee_id )
                WHERE ( ( employee_client_permissions.fk_employee_id IS NOT NULL ) OR ( client.fk_created_by_id = $employee_id ) )
      ";
    }
    $sql .= "
    ORDER BY $order_by $order_by_sort
    LIMIT $limit_start, $limit_stop
    ";
    $results_clients = $db->queryArray($sql);

    $returned_results = $results_clients;

  } else {
    $search_query = $db->safe(trim(str_replace(" ", "%", $search_query)));

    $sql = "

    SELECT c.*, ct.name as client_type
    FROM client AS c
    LEFT JOIN client_type AS ct ON ct.id = c.fk_client_type_id

    ";

    if ($my_client_only) {
      $sql .= "
        LEFT JOIN
            employee_client_permissions AS empcp ON ( empcp.fk_client_id = c.id
                AND empcp.fk_employee_id = $employee_id )

      ";
    }

    $sql .= "
      WHERE
        (
        c.entreprise LIKE '%".$search_query."%'
        OR ct.name LIKE '%".$search_query."%'
        OR date_format(c.modified,'%e, %b %Y') LIKE '%".$search_query."%'
        )
    ";

    if ($my_client_only) {
      $sql .= "
        AND ( ( empcp.fk_employee_id IS NOT NULL ) OR ( client.fk_created_by_id = $employee_id ) )
      ";
    }

    $results_clients = $db->queryArray($sql);

    $sql = "

    SELECT cl.*, ct.name as client_type
    FROM contact AS c
    LEFT JOIN client as cl ON cl.id = c.fk_client_id
    LEFT JOIN client_type AS ct ON ct.id = cl.fk_client_type_id
    ";

    if ($my_client_only) {
      $sql .= "
        LEFT JOIN
            employee_client_permissions AS empcp ON ( empcp.fk_client_id = cl.id
                AND empcp.fk_employee_id = $employee_id )

      ";
    }

    $sql .= "

    WHERE
    (
      c.email LIKE '%".$search_query."%'
      OR c.phone LIKE '%".$search_query."%'
      OR c.cell LIKE '%".$search_query."%'
      OR c.first_name LIKE '%".$search_query."%'
      OR c.last_name LIKE '%".$search_query."%'
    )
    ";

    if ($my_client_only) {
      $sql .= "
        AND ( empcp.fk_employee_id IS NOT NULL )
      ";
    }

    $results_contacts = $db->queryArray($sql);

    $sql = "

    SELECT cl.*, ct.name as client_type
    FROM address AS a
    LEFT JOIN client as cl ON cl.id = a.fk_client_id
    LEFT JOIN client_type AS ct ON ct.id = cl.fk_client_type_id
    ";

    if ($my_client_only) {
      $sql .= "
        LEFT JOIN
            employee_client_permissions AS empcp ON ( empcp.fk_client_id = cl.id
                AND empcp.fk_employee_id = $employee_id )

      ";
    }

    $sql .= "
    WHERE
    (
    a.address LIKE '%".$search_query."%'
    )
    ";

    if ($my_client_only) {
      $sql .= "
        AND ( empcp.fk_employee_id IS NOT NULL )
      ";
    }

    $results_addresses = $db->queryArray($sql);

    $sql = " SELECT
      count(*) as TOTAL
      FROM client
    ";
    $results_clients_total = $db->queryFirst($sql);
    $recordsTotal = $results_clients_total["TOTAL"];

    $results = [];

    foreach($results_clients as $client) {
      if (empty($client["entreprise"])) { continue; }
      $id_client = $client["id"];
      $results[$id_client] = $client;
    }

    foreach($results_contacts as $client) {
      if (empty($client["entreprise"])) { continue; }
      $id_client = $client["id"];
      $results[$id_client] = $client;
    }

    foreach($results_addresses as $client) {
      if (empty($client["entreprise"])) { continue; }
      $id_client = $client["id"];
      $results[$id_client] = $client;
    }

    $total_items = count($results);

    $nb_pages = $total_items / $nb_items_per_page;

    $offset = $limit_start ;
    // $offset = ( $current_page * $nb_items_per_page ) - 1 ;

    $length = $limit_length ;
    // $length = ( $nb_items_per_page - 1 );

    $recordsFiltered = count($results);

    $current_page_results = array_splice($results, $offset, $length);
    // echo '<pre>';

    if ($order_by_sort == "ASC") {
      usort($current_page_results, function($a, $b) {
        return $a[$order_by] - $b[$order_by];
      });
    }

    if ($order_by_sort == "DESC") {
      usort($current_page_results, function($a, $b) {
        return $a[$order_by] + $b[$order_by];
      });
    }

    $returned_results = $current_page_results;

  }

  $data = [
    "data" => $returned_results,
    "recordsTotal" => $recordsTotal,
    "recordsFiltered" => $recordsFiltered,
  ];

  return $data;

}

function get_all_clients($employeesOnly = true) {
    global $db;

    $query = "  SELECT c.*
                FROM client AS c
                ORDER BY c.entreprise ASC
    ";

    return $db->queryArray($query);
}

function get_client_by_name($name){
    global $db;
    $name = $db->safe($name);
    $query = "  SELECT client.*
                FROM client
                WHERE client.entreprise = '" . $name . "'
    ";
    return $db->queryFirst($query);
}

function get_client($client_id) {
  $client = new Client($client_id);

    global $db;

    $client_id = $db->safe($client_id);

    $query = "  SELECT client.*, client.entreprise AS `title`
                FROM client
                WHERE client.id = '" . $client_id . "'
    ";
    $results = $db->queryFirst($query);

    $results['client'] = $results;
    $results['contacts'] = get_contact_by_client($client_id);
    $results['employees'] = get_employees_by_client($client_id);
    $results['addresses'] = get_client_addresses($client_id);

    return $results;
}

function get_client_and_self_addresses($client_id) {
  global $db;

  $client_id = $db->safe($client_id);

  $query = "  SELECT contact.* FROM
              WHERE
                contact.fk_client_id = $client_id";

  $results['addresses'] = get_client_addresses($client_id);
  $results['contacts'] = $db->queryArray($query);

  return $results;
}

function get_new_clients() {
    global $db;

    $query = "  SELECT c.*
                FROM client AS c
                INNER JOIN quote AS q ON q.fk_client_id = c.id
                WHERE q.quote_update_datetime > DATE_ADD(NOW(), INTERVAL -". NEW_DATA_RANGE_DAYS ." DAY)
                AND q.fk_status_id = '2'
    ";

    return $db->queryArray($query);
}

function get_new_clients_count() {
    global $db;

    $query = "  SELECT COUNT(c.id) AS new_clients_count
                FROM client AS c
                INNER JOIN quote AS q ON q.fk_client_id = c.id
                WHERE q.quote_update_datetime > DATE_ADD(NOW(), INTERVAL -". NEW_DATA_RANGE_DAYS ." DAY)
                AND q.fk_status_id = '2'
    ";

    return $db->queryFirst($query);
}

function create_client ($data) {  // TODO Need global security validation
    global $db;

    $entreprise = $db->safe($data['entreprise']);
    $client_type = $db->safe($data['client_type']);
    $note = strip_tags($db->safe($data['note']));
    $contacts = $db->safe($data['contacts']);
    $client_contacts_json = json_decode($data['client_contacts_json'], true);
    $client_addresses_json = json_decode($data['client_addresses_json'], true);
    $current = $db->safe(get_current_employee()['id']);

    $query = "  INSERT INTO `client` (
                    `entreprise`,
                    `fk_client_type_id`,
                    `note`,
                    `fk_created_by_id`
                ) VALUE (
                    '". $entreprise ."',
                    '". $client_type ."',
                    '". $note ."',
                    $current
                )
            ";

      if ($db->query($query)) {
          $newId = $db->getLastInsertId();
          $data['id'] = $newId;


          if($client_addresses_json){
              add_client_address_from_json($newId, $client_addresses_json);
          }
          // create_client_relation($newId, get_current_employee()['id']);

          if($client_contacts_json){
              foreach ($client_contacts_json as $contact) {
                  create_client_contact_relation($newId, $contact["id"]);
              }
          }

          log_data(get_current_employee()['id'], '/' . LoggerSection::client . '/' . LoggerType::add . '/{' . $newId . '}', '{"data": ' . json_encode($data) . '}', LoggerSection::client, LoggerType::add);
          return $newId;
      }
      else
          return false;
}

function update_client ($data) {  // TODO Need global security validation
    global $db;

    $id = $db->safe($data['id']);
    $entreprise = $db->safe($data['entreprise']);
    $client_type = $db->safe($data['client_type']);
    $note = strip_tags($db->safe($data['note']));
    $client_addresses_json = json_decode($data['client_addresses_json'], true);
    $client_contacts_json = json_decode($data['client_contacts_json'], true);
    // print_r($client_contacts_json);die();
    $contacts = array();
    if (isset($data['contacts'])) {
      $contacts = $db->safe($data['contacts']);
    }
    $employees = array();
    if (isset($data['employees'])) {
      $employees = $db->safe($data['employees']);
    }

    delete_client_contact_relation_by_client($id);
    foreach ($client_contacts_json as $contact) {

        create_client_contact_relation($id, $contact["id"]);
    }

    remove_client_permission_by_client($id);
    foreach ($employees as $employee) {
        add_client_permission($employee, $id);
    }

    // remove_client_addresses($id);
    // add_client_address($data);
    add_client_address_from_json($id, $client_addresses_json);
    $query = "  UPDATE client
                SET entreprise = '". $entreprise ."',
                fk_client_type_id = '". $client_type ."',
                note = '". $note ."'
                WHERE id = '". $id ."'
            ";

    if ($result = $db->query($query)) {
        log_data(get_current_employee()['id'], '/' . LoggerSection::client . '/' . LoggerType::update . '/{' . $id . '}', '{"data": ' . json_encode($data) . '}', LoggerSection::client, LoggerType::update);
        return $result;
    } else {
        return false;
    }
}

function delete_client ($client_id) {  // TODO Need global security validation
    global $db;

    $client_id = $db->safe($client_id);

    $query = "  DELETE FROM client
                WHERE client.id = '". $client_id ."'
            ";

    if ($result = $db->query($query)) {
        delete_client_relation($client_id, get_current_employee()['id']);
        log_data(get_current_employee()['id'], '/' . LoggerSection::client . '/' . LoggerType::delete . '/{' . $client_id . '}', '{"data": ' . json_encode($result) . '}', LoggerSection::client, LoggerType::delete);
        return $result;
    } else {
        return false;
    }
}

function get_all_cities() {
    global $db;

    $query = "  SELECT DISTINCT c.city
                FROM client AS c
                ORDER BY c.city ASC
    ";

    return $db->queryArray($query);
}

function search_city($keyword) {
    global $db;

    $keyword = $db->safe($keyword);

    $query = "  SELECT DISTINCT c.city
                FROM client AS c
                WHERE c.city LIKE '" . $keyword . "%'
    ";

    return $db->queryArray($query);
}

function get_all_client_types() {
    global $db;

    $query = "  SELECT DISTINCT *
                FROM client_type;
    ";

    return $db->queryArray($query);
}

function get_client_type_badge($id) {
    global $db;

    $id = $db->safe($id);

    $badge = 'secondary';

    switch ($id) {
        case 0:
            $badge = 'secondary';
        break;
        case 1:
            $badge = 'success';
        break;
        case 2:
            $badge = 'warning';
        break;
        case 3:
            $badge = 'primary';
        break;
        default:
            $badge = 'secondary';
        break;
    }

    if ($id == 0) {
        return [
            'id' => 0,
            'name' => 'Non défini',
            'descriptions' => 'À définir',
            'badge' => $badge,
        ];
    }

    $query = "  SELECT *
                FROM client_type
                WHERE id = $id;
    ";

    $result = $db->queryFirst($query);


    $result['badge'] = $badge;

    return $result;
}

function update_client_type($clientId, $fk_client_type_id) {
    global $db;

    $clientId = $db->safe($clientId);
    $fk_client_type_id = $db->safe($fk_client_type_id);

    $query = "UPDATE client SET
                fk_client_type_id=" . $fk_client_type_id . "
              WHERE id=" . $clientId . ";";

    return $db->query($query);
}

function get_client_contacts($clientId, $max = false) {
    global $db;

    $clientId = $db->safe($clientId);

    $query = "  SELECT * from contact
                WHERE contact.fk_client_id = $clientId";

    if ($max) {
        $query .= " LIMIT $max";
    }

    return $db->queryArray($query);
}

function get_client_addresses($clientId, $default_facturation = false, $default_shipping = false) {
    global $db;

    $clientId = $db->safe($clientId);
    $default_facturation = $db->safe($default_facturation) ? 1 : 0;
    $default_shipping = $db->safe($default_shipping) ? 1 : 0;

    $query = "  SELECT * FROM address WHERE fk_client_id = $clientId";

    if ($default_facturation == 1) {
        $query .= " AND is_default_facturation = TRUE";
    }

    if ($default_shipping == 1) {
        $query .= " AND is_default_shipping = TRUE";
    }

    return $db->queryArray($query);
}

function remove_client_addresses($clientId) {
    global $db;

    $clientId = $db->safe($clientId);

    $query = "  DELETE FROM address WHERE fk_client_id = $clientId";

    return $db->query($query);
}

function add_one_address_to_client($address, $client_id) {
  global $db;

  $client_id = $db->safe($client_id);

  $query = "  INSERT INTO `address` (`fk_client_id`, `address`, `city`, `province`, `country`, `postal_code`)
              VALUES
                ($client_id, '{$db->safe($address->address)}', '{$db->safe($address->city)}', '{$db->safe($address->province)}', '{$db->safe($address->country)}', '{$db->safe($address->postal_code)}')";
  if (!$db->query($query)) {
    return false;
  }
  return $db->getLastInsertId();
}

function add_one_contact_to_client($contact, $client_id) {
  global $db;

  $client_id = $db->safe($client_id);
  $employee = $db->safe(get_current_employee()['id']);

  $query = "  INSERT INTO `contact` (`first_name`, `last_name`, `email`, `phone`, `cell`, `fk_created_by_id`, `fk_client_id`)
              VALUES
                ('{$db->safe($contact->first_name)}', '{$db->safe($contact->last_name)}', '{$db->safe($contact->email)}', '{$db->safe($contact->phone)}', '{$db->safe($contact->cell)}', $employee, $client_id)";


  if (!$db->query($query)) {
    return false;
  }

  $contact_id = $db->getLastInsertId();

  // $query = "  INSERT INTO `client_contacts` (`fk_client_id`, `fk_contact_id`) VALUES ($client_id, $contact_id)";
  // $db->query($query);

  return $contact_id;
}

function add_client_address_from_json($client_id, $addresses) {
  global $db;
  $query = "DELETE FROM address WHERE fk_client_id = '$client_id'";
  $db->query($query);
  foreach ( $addresses as $address ) {
    $query = "  INSERT INTO `address`
                (`address`, `city`, `province`, `country`, `postal_code`, `is_default_facturation`, `is_default_shipping`, `fk_client_id`)
                    VALUES
                (
                    '{$address['address']}',
                    '{$address['city']}',
                    '{$address['province']}',
                    '{$address['country']}',
                    '{$address['postal_code']}',
                    {$address['is_default_facturation']},
                    {$address['is_default_shipping']},
                    $client_id
                )";

                $db->query($query);
  }
}

function add_client_address($data) {
    global $db;

    $id = $db->safe($data['id']);

    $addresses = [];

    // Addresses
    if (($addressCount = count($values = preg_grep('/^address_address_[0-9]*/', array_keys($data)))) > 0) {
        foreach ($values as $key) {
            $index = explode('_', $key)[2];

            if (isset($data['address_address_' . $index])) {
                $addresses[] = [
                    "address" => $db->safe($data['address_address_' . $index]),
                    "city" => $db->safe($data['address_city_' . $index]),
                    "province" => $db->safe($data['address_province_' . $index]),
                    "country" => $db->safe($data['address_country_' . $index]),
                    "postal_code" => $db->safe($data['address_postal_code_' . $index]),
                    "is_default_facturation" => $db->safe($data['address_facturation_' . $index]) ? 1 : 0,
                    "is_default_shipping" => $db->safe($data['address_shipping_' . $index]) ? 1 : 0,
                ];
            }
        }
    }

    foreach ($addresses as $key => $address) {
        $query = "  INSERT INTO `address`
                    (`address`, `city`, `province`, `country`, `postal_code`, `is_default_facturation`, `is_default_shipping`, `fk_client_id`)
                        VALUES
                    (
                        '{$address['address']}',
                        '{$address['city']}',
                        '{$address['province']}',
                        '{$address['country']}',
                        '{$address['postal_code']}',
                        {$address['is_default_facturation']},
                        {$address['is_default_shipping']},
                        $id
                    )";

        $addresses[$key]['done'] = $db->query($query);
    }

    return true;
}
