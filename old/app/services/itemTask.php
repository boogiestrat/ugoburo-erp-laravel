<?php

function create_item_task ($data) {
    global $db;
    $fk_item_id = (int)$db->safe($data["fk_item_id"]);
    $fk_item_variant_id = (int)$db->safe($data["fk_item_variant_id"]);
    $task = $db->safe($data["task"]);
    $points = (int)$db->safe($data["points"]);
    $delay = (int)$db->safe($data["delay"]);

    $query = "  INSERT INTO
                    item_tasks
                (
                    fk_item_id,
                    fk_item_variant_id,
                    task,
                    points,
                    normal_delay_in_days
                )
                VALUE (
                    $fk_item_id,
                    null,
                    '$task',
                    $points,
                    $delay
                );";

    return $db->query($query);
}

function get_all_tasks () {
    global $db;

    $query = "  SELECT
                    *
                FROM
                    item_tasks;";

    return $db->queryArray($query);
}

function get_item_tasks_by_item_id ($itemId, $isVariant = false) {
    global $db;

    $itemId = $db->safe($itemId);

    if (!$isVariant) {
        $query = "  SELECT
                        *
                    FROM
                        item_tasks
                    WHERE
                        fk_item_id = $itemId;";
    } else {
        $query = "  SELECT
                        *
                    FROM
                        item_tasks
                    WHERE
                        fk_item_variant_id = $itemId;";
    }

    return $db->queryArray($query);
}

function get_task_item_by_id ($taskId) {
    global $db;

    $taskId = $db->safe($taskId);

    $query = "  SELECT
                    *
                FROM
                    item_tasks
                WHERE
                    id = $taskId
                LIMIT
                    1;";

    return $db->queryFirst($query);
}

function update_item_tasks_with_id ($data) {
    global $db;

    $taskId = (int)$db->safe($data["id"]);

    $queryIfExists = "SELECT COUNT(*) AS TOTAL FROM item_tasks WHERE id=$taskId LIMIT 1;";

    if ($db->queryFirst($queryIfExists)['TOTAL'] == 0) {
        return create_item_task($data);
    }

    $task = $db->safe($data["task"]);
    $points = $db->safe($data["points"]);
    $delay = $db->safe($data["delay"]);

    $query = "  UPDATE
                    item_tasks
                SET
                    task='$task',
                    points=$points,
                    normal_delay_in_days='$delay'
                WHERE
                    id=$taskId;";

    return $db->query($query);
}

function create_project_progress_from_task ($projectId, $taskId) {
    global $db;

    $projectId = (int)$db->safe($projectId);
    $taskId = (int)$db->safe($taskId);

    $taskQuery = "  SELECT
                        *
                    FROM
                        item_tasks
                    WHERE
                        id = $taskId
                    LIMIT
                        1;";
    $task = $db->queryFirst($taskQuery);

    if (!$task || !$task['task'] || $task['task'] == '') {
        return false;
    }

    $query = "  INSERT INTO
                    project_progress (
                        project_id,
                        status,
                        badge_class,
                        color,
                        task,
                        limit_date,
                        points
                    )
                VALUE (
                    $projectId,
                    'À faire',
                    'default',
                    '#5C6BC0',
                    '" . $task['task'] . "',
                    '" . date('Y-m-d', strtotime('+ ' . $task['normal_delay_in_days'] . ' days')) . "',
                    " . $task['points'] . "
                )";

    if ($db->query($query))
        return $db->getLastInsertId();
    else
        return false;
}

function delete_all_item_tasks_from_parent ($itemId) {
    global $db;

    $itemId = (int)$db->safe($itemId);

    $query = "  DELETE FROM
                    item_tasks
                WHERE
                    fk_item_id = '". $itemId ."'
            ";

    return $db->query($query);
}

function delete_item_tasks ($id) {
    global $db;

    $taskId = (int)$db->safe($id);

    $query = "  DELETE FROM
                    item_tasks
                WHERE
                    id = '". $taskId ."'
            ";

    return $db->query($query);
}
