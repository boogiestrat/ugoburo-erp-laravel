<?php

function get_manufacturier($manufacturier_id) {
    global $db;

    $manufacturier_id = $db->safe($manufacturier_id);

    $query = "  SELECT b.*
                FROM manufacturier AS b
                WHERE b.id = '" . $manufacturier_id . "';
    ";

    $results = $db->queryFirst($query);
    // $results['items'] = get_manufacturier_items($manufacturier_id);
    $results['discounts'] = get_manufacturier_discounts($manufacturier_id);

    return $results;
}

function get_all_manufacturiers() {
    global $db;

    $query = "  SELECT *
                FROM manufacturier;";

    return $db->queryArray($query);
}

function get_all_manufacturier_discounts($terms) {
  global $db;

  $term = $db->safe($terms);

    $query = "  SELECT *, CONCAT(d.title, ' (', m.name, ')') as title
                FROM discount AS d
                LEFT JOIN manufacturier AS m ON m.id = d.fk_manufacturier_id
                WHERE title LIKE '%$term%' OR m.name LIKE '%$term%';";

    return $db->queryArray($query);
}

function get_manufacturier_items($manufacturier_id) {
    global $db;

    $manufacturier_id = $db->safe($manufacturier_id);

    $query = "  SELECT b.*
                FROM item AS b
                WHERE b.fk_manufacturier_id = '" . $manufacturier_id . "';
    ";

    return $db->queryArray($query);
}

function get_manufacturier_discounts($manufacturier_id) {
    global $db;

    $manufacturier_id = $db->safe($manufacturier_id);

    $query = "  SELECT b.*
                FROM discount AS b
                WHERE b.fk_manufacturier_id = '" . $manufacturier_id . "';
    ";

    return $db->queryArray($query);
}

function update_manufacturier ($data) {
    global $db;

    $id = $db->safe($data['id']);
    $name = $db->safe($data['name']);
    $pays = $db->safe($data['pays']);
    $code = $db->safe($data['code']);

    $query = "  UPDATE manufacturier
                SET name = '$name',
                code = '$code',
                pays = '$pays'
                WHERE id = $id
            ";

    remove_manufacturier_discount($id);
    add_manufacturier_discount($data);

    try {
        $results = $db->query($query);
        log_data(get_current_employee()['id'], '/' . LoggerSection::manufacturier . '/' . LoggerType::update . '/{' . $id . '}', '{"data": ' . json_encode($data) . '}', LoggerSection::manufacturier, LoggerType::update);
        return $results;
    } catch (Exception $e) {
        return $e;
    }

}

function delete_manufacturier($manufacturier_id) {
    global $db;

    $manufacturier_id = $db->safe($manufacturier_id);

    remove_manufacturier_discount($manufacturier_id);

    $query = "  DELETE FROM manufacturier
                WHERE id = $manufacturier_id;";

    if ($db->query($query)) {
        return true;
    }
}

function delete_all_manufacturier_items($manufacturier_id) {
    global $db;

    $manufacturier_id = $db->safe($manufacturier_id);

    $query = "  UPDATE item SET
                    fk_manufacturier_id = 0
                WHERE fk_manufacturier_id = $manufacturier_id;";

    if ($db->query($query)) {
        return true;
    }
}

function update_item_manufacturier($item_id, $manufacturier_id) {
    global $db;

    $item_id = $db->safe($item_id);
    $manufacturier_id = $db->safe($manufacturier_id);

    $query = "  UPDATE item
                SET fk_manufacturier_id = $manufacturier_id
                WHERE id = $item_id;";
    try {
        if ($db->query($query)) {
            log_data(get_current_employee()['id'], '/' . LoggerSection::item . '/' . LoggerType::update . '/{' . $item_id . '}', '{"data": ' . json_encode(['item_id' => $item_id, 'manufacturier_id' => $manufacturier_id]) . '}', LoggerSection::item, LoggerType::update);
        }
    } catch (Exception $e) {
        return $e;
    }
}

function create_manufacturier($data) {
    global $db;

    $name = $db->safe($data['name']);
    $pays = $db->safe($data['pays']);

    $query = "  INSERT INTO manufacturier
                    (name, pays)
                VALUES
                    ('$name', '$pays');";

    if ($db->query($query)) {
        $newId = $db->getLastInsertId();

        log_data(get_current_employee()['id'], '/' . LoggerSection::manufacturier . '/' . LoggerType::add . '/{' . $newId . '}', '{"data": ' . json_encode($data) . '}', LoggerSection::manufacturier, LoggerType::add);

        $data['id'] = $newId;
        add_manufacturier_discount($data);

        return $newId;
    }

    return false;
}

function search_manufacturiers($keyword) {
    global $db;

    $keyword = $db->safe($keyword);

    $query = "  SELECT m.*
                FROM manufacturier AS m
                WHERE m.name LIKE '%" . $keyword . "%'
    ";

    return $db->queryArray($query);
}

function add_manufacturier_discount($data) {
  global $db;

  $id = $db->safe($data['id']);
  if (!is_array($discounts)) { $discounts = []; }
  if (count($values = preg_grep('/^discount_code_[0-9]*/', array_keys($data))) > 0) {
      foreach ($values as $key) {
          $index = explode('_', $key)[2];

          if (isset($data['discount_code_' . $index])) {
              $discounts[] = [
                  "fk_manufacturier_id" => $id,
                  "code" => $db->safe($data['discount_code_' . $index]),
                  "codeimport" => $db->safe($data['discount_codeimport_' . $index]),
                  "title" => $db->safe($data['discount_title_' . $index]),
                  "values" => json_encode([
                    $db->safe($data['discount_values_' . $index . '_0']),
                    $db->safe($data['discount_values_' . $index . '_1']),
                    $db->safe($data['discount_values_' . $index . '_2']),
                    $db->safe($data['discount_values_' . $index . '_3']),
                    $db->safe($data['discount_values_' . $index . '_4']),
                  ]),
              ];
          }
      }
  }

  foreach ($discounts as $key => $discount) {
      $query = "  INSERT INTO discount
                  (fk_manufacturier_id, code, codeimport, title, `values`)
                      VALUES
                  (
                      '{$discount['fk_manufacturier_id']}',
                      '{$discount['code']}',
                      '{$discount['codeimport']}',
                      '{$discount['title']}',
                      '{$discount['values']}'
                  )";

      $discounts[$key]['done'] = $db->query($query);
  }

  return true;
}

function remove_manufacturier_discount($manufacturier_id) {
  global $db;

  $manufacturier_id = $db->safe($manufacturier_id);

  $query = "  DELETE FROM discount WHERE fk_manufacturier_id = $manufacturier_id;";

  if ($db->query($query)) {
      return true;
  }
}
