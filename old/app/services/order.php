<?php

function get_order($id) {
  global $db;

  $sql = "SELECT `orders`.*, CONCAT(`users`.`first_name`, ' ', `users`.`last_name`) AS `created_by`
          FROM `orders`
          LEFT JOIN `users` ON `users`.`id` = `orders`.`created_fk_employee_id`
          WHERE `orders`.`id` = $id";

  $results = $db->queryFirst($sql);

  $query = "  SELECT `order_addresses`.*
              FROM `order_addresses`
              WHERE `fk_order_id` = $id AND `type` = 'bill_to' ";

  $bill_to = $db->queryFirst($query);
  $results['bill_to'] = $bill_to;

  $query = "  SELECT `order_addresses`.*
              FROM `order_addresses`
              WHERE `fk_order_id` = $id AND `type` = 'ship_to' ";

  $ship_to = $db->queryFirst($query);
  $results['ship_to'] = $ship_to;

  $query = "  SELECT *, `order_statuses`.`desc_fr` AS `status_desc_fr`, `order_statuses`.`desc_en` AS `status_desc_en`, `shipping_suppliers`.`name` AS `supplier`, `users`.`first_name` AS `emp_first_name`, `users`.`last_name` AS `emp_last_name`
              FROM `order_histories`
              LEFT JOIN `order_statuses` ON `order_statuses`.`id` = `order_histories`.`fk_status_id`
              LEFT JOIN `shipping_suppliers` ON `shipping_suppliers`.`id` = `order_histories`.`fk_shipping_by_id`
              LEFT JOIN `users` ON `users`.`id` = `order_histories`.`fk_created_by_id`
              WHERE `fk_order_id` = $id
              ORDER BY `order_histories`.`created_at` DESC;";
  $results['histories'] = $db->queryArray($query);
  foreach( $results['histories'] as $k => $v ) {
    $results['histories'][$k]["estimated_delivery_date"] = date("Y-m-d", strtotime($v["estimated_delivery_date"]));
  }

  $query = "  SELECT * FROM `order_item`
              WHERE `order_item`.`fk_order_id` = $id;";
  $results['items'] = $db->queryArray($query);

  $query = "  SELECT * FROM `order_taxes`
              LEFT JOIN `taxes` ON `taxes`.`id` = `order_taxes`.`fk_tax_id`
              WHERE `order_taxes`.`fk_order_id` = $id;";
  $results['taxes'] = $db->queryArray($query);

  $query = "SELECT * FROM quote_payment WHERE quote_id='".$results["fk_order_id"]."'";
  $results['payments'] = $db->queryArray($query);

  $sql = "SELECT * FROM quotes WHERE id='".$results["fk_order_id"]."'";
  $results['quote'] = $db->queryFirst($sql);

  return $results;

}

function get_all_orders_datatables() {
  global $db;

  $query = "  SELECT orders.*, client.entreprise as client, order_statuses.desc_fr, order_statuses.desc_en
              FROM orders
              LEFT JOIN client ON client.id = orders.fk_client_id
              LEFT JOIN order_statuses ON order_statuses.id = orders.fk_status_id
  ";

  $results = $db->queryArray($query);

  foreach($results as $idx => $result) {
    $results[$idx]["options"] = '<a href="#">x</a>';
  }

  return $results;
}

function get_all_orders_magento2($email) {
  global $db;

  $query = "  SELECT orders.*, client.entreprise as client, order_statuses.desc_fr, order_statuses.desc_en, contact.email
              FROM orders
              LEFT JOIN client ON client.id = orders.fk_client_id
              LEFT JOIN order_statuses ON order_statuses.id = orders.fk_status_id
              LEFT JOIN contact ON contact.fk_client_id = orders.fk_client_id
              WHERE contact.email LIKE '%$email%'
  ";

  $results = $db->queryArray($query);

  foreach($results as $idx => $result) {
    $results[$idx]["options"] = '<a href="#">x</a>';
  }

  return $results;
}

function get_all_order_statuses() {
  global $db;

  $query = "  SELECT * FROM `order_status`;";

  $results = $db->queryArray($query);

  return $results;
}

function get_all_carriers() {
  global $db;

  $query = "  SELECT * FROM `shipping_suppliers`;";

  $results = $db->queryArray($query);

  return $results;
}

function add_history($data, $files) {
  global $db;

  $filepath = '';
  $filename = '';
  $filesize = '';
  $filetype = '';
  if (isset($files["comment_file"])) {
    if (file_exists($files["comment_file"]["tmp_name"])) {
      $new_path = dirname(__FILE__)."/../../web/assets/upload/orders/".$files["comment_file"]["name"];
      $filepath = "/assets/upload/orders/".$files["comment_file"]["name"];
      $filename = $files["comment_file"]["name"];
      $filesize = $files["comment_file"]["size"];
      $filetype = $files["comment_file"]["type"];
      rename($files["comment_file"]["tmp_name"], $new_path);
    }
  }
  //
  // if (!empty($data["tracking_no"])) {
  //   $data["order_status_comment"] = 3;
  // }

  // if (!empty($data["delivery_date"])) {
  //   $data["order_status_comment"] = 2;
  // }

  if (empty($data["order_status_comment"])) {
    $data["order_status_comment"] = 0;
  }

  if ($data["notify"] == "on") {
    $data["notify"] = 1;
  } else {
    $data["notify"] = 0;
  }

  if ($data["front"] == "on") {
    $data["front"] = 1;
  } else {
    $data["front"] = 0;
  }

  if ($data["action"] == "eta_form_save") {

  }

  $data["comment_fr"] = "";
  $data["comment_en"] = "";

  $order = get_order($data["order_id"]);

  if ($data["action"] == "eta_form_save_and_notify_without_meeting") {
    $sql = "UPDATE orders SET
      estimated_delivery_date='".$db->safe($data["delivery_date"])."'
      WHERE id='".$db->safe($data["order_id"])."'
      ";

    $db->query($sql);
    // ------------ FR

    $data["comment_fr"] = $data["comment_fr"] . "\n
    La présente est pour vous aviser que votre livraison est prévue pour ou vers le ";

    $month = array("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");
    $month = $month[(date("n", strtotime($data["delivery_date"])) - 1 )];
    $date_str = date("d ", strtotime($data["delivery_date"]));
    $date_str .= $month;
    $date_str .= date(" Y", strtotime($data["delivery_date"]));

    $data["comment_fr"] = $data["comment_fr"] . $date_str;

    // ------------ EN

    $data["comment_en"] = $data["comment_en"] . "\n
    This is to confirm your order (#{$order["order_number"]}) will be delivered on or towards ";

    $date_str = date("F jS, Y", strtotime($data["delivery_date"]));

    $data["comment_en"] = $data["comment_en"] . $date_str;

    $data["front"] = 1;

    $sql = "INSERT INTO order_histories(
      created_at,
      updated_at,
      fk_status_id,
      fk_order_id,
      fk_created_by_id,
      fk_shipping_by_id,
      acknow_no,
      tracking_no,
      comment,
      comment_en,
      comment_fr,
      is_customer_notified,
      is_visible_on_front,
      is_exported_to_quickbooks,
      filepath,
      filename,
      filesize,
      estimated_delivery_date
    ) VALUES(
      '".date("Y-m-d H:i:s")."',
      '".date("Y-m-d H:i:s")."',
      '".$db->safe($data["order_status_comment"])."',
      '".$db->safe($data["order_id"])."',
      '".get_current_employee()['id']."',
      '0',
      '',
      '',
      '".$db->safe($data["comment"])."',
      '".$db->safe($data["comment_en"])."',
      '".$db->safe($data["comment_fr"])."',
      '".$db->safe($data["notify"])."',
      '".$db->safe($data["front"])."',
      '0',
      '".$filepath."',
      '".$filename."',
      '".$filesize."',
      '".$db->safe($data["delivery_date"])."'
    )";


  $db->query($sql);
  }

  if ($data["action"] == "eta_form_save_and_notify_with_meeting") {
    $sql = "UPDATE orders SET
      estimated_delivery_date='".$db->safe($data["delivery_date"])."'
      WHERE id='".$db->safe($data["order_id"])."'
      ";

    $db->query($sql);
        // ------------ FR

        $data["comment_fr"] = $data["comment_fr"] . "\n
        La présente est pour vous aviser qu’un membre de notre équipe de livraison vous contactera le ou vers le ";

        $month = array("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");
        $month = $month[(date("n", strtotime($data["delivery_date"])) - 1 )];
        $date_str = date("d ", strtotime($data["delivery_date"]));
        $date_str .= $month;
        $date_str .= date(" Y", strtotime($data["delivery_date"]));

        $data["comment_fr"] = $data["comment_fr"] . $date_str;
        $data["comment_fr"] .= " pour prendre rendez-vous et planifier la livraison de votre commande (#{$order["order_number"]}).";

        // ------------ EN

        $data["comment_en"] = $data["comment_en"] . "\n
        This message is to confirm that a member of our delivery team will contact you on or towards ";

        $date_str = date("F jS, Y", strtotime($data["delivery_date"]));

        $data["comment_en"] = $data["comment_en"] . $date_str;

        $data["comment_en"] .= " to take an appointment and plan your order delivery (#{$order["order_number"]}).";

        $data["front"] = 1;

        $sql = "INSERT INTO order_histories(
          created_at,
          updated_at,
          fk_status_id,
          fk_order_id,
          fk_created_by_id,
          fk_shipping_by_id,
          acknow_no,
          tracking_no,
          comment,
          comment_en,
          comment_fr,
          is_customer_notified,
          is_visible_on_front,
          is_exported_to_quickbooks,
          filepath,
          filename,
          filesize,
          estimated_delivery_date
        ) VALUES(
          '".date("Y-m-d H:i:s")."',
          '".date("Y-m-d H:i:s")."',
          '".$db->safe($data["order_status_comment"])."',
          '".$db->safe($data["order_id"])."',
          '".get_current_employee()['id']."',
          '0',
          '',
          '',
          '".$db->safe($data["comment"])."',
          '".$db->safe($data["comment_en"])."',
          '".$db->safe($data["comment_fr"])."',
          '".$db->safe($data["notify"])."',
          '".$db->safe($data["front"])."',
          '0',
          '".$filepath."',
          '".$filename."',
          '".$filesize."',
          '".$db->safe($data["delivery_date"])."'
        )";


      $db->query($sql);
  }

  $sql = "UPDATE orders SET fk_status_id='".$data["order_status_comment"]."' WHERE id='".$db->safe($data["order_id"])."'";
  $db->query($sql);

  if ($data["action"] == "save_bill_to_address") {
    $sql = "UPDATE order_addresses SET
      formatted='".$db->safe($data["order_bill_to"])."'
      WHERE type='bill_to' AND fk_order_id='".$db->safe($data["order_id"])."'
      ";

    $db->query($sql);
  }

  if ($data["action"] == "save_ship_to_address") {
    $sql = "UPDATE order_addresses SET
      formatted='".$db->safe($data["order_ship_to"])."'
      WHERE type='ship_to' AND fk_order_id='".$db->safe($data["order_id"])."'
      ";

    $db->query($sql);
  }

  if ($data["action"] == "shipping_form_save") {
    $sql = "UPDATE orders SET
      carrier_assigned='".$db->safe($data["carrier_assigned"])."',
      acknow_no='".$db->safe($data["acknow_no"])."',
      tracking_no='".$db->safe($data["tracking_no"])."',
      installation='".$db->safe($data["installation"])."'
      WHERE id='".$db->safe($data["order_id"])."'
      ";

    $db->query($sql);

  }

  if ($data["action"] == "eta_form_save") {
    $sql = "UPDATE orders SET
      estimated_delivery_date='".$db->safe($data["delivery_date"])."'
      WHERE id='".$db->safe($data["order_id"])."'
      ";

    $db->query($sql);
  }

  if ($data["action"] == "new_comment_form_save") {

      $sql = "UPDATE orders SET fk_status_id='".$data["order_status_comment"]."' WHERE id='".$db->safe($data["order_id"])."'";
      $db->query($sql);

      /*
        status change
      */
      if ($order["fk_status_id"] != $data["order_status_comment"]) {
        // REST API call to Magento2 Website to update it's order status
        $magento2_order_id = $order["magento2_order_id"];

        $status = "processing";

        if ($data["order_status_comment"] == 1) { // En traitement
          $status = "processing";
        }
        if ($data["order_status_comment"] == 2) { // En production
          $status = "processing";
        }
        if ($data["order_status_comment"] == 3) { // En transit
          $status = "processing";
        }
        if ($data["order_status_comment"] == 4) { // Complété
          $status = "complete";
        }
        if ($data["order_status_comment"] == 5) { // Annulé
          $status = "canceled";
        }
        if ($data["order_status_comment"] == 6) { // En attente
          $status = "holded";
        }

        $action = "change_order_status";
        $api_key = $_ENV['MAGE2_API_KEY'];
        $url = $_ENV['MAGE2_REST_API'];
        $url .= "?api_key=$api_key&action=$action&order_id=$magento2_order_id&status=$status";

        if (!empty($magento2_order_id)) {
          @file_get_content($url);
        }
      }

      $sql = "INSERT INTO order_histories(
        created_at,
        updated_at,
        fk_status_id,
        fk_order_id,
        fk_created_by_id,
        fk_shipping_by_id,
        acknow_no,
        tracking_no,
        comment,
        comment_en,
        comment_fr,
        is_customer_notified,
        is_visible_on_front,
        is_exported_to_quickbooks,
        filepath,
        filename,
        filesize,
        estimated_delivery_date
      ) VALUES(
        '".date("Y-m-d H:i:s")."',
        '".date("Y-m-d H:i:s")."',
        '".$db->safe($data["order_status_comment"])."',
        '".$db->safe($data["order_id"])."',
        '".get_current_employee()['id']."',
        '0',
        '',
        '',
        '".$db->safe($data["comment"])."',
        '".$db->safe($data["comment_en"])."',
        '".$db->safe($data["comment_fr"])."',
        '".$db->safe($data["notify"])."',
        '".$db->safe($data["front"])."',
        '0',
        '".$filepath."',
        '".$filename."',
        '".$filesize."',
        '".$db->safe($data["delivery_date"])."'
      )";


    $db->query($sql);
  }
  $order = get_order($data["order_id"]);
  $order["filepath"] = $filepath;
  $order["filename"] = $filename;
  $order["filetype"] = $filetype;
  return $order;
}

function get_order_address($order_id, $type = 'ship_to') {
  global $db;
  $id = $db->safe($order_id);
  $type = $db->safe($type);
  $query = "  SELECT * FROM `order_addresses` WHERE `fk_order_id` = $id AND `type` = '$type' LIMIT 1;";
  if (!$address = $db->queryFirst($query)) {
    return false;
  }

  return $address;
}
