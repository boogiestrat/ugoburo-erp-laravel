<?php

function get_all_contact() {
    global $db;

    $query = "  SELECT b.*
                FROM contact AS b
                ORDER BY b.last_name ASC
    ";

    return $db->queryArray($query);
}
function search_unassigned_contact($keyword) {
    global $db;

    $keyword = $db->safe($keyword);

    $employeeLevel = get_current_employee()['fk_user_level_id'];
    $employeeId = get_current_employee()['id'];
    $showMine = true;
    if ($employeeLevel > 1) {
      $showMine = false;
    }

    if ($showMine) {
      $query = "  SELECT b.*
                  FROM contact AS b LEFT JOIN employee_contacts ON b.id = employee_contacts.fk_contact_id WHERE (employee_contacts.fk_employee_id = $employeeId OR b.fk_created_by_id = $employeeId)
                  AND (b.last_name LIKE '%" . $keyword . "%'
                  OR b.first_name LIKE '%" . $keyword . "%')
      ";
    } else {
      $query = "  SELECT b.*
                  FROM contact AS b
                  WHERE ( b.last_name LIKE '%" . $keyword . "%'
                  OR b.first_name LIKE '%" . $keyword . "%' )
      ";
    }
// print_r($query);
    return $db->queryArray($query);
}
function search_contact($keyword) {
    global $db;

    $keyword = $db->safe($keyword);

    $employeeLevel = get_current_employee()['fk_user_level_id'];
    $employeeId = get_current_employee()['id'];
    $showMine = true;
    if ($employeeLevel > 1) {
      $showMine = false;
    }

    if ($showMine) {
      $query = "  SELECT b.*
                  FROM contact AS b LEFT JOIN employee_contacts ON b.id = employee_contacts.fk_contact_id WHERE (employee_contacts.fk_employee_id = $employeeId OR b.fk_created_by_id = $employeeId)
                  AND (b.last_name LIKE '%" . $keyword . "%'
                  OR b.first_name LIKE '%" . $keyword . "%')
      ";
    } else {
      $query = "  SELECT b.*
                  FROM contact AS b
                  WHERE b.last_name LIKE '%" . $keyword . "%'
                  OR b.first_name LIKE '%" . $keyword . "%'
      ";
    }
// print_r($query);
    return $db->queryArray($query);
}

function get_contact($contact_id) {
    global $db;

    $contact_id = $db->safe($contact_id);

    $query = "  SELECT b.*
                FROM contact AS b
                WHERE b.id = '" . $contact_id . "';
    ";

    $results = $db->queryFirst($query);
    $results['clients'] = get_contact_clients_relations($contact_id);
    $results['employees'] = get_contact_employees_relations($contact_id);

    return $results;
}

function get_contact_by_client($client_id) {
    global $db;

    $client_id = $db->safe($client_id);

    $query = "  SELECT *
                FROM contact
                WHERE fk_client_id = $client_id ";

    return $db->queryArray($query);
}

function get_contact_clients_relations($contact_id) {
    global $db;

    $contact_id = $db->safe($contact_id);

    $query = "  SELECT client.*
                FROM contact
                LEFT JOIN client ON client.id = contact.fk_client_id
                WHERE contact.id = $contact_id;
    ";

    return $db->queryArray($query);
}

function get_contact_employees_relations($contact_id) {
    global $db;

    $contact_id = $db->safe($contact_id);

    $query = "  SELECT b.*, `employee`.first_name, `employee`.last_name
                FROM `employee_contacts` AS b
                INNER JOIN `employee` ON `employee`.id = b.fk_employee_id
                WHERE b.fk_contact_id = $contact_id;
    ";

    return $db->queryArray($query);
}

function get_new_contact() {
    global $db;

    $query = "  SELECT b.*
                FROM contact AS b
                WHERE b.created_at > DATE_ADD(NOW(), INTERVAL -". NEW_DATA_RANGE_DAYS ." DAY)
    ";

    return $db->queryArray($query);
}

function get_new_contact_count() {
    global $db;

    $query = "  SELECT COUNT(b.id) AS new_contact_count
                FROM contact AS b
                WHERE b.created_at > DATE_ADD(NOW(), INTERVAL -". NEW_DATA_RANGE_DAYS ." DAY)
    ";

    return $db->queryFirst($query);
}

function create_contact ($data) {  // TODO Need global security validation
    global $db;

    $firstname = $db->safe($data['first_name']);
    $lastname = $db->safe($data['last_name']);
    $email = $db->safe($data['email']);
    $clientId = !empty($data['fk_client_id']) ? $db->safe($data['fk_client_id']) : false;

    $tel ="";
    if(@$data['tel']) $tel = format_phone_number($db->safe($data['tel']));
    if(@$data['phone']) $tel = format_phone_number($db->safe($data['phone']));

    $mobile = format_phone_number($db->safe(@$data['mobile']));
    $comments = $db->safe(@$data['comments']);
    $createdById = $db->safe(get_current_employee()['id']);
    $employees = $db->safe(@$data['employees']);
    $results = $db->queryArray("SELECT * FROM contact WHERE email='$email'");
    if (count($results)) {
      return "Ce contact est déjà présent dans le système, mais vous n'y avez pas accès. Veuillez contacter un administrateur pour vous l'assigner.";
    }
    $query = "  INSERT INTO `contact` (
                    `first_name`,
                    `last_name`,
                    `email`,
                    `phone`,
                    `cell`,
                    `fk_created_by_id`,
                    `fk_client_id`,
                    `comments`
                ) VALUE (
                    '$firstname',
                    '$lastname',
                    '$email',
                    '$tel',
                    '$mobile',
                    $createdById,
                    0,
                    '$comments'
                )
            ";

    if ($db->query($query)) {
        $newId = $db->getLastInsertId();
        if ($clientId) {
          create_client_contact_relation($clientId, $newId);
        }
        // log_data(get_current_employee()['id'], '/' . LoggerSection::contact . '/' . LoggerType::add . '/{' . $newId . '}', '{"data": ' . json_encode($data) . '}', LoggerSection::contact, LoggerType::add);

        return $newId;
    }
    else
        return false;
}

function update_contact ($data) {
    global $db;

    $id = $db->safe($data['id']);
    $firstname = $db->safe($data['first_name']);
    $lastname = $db->safe($data['last_name']);
    $email = $db->safe($data['email']);
    $is_default_contact = $db->safe($data['is_default_contact']);
    $clientId = $db->safe($data['fk_client_id']);
    if (empty($clientId)) {
      $clientId=0;
    }
    $tel = format_phone_number($db->safe($data['tel']));
    $mobile = format_phone_number($db->safe($data['mobile']));
    $comments = $db->safe($data['comments']);
    $employees = isset($data['employees']) ? $db->safe($data['employees']) : false;

    remove_contact_permission_by_contact($id);

    if ($employees) {
      foreach ($employees as $employee) {
          add_contact_permission($employee, $id);
      }
    }
    $results = $db->queryArray("SELECT * FROM contact WHERE email='$email' AND id != '$id'");
    if (count($results)) {
      return "Ce contact est déjà présent dans le système, mais vous n'y avez pas accès. Veuillez contacter un administrateur pour vous l'assigner.";
    }
    $query = "  UPDATE contact
                SET
                is_default_contact = '0'
                WHERE fk_client_id = '$clientId'
            ";
    $results = $db->query($query);
    $query = "  UPDATE contact
                SET first_name = '$firstname',
                last_name = '$lastname',
                email = '$email',
                phone = '$tel',
                fk_client_id = '$clientId',
                cell = '$mobile',
                is_default_contact = '$is_default_contact',
                comments = '$comments'
                WHERE id = $id
            ";

    try {

        $results = $db->query($query);
        log_data(get_current_employee()['id'], '/' . LoggerSection::contact . '/' . LoggerType::update . '/{' . $id . '}', '{"data": ' . json_encode($data) . '}', LoggerSection::contact, LoggerType::update);
        return $results;
    } catch (Exception $e) {
        return $e;
    }

}

function create_client_contact_relation($clientId, $contactId) {
    global $db;

    $clientId = $db->safe($clientId);
    $contactId = $db->safe($contactId);

    $query = "  UPDATE contact SET fk_client_id='$clientId' WHERE id='$contactId'";

    return $db->query($query);
}

function delete_client_contact_relation_by_client($clientId) {
    global $db;

    $clientId = $db->safe($clientId);

    $query = "  UPDATE `contact` SET fk_client_id = '0'
                WHERE `fk_client_id` = $clientId;";

    return $db->query($query);
}

function delete_client_contact_relation($contactId) {
    global $db;

    $contactId = $db->safe($contactId);

    $query = "  UPDATE `contact` fk_client_id = '0'
                WHERE id = $contactId;";

    return $db->query($query);
}

function delete_contact ($contact_id) {  // TODO Need global security validation
    global $db;

    $contact_id = $db->safe($contact_id);

    $query = "  DELETE FROM `contact`
                WHERE contact.id = '". $contact_id ."'
            ";

    if ($result = $db->query($query)) {
        delete_client_contact_relation($contact_id);
        log_data(get_current_employee()['id'], '/' . LoggerSection::contact . '/' . LoggerType::delete . '/{' . $contact_id . '}', '{"data": ' . json_encode($result) . '}', LoggerSection::contact, LoggerType::delete);
        return $result;
    } else {
        return false;
    }
}

function get_contact_addresses($contact_id) {
  global $db;

  $contact_id = $db->safe($contact_id);

  $query = "  SELECT DISTINCT a.*, c.entreprise, co.first_name, co.last_name, co.title, co.id AS contact_id
              FROM `address` AS a
              LEFT JOIN client AS c ON c.id = a.fk_client_id
              LEFT JOIN contact AS co ON co.id = cc.fk_contact_id AND cc.fk_client_id = c.id
              WHERE co.id=$contact_id;
          ";

  if ($res = $db->queryArray($query)) {
    return $res;
  }

  return false;
}

function get_contact_by_email_and_address($email,$address){
    global $db;

    $email = $db->safe(trim($email));
    $address = $db->safe(trim($address));

    $query = "  SELECT contact.*
                FROM contact
                WHERE email = '" . $email . "' and address = '" . $address . "';
    ";
   $result =  $db->queryFirst($query);
   return $result;
}

function get_contact_by_email($email){
    global $db;

    $email = $db->safe(trim($email));
    $address = $db->safe(trim($address));

    $query = "  SELECT contact.*
                FROM contact
                WHERE email = '" . $email . "';
    ";
   $result =  $db->queryFirst($query);
   return $result;
}
