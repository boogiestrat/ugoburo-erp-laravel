<?php

function get_html_template_part($data=[],$template,$isPDF=false){
    ob_start();
    extract($data);
    $path = APP_ROOT.'includes/html/'.$template;
    include $path;
    return ob_get_clean();
}
