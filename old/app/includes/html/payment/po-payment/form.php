<?php
/** @var $gateway \Braintree\Gateway */
global $i18n;

?>


<button id="pay-po" class="button btn btn-secondary btn-lg btn-block btn-pay-po" type="button"><span><?=__('po-payment_pay')?></span></button>

<div class="modal fade" id="payment-modal-po" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="form-horizontal modal-content">
            <div class="modal-header p-4">
                <h5 class="modal-title"><?=__('po-payment_pay')?></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="<?=__('payment_close')?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <form method="post" id="payment-form" action="<?=$url ?>">

                    <div class="form-group">
                        <label for="po_number"><?=__('po-payment_po-number')?></label>
                        <input type="text" class="form-control" id="po_number" name="po_number" aria-describedby="emailHelp" required="required" placeholder="<?=__('po-payment_po-number-placeholder')?>">
                    </div>


                    <input id="amount" name="amount" type="hidden"  value="<?=$amount?>">
                    <input id="quote_id" name="quote_id" type="hidden"  value="<?=$quoteId?>">
                    <input id="firstname" name="firstname" type="hidden"  value="<?=$firstName?>">
                    <input id="lastname" name="lastname" type="hidden"  value="<?=$lastName?>">
                    <input id="is_deposit" name="is_deposit" type="hidden"  value="<?=$isDeposit?>">
                    <input id="language" name="language" type="hidden"  value="<?=$language?>">

                    <?php foreach((array)@$emails as $email): ?>
                        <input id="recipients" name="recipients[]" type="hidden"  value="<?=$email?>">
                    <?php endforeach; ?>

                    <section>
                        <div class="bt-drop-in-wrapper">
                            <div id="bt-dropin"></div>
                        </div>
                    </section>

                    <button class="button btn btn-primary submit-btn" type="submit"><span><?=__('po-payment_pay')?></span></button>
                </form>

            </div>
        </div>
    </div>
</div>

<script>
    $(function(){

        $('.btn-pay-po').click(function(){
            $('#payment-modal-po').modal();
        });
    });

</script>