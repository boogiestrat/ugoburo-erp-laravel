<?php

/**
 * @var $store_url
 * @var $logo_url
 * @var $store_name
 * @var $customer_name
 * @var $contact_email
 * @var $quote_id
 * @var $billing
 * @var $shipping
 * @var $is_deposit
 * @var $contact_phone
 * @var $contact_phone
 * @var $quote_number
 * @var $quote_date
 * @var $quote_payment_method
 * @var $quote_items
 * @var $quote_totals
 * */

$typeName = $is_deposit?__('payment-confirmation-email_type-quotation'):__('payment-confirmation-email_type-order');


?>

<?php include(APP_ROOT . 'includes/html/email-header_en.php'); ?>

                        <p style="margin-top: 0; margin-bottom: 10px;">
                            <strong><?=__('payment-confirmation-email_hello',$customer_name);?></strong>,
                            <br/><br/>
                            <?=__('payment-confirmation-email_thanks-for',$store_name)?>.
                        </p>

                        <?php if($is_deposit): ?>
                            <p style="margin-top: 0; margin-bottom: 10px;">Message depot</p>
                        <?php else: ?>
                            <p style="margin-top: 0; margin-bottom: 10px;"><?=__('payment-confirmation-email_execution')?></p>
                        <?php endif; ?>

                        <p style="margin-top: 0; margin-bottom: 10px;"><?=__('quote-email_contact',$contact_email,$contact_email,$contact_phone); ?></p>

                        <p style="margin-top: 0; margin-bottom: 10px;"><?=__('payment-confirmation-email_confirmation',$typeName)?>.</p>

                        <h3 style="border-bottom:2px solid #eee; padding-bottom:1px; "><?=$typeName?> n°<?=$quote_number?> <small>(<?=__('payment-confirmation-email_dated',$quote_date)?>)</small></h3>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <thead>
                            <tr>
                                <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;"><?=__('payment-confirmation-email_billing-info')?> :</th>
                                <th width="3%"></th>
                                <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;"><?=__('payment-confirmation-email_payment-method')?> :</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><?php
                                echo $billing;
                                ?></td>
                                <td>&nbsp;</td>
                                <td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><?php
                                echo $quote_payment_method;
                                ?></td>
                            </tr>
                            </tbody>
                        </table>

                        <br/>

                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <thead>
                            <tr>
                                <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;"><?=__('payment-confirmation-email_shipping-info')?> :</th>
                                <!--<th width="3%"></th>
                                <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">Mode de livraison :</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><?=$shipping?></td>
                                <td>&nbsp;</td>
                                <!--<td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">
                                    {{var order.getShippingDescription()}}
                                    &nbsp;
                                </td>-->
                            </tr>
                            </tbody>
                        </table>
                        <br/>

                        <?=$quote_items?>
                        <br/>
                        <?=$quote_totals?>
                        <br>
                        <!--{{var order.getEmailCustomerNote()}}-->
                        <p><?=__('quote-email_thanks')?>,</p>
                        <!--<p>TPS 839049913</p>-->

<?php include(APP_ROOT . 'includes/html/email-footer_en.php'); ?>
