<?php
/** @var $gateway \Braintree\Gateway */
global $i18n;
?>

<script src="https://js.braintreegateway.com/web/dropin/1.20.1/js/dropin.min.js"></script>


<div class="ibox">
    <div class="ibox-body">
        <h5 class="font-strong mb-4"><?=__('payment_payment')?></h5>
        <button id="pay" class="button btn btn-primary btn-lg btn-block btn-pay" type="button"><span><?=__('payment_pay-now')?></span></button>
    </div>
</div>

<div class="modal fade" id="payment-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="form-horizontal modal-content">
            <div class="modal-header p-4">
                <h5 class="modal-title"><?=__('payment_form')?></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="<?=__('payment_close')?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <form method="post" id="payment-form" action="<?=$url ?>">

                    <input id="amount" name="amount" type="hidden"  value="<?=$amount?>">
                    <input id="quote_id" name="quote_id" type="hidden"  value="<?=$quoteId?>">
                    <input id="firstname" name="firstname" type="hidden"  value="<?=$firstName?>">
                    <input id="lastname" name="lastname" type="hidden"  value="<?=$lastName?>">
                    <input id="is_deposit" name="is_deposit" type="hidden"  value="<?=$isDeposit?>">
                    <input id="language" name="language" type="hidden"  value="<?=$language?>">

                    <?php foreach((array)@$emails as $email): ?>
                        <input id="recipients" name="recipients[]" type="hidden"  value="<?=$email?>">
                    <?php endforeach; ?>

                    <section>
                        <div class="bt-drop-in-wrapper">
                            <div id="bt-dropin"></div>
                        </div>
                    </section>

                    <input id="nonce" name="payment_method_nonce" type="hidden" />
                    <button class="button btn btn-primary submit-btn" type="submit"><span>Payer</span></button>
                </form>

            </div>
        </div>
    </div>
</div>

<script>
    var form = document.querySelector('#payment-form');
    var client_token = "<?php echo($gateway->ClientToken()->generate()); ?>";
    braintree.dropin.create({
        authorization: client_token,
        selector: '#bt-dropin',
        locale: '<?= $i18n->getAppliedLang() ?>_CA',
        paypal: {
            flow: 'vault'
        }
    }, function (createErr, instance) {
        if (createErr) {
            console.log('Create Error', createErr);
            return;
        }
        form.addEventListener('submit', function (event) {
            event.preventDefault();
            instance.requestPaymentMethod(function (err, payload) {
                if (err) {
                    console.log('Request Payment Method Error', err);
                    return;
                }
                // Add the nonce to the form and submit
                document.querySelector('#nonce').value = payload.nonce;
                form.submit();
            });
        });
    });
</script>


