<?php
/** @var $gateway \Braintree\Gateway */
global $i18n;

$formattedAmount = currency_format($amount);

?>

<button id="pay-payflow" class="button btn btn-primary btn-lg btn-block btn-pay-payflow" type="button"><span><?=__('payment_pay-now')?></span></button>

<div class="modal fade" id="payment-modal-payflow" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="form-horizontal modal-content">
            <div class="modal-header p-4">
                <h5 class="modal-title"><?=__('payment_pay-now')?></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="<?=__('payment_close')?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <form method="post" id="payment-form" action="<?=$url ?>">

                    <div class="form-group text-center">
                        <ul class="list-inline">
                            <li class="list-inline-item"><i class="text-muted fa fa-cc-visa fa-2x"></i></li>
                            <li class="list-inline-item"><i class="fa fa-cc-mastercard fa-2x"></i></li>
                            <li class="list-inline-item"><i class="fa fa-cc-amex fa-2x"></i></li>
                            <li class="list-inline-item"><i class="fa fa-cc-discover fa-2x"></i></li>
                        </ul>
                    </div>
                    <div class="form-group">
                        <label for="cc-payment" class="control-label mb-1"><?=__('payment_payment-amount')?></label>
                        <input id="cc-payment" name="cc-payment" type="text" class="form-control" aria-required="true" aria-invalid="false" required value="<?=money_format('%.2n$', $amount)?>" disabled="disabled">
                        <span class="invalid-feedback"><?=__('payment_payment-amount-notice')?></span>
                    </div>
                    <div class="form-group has-success">
                        <label for="cc-name" class="control-label mb-1"><?=__('payment_card-name')?></label>
                        <input id="cc-name" name="cc-name" type="text" class="form-control cc-name" required autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
                        <span class="invalid-feedback"><?=__('payment_card-name-notice')?></span>
                    </div>
                    <div class="form-group">
                        <label for="cc-number" class="control-label mb-1"><?=__('payment_card-number')?></label>
                        <input id="cc-number" name="cc-number" type="tel" class="form-control cc-number identified visa" required="" pattern="[0-9]{16}">
                        <span class="invalid-feedback"><?=__('payment_card-number-notice')?></span>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1"><?=__('payment_expiration')?></label>
                                <input id="cc-exp" name="cc-exp" type="tel" class="form-control cc-exp" required placeholder="MM / YY" autocomplete="cc-exp">
                                <span class="invalid-feedback"><?=__('payment_expiration-notice')?></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <label for="x_card_code" class="control-label mb-1"><?=__('payment_security-code')?></label>
                            <div class="input-group">
                                <input id="x_card_code" name="x_card_code" type="tel" class="form-control cc-cvc" required autocomplete="off">
                                <span class="invalid-feedback order-last"><?=__('payment_security-code-notice')?></span>
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fa fa-question-circle fa-lg" style="z-index: 8;" data-toggle="popover" data-container="body" data-html="true" data-title="<?=__('payment_security-code')?>"
                                              data-content="<div class='text-center one-card'><?=__('payment_security-code-help')?><div class='visa-mc-cvc-preview'></div></div>"
                                              data-trigger="hover"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input id="amount" name="amount" type="hidden"  value="<?=$amount?>">
                    <input id="quote_id" name="quote_id" type="hidden"  value="<?=$quoteId?>">
                    <input id="firstname" name="firstname" type="hidden"  value="<?=$firstName?>">
                    <input id="lastname" name="lastname" type="hidden"  value="<?=$lastName?>">
                    <input id="is_deposit" name="is_deposit" type="hidden"  value="<?=$isDeposit?>">
                    <input id="language" name="language" type="hidden"  value="<?=$language?>">

                    <?php foreach((array)@$emails as $email): ?>
                        <input id="recipients" name="recipients[]" type="hidden"  value="<?=$email?>">
                    <?php endforeach; ?>

                    <section>
                        <div class="bt-drop-in-wrapper">
                            <div id="bt-dropin"></div>
                        </div>
                    </section>

                    <div>
                        <button id="payment-button" type="submit" class="btn btn-lg btn-primary btn-block">
                            <i class="fa fa-lock fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Payer <?=$formattedAmount?></span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                    </div>


                    <?php /*<button class="button btn btn-primary submit-btn" type="submit"><span><?=__('po-payment_pay')?></span></button>*/ ?>
                </form>

            </div>
        </div>
    </div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>

<script>
    $(function(){

        $('[data-toggle="popover"]').popover({ container: 'body' });
        $("#cc-exp").mask( "99/99", {definitions:{'*':"^[1-12]\/[0-9][0-9]?$"}} );

        $('.btn-pay-payflow').click(function(){
            $('#payment-modal-payflow').modal();
        });
    });
</script>
