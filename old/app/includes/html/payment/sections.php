
<?php

$showImage=false;

if(!isset($isAdmin))
    $isAdmin = false;

foreach ($data->sections as $sectionKey => $section):

    $section_subtotal = 0;


    ?>

    <p><?=$section->title?></p>

    <table cellspacing="0" cellpadding="0" border="0" width="650" style="font-size:13px;">
        <thead class="thead-default thead-lg">
        <tr>
            <?php if($showImage):?>
            <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">Image</th>
            <?php endif; ?>
            <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">Code</th>
            <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">Desc.</th>
            <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;"><?=__('quote-item_qty')?></th>
            <th align="right" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;"><?=__('quote-item_unit-price')?></th>
            <th align="right" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">Ext</th>
        </tr>

        </thead>

        <tbody>

        <?php foreach ($section->items as $quote_item):

            $id = $quote_item->id;
            $code = $quote_item->SKU;
            $description = "<strong>{$quote_item->title}</strong><br>{$quote_item->description}";
            $quantity = (float)str_replace(',', '.', $quote_item->quantity);
            $cost = $quote_item->price;
            $unit_price = calculate_quote_item_unit_price($quote_item);
            $unit_total = calculate_quote_item_total($quote_item);
            $section_subtotal+=$unit_total;
            $gp = number_format((($unit_price - $cost) / $unit_price) * 100,2);


            ?>
            <tr>
                <?php if($showImage):?>
                <td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">
                    <?php if(@$quote_item->pictureURL):

                        if($isPDF) {
                            $imagePath = str_replace(getenv('BASE_URL'), getenv('BASE_DIR'), $quote_item->pictureURL);
                        }else{
                            $imagePath =  $quote_item->pictureURL;
                        }
                        ?>

                        <img width="50px" src="<?=$imagePath?>" />
                    <?php endif; ?>
                </td>
                <?php endif; ?>
                <td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><?=$quote_item->SKU?></td>
                <td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><?=$description?></td>
                <td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><?=$quantity?></td>
                <td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><?=currency_format($unit_price);?></td>
                <td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><?=currency_format($unit_total);?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="<?=$showImage?5:4?>"></td>
            <td align="right" valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><b><?=__('quote_subtotal')?>:&nbsp;</b> <?= currency_format($section_subtotal); ?></td>
        </tr>

        </tbody>
    </table>
<?php endforeach; ?>