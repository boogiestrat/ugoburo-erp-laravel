
<?php

/** @var $payment */
/** @var $deposit */
/** @var $quote */
/** @var $quoteJsonData */
/** @var $quoteTotal */

$discount = $quoteJsonData->discountPrice;
$subTotal = $quoteTotal->getSubTotal();


$balance = $quoteTotal->getTotal();

if($deposit)$balance-=$deposit->getAmount();
if($payment)$balance-=$payment->getAmount()


?>


<table cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size:13px;">

    <thead class="thead-default thead-lg">
        <tr>
            <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;"></th>
            <th align="right" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;"><?=__('payment_amount')?></th>
        </tr>
    </thead>

    <tr>
        <td valign="top" align="right" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><b><?=__('quote_subtotal-items')?>:</b></td>
        <td valign="top" align="right" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"> <?= currency_format($subTotal); ?></td>
    </tr>

    <?php if (!empty($discount) && $discount > 0) : ?>
        <tr>
            <td valign="top" align="right"  style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><?=__('quote_discount')?></td>
            <td valign="top" align="right"  style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><?= currency_format($discount); ?></th>
        </tr>
    <?php endif ?>

    <?php foreach($quoteTotal->getTaxes() as $taxName=>$taxValue):?>
        <tr>
            <td valign="top" align="right"  style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><b><?=$taxName?>:</b></td>
            <td valign="top" align="right"  style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"> <?= currency_format($taxValue); ?></td>
        </tr>
    <?php endforeach; ?>

    <?php if ($deposit) : ?>
        <tr>
            <td valign="top" align="right"  style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><b><?=__('quote_deposit')?></b></td>
            <td valign="top" align="right"  style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">-<?= currency_format($deposit->getAmount()); ?></td>
        </tr>
    <?php endif ?>

    <?php if ($payment) : ?>
        <tr>
            <td valign="top" align="right"  style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><b><?=__('payment_payment')?></b></td>
            <td valign="top" align="right"  style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">-<?= currency_format($payment->getAmount()); ?></td>
        </tr>
    <?php endif ?>

    <tr>
        <td valign="top" align="right"  style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><b>Total:</b></td>
        <td valign="top" align="right"  style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"> <?= currency_format($quoteTotal->getTotal()); ?></td>
    </tr>

    <?php if($balance) :?>

        <tr>
            <td valign="top" align="right"  style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"><b>Balance:</b></td>
            <td valign="top" align="right"  style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;"> <?= currency_format($balance); ?></td>
        </tr>
    <?php endif ?>

    <tr>
        <td colspan="2" align="right"><?=__('quote_price-shown-cad')?></td>
    </tr>

</table>