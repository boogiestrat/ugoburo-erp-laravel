<?php

/** @var $apiURL */
/** @var $requestEndpoint */
/** @var $responseEndpoint */
/** @var $jsonResponse */
?>

<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script src="<?=PATH?>assets/js/payment/rxp-js.js"></script>

<script>
    $(document).ready(function() {
        var jsonFromServerSdk = <?=$jsonResponse?>;
        RealexHpp.setHppUrl('<?=$apiURL?>');
        RealexHpp.lightbox.init("pay-gp", '<?=$responseEndPoint?>', jsonFromServerSdk);
        $('body').addClass('loaded');
    });
</script>

<?php /*
<div class="ibox">
    <div class="ibox-body">
        <h5 class="font-strong mb-4"><?=__('payment_payment')?></h5>
        <button id="pay-gp" class="button btn btn-primary btn-lg btn-block btn-pay-gp" type="button"><span><?=__('payment_pay-now')?></span></button>
    </div>
</div>*/ ?>

<button id="pay-gp" class="button btn btn-primary btn-lg btn-block btn-pay-gp" type="button"><span><?=__('payment_pay-now')?></span></button>



