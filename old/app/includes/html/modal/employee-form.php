<!-- Employee Form Dialog-->
<div class="modal fade" id="employee-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content form-horizontal" id="employee-form" action="<?=PATH?>webservices/employee/add" method="post">
            <div class="modal-header p-4">
                <h5 class="modal-title">NOUVEL EMPLOYÉ</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-12">
                        <div class="card-avatar mt-3 mb-4 text-center img-circle mx-auto" style="display:none;">
                            <label for="avatar-upload" id="avatar-label" title="Modifier" class="w-100 h-100" style="cursor:cell;"></label>
                        </div>
                        <div class="mt-3 mb-4 text-center">
                            <label class="btn btn-success file-input">
                                <span class="btn-icon"><i class="la la-camera"></i>Photo</span>
                                <input type="file" name="avatar" id="avatar-upload" accept="image/*">
                            </label>
                            <input type="hidden" name="MAX_FILE_SIZE" value="10000000"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group mb-4">
                        <input class="form-control form-control-line" id="employee-first-name" type="text" name="first_name" placeholder="Prénom*">
                    </div>
                    <div class="col-6 form-group mb-4">
                        <input class="form-control form-control-line" id="employee-last-name" type="text" name="last_name" placeholder="Nom*">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group mb-4">
                        <div class="input-group-icon input-group-icon-left">
                            <span class="input-icon input-icon-left"><i class="ti-email"></i></span>
                            <input class="form-control form-control-line" id="employee-email" type="text" name="email" placeholder="Courriel*">
                        </div>
                    </div>
                    <div class="col-6 form-group mb-4">
                    <div class="input-group-icon input-group-icon-left">
                            <span class="input-icon input-icon-left"><i class="ti-mobile"></i></span>
                            <input class="form-control form-control-line" id="employee-mobile" type="text" name="mobile" placeholder="555-555-5555">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-4">
                    <label><?=$i18n->trans('accessLevel')?></label>
                    <select class="form-control" id="employee-user-level" name="user_level">
                    <?php
                        foreach ($user_levels as $key => $user_level) {
                            if ($user_level['id'] == $default_user_level)
                                $selected = 'selected="selected"';
                            else
                                $selected = '';
                    ?>
                        <option value="<?=$user_level['id']?>" <?=$selected?>><?=$user_level['name']?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="modal-footer text-right">
                    <input type="hidden" name="id" id="employee-id">
                    <input type="hidden" name="avatar" id="employee-avatar">
                    <button class="btn btn-rounded" data-dismiss="modal" id="cancel-button" type="button"><?=$i18n->trans('cancel')?></button>
                    <button class="btn btn-primary btn-rounded submit-btn" id="submit-button" type="submit"><?=$i18n->trans('save')?></button>
                    <div class="preloader-backdrop spinner" style="display:none;">
                        <div class="page-preloader"><?=$i18n->trans('loading')?></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Client Form Dialog-->
