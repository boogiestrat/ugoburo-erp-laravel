<!-- Item Dialog-->
<div class="modal fade" id="item-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content form-horizontal" id="item-form" action="<?=PATH?>webservices/item/add" method="post">
          <input type="hidden" name="fk_type_id" value="2">
            <div class="modal-header p-4">
                <h5 class="modal-title">NOUVEAU SERVICE</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-12">
                        <div class="card-picture mt-3 mb-4 text-center img-circle mx-auto" style="display:none;">
                            <label for="picture-upload" id="picture-label" title="Modifier" class="w-100 h-100" style="cursor:cell;"></label>
                        </div>
                        <div class="mt-3 mb-4 text-center">
                            <label class="btn btn-success file-input">
                                <span class="btn-icon"><i class="la la-camera"></i>Parcourir</span>
                                <input type="file" name="picture" id="picture-upload" accept="image/*">
                            </label>
                            <input type="hidden" name="MAX_FILE_SIZE" value="10000000"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group mb-4">
                        <input class="form-control form-control-line" id="item-name" type="text" name="name" placeholder="Nom*">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group mb-4">
                        <input class="form-control form-control-line" id="item-sku" type="text" name="sku" placeholder="SKU*">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group mb-4 price-fields-container">
                        <div class="input-group-icon input-group-icon-left price-field" id="price-field">
                            <span class="input-icon input-icon-left"><i class="ti-money"></i></span>
                            <input class="form-control form-control-line" id="item-price" type="text" name="price" placeholder="Prix*">
                        </div>
                    </div>
                    <?php /*
                    <div class="col-12 mb-4">
                        <button class="btn btn-rounded btn-sm btn-primary" id="item-add-price-line-js" onclick="javascript:;">Ajouter un prix</button>
                    </div>
                    */ ?>
                </div>
                <div class="row">
                    <div class="col-12 form-group mb-4">
                        <input class="form-control form-control-line" id="item-description" type="text" name="description" placeholder="Description">
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-12 form-group mb-4">
                        <label>Type d'item</label>
                        <select class="form-control" id="item-type" name="fk_type_id">
                        <?php
                            foreach ($types as $key => $type) {
                                if ($type['id'] == $default_type)
                                    $selected = 'selected="selected"';
                                else
                                    $selected = '';
                        ?>
                            <option value="<?=$type['id']?>" <?=$selected?>><?=$type['name']?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>
                </div> -->
                <!-- <div class="row">
                    <div class="col-12 form-group mb-4">
                        <label><?=$i18n->trans('menu_suppliers')?></label>
                        <div class="py-2 select-manufacturier collapse show">
                            <select class="form-control" id="item-manufacturier" name="fk_manufacturier_id" style="width: 100%;"></select>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="row">
                    <div class="col-12 form-group mb-4" id="table-list-child" style="display:none;">
                        <table class="table table-bordered" id="datatable-list">
                            <thead class="thead-default thead-lg">
                                <tr>
                                    <th>Nom</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Prix de vente</th>
                                    <th class="no-sort no-search">Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" id="first-row-children-js" class="odd" data-id="0">

                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <a class="font-20 add-item-row-btn" href="javascript:;" id="add-item-row" data-id="" data-label=""><i class="la la-plus"></i></a>
                            <a href="javascript:;" onclick="dropRow('all', true)" style="margin-left:20px;" class="btn btn-sm btn-warning">Tout supprimer</a>
                        </div>
                    </div>
                    <div class="col-6 form-group mb-4">
                        &nbsp;
                    </div>
                </div> -->
                <div class="modal-footer text-right">
                    <input type="hidden" name="id" id="item-id">
                    <input type="hidden" name="picture" id="item-picture">
                    <button class="btn btn-rounded" data-dismiss="modal" id="cancel-button" type="button"><?=$i18n->trans('cancel')?></button>
                    <button class="btn btn-primary btn-rounded submit-btn" id="submit-button" type="submit"><?=$i18n->trans('save')?></button>
                    <div class="preloader-backdrop spinner" style="display:none;">
                        <div class="page-preloader"><?=$i18n->trans('loading')?></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Client Dialog-->
