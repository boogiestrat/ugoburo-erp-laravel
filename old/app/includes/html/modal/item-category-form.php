<!-- Item Dialog-->
<div class="modal fade" id="item-category-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content form-horizontal" id="item-category-form" action="<?=PATH?>webservices/item/category/add" method="post">
            <div class="modal-header p-4">
                <h5 class="modal-title">NOUVELLE CATÉGORIE</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-12 form-group mb-4">
                        <input class="form-control form-control-line" id="item-category-name" type="text" name="name" placeholder="Nom*">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group mb-4">
                        <input class="form-control form-control-line" id="item-category-description" type="text" name="description" placeholder="Description">
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <input type="hidden" name="id" id="item-category-id">
                    <input type="hidden" name="picture" id="item-category-picture">
                    <button class="btn btn-rounded" data-dismiss="modal" id="cancel-button" type="button"><?=$i18n->trans('cancel')?></button>
                    <button class="btn btn-primary btn-rounded submit-btn" id="submit-button" type="submit"><?=$i18n->trans('save')?></button>
                    <div class="preloader-backdrop spinner" style="display:none;">
                        <div class="page-preloader"><?=$i18n->trans('loading')?></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Client Dialog-->
