<!-- Manufacturier Dialog-->
<div class="modal fade" id="manufacturier-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content form-horizontal" id="manufacturier-form" action="<?=PATH?>webservices/manufacturier/add" method="post">
            <div class="modal-header p-4">
                <h5 class="modal-title"><?=strtoupper($i18n->trans('supplier_new'))?>/h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-6 form-group mb-4">
                        <input class="form-control form-control-line" id="manufacturier-name" type="text" name="name" placeholder="Nom*">
                    </div>
                    <div class="col-6 form-group mb-4">
                        <input class="form-control form-control-line" id="manufacturier-pays" type="text" name="pays" placeholder="Pays*">
                    </div>
                    <div class="col-6 form-group mb-4">
                        <input class="form-control form-control-line" id="manufacturier-code" type="text" name="code" placeholder="Code*">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group mb-4" id="table-list-discounts">
                        <table class="table table-bordered" id="datatable-discounts">
                            <thead class="thead-default thead-lg">
                                <tr>
                                    <th>Escompte</th>
                                    <th>1</th>
                                    <th>2</th>
                                    <th>3</th>
                                    <th>4</th>
                                    <th>5</th>
                                    <th>No Enr.</th>
                                    <th>Code</th>
                                    <th class="no-sort no-search"><a class="font-20 add-address-row-btn" href="javascript:;" id="add-discount-row" ><i class="la la-plus"></i></a></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <input type="hidden" name="id" id="manufacturier-id">
                    <input type="hidden" name="picture" id="manufacturier-picture">
                    <button class="btn btn-rounded" data-dismiss="modal" id="cancel-button" type="button"><?=$i18n->trans('cancel')?></button>
                    <button class="btn btn-primary btn-rounded submit-btn" id="submit-button" type="submit"><?=$i18n->trans('save')?></button>
                    <div class="preloader-backdrop spinner" style="display:none;">
                        <div class="page-preloader"><?=$i18n->trans('loading')?></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Client Dialog-->
