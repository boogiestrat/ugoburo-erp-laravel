<!-- Client Form Dialog-->
<div class="modal fade" id="client-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content form-horizontal" id="client-form" action="<?=PATH?>webservices/client/add" method="post">
            <div class="modal-header p-4">
                <h5 class="modal-title" style="width:100%;"><input class="form-control form-control-line" id="client-entreprise" type="text" name="entreprise" placeholder="Nouveau client"></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">

                <div class="row">
                    <div class="col-12 form-group form-inline mb-4">
                        <?php foreach(get_all_client_types() as $clientType): ?>
                            <label class="radio radio-grey radio-primary mr-2">
                            <input type="radio" id="client-type-<?=$clientType['id']?>" name="client_type" value="<?=$clientType['id']?>">
                            <span class="input-span"></span><?=$clientType['name']?></label>
                        <?php endforeach ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 form-group mb-4" id="table-list-addresses">
                        <table class="table table-bordered" id="datatable-clients-addresses">
                            <thead class="thead-default thead-lg">
                                <tr>
                                    <th>Adresse</th>
                                    <!-- <th>Facturation par défaut</th>
                                    <th>Livraison par défaut</th> -->
                                    <th class="no-sort no-search text-right" style="width:20px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                              <tr role="row" id="clients_address_rowid_">
                                  <td>
                                    <input class="form-control form-control-line address-address autocomplete" type="text" id="selected_address" name="selected_address" placeholder="Adresse">
                                    <input type="hidden" class="locality" name="address_city" id="address_locality" />
                                    <input type="hidden" class="administrative_area_level_1" name="address_province" id="address_administrative_area_level_1" />
                                    <input type="hidden" class="postal_code" name="address_postal_code" id="address_postal_code" />
                                    <input type="hidden" class="country" name="address_country" id="address_country" />
                                  </td>
                                  <td class="text-right">
                                    <a class="font-20 add-address-row-btn" href="javascript:;" id="add-address-row" onclick="add_selected_address();" data-id="" data-label=""><i class="la la-plus"></i></a>
                                  </td>
                              </tr>
                            </tbody>
                        </table>
                        <div>

                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-12 form-group mb-4">
                    <div class="py-2 select-contact multi-collapse-contact collapse show" style="position: relative;">
                      <label for="client-contacts">Contacts<?php /* <button type="javascript:;" id="btn-new-contact-from-client-js" class="btn btn-primary btn-sm">Créer un nouveau contact</button> */ ?></label>
                      <table class="table table-bordered" id="datatable-clients-contact">
                          <thead class="thead-default thead-lg">
                              <tr>
                                  <th>Contact</th>
                                  <!-- <th>Facturation par défaut</th>
                                  <th>Livraison par défaut</th> -->
                                  <th class="no-sort no-search text-right" style="width:20px;"></th>
                              </tr>
                          </thead>
                          <tbody>
                            <tr role="row" id="clients_contact_rowid_">
                              <!--
                                <td>
                                  <select id="client-contacts" name="contacts[]" style="width:100%;" class="form-control search-contact-form select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                  </select>
                                </td>
                                <td class="text-right">
                                  <a class="font-20 add-address-row-btn" href="javascript:;" id="add-contact-row" onclick="add_selected_contact();" data-id="" data-label=""><i class="la la-plus"></i></a>
                                </td>
                              -->
                            </tr>
                          </tbody>
                      </table>
                      <div>
                        <button type="button" class="btn btn-default btn-rounded btn-sm mr-4" onclick="show_create_contact_form();">
                            <span class="btn-icon"><i class="la la-plus" id="create_contact_button"></i> Créer un contact</span>
                        </button>
                      </div>
                      <div style="display:none;" id="create_contact_form">
                        <br/>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="row">
                                <div class="col-6 form-group mb-4">
                                    <input class="form-control form-control-line" id="contact-first-name" type="text" name="first_name" placeholder="Prénom*">
                                </div>
                                <div class="col-6 form-group mb-4">
                                    <input class="form-control form-control-line" id="contact-last-name" type="text" name="last_name" placeholder="Nom*">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 form-group mb-4">
                                    <div class="input-group-icon input-group-icon-left">
                                        <span class="input-icon input-icon-left"><i class="ti-email"></i></span>
                                        <input class="form-control form-control-line" id="contact-email" type="text" name="email" placeholder="Courriel">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 form-group mb-4">
                                    <div class="input-group-icon input-group-icon-left">
                                        <span class="input-icon input-icon-left"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                        <input class="form-control form-control-line" id="contact-tel" type="text" name="tel" placeholder="555-555-5555*">
                                    </div>
                                </div>
                                <div class="col-6 form-group mb-4">
                                    <div class="input-group-icon input-group-icon-left">
                                        <span class="input-icon input-icon-left"><i class="ti-mobile"></i></span>
                                        <input class="form-control form-control-line" id="contact-mobile" type="text" name="mobile" placeholder="555-555-5555">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-right">
                                  <button type="button" class="btn btn-default" onclick="show_create_contact_form();">
                                    Annuler
                                  </button>
                                  <button type="button" class="btn btn-success" onclick="save_create_contact();">
                                    Ajouter le contact
                                  </button>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr>
                <?php if (user_level(3)): ?>
                  <div class="row">
                    <div class="col-12 form-group mb-4">
                      <h5>Gestion des permissions <span class="label label-warning">Administrateurs seulement</span></h5>
                      <div class="py-2 select-contact multi-collapse-contact collapse show">
                        <label for="list-employees">Ce client peut être vu par les administrateurs et par: </label>
                        <select id="list-employees" name="employees[]" style="width:100%;" class="form-control select2_employees select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                        </select>
                      </div>
                    </div>
                  </div>
                  <hr>
                <?php endif ?>
                <div class="row">
                    <div class="col-12 form-group mb-4">
                    <label for="client-note">Note</label>
                    <textarea class="form-control form-control-air" rows="3" id="client-note" name="note"></textarea>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <input type="hidden" name="id" id="client-id">
                    <input type="hidden" name="client_contacts_json" id="client_contacts_json">
                    <input type="hidden" name="client_addresses_json" id="client_addresses_json">
                    <button class="btn btn-rounded" data-dismiss="modal" id="cancel-button" type="button"><?=$i18n->trans('cancel')?></button>
                    <button class="btn btn-primary btn-rounded submit-btn" id="submit-button" type="submit"><?=$i18n->trans('save')?></button>
                    <div class="preloader-backdrop spinner" style="display:none;">
                        <div class="page-preloader"><?=$i18n->trans('loading')?></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Client Form Dialog-->
