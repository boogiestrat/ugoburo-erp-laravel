<!-- Contact Form Dialog-->
<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content form-horizontal" id="contact-form" action="<?=PATH?>webservices/contact/add" method="post" autocomplete="off">
            <div class="modal-header p-4">
                <h5 class="modal-title">NOUVEAU CONTACT</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-6 form-group mb-4">
                        <input class="form-control form-control-line" id="contact-first-name" type="text" name="first_name" placeholder="Prénom*">
                    </div>
                    <div class="col-6 form-group mb-4">
                        <input class="form-control form-control-line" id="contact-last-name" type="text" name="last_name" placeholder="Nom*">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group mb-4">
                        <div class="input-group-icon input-group-icon-left">
                            <span class="input-icon input-icon-left"><i class="ti-email"></i></span>
                            <input class="form-control form-control-line" id="contact-email" type="text" name="email" placeholder="Courriel">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group mb-4">
                        <div class="input-group-icon input-group-icon-left">
                            <span class="input-icon input-icon-left"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input required="required" class="form-control form-control-line" id="contact-tel" type="text" name="tel" placeholder="555-555-5555*">
                        </div>
                    </div>
                    <div class="col-6 form-group mb-4">
                        <div class="input-group-icon input-group-icon-left">
                            <span class="input-icon input-icon-left"><i class="ti-mobile"></i></span>
                            <input class="form-control form-control-line" id="contact-mobile" type="text" name="mobile" placeholder="555-555-5555">
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-12 form-group mb-4">
                    <div class="py-2 select-contact multi-collapse-contact collapse show">
                      <label for="select_client_contact_form_js">Sélectionner le compte client associée au contact*</label>
                      <select id="select_client_contact_form_js" required="required" name="fk_client_id" style="width:100%" class="form-control list_contact search-contact-form" tabindex="-1" aria-hidden="true">
                      </select>
                    </div>
                  </div>
                </div>
                <hr>
                <?php if (user_level(3)): ?>
                    <!-- <div class="row">
                        <div class="col-12 form-group mb-4">
                          <h5>Gestion des permissions <span class="label label-warning">Administrateurs seulement</span></h5>
                          <div class="py-2 select-contact multi-collapse-contact collapse show">
                            <label for="list-employees">Ce client peut être vu par les administrateurs et par: </label>
                            <select id="list-employees" name="employees[]" style="width:100%;" class="form-control select2_employees select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                            </select>
                          </div>
                        </div>
                    </div> -->
                <?php endif ?>
                <hr>
                <div class="row">
                  <div class="col-12 form-group mb-4">
                    <textarea class="form-control form-control-line" name="comments" id="contact-comments" rows="3" placeholder="Commentaires"></textarea>
                  </div>
                </div>
                <div class="modal-footer text-right">
                    <input type="hidden" name="id" id="contact-id">
                    <button class="btn btn-rounded" data-dismiss="modal" id="cancel-button" data-lang="<?=$i18n->trans('cancel')?>" type="button"><?=$i18n->trans('cancel')?></button>
                    <button class="btn btn-primary btn-rounded submit-btn" id="submit-button" data-lang="<?=$i18n->trans('save')?>" type="submit"><?=$i18n->trans('save')?></button>
                    <div class="preloader-backdrop spinner" style="display:none;">
                        <div class="page-preloader"><?=$i18n->trans('loading')?></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Contact Form Dialog-->
