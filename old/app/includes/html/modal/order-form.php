<!-- Order Form Dialog-->
<div class="modal fade" id="order-modal" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 70%">
        <form class="modal-content form-horizontal" id="history-add" action="<?=PATH?>webservices/orders/histories/add" method="POST">
            <div class="modal-header p-4">
                <h5 class="modal-title"><?=strtoupper($i18n->trans('menu_order'))?></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <?php include(APP_ROOT . 'includes/html/template/order-form.php'); ?>
            <div class="modal-footer text-right">
                <input type="hidden" name="id" id="quote-id" disabled>
                <button class="btn btn-rounded btn-primary" data-dismiss="modal" id="cancel-button" type="button">Fermer</button>
                <div class="preloader-backdrop spinner" style="display:none;">
                    <div class="page-preloader"><?=$i18n->trans('loading')?></div>
                </div>
            </div>
            </div>
        </form>
    </div>
</div>
<!-- End Order Form Dialog -->
