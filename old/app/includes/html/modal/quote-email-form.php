<?php if(!@$_REQUEST['ajax']): ?><div class="hidden"><?php endif ;?>
    <form id="quote-email">
        <div class="form-group">
            <label for="quote-email-recipients">Addresse(s) courriel du/des client(s)</label>
            <input type="text" class="form-control" name="quote-email-recipients" aria-describedby="emailHelp" placeholder="Courriel(s)">
            <small id="emailHelp" class="form-text text-muted">Couriels séparés par des virgules</small>
        </div>
        <div class="form-group">
            <label for="quote-email-language">Langue</label>
            <select class="form-control" id="quote-email-language" name="quote-email-language">
                <option value="en">Anglais</option>
                <option value="fr">Français</option>
            </select>
        </div>
        <div class="form-check">
            <input type="checkbox" class="form-check-input"  name="quote-email-pdf" id="quote-email-pdf" value="1">
            <label class="form-check-label" for="quote-email-pdf">Inclure le PDF</label>
        </div>
        <div class="form-check">
            <input type="checkbox" class="form-check-input"  name="quote-email-weblink" id="quote-email-weblink" value="1">
            <label class="form-check-label" for="quote-email-weblink">Inclure le lien</label>
        </div>

        <?php if(getenv('ENABLE_EMAIL_DEPOSIT')) :?>
        <div class="form-check">
            <input type="checkbox" class="form-check-input"  name="quote-email-deposit" id="quote-email-deposit" value="1">
            <label class="form-check-label" for="quote-email-deposit">Dépôt seulement</label>
        </div>
        <br>
        <div class="deposit-group hidden">
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <input type="text" class="form-control" name="quote-email-deposit-value" aria-describedby="emailHelp" placeholder="Dépôt">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <select class="form-control" name="quote-email-deposit-type">
                            <option value="%">Pourcentage (%)</option>
                            <option value="$">Montant ($)</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="deposit-preview"></label>
            </div>
        </div>
        <?php endif; ?>
    </form>
<?php if(!@$_REQUEST['ajax']): ?></div><?php endif ;?>
