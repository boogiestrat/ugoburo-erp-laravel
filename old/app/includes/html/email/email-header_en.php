<!DOCTYPE html>
<html>

<head>
</head>

<body style="margin:0; padding: 0;">
    <p>&nbsp;</p>
    <table class="wrapper" width="100%" style="border-collapse: collapse; margin: 0 auto;">
        <tbody>
            <tr>
                <td class="wrapper-inner" align="center"
                    style="font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; vertical-align: top; padding-bottom: 30px; width: 100%;">
                    <table class="main" align="center"
                        style="border-collapse: collapse; margin: 0 auto; text-align: left; width: 600px;">
                        <tbody>
                            <tr>
                                <td class="header"
                                    style="font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; vertical-align: top; padding: 10px 10px 0;">
                                    <a class="logo" href="https://www.ugoburo.ca/"
                                        style="color: #1979c3; text-decoration: none;">
                                        <img width="157" height="26"
                                            src="<?=BASE_URL?>assets/img/ugoburo-logo.png"
                                            alt="Ugoburo" border="0"
                                            style="border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="main-content"
                                    style="font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; vertical-align: top; background-color: #ffffff; padding: 10px;">