        <script>
            var BASE_URL = "<?=BASE_URL?>";
            var LOGIN_URL = "<?=LOGIN_URL?>";
            var LOGOUT_URL = "<?=LOGOUT_URL?>";
            var PATH = "<?=PATH?>";
            var PATH_UPLOAD = "<?=PATH_UPLOAD?>";
            var LOCALE = "<?=$_ENV['LANG_ISO_CODE']?>";
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="<?=PATH?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.1/metisMenu.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
        <?php /*<script src="<?=PATH?>assets/vendors/jquery-idletimer/dist/idle-timer.min.js"></script> */ ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.3/toastr.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/localization/messages_fr.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/keypress/2.1.5/keypress.min.js"></script>
        <!-- CORE SCRIPTS-->
        <script src="<?=PATH?>assets/js/app.js?v=<?=date('Ymd')?>"></script>
        <!-- LOGIN SCRIPTS-->
        <script src="<?=PATH?>assets/js/auth.class.js?v=<?=date('Ymd')?>"></script>
        <script>
            $(document).ready(function () {
                window.auth = new SimpleAuth('<?=$_ENV['GOOGLE_CLIENT_ID']?>');

                $('#logout').on('click', () => window.auth.logout());
            });

            </script>
        <script type="text/javascript">

            function onLoadCallback() {
                // gapi.client.setApiKey('<?=$_ENV['GOOGLE_PUBLIC_API_KEY']?>');
                // gapi.client.load('plus', 'v1', function () {
                // });
                gapi.load('auth2', res => {
                    window.auth.init();

                    if (window.location.href.split('/')[window.location.href.split('/').length - 1] == 'logout') {
                        window.auth.logout();
                    }
                });
            }

            (function () {
                var po = document.createElement('script');
                po.type = 'text/javascript';
                po.async = true;
                po.src = 'https://apis.google.com/js/signin.js?onload=onLoadCallback';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(po, s);
            })();
        </script>

    <?php if (!empty($_ENV['GOOGLE_ANALYTICS_ID'])) { ?>
        <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','<?=$_ENV['GOOGLE_ANALYTICS_ID']?>','auto');ga('send','pageview')
        </script>
        <script src="https://www.google-analytics.com/analytics.js" async defer></script>
    <?php }?>
