<!doctype html>
<html class="no-js" lang="<?=$_ENV['LANG_ISO_CODE']?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?=(empty($page_title) ? $_ENV['SITE_NAME'] : $page_title . ' | ' . $_ENV['SITE_NAME'])?></title>
        <meta name="description" content="<?=(empty($page_desc) ? $_ENV['SITE_DESC'] : $page_desc)?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="manifest" href="<?=BASE_URL?>site.webmanifest">
        <link rel="apple-touch-icon" href="<?=BASE_URL?>favicon.png">
        <link rel="shortcut icon" href="<?=BASE_URL?>favicon.png" type="image/x-icon" />
        <!-- Place favicon.ico in the root directory -->

        <!-- GLOBAL MAINLY STYLES-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="<?=BASE_URL?>assets/vendors/line-awesome/css/line-awesome.min.css" />
        <link rel="stylesheet" href="<?=BASE_URL?>assets/vendors/themify-icons/css/themify-icons.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.3/toastr.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css" />
