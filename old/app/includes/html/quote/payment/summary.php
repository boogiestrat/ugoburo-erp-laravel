<h5 class="font-strong mb-4"><?=__('quote_summary')?></h5>
<table class="table table-bordered table-hover">
    <thead class="thead-default thead-lg">
        <tr>
            <th class="text-left name"><?=__('quote_section-name')?></th>
            <th align="right" class="name"><?=__('quote_price')?></th>
        </tr>
    </thead>

    <?php foreach (get_quote_sections($quote) as $section): ?>
        <tr>
            <td><?=strtoupper($section->title)?></td>
            <td align="right"><?=$section->total?></td>
        </tr>
    <?php endforeach; ?>
</table>