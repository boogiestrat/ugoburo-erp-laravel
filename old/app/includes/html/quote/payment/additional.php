<?php 

if(!empty($data->additionalNote)) {

?>
    <div id="additionnal-info">
        <h5 class="font-strong mb-4"><?=__('quote_additional-informations')?></h5>
        <p>
            <?=$data->additionalNote?>
        </p><br>
    </div>
<?php 
}

if(!empty($data->livraisonNote)) {

?>
    <div id="installation-info">
        <h5 class="font-strong mb-4"><?=__('quote_install-informations')?></h5>
        <p>
            <?=$data->livraisonNote?>
        </p><br>
    </div>
<?php 
}
?>