<table class="" width="100%" >
    <tr>
        <td width="50%">
            <table class="" width="100%" >
                <tr>
                    <td>
                        <h5 class="font-strong mb-4"><?=__('address_billing')?></h5>
                        <?=$data->billTo->contact->first_name?> <?=$data->billTo->contact->last_name?><br>
                        <?=$data->billTo->address->address?> <br>
                        <?php /*
                        <?=$data->billTo->address->city?> <br>
                        <?=$data->billTo->address->province?>
                        <?=ucfirst($data->billTo->address->country)?> <br>
                        <?=strtoupper($data->billTo->address->postal_code)?><br> */ ?>
                        <?php  if (!empty($data->billTo->contact->phone)) {?>
                            <?=__('address_phone-abrv')?>: <?=$data->billTo->contact->phone?>
                        <?php } ?>

                        <?php  if (!empty($data->billto->contact->cell)) {?>
                            <?=__('address_cell-abrv')?>: <?=$data->billto->contact->cell?>
                        <?php } ?>

                        <?php  if (!empty($data->billto->contact->email)) {?>
                            <?=$data->billto->contact->email?>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </td>
        <td width="50%">
            <table class="" width="100%" >
                <tr>
                    <td>
                        <h5 class="font-strong mb-4"><?=__('address_shipping')?></h5>
                        <?=$data->shipTo->contact->first_name?> <?=$data->shipTo->contact->last_name?><br>
                        <?=$data->shipTo->address->address?> <br>
                        <?php /*
                        <?=$data->shipTo->address->city?> <br>
                        <?=$data->shipTo->address->province?>
                        <?=ucfirst($data->shipTo->address->country)?> <br>
                        <?=strtoupper($data->shipTo->address->postal_code)?><br> */ ?>
                        <?php  if (!empty($data->shipTo->contact->phone)) {?>
                            <?=__('address_phone-abrv')?>: <?=$data->shipTo->contact->phone?>
                        <?php } ?>

                        <?php  if (!empty($data->shipTo->contact->cell)) {?>
                            <?=__('address_cell-abrv')?>: <?=$data->shipTo->contact->cell?>
                        <?php } ?>

                        <?php  if (!empty($data->shipTo->contact->email)) {?>
                            <?=$data->shipTo->contact->email?>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
