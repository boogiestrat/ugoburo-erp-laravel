
    <?php

        $showImage=false;

        if(!isset($isAdmin))
            $isAdmin = false;

        foreach ($data->sections as $sectionKey => $section):

        $section_subtotal = 0;


        ?>

        <h5 class="section-title" colspan="6" align="left" class="title"><?=$section->title?></h5>

        <table class="table table-bordered table-hover">
            <thead class="thead-default thead-lg">
            <tr>
                <?php if($showImage): ?><th align="left">Image</th><?php endif; ?>
                <th align="left">Code</th>
                <th align="left">Desc.</th>
                <th align="left"><?=__('quote-item_qty')?></th>
                <th align="right" style="text-align:right;"><?=__('quote-item_unit-price')?></th>
                <th align="right" style="text-align:right;">Ext</th>
            </tr>

            </thead>

            <tbody>

            <?php foreach ($section->items as $quote_item):

                $id = $quote_item->id;
                $code = $quote_item->SKU;
                $description = "<strong>{$quote_item->title}</strong><br>{$quote_item->description}";
                $quantity = (float)str_replace(',', '.', $quote_item->quantity);
                $cost = $quote_item->price;
                $unit_price = $quote_item->_ItemPrice;
                $unit_total = $unit_price * $quantity;
                $section_subtotal+=$unit_total;
                // $gp = number_format((($unit_price - $cost) / $unit_price) * 100,2);
                $gp = number_format($quote_item->_GrossProfit, 2);


                ?>
                <tr>
                    <?php if($showImage): ?>
                        <td <?=$isAdmin?'class="no-border"':""?> align="left">
                            <?php if(@$quote_item->pictureURL):

                                if($isPDF) {
                                    $imagePath = str_replace(getenv('BASE_URL'), getenv('BASE_DIR'), $quote_item->pictureURL);
                                }else{
                                    $imagePath =  $quote_item->pictureURL;
                                }
                                ?>

                                <img width="50px" src="<?=$imagePath?>" />
                            <?php endif; ?>
                        </td>
                    <?php endif; ?>
                    <td <?=$isAdmin?'class="no-border"':""?> align="left"><?=$quote_item->SKU?></td>
                    <td <?=$isAdmin?'class="no-border"':""?>  align="left"><?=$description?></td>
                    <td <?=$isAdmin?'class="no-border"':""?>  align="left"><?=$quantity?></td>
                    <td <?=$isAdmin?'class="no-border"':""?>  align="right"><?=currency_format($unit_price);?></td>
                    <td <?=$isAdmin?'class="no-border"':""?>  align="right"><?=currency_format($unit_total);?></td>
                </tr>
                <?php if($isAdmin): ?>
                <tr>
                    <td class="quote-item-admin" colspan="<?=$showImage?6:5?>"><b>List price</b> : <?=currency_format($quote_item->priceParameterValue)?> | <b>Discount</b> : [<?= number_format($quote_item->discount->parts[0],2) ?>] [<?= number_format($quote_item->discount->parts[1],2) ?>] [<?= number_format($quote_item->discount->parts[2],2) ?>] [<?= number_format($quote_item->discount->parts[3],2) ?>] [<?= number_format($quote_item->discount->parts[4],2) ?>] <b>Cost per unit</b> : <?= currency_format($quote_item->price) ?> <b>Ext</b> : <?= currency_format($quote_item->price) ?> <b>GP</b> : <?=$gp?>% </td>
                </tr>
            <?php endif; ?>
            <?php endforeach; ?>
                <tr>
                    <td class="big-border" colspan="<?=$showImage?5:4?>"></td>
                    <td class="big-border" align="right"><b><?=__('quote_subtotal')?>:&nbsp;</b> <?= currency_format($section_subtotal); ?></td>
                </tr>

                </tbody>
            </table>
    <?php endforeach; ?>
