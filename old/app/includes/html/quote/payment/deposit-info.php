<?php

$deposit = \Ekosys\Entities\QuotePayment::getDepositByQuoteId($quote_id);
?>

<?php if($deposit): ?>
<div>
    <?=__('payment_deposit-info',$deposit->getDateCreated()->format('Y-m-d'),currency_format($deposit->getAmount()))?>
</div>
<?php endif;?>