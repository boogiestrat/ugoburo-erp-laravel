<div class="d-flex justify-content-end">
    <div class="text-right" style="width:400px;">
        <div class="row font-strong font-20">
            <div class="col-6"><?=__('payment_amount')?>:</div>
            <div class="col-6">
                <div class="h3 font-strong"><?= currency_format($amount); ?></div>
            </div>
        </div>
    </div>
</div>