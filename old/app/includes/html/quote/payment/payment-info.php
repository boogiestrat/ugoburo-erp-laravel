<?php

$payment = \Ekosys\Entities\QuotePayment::getPaymentByQuoteId($quote_id);
?>

<?php if($payment): ?>
<div>
    <?=__('payment_payment-info',$payment->getDateCreated()->format('Y-m-d'),currency_format($payment->getAmount()))?>
</div>
<?php endif; ?>