
<?php

/**
 * @var $data
 * @var $payment
 * @var \Ekosys\Entities\QuoteTotal $quoteTotal
 *
 */


global $totals;
$totals= [];

// Total vars
$discount = $data->discountPrice;
$subTotal = $quoteTotal->getSubTotal();


//var_dump($data->TPS);
//var_dump($data->TVQ);

//$TPS = $data->TPS;
//$TVQ = $data->TVQ;
$deposit = $data->depot;
$total = $quoteTotal->getTotal();


addTotal('Sous-Total des items',$subTotal);
if($discount) addTotal('Escompte', -$discount);

$finalTotal = $total;
if($discount) $finalTotal -= $discount;
if($deposit) $finalTotal-=$deposit;
if($payment) $finalTotal-=$payment->getAmount();

foreach($quoteTotal->getTaxes() as $taxName=>$taxValue){

    addTotal($taxName,$taxValue);
}

//addTotal('TPS',$TPS);
//addTotal('TVQ',$TVQ);


if($deposit) addTotal(__('quote_deposit'),-$deposit);

if($payment) addTotal(__('payment_final-payment'),-$payment->getAmount());

addTotal('Total',$total,true);
if($finalTotal != $total) addTotal(__('payment_total-of'),$finalTotal,true);





function addTotal($label,$amount,$isTotal=false){
    global $totals;
    $totals[] =[
        'label'=>$label,
        'amount'=>$amount,
        'isTotal'=>$isTotal
    ];
}

function buildTotalLine($label,$amount,$isTotal=false){

    if($isTotal){ ?>

        <div class="row font-strong font-20">
            <div class="col-6"><?=$label?>:</div>
            <div class="col-6">
                <div class="h3 font-strong"><?= currency_format($amount); ?></div>
            </div>
        </div>

   <?php }else{ ?>

        <div class="row mb-2">
            <div class="col-6"><?=$label?>:</div>
            <div class="col-6"><?= currency_format($amount); ?></div>
        </div>

    <?php }

} ?>


<?php if($isPDF): ?>
<table>
    <tr>
        <td class="no-border">
            <table class="quote-total">
                <tr>
                    <td align="right"><b><?=__('quote_subtotal-items')?>:</b></td>
                    <td width="20%" align="right"> <?= currency_format($subTotal); ?></td>
                </tr>

                <?php if (!empty($discount) && $discount > 0) : ?>
                    <tr>
                        <td align="right"><?=__('quote_discount')?></td>
                        <th align="right"><?= currency_format($discount); ?></th>
                    </tr>
                <?php endif ?>

                <tr>
                    <td align="right"><b>TPS:</b></td>
                    <td align="right"> <?= currency_format($data->TPS); ?></td>
                </tr>
                <tr>
                    <td align="right"><b>TVQ:</b> </td>
                    <td align="right"><?= currency_format($data->TVQ); ?></td>
                </tr>

                <?php if (!empty($deposit) && $deposit > 0) : ?>
                    <tr>
                        <td align="right"><b><?=__('quote_deposit')?></b></td>
                        <td align="right"> <?= currency_format($deposit); ?></td>
                    </tr>
                    <tr>
                        <td align="right"><b><?=__('quote_total-to-pay')?></b></td>
                        <td align="right"> <?= currency_format($total); ?></td>
                    </tr>
                <?php else : ?>
                    <tr>
                        <td align="right"><b>Total:</b></td>
                        <td align="right"> <?= currency_format($total); ?></td>
                    </tr>
                <?php endif; ?>

                <tr>
                    <td colspan="2" align="right"><?=__('quote_price-shown-cad')?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php else: ?>

    <div class="d-flex justify-content-end">
        <div class="text-right" style="width:400px;">

           <?php foreach ($totals as $total):
                echo buildTotalLine($total['label'],$total['amount'],$total['isTotal']);
           endforeach; ?>


            <?php /*<div class="row mb-2">
                <div class="col-6">Sous-Total des items:</div>
                <div class="col-6"><?= currency_format($subTotal); ?></div>
            </div>
            <div class="row mb-2">
                <div class="col-6">Discount 5%:</div>
                <div class="col-6">-$63.25</div>
            </div>
            <div class="row font-strong font-20">
                <div class="col-6">Total Price:</div>
                <div class="col-6">
                    <div class="h3 font-strong">$1201.71</div>
                </div>
            </div> */ ?>
        </div>
    </div>


<?php endif; ?>
