<?php

/**
 * @var $amount
 * @var $is_deposit
 * @var DateTime $date_created
 * @var $client_first_name
 * @var $client_last_name
 * @var $card_number
 * @var $payment_method
 * @var $transaction_id
 */
if (!isset($transaction_id)) { $transaction_id=0; }
?>

<b><?=__('payment-method_paid-on')?></b> <?=$date_created->format('Y-m-d')?><br>
<b><?=__('payment-method_paid-by')?></b> <?=$client_first_name?> <?=$client_last_name?><br>
<?php
if ($payment_method == "purchase_order") {
  ?>
  <b><?=__('payment-method_po')?>:</b>
  <?php
  echo $transaction_id;
} else {
  ?>
  <b><?=__('payment-method_card')?>:</b>
  <?php
  echo "************$card_number";
}
?><br>
<b><?=__('payment-method_method')?>:</b> <?php
if ($payment_method == "purchase_order") {
  echo __('payment-method_method_po');
} else {
  echo 'Credit card / carte de crédit';
}
?>
