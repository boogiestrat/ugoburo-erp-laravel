<footer class="page-footer">
    <div class="font-13 text-muted"><?=date('Y')?> © <a href="<?=$_ENV['CLIENT_WEBSITE']?>" target="_blank" class="text-muted"><?=$_ENV['CLIENT_NAME']?></a></div>
    <div>
        <a class="px-3 font-13 text-muted" href="https://www.ekosys.ca" target="_blank">Ekosys - Solutions d'affaires numériques</a>
    </div>
    <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
</footer>
