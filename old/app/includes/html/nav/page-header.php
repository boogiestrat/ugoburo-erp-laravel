                    <div class="page-heading">
                        <div class="flexbox-b mb-3">
                            <span class="mr-4 static-badge badge-pink"><i class="<?=$page_icon?> font-30"></i></span>
                            <div>
                                <h5 class="font-strong"><?=$page_title?></h5>
                                <div class="text-light"><?=$page_subtitle?></div>
                            </div>
                        </div>
                    </div>