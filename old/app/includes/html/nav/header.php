<?php
    $employee = get_current_employee();
?>
<!-- START HEADER-->
<header class="header">
    <div class="page-brand">
        <a href="<?=PATH?>">
            <span class="brand"><img src="<?=PATH?>assets/img/ugoburo-logo.png" class="img-fluid" alt="<?=$_ENV['CLIENT_NAME']?>"></span>
            <span class="brand-mini">ERP</span>
        </a>
    </div>
    <div class="flexbox flex-1">
        <!-- START TOP-LEFT TOOLBAR -->
        <ul class="nav navbar-toolbar">
            <li>
                <a class="nav-link sidebar-toggler js-sidebar-toggler" href="javascript:;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
            </li>
        </ul>
        <!-- END TOP-LEFT TOOLBAR-->
        <!-- START TOP-RIGHT TOOLBAR-->
        <ul class="nav navbar-toolbar">
            <!-- <li>
                <a class="nav-link" href="<?=PATH?>ventes/soumission/nouvelle">
                    <button class="btn btn-info btn-labeled btn-labeled-left btn-icon btn-sm">
                        <span class="btn-label"><i class="fa fa-handshake-o  mr-0"></i></span>
                        Assistant de création de soumission
                    </button>
                </a>
            </li> -->
            <li class="dropdown dropdown-user">
                <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                    <span><?= $employee['first_name'] ?> <?= $employee['last_name'] ?></span>
                    <?php if ($employee['avatar']): ?>
                        <img src="<?= PATH_UPLOAD . 'employee/' . $employee['avatar'] ?>" alt="<?= $employee['first_name'] ?> <?= $employee['last_name'] ?>" />
                    <?php endif ?>
                </a>
                <div class="dropdown-menu dropdown-arrow dropdown-menu-right admin-dropdown-menu">
                    <div class="dropdown-arrow"></div>
                    <div class="dropdown-header">
                        <div class="admin-avatar">
                            <?php if ($employee['avatar']): ?>
                                <img src="<?= PATH_UPLOAD . 'employee/' . $employee['avatar'] ?>" alt="<?= $employee['first_name'] ?> <?= $employee['last_name'] ?>" />
                            <?php endif ?>
                        </div>
                        <div>
                            <h5 class="font-strong text-white"><?= $employee['first_name'] ?> <?= $employee['last_name'] ?></h5>
                        </div>
                    </div>
                    <div class="admin-menu-content">
                        <div class="d-flex justify-content-between mt-2">
                            <a class="d-flex align-items-center" href="javascript:;" id="logout">Déconnexion<i class="ti-shift-right ml-2 font-20"></i></a>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <?php
                    $langCode = 'fr';
                    if (isset($_COOKIE[$languageCookieName])) {
                        $langCode = $_COOKIE[$languageCookieName];
                    }

                    $switchTo = ($langCode == 'fr' ? 'En' : 'Fr');
                    $switchToCode = ($langCode == 'fr' ? 'en' : 'fr');
                ?>
                <a class="nav-link" style="color: #000;font-size:13px;" href="javascript:;" onclick="document.cookie = '<?=$languageCookieName?>=<?=$switchToCode?>; path=/';location.reload()">
                    <button class="btn btn-sm btn-success btn-air"><?=$switchTo?></button>
                </a>
            </li>
        </ul>
        <!-- END TOP-RIGHT TOOLBAR-->
    </div>
</header>
<!-- END HEADER-->
