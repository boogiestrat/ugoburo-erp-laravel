<!-- START SIDEBAR-->
<nav class="page-sidebar" id="sidebar" style="position:fixed">
    <div id="sidebar-collapse">
        <ul class="side-menu metismenu">
            <li class="heading"><?=$i18n->trans('menu_operation')?></li>

            <li>
                <a href="<?=PATH?>clients"><i class="sidebar-item-icon fa fa-address-card-o"></i>
                    <span class="nav-label"><?=$i18n->trans('menu_clients')?></span></a>
            </li>

            <li>
                <a href="<?=PATH?>ventes/soumissions"><i class="sidebar-item-icon fa fa-file-text-o"></i>
                    <span class="nav-label"><?=$i18n->trans('menu_quotes')?></span></a>
            </li>

        <?php if ($employee['fk_user_level_id'] > 1): ?>
            <li>
                <a href="<?=PATH?>ventes/commandes"><i class="sidebar-item-icon ti-credit-card"></i>
                    <span class="nav-label"><?=$i18n->trans('menu_orders')?></span></a>
            </li>
        <?php endif ?>
        
        <?php if ($employee['fk_user_level_id'] > 2): ?>

            <li class="heading"><?=$i18n->trans('menu_configuration')?></li>
            <li>
                <a href="<?=PATH?>employes"><i class="sidebar-item-icon fa fa-users"></i>
                    <span class="nav-label"><?=$i18n->trans('menu_users')?></span></a>
            </li>
            <li>
                <a href="<?=PATH?>services"><i class="sidebar-item-icon ti-shopping-cart"></i>
                    <span class="nav-label"><?=$i18n->trans('menu_itemsServices')?></span>
                </a>
            </li>
            <li>
                <a href="<?=PATH?>manufacturiers"><i class="sidebar-item-icon ti-package"></i>
                    <span class="nav-label"><?=$i18n->trans('menu_suppliers')?></span>
                </a>
            </li>

        <?php endif ?>
        </ul>
    </div>
</nav>
<!-- END SIDEBAR-->
