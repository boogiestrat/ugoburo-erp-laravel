<?php

function user_level($level) {
  $employeeLevel = get_current_employee()['fk_user_level_id'];
  if ($employeeLevel == $level) {
    return true;
  } else {
    return false;
  }
}

function csrf_token() {
  return md5(getenv('SESSION_CSRF_KEY').md5($_SESSION['employee']));
}

function google_autocomplete_form($prefix = '') {
  ?>
    <style>
      .pac-container {z-index: 99999}
    </style>
    <input class="form-control form-control-line" type="text" onFocus="geolocate()" name="address" id="<?=$prefix?>_address" placeholder="Adresse">
    <input type="hidden" name="city" id="<?=$prefix?>_locality" />
    <input type="hidden" name="province" id="<?=$prefix?>_administrative_area_level_1" />
    <input type="hidden" name="postal_code" id="<?=$prefix?>_postal_code" />
    <input type="hidden" name="country" id="<?=$prefix?>_country" />

    <script>
      var placeSearch, autocomplete;

      var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById("<?=$prefix.'_'?>" + component).value = '';
          document.getElementById("<?=$prefix.'_'?>" + component).disabled = false;
        }

        // Get each component of the address from the place details,
        // and then fill-in the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            if (addressType === 'postal_code') {
              $(`#<?=$prefix?>_address`).val(`${$(`#<?=$prefix?>_address`).val()}, ${val}`);
            }
            document.getElementById("<?=$prefix.'_'?>" + addressType).value = val;
          }
        }
      }

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(document.getElementById('<?=$prefix?>_address'), {types: ['geocode']});

        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        autocomplete.setFields(['address_component']);

        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle(
                {center: geolocation, radius: position.coords.accuracy});
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
  <?php
}
