<?php
namespace Ekosys\Entities;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Ekosys\Entities\Entity;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="province")
 */
class Province extends Entity{

    /**
     * @param $name
     * @return self
     */
    public static function getByName($name){

        switch ($name) {
            // case 'Newfoundland and Labrador':
            //     $name= 'Terre-Neuve-et-Labrador';
            //     break;
            // case 'British Columbia':
            //     $name='Colombie-Britannique';
            //     break;
            // case 'Northwest Territories':
            //     $name='Territoires du Nord-Ouest';
            //     break;
            // case 'New Brunswick':
            // case 'New-Brunswick':
            // case 'new-brunswick':
            // case 'new brunswick':
            // case 'newbrunswick':
            // case 'Nouveau Brunswick':
            // case 'nouveau brunswick':
            //     $name = 'Nouveau-Brunswick';
            //     break;
            // case 'Nova Scotia':
            // case 'Nova-Scotia':
            // case 'Novascotia':
            // case 'novascotia':
            // case 'nova-scotia':
            // case 'Nouvelle Écosse':
            // case 'nouvelle écosse':
            // case 'nouvelle ecosse':
            //     $name = 'Nouvelle-Écosse';
            //     break;
            // case 'Prince Edward Island':
            //     $name = 'Île-du-Prince-Édouard';
            //     break;
            // case 'British Columbia':
            //     $name = 'Colombie-Britannique';
            //     break;
            // case "Quebec";
            //     $name = "Québec";
            //     break;
            case 'Yukon Territory':
                $name='Yukon';
                break;
            case '':
                $name ='Outside of Canada';
                break;
        }


        return self::getOneBy(['provinceName'=>$name]);
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $provinceName;

    /**
     * One product has many features. This is the inverse side.
     * @Orm\OneToMany(targetEntity="Tax", mappedBy="province")
     * @var Tax[]
     */
    protected $taxes;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Province
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvinceName()
    {
        return $this->provinceName;
    }

    /**
     * @param mixed $provinceName
     * @return Province
     */
    public function setProvinceName($provinceName)
    {
        $this->provinceName = $provinceName;
        return $this;
    }

    /**
     * @return PersistentCollection
     */
    public function getTaxes()
    {
        return $this->taxes;
    }

    public function getTaxesByYear($year){

        $baseYear = $year;
        $maxCheck = 15;
        $check=0;

        $criterias = Criteria::create();
        $criterias ->where(Criteria::expr()->eq("year", $year));
        $criterias->orderBy(['priority'=>Criteria::ASC,'order'=>Criteria::ASC]);

        $taxes = $this->getTaxes()->matching($criterias);

        while(!count($taxes) && $check<=$maxCheck){

            $criterias = Criteria::create();
            $criterias ->where(Criteria::expr()->eq("year", $year--));
            $criterias->orderBy(['priority'=>Criteria::ASC,'order'=>Criteria::ASC]);
            $taxes = $this->getTaxes()->matching($criterias);
            $check++;
        }

        if(!count($taxes)){
            throw new \Exception("Could not find taxes for $baseYear");
        }



        return $taxes;
        //todo if missing get one year before taxes
    }

    /**
     * @param mixed $taxes
     * @return Province
     */
    public function setTaxes($taxes)
    {
        $this->taxes = $taxes;
        return $this;
    }

}
