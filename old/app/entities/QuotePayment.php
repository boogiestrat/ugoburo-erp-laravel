<?php
namespace Ekosys\Entities;

use Doctrine\ORM\Mapping as ORM;
use Ekosys\Entities\Entity;

/**
 * @ORM\Entity
 * @ORM\Table(name="quote_payment")
 */
class QuotePayment extends Entity{

    /**
     * @param $quoteId
     * @return self
     */
    public static function getDepositByQuoteId($quoteId){
        return self::getOneBy(['quote_id'=>$quoteId,'is_deposit'=>1]);
    }

    /**
     * @param $quoteId
     * @return self
     */
    public static function getPaymentByQuoteId($quoteId){
        return self::getOneBy(['quote_id'=>$quoteId,'is_deposit'=>0]);
    }



    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quote_id;

    /**
     * @ORM\Column(type="float")
     */
    protected $amount;

    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    protected $is_deposit;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date_created;

    /**
     * @ORM\Column(type="string")
     */
    protected $client_first_name;

    /**
     * @ORM\Column(type="string")
     */
    protected $client_last_name;

    /**
     * @ORM\Column(type="string")
     */
    protected $card_number;

    /**
     * @ORM\Column(type="string")
     */
    protected $payment_method;

    /**
     * @ORM\Column(type="string")
     */
    protected $transaction_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getQuoteId()
    {
        return $this->quote_id;
    }

    /**
     * @param mixed $quote_id
     * @return QuotePayment
     */
    public function setQuoteId($quote_id)
    {
        $this->quote_id = $quote_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return QuotePayment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisDeposit()
    {
        return $this->is_deposit;
    }

    /**
     * @param mixed $is_deposit
     * @return QuotePayment
     */
    public function setIsDeposit($is_deposit)
    {
        $this->is_deposit = $is_deposit;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $date_created
     * @return QuotePayment
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientFirstName()
    {
        return $this->client_first_name;
    }

    /**
     * @param mixed $client_first_name
     * @return QuotePayment
     */
    public function setClientFirstName($client_first_name)
    {
        $this->client_first_name = $client_first_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientLastName()
    {
        return $this->client_last_name;
    }

    /**
     * @param mixed $client_last_name
     * @return QuotePayment
     */
    public function setClientLastName($client_last_name)
    {
        $this->client_last_name = $client_last_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardNumber()
    {
        return $this->card_number;
    }

    /**
     * @param mixed $card_number
     * @return QuotePayment
     */
    public function setCardNumber($card_number)
    {
        $this->card_number = $card_number;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    /**
     * @param mixed $payment_method
     * @return QuotePayment
     */
    public function setPaymentMethod($payment_method)
    {
        $this->payment_method = $payment_method;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    /**
     * @param mixed $transaction_id
     * @return QuotePayment
     */
    public function setTransactionId($transaction_id)
    {
        $this->transaction_id = $transaction_id;
        return $this;
    }



}