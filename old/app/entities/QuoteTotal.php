<?php
namespace Ekosys\Entities;

class QuoteTotal{

    protected $_quote;
    protected $_subTotal;
    protected $_taxes;
    protected $_total;

    /**
     * @return mixed
     */
    public function getQuote()
    {
        return $this->_quote;
    }

    /**
     * @param mixed $quote
     * @return QuoteTotal
     */
    public function setQuote($quote)
    {
        $this->_quote = $quote;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubTotal()
    {
        return $this->_subTotal;
    }

    /**
     * @param mixed $subTotal
     * @return QuoteTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->_subTotal = $subTotal;
        return $this;
    }

    /**
     * @return []
     */
    public function getTaxes()
    {
        return $this->_taxes;
    }

    /**
     * @param mixed $taxes
     * @return QuoteTotal
     */
    public function setTaxes($taxes)
    {
        $this->_taxes = $taxes;
        return $this;
    }

    public function addTax($name,$value){
        $this->_taxes[$name]=$value;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        if(!$this->_total){

            $total = $this->getSubTotal();
            foreach($this->_taxes as $name=>$amount){
                $total+=$amount;
            }
            $this->_total = $total;
        }

        return $this->_total;
    }

    /**
     * @param mixed $total
     * @return QuoteTotal
     */
    public function setTotal($total)
    {
        $this->_total = $total;
        return $this;
    }

    public function getTps(){
        foreach($this->getTaxes() as $name=>$value){

            if(strpos($name,'GST')!==false || strpos($name,'TPS')!==false){
                return $value;
            }
        }
    }

    public function getTvq(){
        foreach($this->getTaxes() as $name=>$value){
            if(strpos($name,'HST')!==false || strpos($name,'TVQ')!==false){
                return $value;
            }
        }
    }


}