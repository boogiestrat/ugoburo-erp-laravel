<?php
namespace Ekosys\Entities;

abstract class Entity{

    function save(){
        global $em;
        $em->persist($this);
        $em->flush();
    }

    static function getClass(){
        $class = get_called_class();
        return $class;
    }

    static function get($id){
        global $em;
        return $em
            ->getRepository(self::getClass())
            ->find($id);

    }

    static function getBy($criterias){
        global $em;
        $respository =  $em->getRepository(self::getClass());
        return $respository->findBy($criterias);
    }

    static function getOneBy($criterias){
        global $em;
        $respository =  $em->getRepository(self::getClass());
        return $respository->findOneBy($criterias);
    }

    public function toJson(){
        return json_encode($this);
    }

    public function toArray(){
        $array = [];

        foreach($this as $key => $value){
            if(is_a($value,Entity::getClass())){
                $value = $value->getId();
            }
            $array[$key]=$value;
        }

        return $array;
    }

}