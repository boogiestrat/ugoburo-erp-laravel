<?php
namespace Ekosys\Entities;

use Doctrine\ORM\Mapping as ORM;
use Ekosys\Entities\Entity;

/**
 * @ORM\Entity
 * @ORM\Table(name="tax")
 */
class Tax extends Entity{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * One Product has One Shipment.
     * @ORM\ManyToOne(targetEntity="Province",inversedBy="features")
     * @ORM\JoinColumn(name="fk_province_id", referencedColumnName="id")
     */
    protected $province;

    /**
     * @ORM\Column(type="string")
     */
    protected $valueDescription;

      /**
       * @ORM\Column(type="float")
       */
    protected $value;

    /**
     * @ORM\Column(type="string")
     */
    protected $year;

    /**
     * @ORM\Column(type="integer")
     */
    protected $priority;

    /**
     * @ORM\Column(type="integer")
     */
    protected $order;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Tax
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param mixed $province
     * @return Tax
     */
    public function setProvince($province)
    {
        $this->province = $province;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueDescription()
    {
        return $this->valueDescription;
    }

    /**
     * @param mixed $valueDescription
     * @return Tax
     */
    public function setValueDescription($valueDescription)
    {
        $this->valueDescription = $valueDescription;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return Tax
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     * @return Tax
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     * @return Tax
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     * @return Tax
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }


    public function calculateOn($amount){
        return $amount * $this->getValue();
    }


    /**
     * @param $quote integer|object
     * @throws \JsonMapper_Exception
     * @return QuoteTotal
     */
    public static function calculate($quote){

        if(!is_array($quote)) $quote= get_quote($quote);

        $jsonMapper = new \JsonMapper();
        $phpQuote = new \Ekosys\Api\Model\Import\Quote();
        $jsonMapper->map(json_decode($quote['data']),$phpQuote);

        $quoteTotal = new QuoteTotal();
        $quoteTotal->setQuote($quote);
        $quoteTotal->setSubTotal($phpQuote->subTotal);

        $year = $phpQuote->date->format('Y');

        $provinceName = $phpQuote->billTo->address->province;

        //todo change year base on status
        $province = Province::getByName($provinceName);

        $taxes = $province->getTaxesByYear($year);

        $subTotal = $phpQuote->subTotal;

        /** @var Tax $tax */
        foreach($taxes as $tax){

            $taxAmount = $tax->calculateOn($subTotal);

            $quoteTotal->addTax($tax->getValueDescription(),$taxAmount);

            if($tax->getPriority()){
                $subTotal+=$taxAmount;
            }
        }


        return $quoteTotal;

    }


}
