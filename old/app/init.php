<?php
// load general config
require_once 'config/general.php';

// include autoloader
require_once ROOT . 'vendor/autoload.php';
// use Tracy\Debugger;

// i18n
require_once ROOT . 'app/services/i18n.class.php';
$languageCookieName = 'LANGUAGE';
$acceptedLanguage = ['fr', 'en'];

function debug($data){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

if(@$isCli){

    $_COOKIE[$languageCookieName]="en";
}

if (!isset($_COOKIE[$languageCookieName])) {
    // $browserLang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    $browserLang = 'fr';

    if (in_array($browserLang, $acceptedLanguage)) {
        $i18n = new i18n(ROOT . 'app/services/lang/lang_' . $browserLang . '.ini', $browserLang);
        setcookie($languageCookieName, $browserLang, time() + (86400 * 30), "/");
    } else {
        $i18n = new i18n(ROOT . 'app/services/lang/lang_fr.ini', 'fr');
        setcookie($languageCookieName, 'fr', time() + (86400 * 30), "/");
    }
} else {
    if (in_array($_COOKIE[$languageCookieName], $acceptedLanguage)) {
        $i18n = new i18n(ROOT . 'app/services/lang/lang_' . $_COOKIE[$languageCookieName] . '.ini', $_COOKIE[$languageCookieName]);
    } else {
        $i18n = new i18n(ROOT . 'app/services/lang/lang_fr.ini', 'fr');
        setcookie($languageCookieName, 'fr', time() + (86400 * 30), "/");
    }
}

global $i18n;
$i18n->init();
function __(){
    global $i18n;
    $args = func_get_args();
    $nArgs = func_num_args();
    $phrase = array_shift($args);
    $nArgs--;

    global $i18n;
    if(!$args) return $i18n->trans($phrase);
    return vsprintf($i18n->trans($phrase), $args);

}

function switchLocale($lang){
    global $i18n;
    $i18n = new i18n(ROOT . "app/services/lang/lang_{$lang}.ini", $lang);
    $i18n->init();
}

// load .env file (overload system env variables)
$dotenv = new Dotenv\Dotenv(ROOT);
$dotenv->overload();

// debug mode
if(isset($debug)) //Page level config
    if(!empty($debug))
        define('DEBUG', true);
    else
        define('DEBUG', false);
elseif(isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
    define('DEBUG', true);
else
    define('DEBUG', false);

if(!@$isCli) {
    // if (DEBUG) {
    //     Debugger::enable(Debugger::DEVELOPMENT, APP_ROOT . 'logs/');
    //     Debugger::$showBar = true;
    // } else {
    //     Debugger::enable(Debugger::PRODUCTION, APP_ROOT . 'logs/');
    //     Debugger::$showBar = false;
    // }
}

if (DEBUG) {
  ini_set('display_errors', 1);
  error_reporting(E_ALL & ~E_NOTICE);
} else {
  error_reporting(0);  
}

// load services
require_once APP_ROOT . 'services/autoload.php';

// load utils
require_once ROOT . 'utils/autoload.php';

// load template functions
require_once ROOT . 'app/includes/functions.php';

// Define URL variables
$dotenv->required(['DOMAIN', 'PATH', 'PATH_UPLOAD', 'PROTOCOL']);
define('DOMAIN', $_ENV['DOMAIN']);
define('PATH', $_ENV['PATH']);
define('PATH_UPLOAD', $_ENV['PATH_UPLOAD']);
define('PROTOCOL', $_ENV['PROTOCOL']);
define('BASE_URL', $_ENV['BASE_URL']);
define('LOGIN_URL', BASE_URL . 'webservices/session/login');
define('LOGOUT_URL', BASE_URL . 'webservices/session/logout');

// configure locale
$dotenv->required(['TIME_ZONE', 'LANG_PHP_CODE']);
date_default_timezone_set($_ENV['TIME_ZONE']);
setlocale(LC_ALL, $_ENV['LANG_PHP_CODE']);
### TODO À ajouter au paramètre d'application sauvegardé dans la BD ###
define('DATE_FORMAT', '%e %b %Y');
define('DATETIME_FORMAT', '%e %b %Y, %H:%M');
##################################

// MYSQL Connection
$dotenv->required(['MYSQL_HOST', 'MYSQL_NAME', 'MYSQL_USER', 'MYSQL_PASS', 'MYSQL_CHARSET']);
define('MYSQL_HOST', $_ENV['MYSQL_HOST']);
define('MYSQL_NAME', $_ENV['MYSQL_NAME']);
define('MYSQL_USER', $_ENV['MYSQL_USER']);
define('MYSQL_PASS', $_ENV['MYSQL_PASS']);
define('MYSQL_CHARSET', $_ENV['MYSQL_CHARSET']);
$db = SimpleMySQLi::getInstance();

if (isset($_ENV['TIME_ZONE']))
    $db->query("SET time_zone = '". $_ENV['TIME_ZONE'] ."'", false);

// SESSION
session_name($_ENV['SESSION_NAMESPACE']);
session_set_cookie_params(60*60*24*7, PATH); //1 week lifetime
session_start();


// CHECKLOGIN

if (isset($login_page)) {
    if (empty($_SESSION['employee']) && !$login_page) {
        header('Location: ' . PATH . 'login');
        exit();
    }
}

/* AJAX calls must be authenticated */
// if ($_SERVER['SERVER_NAME'] == getenv('DOMAIN') && $_SESSION['csrf_token'] != csrf_token() && !$login_page) {
//   unset($_SESSION["csrf_token"]);
//   unset($_SESSION["employee"]);
//   unset($_SESSION["token"]);
//   session_destroy();
//   header('Location: ' . PATH . 'login');
//   die();
// }
/* CSRF protection */
