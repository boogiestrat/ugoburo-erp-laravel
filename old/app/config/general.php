<?php

# General configurations
#####################################################################
define('ROOT', dirname(dirname(__DIR__)). '/');
define('APP_ROOT', ROOT . 'app/');
define('WEB_ROOT', ROOT . 'web/');

# Range in days to be considered new data
define('NEW_DATA_RANGE_DAYS', '7');

# Assets version to break Browser Cache
define('ASSETS_VERSION', '1');

# Avatar size
define('AVATAR_SIZE', 300);

# PDF Generation Password Protection
define('PDF_PASSWORD', '<7Zs56P=w8n7{%@*');