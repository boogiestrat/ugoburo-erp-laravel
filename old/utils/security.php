<?php

namespace Ekosys;

class Security{

    const METHOD = 'aes-256-ctr';
    const PAYMENT_URL_KEY = '0123';

    const SEARCH = ['=','+','/'];
    const REPLACE = ['[equal]','[plus]','slash'];

    /**
     * Encrypts (but does not authenticate) a message
     *
     * @param string $message - plaintext message
     * @param string $key - encryption key (raw binary expected)
     * @param boolean $encode - set to TRUE to return a base64-encoded
     * @return string (raw binary)
     */
    public static function encrypt($message, $key, $encode = false)
    {
        $nonceSize = openssl_cipher_iv_length(self::METHOD);
        $nonce = openssl_random_pseudo_bytes($nonceSize);

        $message = json_encode($message);

        $ciphertext = openssl_encrypt(
            $message,
            self::METHOD,
            $key,
            OPENSSL_RAW_DATA,
            $nonce
        );

        // Now let's pack the IV and the ciphertext together
        // Naively, we can just concatenate

        $crypted =  base64_encode($nonce.$ciphertext);
        $crypted = str_replace(self::SEARCH, self::REPLACE, $crypted);

        return $crypted;

    }

    /**
     * Decrypts (but does not verify) a message
     *
     * @param string $message - ciphertext message
     * @param string $key - encryption key (raw binary expected)
     * @param boolean $encoded - are we expecting an encoded string?
     * @return string
     */
    public static function decrypt($message, $key)
    {
        $message = str_replace(self::REPLACE, self::SEARCH, $message);

        $message = base64_decode($message,true);

        if ($message === false) {
            throw new \Exception('Encryption failure');
        }

        $nonceSize = openssl_cipher_iv_length(self::METHOD);
        $nonce = mb_substr($message, 0, $nonceSize, '8bit');
        $ciphertext = mb_substr($message, $nonceSize, null, '8bit');

        $plaintext = openssl_decrypt(
            $ciphertext,
            self::METHOD,
            $key,
            OPENSSL_RAW_DATA,
            $nonce
        );

        $plaintext = json_decode($plaintext);

        return $plaintext;
    }


}