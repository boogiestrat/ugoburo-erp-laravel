<?php

/**
 * Remplace les espaces et underscore par des traits-d'unions
 * Enleve les caracteres spéciaux
 * Enlève les - multiples
 *
 * @param String $string chaine à convertir
 * @param int $limit nombre max de caractères
 *
 * @return String
 */
function url_safe($string, $limit = 500)
{

    // Utilisation de Doctrine_Inflector::urlize qui fonctionne mieux !
    return substr(Doctrine_Inflector::urlize($string), 0, $limit);
}


function stringStartWithVowel($str)
{
    $vowels = array('a', 'e', 'i', 'o', 'u', 'y', 'h');

    if (in_array(substr($str, 0, 1), $vowels)) {
        return true;
    }

    return false;
}

function stripAccents($string)
{
    return strtr(
        $string,
        'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
        'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'
    );
}

function stripSpecialChar($string, $exceptions = '')
{
    return preg_replace(
        '/[^a-z0-9Ã Ã¡Ã¢Ã£Ã¤Ã§Ã¨Ã©ÃªÃ«Ã¬Ã­Ã®Ã¯Ã±Ã²Ã³Ã´ÃµÃ¶Ã¹ÃºÃ»Ã¼Ã½Ã¿' . $exceptions . ']/i',
        ' ',
        $string
    );
}

function cutInSpace($string, $limit, $dots = false)
{
    $string = strip_tags($string);

    if (strlen($string) < $limit) {
        return $string;
    } else {
        $result = substr($string, 0, -(strlen($string) - strrpos(substr($string, 0, $limit), ' ')));
    }

    $regEndDot = "/\.[\s]*$/i";

    if (($dots && preg_match($regEndDot, $result)) || !$dots) {
        return trim($result);
    } else {
        return trim($result) . '...';
    }
}

function br2nl($string)
{
    return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
}

function glue($glue, $str1, $str2)
{
    return $str1 . $glue . $str2;
}

function stripSmallWords($string = '')
{
    $list_of_small_words = array(
        'le',
        'la',
        'les',
        'l\'',
        'de',
        'du',
        'des',
        'd\'',
        'à',
        'aux',
        'un',
        'une',
        'mon',
        'ma',
        'mes',
        'ton',
        'ta',
        'tes',
        'sa',
        'ses',
        'son',
        'notre',
        'votre',
        'leur',
        'nos',
        'vos',
        'leurs',
    );

    foreach ($list_of_small_words as $word) {
        $var_string = (!isset($good_string)) ? 'string' : 'good_string';
        $good_string = str_replace(' ' . $word . ' ', '', ${$var_string});
    }

    return $good_string;
}

function currency_format ($number) 
{
    global $i18n;

    if($i18n->getAppliedLang()=="fr") return number_format($number, 2, ',', '&nbsp;') . '&nbsp;$ ';
    else return '$'.number_format($number, 2, '.', ',');
}

function parseCADCurrency($amount) {
    $amount = preg_replace('~\x{00a0}~siu', '', $amount); //Non breaking space
    $amount = str_replace(',', '.', $amount);
    return $amount;
}

function format_phone_number($phoneNumber)
{
    if(  preg_match( '/^[^\d]*\d?[^\d]*(\d{3})[^\d]*(\d{3})[^\d]*(\d{4})[^\d]*$/', $phoneNumber,  $matches ) )
    {
        $result = $matches[1] . '-' .$matches[2] . '-' . $matches[3];
        return $result;
    }

    return $phoneNumber;

}

function valid_email($email) {
    return !!filter_var($email, FILTER_VALIDATE_EMAIL);
}