<?php

function extract_prefixed_data ($array, $prefix){
    $prefix_len = strlen($prefix);
    foreach ($array as $key => $value) {
        if (strpos($key, $prefix) === 0) {
            $new_key = substr($key, $prefix_len);
            $data[$new_key] = $value;
        }
    }

    return $data;
}