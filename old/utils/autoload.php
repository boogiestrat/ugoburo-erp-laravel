<?php

## Needs to be automated
## include_once(WEB_ROOT . 'services/serviceName.php');

include_once ROOT . 'utils/functions.array.php';
include_once ROOT . 'utils/functions.date.php';
include_once ROOT . 'utils/functions.string.php';
include_once ROOT . 'utils/Doctrine_Inflector.php';
include_once ROOT . 'utils/SimpleMySQL/mysqli.php';
include_once ROOT . 'utils/security.php';
include_once ROOT . 'utils/Doctrine_EM.php';