<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

global $em;

// replace with mechanism to retrieve EntityManager in your app
$isDevMode = true;
$dbParams = array(
    'driver' => 'pdo_mysql',
    'host'=>getenv('MYSQL_HOST'),
    'user' => getenv('MYSQL_USER'),
    'password' => getenv('MYSQL_PASS'),
    'dbname' => getenv('MYSQL_NAME'),
    'charset'  => 'utf8',
);

$paths = array(getenv('BASE_DIR').DIRECTORY_SEPARATOR."app".DIRECTORY_SEPARATOR."entities");

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);

$namingStrategy = new \Doctrine\ORM\Mapping\UnderscoreNamingStrategy();
$config->setNamingStrategy($namingStrategy);

$entityManager = EntityManager::create($dbParams, $config);
$platform = $entityManager->getConnection()->getDatabasePlatform();
$platform->registerDoctrineTypeMapping('enum', 'string');

$em = $entityManager;

?>