<?php

function format_locale_date ($timestamp){

    if (empty($timestamp))
        return false;

    $timestamp = (is_numeric($timestamp)) ? $timestamp : strtotime($timestamp);

    return utf8_encode(strftime(DATE_FORMAT, $timestamp));

}

function format_locale_datetime ($timestamp){
    
    if (empty($timestamp))
        return false;

    $timestamp = (is_numeric($timestamp) ? $timestamp : strtotime($timestamp));
    
    return utf8_encode(strftime(DATETIME_FORMAT, $timestamp));
    
}