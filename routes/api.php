<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

 /* middleware('auth:api')-> */

Route::get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/user/search/{searchTerm?}', 'EmployesController@search');
Route::post('/user', 'EmployesController@show');

Route::post('/client/show', 'ClientsController@show');
Route::post('/client/search', 'ClientsController@search');
Route::post('/client/add', 'ClientsController@storeClient');

Route::post('/contact/store-contact', 'ClientsController@storeContact');

Route::post('/address/search', 'ClientsController@searchAddress');

Route::get('/manufacturier/discounts/{id}/{q}', 'ManufacturiersController@discountSearch');

Route::post('/quote', 'QuotesController@index');
Route::get('/quote/get', 'QuotesController@show');
Route::post('/quote/update/{id}', 'QuotesController@update');
Route::post('/quote/updatejson/{id}', 'QuotesController@updatejson');
Route::post('/quote/nextid', 'QuotesController@nextId');


Route::post('/province/taxes', 'ProvincesController@taxes');

Route::get('/env', function (Request $request) {
    return json_encode(['GOOGLE_PUBLIC_API_KEY' => env('GOOGLE_PUBLIC_API_KEY')]);
});