<?php
  use App\Http\Controllers\LanguageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');

// Route url
Route::get('/', 'DashboardController@dashboardAnalytics')->name('home')->middleware('auth');
Route::get('/home', 'DashboardController@dashboardAnalytics')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
  // Route Dashboards
  Route::get('/dashboard-analytics', 'DashboardController@dashboardAnalytics');

  // Content resources
  Route::get('/soumissions', 'QuotesController@list');
  Route::get('/soumissions/create', 'QuotesController@create')->name('quotes.create');
  Route::get('/soumissions/{id}/edit', 'QuotesController@edit')->name('quotes.edit');
  Route::get('/soumissions/{id}/delete', 'QuotesController@destroy')->name('quotes.destroy');
  Route::resource('clients','ClientsController');
  Route::resource('employes','EmployesController');

  // Order resources
  Route::get('/commandes', 'OrdersController@index');
  Route::get('/commandes/{order}/edit', 'OrdersController@edit')->name('orders.edit');
  Route::get('/commandes/{order}/delete', 'OrdersController@destroy')->name('orders.destroy');
});

// locale Route
Route::get('lang/{locale}',[LanguageController::class,'swap']);

