<?php

use App\Tax;
use Illuminate\Database\Seeder;

class TaxesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tax::truncate();
        Tax::create(['fk_province_id' => 3, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 5, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 4, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 7, 'value_description' => '15% HST - 839049913', 'value' => 0.15, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 10, 'value_description' => '15% HST - 839049913', 'value' => 0.15, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 8, 'value_description' => '15% HST - 839049913', 'value' => 0.15, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 12, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 11, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 1, 'value_description' => '13% HST - 839049913', 'value' => 0.13, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 6, 'value_description' => '15% HST - 839049913', 'value' => 0.15, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 2, 'value_description' => '5% TPS - 839049913', 'value' => 0.05, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 9, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 13, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2019', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 2, 'value_description' => '9,975% TVQ - 1217187512', 'value' => 0.09975, 'year' => '2019', 'priority' => 0, 'order' => 1]);
    	Tax::create(['fk_province_id' => 3, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2008', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 5, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2008', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 4, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2008', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 7, 'value_description' => '15% HST - 839049913', 'value' => 0.15, 'year' => '2016', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 10, 'value_description' => '15% HST - 839049913', 'value' => 0.15, 'year' => '2016', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 8, 'value_description' => '15% HST - 839049913', 'value' => 0.15, 'year' => '2016', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 12, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2008', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 11, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2008', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 1, 'value_description' => '13% HST - 839049913', 'value' => 0.13, 'year' => '2016', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 6, 'value_description' => '15% HST - 839049913', 'value' => 0.15, 'year' => '2017', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 2, 'value_description' => '5% TPS - 839049913', 'value' => 0.05, 'year' => '2008', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 9, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2008', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 13, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2008', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 2, 'value_description' => '7,5% TVQ - 1217187512', 'value' => 0.075, 'year' => '2008', 'priority' => 1, 'order' => 1]);
    	Tax::create(['fk_province_id' => 2, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2011', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 2, 'value_description' => '8,5% TVQ - 1217187512', 'value' => 0.085, 'year' => '2011', 'priority' => 1, 'order' => 1]);
    	Tax::create(['fk_province_id' => 2, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2012', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 2, 'value_description' => '9,5% TVQ - 1217187512', 'value' => 0.095, 'year' => '2012', 'priority' => 1, 'order' => 1]);
    	Tax::create(['fk_province_id' => 2, 'value_description' => '5% GST - 839049913', 'value' => 0.05, 'year' => '2013', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 2, 'value_description' => '9,975% TVQ - 1217187512', 'value' => 0.09975, 'year' => '2013', 'priority' => 0, 'order' => 1]);
    	Tax::create(['fk_province_id' => 1, 'value_description' => '13% HST - 839049913', 'value' => 0.13, 'year' => '2013', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 6, 'value_description' => '14% HST - 839049913', 'value' => 0.14, 'year' => '2013', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 7, 'value_description' => '13% HST - 839049913', 'value' => 0.13, 'year' => '2013', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 8, 'value_description' => '15% HST - 839049913', 'value' => 0.15, 'year' => '2013', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 10, 'value_description' => '13% HST - 839049913', 'value' => 0.13, 'year' => '2013', 'priority' => 0, 'order' => 0]);
    	Tax::create(['fk_province_id' => 3, 'value_description' => '7% PST - 1143-4773', 'value' => 0.07, 'year' => '2020', 'priority' => 0, 'order' => 1]);
    }
}
