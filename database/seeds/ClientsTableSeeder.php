<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Client::class, 50)->create()->each(function ($client) {
            $client->contacts()->saveMany(factory(App\Contact::class, 2)->make());
            $client->addresses()->saveMany(factory(App\Address::class, 2)->make());
        });
    }
}
