<?php

use App\Province;
use Illuminate\Database\Seeder;

class ProvincesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Province::truncate();
        Province::create(['province_name' => 'Ontario']);
        Province::create(['province_name' => 'Quebec']);
        Province::create(['province_name' => 'British Columbia']);
        Province::create(['province_name' => 'Manitoba']);
        Province::create(['province_name' => 'Alberta']);
        Province::create(['province_name' => 'Prince Edward Island']);
        Province::create(['province_name' => 'New Brunswick']);
        Province::create(['province_name' => 'Nova Scotia']);
        Province::create(['province_name' => 'Saskatchewan']);
        Province::create(['province_name' => 'Newfoundland and Labrador']);
        Province::create(['province_name' => 'Nunavut']);
        Province::create(['province_name' => 'Northwest Territories']);
        Province::create(['province_name' => 'Yukon']);
        Province::create(['province_name' => 'Outside of Canada']);
    }
}
