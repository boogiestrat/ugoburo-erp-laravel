<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'first_name' => 'Denis',
            'last_name' => 'Ricard',
            'email' => 'boogiestrat@gmail.com',
            'address' => '136 Jacques-Plante',
            'city' => 'Repentigny',
            'province' => 'QC',
            'country' => 'Canada',
            'postal_code' => 'J5Y 0A8',
            'level_id' => 3,
        ]);
    }
}
