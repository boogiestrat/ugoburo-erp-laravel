<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {

    return [
        'fk_client_id' => 1,
        'titre' => 'Titre',
        'address' => $faker->streetAddress,
        'city' => $faker->city,
        'province' => 'QC',
        'country' => 'Canada',
        'postal_code' => $faker->postcode,
        'is_default_facturation' => $faker->boolean($chanceOfGettingTrue = 50),
        'is_default_shipping' => $faker->boolean($chanceOfGettingTrue = 50)
    ];
});
