<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'entreprise' => $faker->company,
        'is_active' => 1,
        'client_type_id' => 1,
        'employe_id' => 1,
        'notes' => 'This is a note',
        'created_at' => now()->subDays(rand(1, 180))
    ];
});
