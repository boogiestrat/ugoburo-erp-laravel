<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_item', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('fk_order_id');
            $table->string('SKU', 255);
            $table->string('title', 255);
            $table->double('price');
            $table->double('quantity');
            $table->text('description');
            $table->double('priceParameterValue');
            $table->double('expeditionPrice');
            $table->text('expeditionPriceMethod');
            $table->double('expeditionPercent');
            $table->text('pictureURL')->nullable();
            $table->integer('profitMethod');
            $table->string('discountCode', 255);
            $table->string('discountName', 255);
            $table->string('discountParts', 255)->comment('number split by comma \',\' max 5');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item');
    }
}
