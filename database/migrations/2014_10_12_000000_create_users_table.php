<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('address', 60)->nullable();
            $table->string('city', 60)->nullable();
            $table->string('province', 20)->nullable();
            $table->string('country', 20)->nullable();
            $table->string('postal_code', 10)->nullable();
            $table->string('mobile', 20)->nullable();
            $table->string('avatar', 120)->nullable();
            $table->integer('level_id')->default(1);
            $table->timestamp('last_connection')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
