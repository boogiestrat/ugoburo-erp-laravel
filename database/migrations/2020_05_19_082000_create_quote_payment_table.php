<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_payment', function (Blueprint $table) {
            $table->id();
            $table->integer('quote_id');
            $table->double('amount');
            $table->tinyInteger('is_deposit')->nullable();
            $table->dateTime('date_created');
            $table->string('client_first_name', 255);
            $table->string('client_last_name', 255);
            $table->string('card_number', 255);
            $table->string('payment_method', 255);
            $table->string('transaction_id', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_payment');
    }
}
