<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->id();
            $table->decimal('sub_total', 10, 2)->nullable();
            $table->string('discount_method', 25)->nullable()->default('percent');
            $table->decimal('discount_param', 10, 2)->nullable();
            $table->decimal('tps', 10, 2)->nullable();
            $table->decimal('tvq', 10, 2)->nullable();
            $table->decimal('total', 10, 2)->nullable();
            $table->decimal('deposit', 10, 2)->nullable();
            $table->decimal('balance', 10, 2)->nullable();
            $table->text('data_config')->nullable();
            $table->text('data_items')->nullable();
            $table->text('data')->nullable();
            $table->tinyInteger('fk_status_id')->nullable()->default(1);
            $table->integer('fk_client_id')->nullable();
            $table->integer('fk_beneficiary_id')->nullable();
            $table->integer('fk_type_id')->nullable();
            $table->integer('fk_payment_mode_id')->nullable();
            $table->integer('created_fk_employee_id')->nullable();
            $table->integer('modified_fk_employee_id')->nullable();
            $table->dateTime('started')->nullable();
            $table->dateTime('creation')->nullable();
            $table->timestamp('last_modification')->nullable()->default(\DB::raw('current_timestamp() ON UPDATE CURRENT_TIMESTAMP'));
            $table->string('language', 10)->nullable();
            $table->text('data_sections')->nullable();
            $table->string('title', 255)->nullable();
            $table->text('additionnal_note')->nullable();
            $table->text('livraison_note')->nullable();
            $table->dateTime('valid_until')->nullable();
            $table->string('expedition_and_installation_method', 25)->nullable()->default('percent');
            $table->double('expedition_and_installation_param')->nullable();
            $table->text('data_address')->nullable();
            $table->text('data_clauses')->nullable();
            $table->string('number', 100)->nullable();
            $table->text('data_specialist')->nullable();
            $table->text('data_organisation')->nullable();
            $table->integer('fk_bill_to_id')->nullable();
            $table->text('data_bill_to')->nullable();
            $table->integer('fk_ship_to_id')->nullable();
            $table->text('data_ship_to')->nullable();
            $table->text('data_additionnal_fees')->nullable();
            $table->text('additionnal_note_interne')->nullable();
            $table->text('livraison_note_interne')->nullable();
            $table->unique('number', 'number');
            $table->index(['fk_status_id', 'fk_client_id', 'fk_beneficiary_id', 'fk_type_id', 'fk_payment_mode_id', 'created_fk_employee_id', 'modified_fk_employee_id', 'language', 'number', 'fk_bill_to_id', 'fk_ship_to_id'], 'idx');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
