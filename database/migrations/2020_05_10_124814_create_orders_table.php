<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->double('gt_base');
            $table->string('order_source', 45)->nullable();
            $table->string('magento2_order_id', 45)->nullable();
            $table->string('magento2_customer_id', 45)->nullable();
            $table->integer('fk_client_id')->nullable();
            $table->double('discount');
            $table->double('fees');
            $table->unsignedInteger('fk_status_id');
            $table->timestamps();
            $table->dateTime('purchased_on')->default(\DB::raw('current_timestamp()'));
            $table->unsignedInteger('created_fk_employee_id');
            $table->string('order_number', 45)->nullable();
            $table->integer('fk_order_id')->nullable();
            $table->text('carrier_assigned')->nullable();
            $table->text('acknow_no')->nullable();
            $table->text('tracking_no')->nullable();
            $table->string('installation', 45)->nullable();
            $table->text('estimated_delivery_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
