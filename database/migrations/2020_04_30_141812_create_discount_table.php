<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('fk_manufacturier_id')->nullable();
            $table->string('code', 255)->nullable();
            $table->string('title', 255)->nullable();
            $table->string('values', 255)->nullable();
            $table->text('codeimport')->nullable();
            $table->index(['fk_manufacturier_id', 'code'], 'idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount');
    }
}
