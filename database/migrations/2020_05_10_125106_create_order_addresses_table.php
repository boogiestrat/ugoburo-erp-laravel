<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_addresses', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['ship_to', 'bill_to'])->default('bill_to');
            $table->text('formatted')->nullable();
            $table->string('firstname', 255);
            $table->string('email', 255);
            $table->string('lastname', 255);
            $table->string('address', 255);
            $table->string('city', 255);
            $table->string('province', 255);
            $table->string('postal_code', 10);
            $table->string('country', 255);
            $table->string('entreprise', 255);
            $table->unsignedInteger('fk_order_id');
            $table->integer('fk_contact_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_addresses');
    }
}
