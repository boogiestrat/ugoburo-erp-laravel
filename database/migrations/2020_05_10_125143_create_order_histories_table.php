<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_histories', function (Blueprint $table) {
            $table->id();
            $table->string('acknow_no', 255);
            $table->string('tracking_no', 255);
            $table->unsignedInteger('fk_status_id');
            $table->unsignedInteger('fk_order_id');
            $table->unsignedInteger('fk_created_by_id');
            $table->unsignedInteger('fk_shipping_by_id');
            $table->timestamps();
            $table->text('comment');
            $table->tinyInteger('is_customer_notified')->default(0);
            $table->tinyInteger('is_visible_on_front')->default(0);
            $table->tinyInteger('is_exported_to_quickbooks')->default(0);
            $table->text('estimated_delivery_date');
            $table->text('filepath')->nullable();
            $table->text('comment_fr')->nullable();
            $table->text('comment_en')->nullable();
            $table->text('filename')->nullable();
            $table->text('filesize')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_histories');
    }
}
