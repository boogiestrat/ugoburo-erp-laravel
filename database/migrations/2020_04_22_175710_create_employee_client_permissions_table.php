<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeClientPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_client_permissions', function (Blueprint $table) {
            $table->id();
            $table->integer('fk_employee_id');
            $table->integer('fk_client_id');
            $table->tinyInteger('read_data');
            $table->tinyInteger('update_data');
            $table->tinyInteger('delete_data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_client_permissions');
    }
}
