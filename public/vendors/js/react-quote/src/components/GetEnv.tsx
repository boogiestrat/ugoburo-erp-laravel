import React from 'react';
import { getBaseUrl } from '../services';

interface IGetEnvProps {
  readonly children?: any;
}

interface IGetEnvState {
  readonly env: any;
  readonly loading: boolean;
}

export default class GetEnv extends React.Component<IGetEnvProps> {
  public state: IGetEnvState = {
    env: {},
    loading: true,
  };

  public componentDidMount() {
    fetch(getBaseUrl() + 'webservices/env/list')
      .then(res => res.json())
      .then(json => this.setState({ env: json, loading: false }))
      .catch(() => this.setState({ loading: false }));
  }

  public render() {
    if (this.state.loading) {
      return <div>Chargement...</div>
    } else {
      return this.props.children && this.props.children({ ...this.props, ...this.state });
    }
  }
}
