import React from 'react';
import _ from 'lodash';

import UBQuote from '../models/UBQuote';

import styled from 'styled-components';
import { IDiscount } from '../entities';
import IIntextInput from './Inputs/IntextInput';
import InlineTextInput from './Inputs/InlineTextInput';
import { moneyFormat, percentFormat, numberParse } from '../services';

const Container = styled.div`
  .ibox-title {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;

    .la {
      font-size: 20pt;
    }
  }
`

const Body = styled.div`
  transition: all 0.3s ease-in-out;
  height: 100%;

  &.closed {
    height: 0px;
    overflow: hidden;
    padding-top: 0px;
    padding-bottom: 0px;
  }
`

const LastRow = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

const Stats = styled.div`

`

const StatsMoneyValueCol = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 10px;
  text-align: right;

  span:first-child {
    font-weight: 700;
  }
`

interface ISuppliersProps {
  readonly quote: UBQuote;
  readonly updateQuote: (param: string, value: any) => Promise<any>;
  readonly availableDiscounts: Array<IDiscount>;
  readonly updateDiscounts: (discounts: Array<IDiscount>) => void;
  readonly loadPossibleDiscount: (inputValue: string, callback: Function) => void;
  readonly deleteDiscount: (discountIndex: number, discountName: string, callback: Function) => void;
  readonly confirmationWithCallback: (callback: Function, msg: string) => void;
}

interface ISupplierState {
  readonly openned: boolean;
  readonly availableDiscounts: Array<IDiscount>;
  readonly applyTo: Array<{ readonly index: number; readonly value: string; }>;
}

export default class Suppliers extends React.Component<ISuppliersProps> {
  public state: ISupplierState = {
    openned: false,
    availableDiscounts: [],
    applyTo: []
  }

  public componentDidMount() {
    if (this.props.availableDiscounts.length > 0 && this.state.availableDiscounts.length === 0) {
      this.setState({ availableDiscounts: this._getFilteredAvailabledDiscountsInProps(), applyTo: _.map(this._getFilteredAvailabledDiscountsInProps(), (ad, adIndex) => ({ index: adIndex, value: '-1' })) });
    }
  }

  private _getFilteredAvailabledDiscountsInProps = () => this.props.availableDiscounts;

  private _loadDiscounts = () => {
    this.setState({ availableDiscounts: this._getFilteredAvailabledDiscountsInProps(), applyTo: _.map(this._getFilteredAvailabledDiscountsInProps(), (ad, adIndex) => ({ index: adIndex, value: '-1' })) });
  };

  private _itemIsTouchedByAvailableDiscount = (itemDiscount: IDiscount, sectionIndex: Number, applyTo: string): boolean => {
    let returnValue: boolean = false;

    if (applyTo === 'all') {
      returnValue = true;
    }

    if (applyTo === 'empty') {
      returnValue = itemDiscount.name === '';
    }

    if (applyTo.substr(0, 2) === 's-' && Number(applyTo.substr(2)) === sectionIndex) {
      returnValue = true;
    }

    return returnValue;
  }

  private _apply = (discountIndex: number, applyTo: string) => {
    const quote = this.props.quote.get();
    this.props.updateDiscounts(this.state.availableDiscounts);

    this.props.updateQuote(
      'sections',
      _.map(quote.sections, (section, sectionIndex) => {
        return {
          ...section,
          items: _.map(section.items, item => {
            if (this._itemIsTouchedByAvailableDiscount(item.discount, sectionIndex, applyTo)) {
              return {
                ...item,
                discount: this.state.availableDiscounts[discountIndex],
              }
            }

            return item;
          })
        };
      })
    );
  }

  private _addEmptySupplier = () => {
    if (!this.state.openned) {
      this.setState({ openned: true });
    }

    if (this.state.availableDiscounts.filter(dis => dis.name === '').length > 0) {
      return;
    }

    this.setState({ availableDiscounts: [ ...this.state.availableDiscounts.filter(dis => dis.name !== ''), { code: '', name: '', parts: [0, 0, 0, 0, 0] } as IDiscount ], applyTo: [...this.state.applyTo, { index: this.state.applyTo.length, value: '-1' } ] }, () => {
      this.props.updateQuote('availableDiscounts', _.uniqBy(this.state.availableDiscounts, dis => dis.name));
    });
  }

  public render() {
    return (
      <Container>
        <div className="ibox">
          <div className="ibox-head">
            <div className="ibox-title">
              <h3>Fournisseurs</h3>
              <div>
                <i style={{ cursor: 'pointer', marginRight: 10 }} className={`la la-plus-circle`} onClick={() => this._addEmptySupplier()}></i>
                <i style={{ cursor: 'pointer' }} className={`la la-chevron-${this.state.openned ? 'up' : 'down'}`} onClick={() => this.setState({ openned: !this.state.openned })}></i>
              </div>
            </div>
          </div>
          <Body className={`ibox-body ${!this.state.openned ? 'closed' : ''}`}>

                {_.map(this.state.availableDiscounts, (discount, discountIndex) => {
                  return (
                    _.map([0, 1], i => (
                      <table className={`table table-bordered ${this.state.availableDiscounts.length == 0 ? 'custom-display-hidden' : '' }`}>
                        <thead className="thead-default thead-lg">
                          {i === 0 &&
                            <tr>
                              <th>Nom du fournisseur</th>
                              <th>ESC 1</th>
                              <th>ESC 2</th>
                              <th>ESC 3</th>
                              <th>ESC 4</th>
                              <th>ESC 5</th>
                              <th>Enregistrement</th>
                              <th>Assigner à</th>
                              <th>Actions</th>
                            </tr>
                          }
                        </thead>
                        <tbody>
                          {i === 0 &&
                            <tr key={`discount-row-${discountIndex}`}>
                              <td>
                                <InlineTextInput
                                  forceValue
                                  autofocus={true}
                                  loadOptions={this.props.loadPossibleDiscount}
                                  autosuggestCallback={async (item: any) => {
                                    this.setState({ availableDiscounts: _.map(this.state.availableDiscounts, (dis, disIndex) => {
                                      if (disIndex !== discountIndex) {
                                        return dis;
                                      }

                                      return { code: item.code, name: item.title, parts: JSON.parse(item.values) } as IDiscount;
                                    }) }, async () => await this.props.updateQuote('availableDiscounts', this.state.availableDiscounts));
                                  }}
                                  value={discount.name}
                                  title=""
                                  placeholder=""
                                  noLabel={true}
                                  onChange={value => {
                                    this.setState({ availableDiscounts: _.map(this.state.availableDiscounts, (avDis, avDisIndex) => {
                                      if (avDisIndex !== discountIndex) {
                                        return avDis;
                                      }

                                      return { ...avDis, name: value };
                                    }) });
                                  }}
                                />
                              </td>
                              {_.map(discount.parts, (part, partIndex) => (
                                <td key={`discount-${discountIndex}-parts}`}>
                                  <InlineTextInput
                                    style={{
                                      container: {
                                        display: 'flex',
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                      },
                                      input: {
                                        width: 40,
                                        marginRight: '0px!important',
                                        paddingRight: 0,
                                      }
                                    }}
                                    forceValue
                                    key={`discount-${discountIndex}-part-${partIndex}`}
                                    value={part.toString()}
                                    title=""
                                    placeholder=""
                                    noLabel={true}
                                    inputWidth={30}
                                    noPadding={true}
                                    onChange={(value) => {
                                      this.setState({ availableDiscounts: _.map(this.state.availableDiscounts, (dis, disIndex) => {
                                        if (disIndex !== discountIndex) {
                                          return dis;
                                        }

                                        return { ...dis, parts: _.map(dis.parts, (part, partInStateIndex) => {
                                          if (partInStateIndex !== partIndex) {
                                            return part;
                                          }

                                          return value;
                                        })}
                                      })}, () => this.props.updateQuote('availableDiscounts', this.state.availableDiscounts));
                                    }}
                                  />
                                </td>
                              ))}
                              <td>
                                <InlineTextInput
                                  value={discount.code}
                                  title=""
                                  placeholder=""
                                  noLabel={true}
                                  onChange={value => this.setState({ availableDiscounts: _.map(this.state.availableDiscounts, (discount, disIndex) => {
                                    if (discountIndex !== disIndex) {
                                      return discount;
                                    }

                                    return { ...discount, code: value };
                                  }) }, () => this.props.updateQuote('availableDiscounts', this.state.availableDiscounts))}
                                />
                              </td>
                              <td style={{ display: 'flex', flexDirection: 'row' }}>
                                {this.state.applyTo[discountIndex] &&
                                  <select
                                    value={this.state.applyTo[discountIndex].index === discountIndex ? this.state.applyTo[discountIndex].value : '-1'}
                                    className="form-control"
                                    onChange={e => {
                                      this._apply(this.state.applyTo[discountIndex].index, e.currentTarget.value);
                                      // this.setState({ applyTo: [...this.state.applyTo.slice(0, discountIndex), { index: discountIndex, value: e.currentTarget.value }, ...this.state.applyTo.slice(discountIndex + 1)] });
                                  }}>
                                    <option value="-1"></option>
                                    <option value="all">Tous</option>
                                    <option value="empty">Vide</option>
                                    <optgroup label="Sections">
                                      {_.map(this.props.quote.getSections(), (section, sectionIndex) => {
                                        return (
                                          <option value={`s-${sectionIndex}`}>{section.title}</option>
                                        )
                                      })}
                                    </optgroup>
                                  </select>
                                }
                              </td>
                              <td>
                                <a className="font-20 mx-2" href="javascript:;" onClick={() => {
                                  this.props.confirmationWithCallback(() => {
                                    this.props.deleteDiscount(discountIndex, discount.name, this._loadDiscounts);
                                  }, 'Voulez-vous supprimer ce fournisseur?');
                                }}>
                                  <i className="ti-trash">
                                    {process.env.NODE_ENV === 'development' &&
                                      'X'
                                    }
                                  </i>
                                </a>
                              </td>
                            </tr>
                          }

                          {i === 1 &&
                            <tr style={{ background: '#f2f2f2' }}>
                              <td colSpan={9}>
                                <div style={{ display: 'flex', flexDirection: 'row' }}>
                                  <StatsMoneyValueCol>
                                    <span>Total liste</span>
                                    <span>{moneyFormat(this.props.quote.calculateTotalListPriceBySupplier(discount.name))}</span>
                                  </StatsMoneyValueCol>
                                  <StatsMoneyValueCol>
                                    <span>Total vendant</span>
                                    <span>{moneyFormat(this.props.quote.calculateListPriceBySupplier(discount.name))}</span>
                                  </StatsMoneyValueCol>
                                  <StatsMoneyValueCol>
                                    <span>Total coûtant</span>
                                    <span>{moneyFormat(this.props.quote.calculateTotalCostBySupplier(discount.name))}</span>
                                  </StatsMoneyValueCol>
                                  <StatsMoneyValueCol>
                                    <span>Profits</span>
                                    <span>{moneyFormat(this.props.quote.calculateProfitBySupplier(discount.name))}</span>
                                  </StatsMoneyValueCol>
                                  <StatsMoneyValueCol>
                                    <span>Profit brut</span>
                                    <span>{this.props.quote.calculateListPriceBySupplier(discount.name) === 0 ? 'N/A' : percentFormat(((this.props.quote.calculateListPriceBySupplier(discount.name) - this.props.quote.calculateTotalCostBySupplier(discount.name)) / this.props.quote.calculateListPriceBySupplier(discount.name)) * 100)}</span>
                                  </StatsMoneyValueCol>
                                </div>
                              </td>
                            </tr>
                          }
                        </tbody>
                      </table>
                    ))
                  )
                })}

            <LastRow>
              <div style={{ display: 'flex', flexDirection: 'row' }}>
                <StatsMoneyValueCol>
                  <span>Total liste</span>
                  <span>{moneyFormat(this.props.quote.calculateTotalListPriceBySupplier())}</span>
                </StatsMoneyValueCol>
                <StatsMoneyValueCol>
                  <span>Total vendant</span>
                  <span>{moneyFormat(this.props.quote.calculateListPriceBySupplier())}</span>
                </StatsMoneyValueCol>
                <StatsMoneyValueCol>
                  <span>Total coûtant</span>
                  <span>{moneyFormat(this.props.quote.calculateTotalCostBySupplier())}</span>
                </StatsMoneyValueCol>
                <StatsMoneyValueCol>
                  <span>Profits</span>
                  <span>{moneyFormat(this.props.quote.calculateProfitBySupplier())}</span>
                </StatsMoneyValueCol>
                <StatsMoneyValueCol>
                  <span>Profit brut</span>
                  <span>{Number(((this.props.quote.calculateListPriceBySupplier() - this.props.quote.calculateTotalCostBySupplier()) / this.props.quote.calculateListPriceBySupplier()) * 100).toFixed(2)} %</span>
                </StatsMoneyValueCol>
              </div>
              <div>
                <button onClick={() => this._addEmptySupplier()} className="btn btn-outline-success btn-air btn-rounded btn-sm">
                  <span className="btn-icon">
                    <i className="la la-plus-circle"></i>
                    Ajouter un fournisseur
                  </span>
                </button>
              </div>
            </LastRow>
          </Body>
        </div>
      </Container>
    );
  }
}
