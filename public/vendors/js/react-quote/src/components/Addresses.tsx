import React from 'react';

// @ts-ignore
import AsyncCreatableSelect from 'react-select/async-creatable';
import ReactGoogleMapLoader from 'react-google-maps-loader'
import ReactGooglePlacesSuggest from 'react-google-places-suggest'
import styled from 'styled-components';

import _ from 'lodash';

import { IAddress, AddressTypeEnum, ISelectValue, IClient, IContact, IAddressInQuote } from '../entities';
import { decomposeAddressFromGoogle, getBaseUrl } from '../services';
import InlineTextInput from './Inputs/InlineTextInput';
import GetEnv from './GetEnv';
import UBQuote from '../models/UBQuote';
import UBContact from '../models/UBContact';
import UBClient from '../models/UBClient';
import UBAddress from '../models/UBAddress';
import IntextInput from './Inputs/IntextInput';

interface IAddressesProps {
  readonly quote: UBQuote;
  readonly onEditing: (isEditing: boolean) => void;
  readonly updateAddress: (address: IAddress, param: AddressTypeEnum) => Promise<any>;
  readonly updateAddressInQuote: (addressInQuote: IAddressInQuote, param: AddressTypeEnum) => {};
  readonly handleSave: (newContact: IClient) => Promise<any>;
  readonly setAddress: (address: IAddress | null, param: AddressTypeEnum) => Promise<any>;
  readonly addAddressToClientAndSelect: (address: IAddress, client_id: number) => Promise<any>;
  readonly addContactToClientAndSelect: (contact: IContact, client_id: number) => Promise<any>;
  readonly confirmationWithCallback: (callback: Function, msg: string) => void;
}

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  span {
    margin-right: 5px;
  }
`

export default class Addresses extends React.Component<IAddressesProps> {
  private _loadClientsOptions = (inputValue: string, callback: Function) => {
    UBClient.searchClient(inputValue).then(clients => {
      callback(_.map(clients, client => ({
        data: client,
        label: client.title,
        value: client.id,
      } as ISelectValue)))
    });
  };

  private _setAddress = (address: IAddress | null, param: AddressTypeEnum) => {
    if (param === AddressTypeEnum.BILL_TO) {
      if (!this.props.quote.get().shipTo) {
        this.props.setAddress(address, AddressTypeEnum.SHIP_TO);
      }
    }

    return this.props.setAddress(address, param);
  }

  public render() {
    return (
      <div className="addresses-container">
        <Address
          title="Facturation"
          loadClientsOptions={this._loadClientsOptions}
          address={this.props.quote.get().billTo}
          handleSave={this.props.handleSave}
          param={AddressTypeEnum.BILL_TO}
          updateAddress={this.props.updateAddress}
          setAddress={this._setAddress}
          onEditing={this.props.onEditing}
          updateAddressInQuote={this.props.updateAddressInQuote}
          addAddressToClientAndSelect={this.props.addAddressToClientAndSelect}
          addContactToClientAndSelect={this.props.addContactToClientAndSelect}
          confirmationWithCallback={this.props.confirmationWithCallback}
        />
        <Address
          title="Livraison"
          loadClientsOptions={this._loadClientsOptions}
          address={this.props.quote.get().shipTo}
          handleSave={this.props.handleSave}
          param={AddressTypeEnum.SHIP_TO}
          updateAddress={this.props.updateAddress}
          setAddress={this._setAddress}
          onEditing={this.props.onEditing}
          updateAddressInQuote={this.props.updateAddressInQuote}
          addAddressToClientAndSelect={this.props.addAddressToClientAndSelect}
          addContactToClientAndSelect={this.props.addContactToClientAndSelect}
          confirmationWithCallback={this.props.confirmationWithCallback}
        />
      </div>
    );
  }
}

const EMPTY_ADDRESS: IAddress = { city: '', country: '', postal_code: '', province: '', address: '', id: -1 } as IAddress;

interface IAddressProps {
  readonly address: IAddressInQuote;
  readonly loadClientsOptions: Function;
  readonly updateAddress: (address: IAddress, param: AddressTypeEnum) => Promise<any>;
  readonly updateAddressInQuote: (addressInQuote: IAddressInQuote, param: AddressTypeEnum, stillEditing?: boolean) => {};
  readonly setAddress: (address: IAddress | null, param: AddressTypeEnum) => Promise<any>;
  readonly handleSave: (client: IClient) => Promise<any>;
  readonly title: string;
  readonly param: AddressTypeEnum;
  readonly onEditing: (isEditing: boolean) => void;
  readonly addAddressToClientAndSelect: (address: IAddress, client_id: number) => Promise<any>;
  readonly addContactToClientAndSelect: (contact: IContact, client_id: number) => Promise<any>;
  readonly confirmationWithCallback: (callback: Function, msg: string) => void;
}

interface IAddressState {
  readonly isEditing: boolean;
  readonly isSelecting: boolean;
  readonly loading: boolean;
  readonly search: string;
  readonly value: string;
  readonly newContact: IContact;
  readonly currentlyEditing: AddressTypeEnum;
  readonly isLoading: boolean;
  readonly addressInQuote: IAddressInQuote;
  readonly formIsInvalid: boolean;
  readonly isCreatingNewAddress: boolean;
  readonly newAddress: IAddress;
  readonly isCreatingNewContact: boolean;
}

export class Address extends React.Component<IAddressProps> {
  public state: IAddressState = {
    isSelecting: false,
    loading: false,
    isEditing: false,
    search: '',
    value: '',
    currentlyEditing: AddressTypeEnum.BILL_TO,
    newContact: UBContact.createEmptyContact(),
    isLoading: false,
    addressInQuote: {
      client: { ...UBClient.createEmptyClient() }
    },
    formIsInvalid: false,
    isCreatingNewAddress: false,
    isCreatingNewContact: false,
    newAddress: EMPTY_ADDRESS,
  }

  private _handleCreate = (value: string) => {
    this.setState({ addressInQuote: { ...this.state.addressInQuote, client: { ...this.state.addressInQuote.client, title: value } } }, () => {
      this.props.onEditing(true);
      this.setState({ isEditing: true });
    });
  }

  private _getEditingStyle = (): React.CSSProperties => {
    const sideBar = document.getElementById('sidebar') as HTMLElement;
    const sideBarWidth = sideBar ? sideBar.clientWidth : 0;

    return (this.state.isEditing || this.state.isSelecting) ? ({
      zIndex: 11,
      backgroundColor: 'transparent',
      padding: 20,
      position: 'fixed',
      width: window.innerWidth - 200 - sideBarWidth,
      height: window.innerHeight - 200,
      top: 100,
      left: 100 + sideBarWidth,
    }) : {}
  }

  private _newClientIsValid = () => {
    if (!this.state.addressInQuote.client) {
      return false;
    }

    if (!this.state.addressInQuote.contact) {
      return false;
    }

    if (!this.state.addressInQuote.address) {
      return false;
    }

    if (this.state.addressInQuote.client.title === '') {
      return false;
    }

    if (this.state.addressInQuote.contact.first_name === '') {
      return false;
    }

    if (this.state.addressInQuote.contact.last_name === '') {
      return false;
    }

    if (this.state.addressInQuote.contact.phone === '') {
      return false;
    }

    return true;
  }

  private _handleSave = () => {
    if (this._newClientIsValid()) {
      this.setState({ formIsInvalid: true, isEditing: false, isSelecting: false }, () => {
        this.props.updateAddressInQuote(this.state.addressInQuote, this.props.param);
        this.props.onEditing(false);
      });
    } else {
      this.setState({ formIsInvalid: true });
    }
  }

  private _handleInputChange = (e: string) => {
    this.setState({search: e, value: e});
  }

  private _handleSelectSuggest = (suggest: any, save: boolean = false) => {
    const addressFromGoogle: IAddress = decomposeAddressFromGoogle(suggest.address_components);
    const { address, city, province, country, postal_code } = addressFromGoogle;
    const newAddress = { address: `${address}, ${city}, ${province}, ${country}, ${postal_code}`, city, province, country, postal_code } as IAddress;

    this.setState({ newAddress }, () => {
      if (save) {
        this.props.address.client ? this.props.addAddressToClientAndSelect(this.state.newAddress, this.props.address.client.id).then(({ id }) => {
          this.setState({
            addressInQuote: {
              ...this.state.addressInQuote,
              address: { ...this.state.newAddress, id },
              client: {
                ...this.state.addressInQuote.client,
                addresses: this.state.addressInQuote.client ? [...this.state.addressInQuote.client.addresses, { ...this.state.newAddress, id }] : [{ ...this.state.newAddress }]
              },
            },
          }, () => {
            if (this.props.address.client) {
              console.log(this.state.addressInQuote)
              this.props.updateAddressInQuote(this.state.addressInQuote, this.props.param, true);
            }
            this.setState({ isCreatingNewAddress: false, newAddress: EMPTY_ADDRESS });
          });
        }) : this.setState({ isCreatingNewAddress: false, newAddress: EMPTY_ADDRESS })
      }
    });
  }

  private _setAddress = (addressFromGoogle: IAddress | null) => {
    this.props.setAddress(addressFromGoogle, this.props.param);

  }

  private _handleSelecting = async () => {
    await new Promise((resolve) => { this.setState({ loading: true }, () => resolve(true)) });
    let client: null | IClient = null;

    if (this.props.address.client) {
      client = await UBClient.getClientById(this.props.address.client.id);
    }

    this.setState({
      isSelecting: true,
      addressInQuote: {
        ...this.props.address,
        client: client ? client : this.props.address.client
      }
    }, () => {
      this.props.onEditing(true);
    });
  }

  private _updateAddressInQuote = (addressInQuote: IAddressInQuote) => {
    this.setState({ addressInQuote });
  }

  private _showContactCreator = (isOpen: boolean = true) => {
    if (!isOpen) {
      this.setState({ newContact: UBContact.createEmptyContact() });
    }

    this.setState({ isCreatingNewContact: isOpen });
  }

  private _showAddressCreator = (isOpen: boolean = true) => {
    if (!isOpen) {
      this.setState({ newAddress: UBAddress.createEmptyAddress() });
    }

    this.setState({ isCreatingNewAddress: isOpen });
  }

  private _updateNewAddress = (suggest: any) => {
    const addressFromGoogle: IAddress = decomposeAddressFromGoogle(suggest.address_components);
    this.setState({ newAddress: { ...addressFromGoogle, address: `${addressFromGoogle.address}, ${addressFromGoogle.city}, ${addressFromGoogle.province}, ${addressFromGoogle.country}, ${addressFromGoogle.postal_code}` } });

  }

  private _handleInputChangeForNewAddress = (e: string) => {
    this.setState({ newAddress: { ...this.state.newAddress, address: e } });
  }

  private _handleInputChangeForNewContact = (e: string, param: string) => {
    this.setState({ newContact: { ...this.state.newContact, [param]: e } });
  }

  private _saveNewContact = () => {
    if (this.state.newContact.last_name === '') {
      this.props.confirmationWithCallback(() => {}, 'Attention, le nom ne peut être vide.');
      return false;
    }

    if (this.state.newContact.first_name === '') {
      this.props.confirmationWithCallback(() => {}, 'Attention, le prénom ne peut être vide.');
      return false;
    }

    this.props.address.client ? this.props.addContactToClientAndSelect(this.state.newContact, this.props.address.client.id).then(({ id }) => {
      this.setState({
        addressInQuote: {
          ...this.state.addressInQuote,
          contact: { ...this.state.newContact, id },
          client: {
            ...this.state.addressInQuote.client,
            contacts: this.state.addressInQuote.client ? [...this.state.addressInQuote.client.contacts, { ...this.state.newContact, id }] : [{ ...this.state.newContact }]
          },
        },
      }, () => {
        if (this.props.address.client) {
          this.props.updateAddressInQuote(this.state.addressInQuote, this.props.param, true);
        }
        this.setState({ isCreatingNewContact: false, newContact: UBContact.createEmptyContact() });
      });
    }) : this.setState({ isCreatingNewContact: false, newContact: UBContact.createEmptyContact() })
  }

  private _saveNewAddress = () => {
    if (this.state.newAddress.address === '') {
      this.props.confirmationWithCallback(() => {}, 'Attention, l\'adresse ne peut être vide.')
      return false;
    }

    if (this.state.newAddress.city === '') {
      this.props.confirmationWithCallback(() => {}, 'Attention, veuillez sélectionner une adresse proposée dans le menu.')
      return false;
    }

    this.props.address.client ? this.props.addAddressToClientAndSelect(this.state.newAddress, this.props.address.client.id).then(({ id }) => {
      this.setState({
        addressInQuote: {
          ...this.state.addressInQuote,
          address: { ...this.state.newAddress, id },
          client: {
            ...this.state.addressInQuote.client,
            addresses: this.state.addressInQuote.client ? [...this.state.addressInQuote.client.addresses, { ...this.state.newAddress, id }] : [{ ...this.state.newAddress }]
          },
        },
      }, () => {
        if (this.props.address.client) {
          this.props.updateAddressInQuote(this.state.addressInQuote, this.props.param, true);
        }
        this.setState({ isCreatingNewAddress: false, newAddress: EMPTY_ADDRESS });
      });
    }) : this.setState({ isCreatingNewAddress: false, newAddress: EMPTY_ADDRESS })
  }

  public render() {
    return (
      <div className="address-bill" style={this._getEditingStyle()}>
        <div className="ibox" style={{ borderRadius: '7px' }}>
          <div className="ibox-head">
            <div className="ibox-title" style={{
                display: 'flex',
                width: '100%',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <h3 style={(this.state.isEditing || this.state.isSelecting) ? {} : {}}>{this.props.title}</h3>
              {(!this.state.isSelecting && !this.state.isEditing) &&
                <button className="close" type="button" onClick={this._handleSelecting} aria-label="Modifier">
                  <i className={`la la-search`} />
                </button>
              }
              {(this.state.isEditing || this.state.isSelecting) &&
                <button className="close" type="button" onClick={() => {
                  //@ts-ignore
                  this.setState({ addressInQuote: undefined, client: UBClient.createEmptyClient(), isEditing: false, isSelecting: false, contactAddress: this.props.address || undefined, organisationsAddress: this.props.address ? (this.props.address.organisations || []) : [], }, () => {
                    this.props.onEditing(false);
                  });
                }} aria-label="Fermer">
                  <span aria-hidden="true">×</span>
                </button>
              }
            </div>
          </div>
          <div className="ibox-body" style={{ ...(this.state.isSelecting && !this.state.isEditing ? { height: window.innerHeight - 400 } : {}), overflowY: 'auto' }}>
          {(this.state.isSelecting && !this.state.isEditing) &&
            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
              <div style={{ width: '50%' }}>
                <AsyncCreatableSelect
                  value={this.props.address.client ? {
                    label: this.props.address.client.title,
                    value: this.props.address.client.id,
                    data: this.props.address.client
                  } : null}
                  loadingMessage={() => 'Recherche...'}
                  noOptionsMessage={() => 'Aucun résultat'}
                  placeholder="Sélectionner ou créer un client"
                  isSearchable={true}
                  onChange={(e: any) => {
                    if (!e) {
                      return this._setAddress(null);
                    }

                    const client = e.data as IClient;

                    var address = client.addresses[0] ? client.addresses[0] : undefined;

                    var contact = client.contacts[0] ? client.contacts[0] : undefined;

                    for ( var idx in client.addresses ) {
                      if (this.props.title=="Adresse de facturation") {
                        if (Number(client.addresses[idx].is_default_facturation) == 1) {
                          address = client.addresses[idx];
                        }
                      }

                      if (this.props.title=="Adresse de livraison") {
                        if (Number(client.addresses[idx].is_default_shipping) == 1) {
                          address = client.addresses[idx];
                        }
                      }
                    }

                    for ( var idx in client.contacts ) {
                      if (Number(client.contacts[idx].is_default_contact) == 1) {
                        contact = client.contacts[idx];
                      }
                    }

                    this.setState({
                      client,
                      isSelecting: true,
                      addressInQuote: {
                        ...this.state.addressInQuote,
                        client,
                        address: this.props.address.address !== undefined ? this.props.address.address : address,
                        contact: this.props.address.contact !== undefined ? this.props.address.contact : contact,
                      }
                    });

                  }}
                  loadOptions={this.props.loadClientsOptions}
                  isValidNewOption={(inputValue: string, selectValue: any, selectOptions: any) => {
                    const isNotDuplicated = !selectOptions
                      .map((option: any) => option.label)
                      .includes(inputValue);
                    const isNotEmpty = inputValue !== '';
                    return isNotEmpty && isNotDuplicated;
                  }}
                  formatCreateLabel={(value: string) => `Créer ${value}`}
                  onCreateOption={this._handleCreate}
                />
              </div>
            </div>
          }
          {(this.state.isSelecting && !this.state.isEditing) &&
            (this.state.isLoading ?
              <div className="page-preloader">Chargement ...</div>
              :
              (this.state.addressInQuote.client && this.state.addressInQuote.client.addresses && (this.state.addressInQuote.client.addresses.length > 0 || this.state.addressInQuote.client.contacts.length > 0)) &&
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '20px 0' }}>
                  <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', width: '80%' }}>
                    <div style={{ display: 'flex', flexDirection: 'column', width: '50%', paddingRight: 10, borderRight: '1px solid #d3d3d3' }}>
                      <h4>Adresses - {this.state.addressInQuote.client.entreprise}</h4>

                      <div style={{ width: '100%', height: 'auto' }} onClick={() => this._showAddressCreator(!this.state.isCreatingNewAddress)} className={`cursored ibox`} key={`organisation-address-new`}>
                        <div className="ibox-head">
                          <button className="close" type="button" onClick={() => this._showAddressCreator(!this.state.isCreatingNewAddress)} aria-label="Créer">
                            <span aria-hidden="true"><i className={`la la-plus`} style={{ ...(this.state.isCreatingNewAddress ? { transform: 'rotate(45deg)' } : {}), transition: 'all 0.25s ease-in-out' }} /></span>
                          </button>
                          {this.state.isCreatingNewAddress ?
                            'Annuler'
                          :
                            'Nouvelle adresse'
                          }
                        </div>
                      </div>

                      {this.state.isCreatingNewAddress &&
                        <div style={{ width: '100%' }} className={`ibox`} key={`organisation-address-new`}>
                          <div className="ibox-head">
                            <GetEnv>
                              {({ env }: any) => {
                                return env.GOOGLE_PUBLIC_API_KEY ?
                                  <ReactGoogleMapLoader
                                    params={{
                                      key: env.GOOGLE_PUBLIC_API_KEY,
                                      libraries: "places,geocode",
                                    }}
                                    render={googleMaps =>
                                      googleMaps && (
                                        <div style={{ width: '100%' }}>
                                          <ReactGooglePlacesSuggest
                                            autocompletionRequest={{input: this.state.newAddress.address}}
                                            googleMaps={googleMaps}
                                            onSelectSuggest={this._updateNewAddress}
                                          >
                                            <InlineTextInput
                                              forceValue
                                              title=""
                                              noLabel={true}
                                              onChange={this._handleInputChangeForNewAddress}
                                              placeholder="Adresse"
                                              value={this.state.newAddress.address}
                                              fullWidth={true}
                                              autofocus
                                            />
                                          </ReactGooglePlacesSuggest>
                                        </div>
                                      )
                                    }
                                  />
                                :
                                  <InlineTextInput
                                    title=""
                                    noLabel={true}
                                    onChange={this._handleInputChangeForNewAddress}
                                    placeholder="Adresse"
                                    value={this.state.newAddress.address}
                                    fullWidth={true}
                                    autofocus
                                  />
                              }}
                            </GetEnv>
                            <button className="btn btn-primary" type="button" onClick={() => this._saveNewAddress()} aria-label="Enregistrer">
                              <span aria-hidden="true"><i className={`la la-save`} /></span>
                            </button>
                          </div>
                        </div>
                      }

                      {_.map(_.orderBy(this.state.addressInQuote.client.addresses, addressToFilter => this.state.addressInQuote.address ? addressToFilter.id === this.state.addressInQuote.address.id : false, 'desc'), (address, index) => {
                        return address && (
                          <div style={{ width: '100%', height: 'auto' }} onClick={() => this.setState({ addressInQuote: { ...this.state.addressInQuote, address } })} className={`cursored ibox ${this.state.addressInQuote && this.state.addressInQuote.address && this.state.addressInQuote.address.id === address.id ? 'selected' : ''}`} key={`organisation-address-${index}`}>
                            <div className="ibox-head">
                              <button className="close" type="button" onClick={() => this.setState({ addressInQuote: { ...this.state.addressInQuote, address } })} aria-label="Sélectionner">
                                <span aria-hidden="true"><i className={`la la-${this.state.addressInQuote && this.state.addressInQuote.address && this.state.addressInQuote.address.id === address.id ? 'check-' : ''}circle`} /></span>
                              </button>
                                {address.address}
                            </div>
                          </div>
                        )
                      })}
                      </div>
                      <div style={{ display: 'flex', flexDirection: 'column', width: '50%', paddingLeft: 10 }}>
                        <h4>Contacts - {this.state.addressInQuote.client.entreprise}</h4>
                        <div style={{ width: '100%', height: 'auto' }} onClick={() => this._showContactCreator(!this.state.isCreatingNewContact)} className={`cursored ibox`} key={`organisation-address-new`}>
                          <div className="ibox-head">
                            <button className="close" type="button" onClick={() => this._showContactCreator(!this.state.isCreatingNewContact)} aria-label="Créer">
                              <span aria-hidden="true"><i className={`la la-plus`} style={{ ...(this.state.isCreatingNewContact ? { transform: 'rotate(45deg)' } : {}), transition: 'all 0.25s ease-in-out' }} /></span>
                            </button>
                            {this.state.isCreatingNewContact ?
                              'Annuler'
                            :
                              'Nouveau contact'
                            }
                          </div>
                        </div>

                        {this.state.isCreatingNewContact &&
                          <div style={{ width: '100%', height: 'auto' }} className={`ibox`} key={`organisation-contact-new`}>
                            <div className="ibox-head">
                              <InlineTextInput
                                title=""
                                noLabel={true}
                                onChange={e => this._handleInputChangeForNewContact(e, 'first_name')}
                                placeholder="Prénom"
                                value={this.state.newContact.first_name}
                                fullWidth={true}
                                autofocus
                              />
                              <InlineTextInput
                                title=""
                                noLabel={true}
                                onChange={e => this._handleInputChangeForNewContact(e, 'last_name')}
                                placeholder="Nom"
                                value={this.state.newContact.last_name}
                                fullWidth={true}
                                autofocus
                              />
                              <InlineTextInput
                                title=""
                                noLabel={true}
                                onChange={e => this._handleInputChangeForNewContact(e, 'email')}
                                placeholder="Courriel"
                                value={this.state.newContact.email}
                                fullWidth={true}
                                autofocus
                              />
                              <button className="btn btn-primary" type="button" onClick={() => this._saveNewContact()} aria-label="Enregistrer">
                                <span aria-hidden="true"><i className={`la la-save`} /></span>
                              </button>
                            </div>
                          </div>
                        }

                        {_.map(_.orderBy(this.state.addressInQuote.client.contacts, contactToFilter => this.state.addressInQuote.contact ? this.state.addressInQuote.contact.id === contactToFilter.id : false, 'desc'), (contact, index) => {
                          return contact && (
                            <div style={{ width: '100%', height: 'auto' }} onClick={() => this.setState({ addressInQuote: { ...this.state.addressInQuote, contact } })} className={`cursored ibox ${this.state.addressInQuote && this.state.addressInQuote.contact && this.state.addressInQuote.contact.id === contact.id ? 'selected' : ''}`} key={`organisation-address-${index}`}>
                              <div className="ibox-head">
                                <button className="close" type="button" onClick={() => this.setState({ addressInQuote: { ...this.state.addressInQuote, contact } })} aria-label="Sélectionner">
                                  <span aria-hidden="true"><i className={`la la-${this.state.addressInQuote && this.state.addressInQuote.contact && this.state.addressInQuote.contact.id === contact.id ? 'check-' : ''}circle`} /></span>
                                </button>
                                {contact.first_name} {contact.last_name} {contact.email ? ' ('+contact.email+')' : ''}
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                </div>
            )
          }
          {this.state.isEditing &&
            <div className="container" style={{ paddingBottom: 20 }}>
              <h4>Informations du client</h4>
              <div className="row">
                <div className="col-6 form-group mb-4">
                  <label htmlFor="client-title">Nom du client</label>
                  <input
                    id="client-title"
                    name="client-title"
                    placeholder="Nom*"
                    type="text"
                    className="form-control form-control-line"
                    value={this.state.addressInQuote.client ? this.state.addressInQuote.client.title : ''}
                    onChange={(e) => this._updateAddressInQuote({ ...this.state.addressInQuote, client: { ...this.state.addressInQuote.client, title: e.currentTarget.value } as IClient })}
                  />
                </div>
                <div className="col-6 form-group mb-4">
                  <label htmlFor="client-type">Type de client</label>
                  <select
                    name="client-type"
                    className="form-control form-control-line"
                    id="client-type"
                    value={this.state.addressInQuote.client ? this.state.addressInQuote.client.client_type_enum : 1}
                    onChange={(e) => this._updateAddressInQuote({ ...this.state.addressInQuote, client: { ...this.state.addressInQuote.client, client_type_enum: Number(e.currentTarget.value) } as IClient })}
                  >
                    <option value={1}>Particulier</option>
                    <option value={2}>Entreprise</option>
                    <option value={3}>Gouvernement</option>
                  </select>
                </div>
              </div>
              <h5>Contact</h5>
              <div className="row">
                <div className="col-6 form-group mb-4">
                  <input
                    value={this.state.addressInQuote.contact ? this.state.addressInQuote.contact.first_name : ''}
                    onChange={(e) => this._updateAddressInQuote({ ...this.state.addressInQuote, contact: { ...this.state.addressInQuote.contact, first_name: e.currentTarget.value } as IContact })}
                    className="form-control form-control-line"
                    type="text"
                    placeholder="Prénom*"
                  />
                </div>
                <div className="col-6 form-group mb-4">
                  <input
                    value={this.state.addressInQuote.contact ? this.state.addressInQuote.contact.last_name : ''}
                    onChange={(e) => this._updateAddressInQuote({ ...this.state.addressInQuote, contact: { ...this.state.addressInQuote.contact, last_name: e.currentTarget.value } as IContact })}
                    className="form-control form-control-line"
                    type="text"
                    placeholder="Nom*"
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-6 form-group mb-4">
                  <div className="input-group-icon input-group-icon-left">
                    <span className="input-icon input-icon-left"><i className="ti-email"></i></span>
                    <input
                      value={this.state.addressInQuote.contact ? this.state.addressInQuote.contact.email : ''}
                      onChange={(e) => this._updateAddressInQuote({ ...this.state.addressInQuote, contact: { ...this.state.addressInQuote.contact, email: e.currentTarget.value } as IContact })}
                      className="form-control form-control-line"
                      type="text"
                      placeholder="Courriel"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-6 form-group mb-4">
                  <div className="input-group-icon input-group-icon-left">
                    <span className="input-icon input-icon-left"><i className="fa fa-phone" aria-hidden="true"></i></span>
                    <input
                      value={this.state.addressInQuote.contact ? this.state.addressInQuote.contact.phone : ''}
                      onChange={(e) => this._updateAddressInQuote({ ...this.state.addressInQuote, contact: { ...this.state.addressInQuote.contact, phone: e.currentTarget.value } as IContact })}
                      className="form-control form-control-line"
                      type="text"
                      placeholder="555-555-5555*"
                    />
                  </div>
                </div>
                <div className="col-6 form-group mb-4">
                  <div className="input-group-icon input-group-icon-left">
                    <span className="input-icon input-icon-left"><i className="ti-mobile"></i></span>
                    <input
                      value={this.state.addressInQuote.contact ? this.state.addressInQuote.contact.cell : ''}
                      onChange={(e) => this._updateAddressInQuote({ ...this.state.addressInQuote, contact: { ...this.state.addressInQuote.contact, cell: e.currentTarget.value } as IContact })}
                      className="form-control form-control-line"
                      type="text"
                      placeholder="555-555-5555"
                    />
                  </div>
                </div>
              </div>
              <h5>Adresse</h5>
              <GetEnv>
                {({ env }: any) => {
                  return env.GOOGLE_PUBLIC_API_KEY ?
                    <ReactGoogleMapLoader
                      params={{
                        key: env.GOOGLE_PUBLIC_API_KEY,
                        libraries: "places,geocode",
                      }}
                      render={googleMaps =>
                        googleMaps && (
                          <div style={{ width: '100%' }}>
                            <ReactGooglePlacesSuggest
                              autocompletionRequest={{input: this.state.search}}
                              googleMaps={googleMaps}
                              onSelectSuggest={suggest => this._handleSelectSuggest(suggest)}
                            >
                              <InlineTextInput
                                title=""
                                noLabel={true}
                                onChange={this._handleInputChange}
                                placeholder="Adresse du contact"
                                value={this.state.value}
                                fullWidth={true}
                              />
                            </ReactGooglePlacesSuggest>
                          </div>
                        )
                      }
                    />
                  :
                    <InlineTextInput
                      title=""
                      noLabel={true}
                      onChange={this._handleInputChange}
                      placeholder="Adresse du contact"
                      value={this.state.value}
                      fullWidth={true}
                    />
                }}
              </GetEnv>
            </div>
          }
          {(!this.state.isEditing && !this.state.isSelecting && this.props.address) &&
            <address style={{ display: 'flex', flexDirection: 'column' }}>
              <Row>
                <IntextInput
                  id={`address-first-name-${this.props.param.toString()}`}
                  value={this.props.address.contact ? this.props.address.contact.first_name : ''}
                  onTextChange={(value: string) => {
                    this.props.updateAddressInQuote({ ...this.state.addressInQuote, contact: this.state.addressInQuote.contact ? { ...this.state.addressInQuote.contact, first_name: value } : { ...UBContact.createEmptyContact(), first_name: value } }, this.props.param);
                  }}
                >
                  <p className="can-click-on-listener">{this.props.address.contact ? this.props.address.contact.first_name : ''}{' '}</p>
                </IntextInput>
                <IntextInput
                  id={`address-last-name-${this.props.param.toString()}`}
                  value={this.props.address.contact ? this.props.address.contact.last_name : ''}
                  onTextChange={(value: string) => {
                    this.props.updateAddressInQuote({ ...this.state.addressInQuote, contact: this.state.addressInQuote.contact ? { ...this.state.addressInQuote.contact, last_name: value } : { ...UBContact.createEmptyContact(), last_name: value } }, this.props.param);
                  }}
                >
                  <p className="can-click-on-listener">{this.props.address.contact ? this.props.address.contact.last_name : ''}{' '}</p>
                </IntextInput>
              </Row>
              {this.props.address.client &&
                <div>
                {this.props.address.client.client_type_enum != 1 &&
                <IntextInput
                  id={`address-entreprise-${this.props.param.toString()}`}
                  value={this.props.address.client ? this.props.address.client.title : ''}
                  onTextChange={(value: string) => {
                    this.props.updateAddressInQuote({ ...this.state.addressInQuote, client: this.state.addressInQuote.client ? { ...this.state.addressInQuote.client, title: value } : { ...UBClient.createEmptyClient(), title: value } }, this.props.param);
                  }}
                >
                  <p className="can-click-on-listener">{this.props.address.client ? this.props.address.client.title : ''}{' '}</p>
                </IntextInput>
                }
                </div>
              }
              <IntextInput
                id={`address-address-${this.props.param.toString()}`}
                value={this.props.address.address ? this.props.address.address.address : ''}
                onTextChange={(value: string) => {
                  this.props.updateAddressInQuote({ ...this.state.addressInQuote, address: this.state.addressInQuote.address ? { ...this.state.addressInQuote.address, address: value } : { ...UBAddress.createEmptyAddress(), address: value } }, this.props.param);
                }}
                custom={() => {
                  return (
                    <GetEnv>
                      {({ env }: any) => {
                        return env.GOOGLE_PUBLIC_API_KEY &&
                          <ReactGoogleMapLoader
                            params={{
                              key: env.GOOGLE_PUBLIC_API_KEY,
                              libraries: "places,geocode",
                            }}
                            render={googleMaps =>
                              googleMaps && (
                                <div className="can-click-on-listener" style={{ width: '100%' }}>
                                  <ReactGooglePlacesSuggest
                                    autocompletionRequest={{input: this.state.search}}
                                    googleMaps={googleMaps}
                                    onSelectSuggest={suggest => {
                                      this.setState({ newContact: this.props.address, addressInQuote: this.props.address }, () => {
                                        this._handleSelectSuggest(suggest, true);
                                      })
                                    }}
                                  >
                                    <InlineTextInput
                                      inputClassName="can-click-on-listener"
                                      title=""
                                      noLabel={true}
                                      onChange={this._handleInputChange}
                                      placeholder="Adresse du contact"
                                      value={this.state.search}
                                      fullWidth={true}
                                    />
                                  </ReactGooglePlacesSuggest>
                                </div>
                              )
                            }
                          />
                      }}
                    </GetEnv>
                );
              }}
              >
                <p className="can-click-on-listener">{this.props.address.address && this.props.address.address.address}</p>
              </IntextInput>
              {/* <Row>
                <IntextInput
                  id={`address-city-${this.props.param.toString()}`}
                  value={this.props.address.city || ''}
                  onTextChange={(value: string) =>
                    this.props.updateAddress({ ...this.props.address, city: value } as IAddress, this.props.param)
                  }
                >
                  <p className="can-click-on-listener">{this.props.address.city},{' '}</p>
                </IntextInput>
                <IntextInput
                  id={`address-province-${this.props.param.toString()}`}
                  value={this.props.address.province || ''}
                  onTextChange={(value: string) =>
                    this.props.updateAddress({ ...this.props.address, province: value } as IAddress, this.props.param)
                  }
                >
                  <p className="can-click-on-listener">{this.props.address.province},{' '}</p>
                </IntextInput>
                <IntextInput
                  id={`address-postal_code-${this.props.param.toString()}`}
                  value={this.props.address.postal_code || ''}
                  onTextChange={(value: string) =>
                    this.props.updateAddress({ ...this.props.address, postal_code: value } as IAddress, this.props.param)
                  }
                >
                  <p className="can-click-on-listener">{this.props.address.postal_code},{' '}</p>
                </IntextInput>
                <IntextInput
                  id={`address-country-${this.props.param.toString()}`}
                  value={this.props.address.country || ''}
                  onTextChange={(value: string) =>
                    this.props.updateAddress({ ...this.props.address, country: value } as IAddress, this.props.param)
                  }
                >
                  <p className="can-click-on-listener">{this.props.address.country}</p>
                </IntextInput>
              </Row> */}
              <IntextInput
                id={`address-phone-${this.props.param.toString()}`}
                value={this.props.address.contact ? this.props.address.contact.phone : ''}
                onTextChange={(value: string) => {
                  this.props.updateAddressInQuote({ ...this.state.addressInQuote, contact: this.state.addressInQuote.contact ? { ...this.state.addressInQuote.contact, phone: value } : { ...UBContact.createEmptyContact(), phone: value } }, this.props.param);
                }}
              >
                <p className="can-click-on-listener">{this.props.address.contact ? this.props.address.contact.phone : ''}</p>
              </IntextInput>
            </address>
          }
          {this.state.isEditing &&
            <button className="btn btn-success" onClick={this._handleSave}>Enregistrer</button>
          }
          </div>
          {(this.state.isSelecting && !this.state.isEditing) &&
            <div className="ibox-footer" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
              <button disabled={this.state.isCreatingNewAddress || this.state.isCreatingNewContact} className="btn btn-success" onClick={() => {
                  if (this.state.addressInQuote) {
                    this.props.updateAddressInQuote(this.state.addressInQuote, this.props.param);
                  }
                  this.setState({ isSelecting: false, addressInQuote: undefined, client: UBClient.createEmptyClient() }, () => {
                    this.props.onEditing(false);
                  });

                }}
              >Sauvegarder</button>
            </div>
          }
        </div>
      </div>
    );
  }
}
