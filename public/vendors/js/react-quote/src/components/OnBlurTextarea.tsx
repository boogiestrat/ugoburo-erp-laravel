import React, { useState } from 'react';

const OnBlurTextarea = (props: any) => {
  const [value, setValue] = useState(props.initialValue);
  const { initialValue, onBlur } = props;
  return (
    <textarea {...props} defaultValue={props.initialValue}></textarea>
  );
}

export default OnBlurTextarea
