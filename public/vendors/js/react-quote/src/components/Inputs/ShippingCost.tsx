import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import _ from 'lodash';
import InlineTextInput from './InlineTextInput';
import { IItem } from '../../entities';

// import Select from 'react-select';

const Group = styled.div`
`

const Row = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  align-items: flex-end;
`

const SELECT_OPTIONS: Array<{ readonly label: string, readonly value: string, readonly data?: any }> = [
  {
    label: '$ forfaitaire',
    value: 'global',
    data: {},
  },
  {
    label: '$ par unité',
    value: 'unit',
    data: {},
  },
  {
    label: '% du prix coûtant',
    value: 'cost',
    data: {},
  },
  {
    label: '% du prix de liste',
    value: 'list',
    data: {},
  }
]

interface IShippingCostProps {
  readonly value: number;
  readonly method: 'global' | 'unit' | 'cost' | 'list';
  readonly onChange: (newValue: number, newMethod: 'global' | 'unit' | 'cost' | 'list') => void;
  readonly basedOn: IItem;
}

const ShippingCost = ({ value = 0, method = 'global', basedOn, onChange }: IShippingCostProps) => {
  const [localVal, setLocalVal] = useState(value);
  const [localMethod, setLocalMethod] = useState(method);

  const updateMethod = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const newMethod = event.currentTarget.value as 'global' | 'unit' | 'cost' | 'list';
    setLocalMethod(newMethod);
    onChange(isNaN(localVal) ? 0 : localVal, newMethod);
  }

  const updateVal = (value: number) => {
    setLocalVal(value);
    onChange(value, localMethod);
  }

  // const calculateShippingCost = (value: number, method: 'global' | 'unit' | 'cost' | 'list'): number => {
  //   switch (method) {
  //     case 'cost':
  //     case 'list':
  //       return (value / 100) * basedOn.price;
  //     case 'unit':
  //       return value * basedOn.quantity;
  //     case 'global':
  //     default:
  //       return value;
  //   }
  // }

  return (
    <Group>
      <td style={{borderTop: 'none',
    borderBottom: 'none',
    borderLeft: 'none',
    borderRight: 'none'}}>
        {/* <Select
          options={SELECT_OPTIONS}
        /> */}
        <select className="form-control form-control-line" value={localMethod} onChange={updateMethod}>
          {_.map(SELECT_OPTIONS, option => (
            <option value={option.value}>{option.label}</option>
          ))}
        </select>
      </td>
      <td style={{borderTop: 'none',
    borderBottom: 'none',
    borderRight: 'none'}}>
        <InlineTextInput
          value={localVal.toString()}
          title=""
          placeholder=""
          noLabel={true}
          groupWith={<span className="input-group-addon">{localMethod === 'global' || localMethod === 'unit' ? '$' : '%'}</span>}
          onChange={updateVal}
        />
      </td>
    </Group>
  );
}

export default ShippingCost;
