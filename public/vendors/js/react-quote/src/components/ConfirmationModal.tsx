import React from 'react';
import { IConfirmation } from '../entities';

interface IConfirmationModalProps {
  readonly confirmation: IConfirmation | null;
}
interface IConfirmationModalState {}

export default class ConfirmationModal extends React.Component<IConfirmationModalProps> {
  private _handleCancel = () => {
    this.props.confirmation && this.props.confirmation.onCancel();
  }

  private _handleOk = () => {
    this.props.confirmation && this.props.confirmation.onOk();
  }

  public render() {
    return (
      <div>
        <div
          className="sweet-overlay"
          tabIndex={-1}
          style={{ opacity: 1.06, display: "block" }}
        />
        <div
          className="sweet-alert showSweetAlert visible"
          tabIndex={-1}
          style={{
            display: "block",
            zIndex: 999999,
            marginTop:
              process.env.NODE_ENV === "development" ? "0px" : "-174px"
          }}
        >
          <div className="sa-icon sa-error" style={{ display: "none" }}>
            <span className="sa-x-mark">
              <span className="sa-line sa-left" />
              <span className="sa-line sa-right" />
            </span>
          </div>
          <div
            className="sa-icon sa-warning pulseWarning"
            style={{ display: "block" }}
          >
            <span className="sa-body pulseWarningIns" />
            <span className="sa-dot pulseWarningIns" />
          </div>
          <div className="sa-icon sa-info" style={{ display: "none" }} />
          <div className="sa-icon sa-success" style={{ display: "none" }}>
            <span className="sa-line sa-tip" />
            <span className="sa-line sa-long" />

            <div className="sa-placeholder" />
            <div className="sa-fix" />
          </div>
          <div className="sa-icon sa-custom" style={{ display: "none" }} />
          <h2>
            {this.props.confirmation && this.props.confirmation.title}
          </h2>
          <p className="lead text-muted " style={{ display: "block" }}>
            {this.props.confirmation && this.props.confirmation.message}
          </p>
          {this.props.confirmation && this.props.confirmation.body && (
            <div>{this.props.confirmation.body()}</div>
          )}
          <div className="sa-button-container">
            <button
              className="cancel btn btn-lg btn-default"
              onClick={this._handleCancel}
              tabIndex={2}
              style={{ display: "inline-block" }}
            >
              Annuler
            </button>
            <div className="sa-confirm-button-container">
              <button
                className="confirm btn btn-lg btn-warning"
                onClick={this._handleOk}
                tabIndex={1}
                style={{ display: "inline-block" }}
              >
                Oui
              </button>
              <div className="la-ball-fall">
                <div />
                <div />
                <div />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
