import React from 'react';

import _ from 'lodash';

import { IConfirmation, IDiscount, ISelectValue, IItem } from '../entities';
import UBQuote from '../models/UBQuote';
import SelectAsync from './SelectAsync';
import InlineTextInput from './Inputs/InlineTextInput';
import Modal from './Modal';
import { CSVImportModal } from './CSVImportModal';
import { getBaseUrl, createEmptyItem } from '../services';
import { ProfitMethod } from '../constants/enums';

interface IActionsProps {
  readonly closeAllSections: (actual: boolean, callback: (value: boolean) => void) => void;
  readonly saveQuote: () => Promise<any>;
  readonly updateQuote: (param: string, value: any) => void;
  readonly closeImportModal: () => void;
  readonly openImportModal: () => void;
  readonly onInLotStateChange: (isInLotUpdating: boolean) => void;
  readonly isInLotUpdating: boolean;
  readonly onModalNeeded: (confirmation: IConfirmation) => void;
  readonly quote: UBQuote;
  readonly addNewSection: () => Promise<void>;
  readonly availableDiscounts: Array<IDiscount>;
  readonly updateDiscounts: (discounts: Array<IDiscount>) => void;
  readonly loadPossibleDiscount: (inputValue: string, callback: Function) => void;
  readonly isCSVImportOpen: boolean;
  readonly deleteDiscount: (discountIndex: number, discountName: string, callback: Function) => void
}

interface IActionsState {
  readonly saving: boolean;
  readonly discounts: Array<IDiscount>;
  readonly discountNameToModidy: string;
  readonly discountToModify: IDiscount;
  readonly discountAfter: IDiscount;
  readonly isModifying: boolean;
  readonly isGenPDF: boolean;
  readonly isAdmin: boolean;
  readonly isClosed: boolean;
  readonly availableDiscounts: Array<IDiscount>;
  readonly applyTo: { readonly index: number; readonly value: string; };
}

export default class Actions extends React.Component<IActionsProps> {
  public state: IActionsState = {
    saving: false,
    discountAfter: {
      code: "",
      name: "",
      parts: [0, 0, 0, 0, 0]
    },
    discountNameToModidy: "",
    isModifying: false,
    isGenPDF: false,
    isAdmin: false,
    isClosed: false,
    discountToModify: {
      code: "",
      name: "",
      parts: [0, 0, 0, 0, 0]
    },
    discounts: [],
    availableDiscounts: [],
    applyTo: { index: -1, value: '-1' }
  };

  private _loadDiscounts = () => {
    this.setState({ availableDiscounts: this.props.availableDiscounts });
  };

  private _loadSectionsForSelect = (keywords: string, callback: Function) => {
    const sections = this.props.quote.getSections();
    const rel = _.filter(sections, section => section.title.toLowerCase().indexOf(keywords.toLowerCase()) > -1);

    callback(_.map(rel, section => ({
      label: section.title,
      value: section.id,
      data: section,
    } as ISelectValue )));
  }

  private _getValuesFromCSV = (): Promise<any> => {
    const input = document.getElementById('file_to_import') as HTMLInputElement;

    if (!input.files) {
      return new Promise(resolve => resolve(false));
    }

    const file = input.files[0];
    const formData = new FormData();

    formData.append('csv', file);

    return fetch(getBaseUrl() + 'webservices/item/import/csv.php', {
      method: 'POST',
      body: formData,
    })
    .then(res => res.json())
    .then(json => json)
    .catch(() => false);
  }

  private _insertDataIntoSection = (data: Array<{}>, section: number | string) => {
    if (typeof section === 'number') {
      return this._insertData(data, section);
    } else {
      this.props.updateQuote('sections', this.props.quote.addSection(section, data));
    }
  }

  private _insertData = (data: Array<{}>, sectionIndex: number): boolean => {
    this.props.updateQuote('sections', _.map(this.props.quote.getSections(), (section, sectionId: number) => {
      if (sectionId !== sectionIndex) {
        return section;
      }

      //@ts-ignore
      return {
        ...section,
        items: [
          ...section.items,
          ..._.map(data, itemFormCSV => ({
            ...createEmptyItem(),
            ...itemFormCSV,
            //@ts-ignore
            SKU: itemFormCSV['Part Number'],
            //@ts-ignore
            title: itemFormCSV['Part Description'],
            //@ts-ignore
            quantity: !isNaN(itemFormCSV['Qty']) ? Number(itemFormCSV['Qty']) : 0,
            profitMethod: ProfitMethod.FIXED_PRICE,
            //@ts-ignore
            price: !isNaN(parseFloat(String(itemFormCSV['List']).replace('$', '').replace(',', '.'))) ? parseFloat(String(itemFormCSV['List']).replace('$', '').replace(',', '.')) : 0,
            // priceParameterValue: !isNaN(parseFloat(String(itemFormCSV['List']).replace('$', '').replace(',', '.'))) ? parseFloat(String(itemFormCSV['List']).replace('$', '').replace(',', '.')) : 0
            priceParameterValue: 0
          } as IItem))
        ]
      };
    }));

    return true;
  }

  public render() {
    return (
      <div>
        <div>
          <div className="actions-container">
            <button
              className="btn btn-success btn-air btn-rounded"
              onClick={() => this.props.closeAllSections(this.state.isClosed, (isClosed) => this.setState({ isClosed }))}
            >
              <span className="btn-icon"><i className={`la la-chevron-${this.state.isClosed ? 'down' : 'up'}`}></i>{this.state.isClosed ? 'Ouvrir' : 'Fermer'} tout</span>
            </button>
            {/* <button
              className="btn btn-success btn-air btn-rounded"
              onClick={() => this.setState({ isModifying: true, applyTo: { index: -1, value: '' } }, () => {
                document.body.classList.add('modal-open');
                this._loadDiscounts();
              })}
            >
              <span className="btn-icon"><i className="la la-search"></i>Fournisseurs</span>
            </button> */}
            <button
              className="btn btn-success btn-air btn-rounded"
              onClick={() => this.props.openImportModal()}
            >
              <span className="btn-icon"><i className="la la-cloud-upload"></i>Import CSV</span>
              </button>
            <a target="_blank" rel="noopener noreferrer" style={{ textDecoration: 'none', display: 'flex', alignItems: 'center', justifyContent: 'center' }} onClick={() => {this.props.quote.saveQuote()}} href={getBaseUrl() + 'webservices/quote/pdf/generate.php?admin=1&id=' + this.props.quote.get().number} className="btn btn-success btn-air btn-rounded"><span className="btn-icon" style={{ fontSize: '11px' }}><i className="la la-file-pdf-o"></i>Admin</span></a>
            <a target="_blank" rel="noopener noreferrer" style={{ textDecoration: 'none', display: 'flex', alignItems: 'center', justifyContent: 'center' }} onClick={() => {this.props.quote.saveQuote()}} href={getBaseUrl() + 'webservices/quote/pdf/generate.php?id=' + this.props.quote.get().number} className="btn btn-success btn-air btn-rounded"><span className="btn-icon" style={{ fontSize: '11px' }}><i className="la la-file-pdf-o"></i>Version PDF</span></a>
            <button
              disabled={this.state.saving}
              className="btn btn-primary btn-air btn-rounded"
              onClick={() => {
                if (!this.props.quote.get().billTo.client || !this.props.quote.get().shipTo.client) {
                  this.props.onModalNeeded({
                    message: 'Les champs "Adresse de facturation" et "Adresse de livraison" doivent avoir une valeur.',
                    onCancel: () => {},
                    onOk: () => {},
                    title: 'Attention'
                  } as IConfirmation)
                  return;
                }

                this.setState({ saving: true });
                this.props.quote
                  .saveQuote()
                  .then(response => {
                    if (response.valid) {
                      if (response.id) {
                        this.props.updateQuote('id', Number(response.id));
                      }
                      this.setState({ saving: false });
                    } else {
                      this.setState({ saving: false });
                    }
                  })
                  .catch(error =>
                    this.setState({ saving: false }, () =>
                      alert(
                        'Attention, erreur lors de la sauvegarde. Veuillez réessayer.'
                      )
                    )
                  );
              }}
            >
              <span className="btn-icon"><i className="la la-save"></i>{this.state.saving ? 'Sauvegarde en cours...' : 'Sauvegarder'}</span>
            </button>
          </div>
        </div>

        {this.state.isModifying && (
          <Modal
            title="Fournisseurs"
            doneBtnText="Appliquer"
            doneBtnAction={() => {
              const quote = this.props.quote.get();
              this.props.updateDiscounts(this.state.availableDiscounts);

              this.props.updateQuote(
                'sections',
                _.map(quote.sections, (section, sectionIndex) => {
                  return {
                    ...section,
                    items: _.map(section.items, item => {
                      const indexOfLastDiscount = _.indexOf(this.props.availableDiscounts, item.discount);

                      if (this.state.applyTo.value.substr(0, 2) === 's-' && Number(this.state.applyTo.value.substr(2)) === sectionIndex) {
                        return {
                          ...item,
                          discount: {
                            ...this.state.availableDiscounts[indexOfLastDiscount],
                            ...this.state.availableDiscounts[this.state.applyTo.index],
                          }
                        };
                      }

                      if (this.state.applyTo.value === 'all') {
                        return {
                          ...item,
                          discount: {
                            ...this.state.availableDiscounts[indexOfLastDiscount],
                            ...this.state.availableDiscounts[this.state.applyTo.index],
                          }
                        };
                      }

                      if (this.state.applyTo.value === 'custom') {
                        if (item.discount.name === '') {
                          return {
                            ...item,
                            discount: {
                              ...this.state.availableDiscounts[indexOfLastDiscount],
                              ...this.state.availableDiscounts[this.state.applyTo.index],
                            }
                          };
                        } else {
                          return {
                            ...item,
                            discount: {
                              ...this.state.availableDiscounts[indexOfLastDiscount],
                            }
                          };
                        }
                      }

                      return {
                        ...item,
                        discount: {
                          ...this.state.availableDiscounts[indexOfLastDiscount],
                        }
                      };
                    })
                  };
                })
              );

              this.setState({ isModifying: false }, () => {
                document.body.classList.remove('modal-open');
              });

              return {};
            }}
            close={() => {
              this.setState({ isModifying: false }, () => {
                document.body.classList.remove('modal-open');
              });
              return {};
            }}
          >
            <div>
              <table className="table table-bordered">
                <thead className="thead-default thead-lg">
                  <tr>
                    <th>Escompte</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>No. Enregistrement</th>
                    <th>Assigner à</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {_.map(this.state.availableDiscounts, (discount, discountIndex) => {
                    return (
                      <tr key={`discount-row-${discountIndex}`} style={(discountIndex === 0 ? { display: 'none' } : {})}>
                        <td>
                          {discountIndex === 0 ?
                            discount.name === '' ? 'Personnalisé' : discount.name
                          :
                            <InlineTextInput
                              loadOptions={this.props.loadPossibleDiscount}
                              autosuggestCallback={(item: any) => {
                                this.setState({ availableDiscounts: _.map(this.state.availableDiscounts, (dis, disIndex) => {
                                  if (disIndex !== discountIndex) {
                                    return dis;
                                  }

                                  return { code: item.code, name: item.title, parts: JSON.parse(item.values) } as IDiscount;
                                }) });
                              }}
                              value={discount.name}
                              title=""
                              placeholder=""
                              noLabel={true}
                              onChange={value => {
                                this.setState({ availableDiscounts: _.map(this.state.availableDiscounts, (avDis, avDisIndex) => {
                                  if (avDisIndex !== discountIndex) {
                                    return avDis;
                                  }

                                  return { ...avDis, name: value };
                                }) })
                              }}
                            />
                          }
                        </td>
                        {_.map(discount.parts, (part, partIndex) => (
                          <td key={`discount-${discountIndex}-part-${partIndex}`}>
                            <InlineTextInput
                              key={`discount-${discountIndex}`}
                              value={part.toString()}
                              title=""
                              placeholder=""
                              noLabel={true}
                              inputWidth={30}
                              noPadding={true}
                              onChange={(value: string) => {
                                if (isNaN(Number(value))) {
                                  return;
                                }

                                this.setState({ availableDiscounts: _.map(this.state.availableDiscounts, (dis, disIndex) => {
                                  if (disIndex !== discountIndex) {
                                    return dis;
                                  }

                                  return { ...dis, parts: _.map(dis.parts, (part, partInStateIndex) => {
                                    if (partInStateIndex !== partIndex) {
                                      return part;
                                    }

                                    return Number(value);
                                  })}
                                })});
                              }}
                            />
                          </td>
                        ))}
                        <td>
                        {discountIndex === 0 ?
                            discount.code
                          :
                            <InlineTextInput
                              value={discount.code}
                              title=""
                              placeholder=""
                              noLabel={true}
                              onChange={value => this.setState({ availableDiscounts: _.map(this.state.availableDiscounts, (discount, disIndex) => {
                                if (discountIndex !== disIndex) {
                                  return discount;
                                }

                                return { ...discount, code: value };
                              }) })}
                            />
                        }
                        </td>
                        <td>
                          <select value={this.state.applyTo.index === discountIndex ? this.state.applyTo.value : '-1'} className="form-control" onChange={e => {
                            this.setState({ applyTo: { index: discountIndex, value: e.currentTarget.value } });
                          }}>
                            <option value="-1"></option>
                            <option value="all">Tous</option>
                            <option value="custom">Personnalisé</option>
                            <optgroup label="Sections">
                              {_.map(this.props.quote.getSections(), (section, sectionIndex) => {
                                return (
                                  <option value={`s-${sectionIndex}`}>{section.title}</option>
                                )
                              })}
                            </optgroup>
                          </select>
                        </td>
                        <td>
                          <a className="font-20 mx-2" href="javascript:;" data-id="36" data-label="#36" onClick={() => {
                            this.props.deleteDiscount(discountIndex, discount.name, this._loadDiscounts);
                          }}>
                            <i className="ti-trash">
                              {process.env.NODE_ENV === 'development' &&
                                'X'
                              }
                            </i>
                          </a>
                        </td>
                      </tr>
                    )
                  })}
                </tbody>
              </table>
              <div>
                <span style={{ cursor: 'pointer' }} className="font-20" onClick={() => this.setState({ availableDiscounts: [ ...this.state.availableDiscounts, { code: '', name: '', parts: [0, 0, 0, 0, 0] } ] })}><i className="la la-plus"></i></span>
              </div>
            </div>
          </Modal>
        )}
      </div>
    );
  }
}
