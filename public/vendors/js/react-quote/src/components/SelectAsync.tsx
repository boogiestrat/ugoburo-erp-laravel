import React from 'react';

//@ts-ignore
import AsyncSelect from 'react-select/async';

//@ts-ignore
import AsyncCreatableSelect from 'react-select/async-creatable';
import { ISelectValue } from '../entities';

interface ISelectAsyncProps {
  readonly isSearchable: boolean;
  readonly isClearable: boolean;
  readonly value?: ISelectValue;
  readonly label?: string;
  readonly isRight?: boolean;
  readonly isCreatable?: boolean;
  readonly loadFunction: (keywords: string, callback: Function) => void;
  readonly onChange: Function;
  readonly handleCreate?: (value: string) => Promise<any>;
  readonly inputWidth?: number;
  readonly style?: {
    readonly container?: React.CSSProperties,
    readonly input?: React.CSSProperties,
  };
  readonly placeholder?: string;
};

export default class SelectAsync extends React.Component<ISelectAsyncProps> {
  public render() {
    return (
      <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', width: '100%', ...(this.props.style && this.props.style.container ? this.props.style.container : {}) }}>
        {this.props.label &&
          <label style={{ marginRight: 10 }}>{this.props.label}</label>
        }
        <div style={{ width: this.props.inputWidth ? this.props.inputWidth : '100%' }}>
          {this.props.isCreatable && this.props.handleCreate ?
            <AsyncCreatableSelect
              isClearable
              value={this.props.value}
              loadingMessage={() => 'Recherche...'}
              noOptionsMessage={() => 'Aucun résultat'}
              placeholder={this.props.placeholder ? this.props.placeholder : "Sélectionner..."}
              isSearchable={true}
              onChange={this.props.onChange}
              loadOptions={this.props.loadFunction}
              isValidNewOption={(inputValue: string, selectValue: any, selectOptions: any) => {
                const isNotDuplicated = !selectOptions
                  .map((option: any) => option.label)
                  .includes(inputValue);
                const isNotEmpty = inputValue !== '';
                return isNotEmpty && isNotDuplicated;
              }}
              formatCreateLabel={(value: string) => `Créer ${value}`}
              onCreateOption={(value: string) => {
                if (this.props.handleCreate) {
                  // Set creating
                  this.props.handleCreate(value).then(() => {

                  });
                }

                return false;
              }}
            />
          :
            <AsyncSelect
              value={this.props.value}
              isClearable={this.props.isClearable}
              isSearchable={this.props.isSearchable}
              cacheOptions
              loadOptions={this.props.loadFunction}
              onChange={this.props.onChange}
              loadingMessage={() => 'Recherche...'}
              noOptionsMessage={() => 'Aucun résultat'}
              placeholder={this.props.placeholder ? this.props.placeholder : "Séletionner..."}
            />
          }
        </div>
      </div>
    );
  }
}
