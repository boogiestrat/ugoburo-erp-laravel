import React from 'react';
import _ from 'lodash';
import { moneyFormat } from '../services';

import UBQuote from '../models/UBQuote';

import styled from 'styled-components';
import IIntextInput from './Inputs/IntextInput';
import { IAdditionnalFee } from '../entities';

const Container = styled.div`
  .ibox-title {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;

    .la {
      font-size: 20pt;
    }
  }
`

const Body = styled.div`
  transition: all 0.3s ease-in-out;
  height: 100%;

  &.closed {
    height: 0px;
    overflow: hidden;
    padding-top: 0px;
    padding-bottom: 0px;
  }
`

const LastRow = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

const Stats = styled.div`

`

interface IOtherCostsProps {
  readonly quote: UBQuote;
  readonly updateQuote: (param: string, value: any) => Promise<any>;
  readonly confirmationWithCallback: (callback: Function, msg: string) => void;
}

interface IOtherCostsState {
  readonly openned: boolean;
}

export default class OtherCosts extends React.Component<IOtherCostsProps> {
  public state: IOtherCostsState = {
    openned: false,
  }

  private _addEmptyFee = () => {
    const feesTemp = this.props.quote.get().additionnalFees;

    if (!this.state.openned) {
      this.setState({ openned: true });
    }

    if (!feesTemp || feesTemp.length === 0) {
      this.props.updateQuote('additionnalFees', [{ title: '', value: 0 } as IAdditionnalFee ]);
    } else {
      this.props.updateQuote('additionnalFees', [ ...feesTemp, { title: '', value: 0 } as IAdditionnalFee ])
    }
  }

  private _calculateTotalFees = () => this.props.quote.calculateTotalFees()

  public render() {
    return (
      <Container>
        <div className="ibox">
          <div className="ibox-head">
            <div className="ibox-title">
              <h3>Coûts Internes</h3>
              <div>
                <i style={{ cursor: 'pointer', marginRight: 10 }} className={`la la-plus-circle`} onClick={() => this._addEmptyFee()}></i>
                <i style={{ cursor: 'pointer' }} className={`la la-chevron-${this.state.openned ? 'up' : 'down'}`} onClick={() => this.setState({ openned: !this.state.openned })}></i>
              </div>
            </div>
          </div>
          <Body className={`ibox-body ${!this.state.openned ? 'closed' : ''}`}>
            <div>
              <table className={`table table-striped list-cost ${this.props.quote.get().additionnalFees.length == 0 ? 'custom-display-hidden' : '' }`}>
                <thead>
                  <tr className="ft-row">
                    <th className="text-left">
                      <span>Description</span>
                    </th>
                    <th className="text-right">
                      <span>Montant</span>
                    </th>
                    <th style={{ width : '20%'  }}></th>
                  </tr>
                </thead>
                <tbody>
                  {_.map(this.props.quote.get().additionnalFees, (fee, index) => {
                    return (
                      <tr key={`row-add-fee-${index}`}>
                        <td className="bold text-left">
                          <IIntextInput
                            alwaysOn
                            id={`additionnal-fees-${index}`}
                            onTextChange={text => {
                              this.props.updateQuote('additionnalFees', _.map(this.props.quote.get().additionnalFees, (fee, indexForChange) => {
                                if (index !== indexForChange) {
                                  return fee;
                                }

                                return { ...fee, title: text };
                              }))
                            }}
                            value={fee.title}
                            style={{ width: '100%' }}
                          >
                            <span className="can-click-on-listener">{fee.title}</span>
                          </IIntextInput>
                        </td>
                        <td className="bold text-right" style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                          <IIntextInput
                            alwaysOn
                            id="other-additionnal-fees"
                            onBlur={text => {
                              const temp = Number(text);

                              if (isNaN(temp)) {
                                return;
                              }

                              this.props.updateQuote('additionnalFees', _.map(this.props.quote.get().additionnalFees, (fee, indexForChange) => {
                                if (index !== indexForChange) {
                                  return fee;
                                }

                                return { ...fee, value: temp };
                              }))
                            }}
                            onTextChange={() => {}}
                            value={fee.value.toString()}
                          >
                            <span className="can-click-on-listener">{moneyFormat(fee.value)}</span>
                          </IIntextInput>
                        </td>
                        <td className="custom-td-no-padding">
                        <button onClick={() => {
                          this.props.confirmationWithCallback(() => {
                            const tempFees = this.props.quote.get().additionnalFees;

                            this.props.updateQuote('additionnalFees', [ ...tempFees.slice(0, index), ...tempFees.slice(index + 1) ]);
                          }, 'Voulez-vous supprimer ce coût?');
                        }} className="btn btn-outline-danger btn-air btn-rounded btn-sm btn-float-right">
                            <i className="la la-trash-o"></i>
                        </button>
                        </td>
                      </tr>
                    )
                  })}
                </tbody>
              </table>
              <LastRow>
                <Stats>
                  <span><strong>Total: </strong>{moneyFormat(this._calculateTotalFees())}</span>
                </Stats>
                <button onClick={() => {
                  this._addEmptyFee();
                }} className="btn btn-outline-success btn-air btn-rounded btn-sm">
                  <span className="btn-icon">
                    <i className="la la-plus-circle"></i>
                    Ajouter un coût
                  </span>
                </button>
              </LastRow>
            </div>
          </Body>
        </div>
      </Container>
    );
  }
}
