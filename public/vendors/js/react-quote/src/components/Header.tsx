import React from 'react';

import _ from 'lodash';

import InlineTextInput from './Inputs/InlineTextInput';
import { DateInput } from './Inputs/DateInput';
import SelectAsync from './SelectAsync';
import Select from 'react-select';

import { IFurnitureSpecialist, ISelectValue, IConfirmation } from '../entities';
import { fetchEmployees, fetchLanguages } from '../services';
import UBQuote from '../models/UBQuote';
import moment from 'moment';

interface IHeaderProps {
  readonly quote: UBQuote;
  readonly updateQuote: (param: string, value: any) => void;
  readonly onModalNeeded: (confirmation: IConfirmation) => void;
  readonly confirmationWithCallback: (callback: Function, msg: string) => void;
}

interface IHeaderState {
  readonly actualEmployee: ISelectValue | undefined;
}

const languages = [
  { value: 'fr', label: 'Français' },
  { value: 'en', label: 'English' }
]

export default class Header extends React.Component<IHeaderProps> {
  public state: IHeaderState = {
    actualEmployee: undefined,
  }

  public componentDidMount() {
    this._getActualSpecialist();
  }

  private _loadEmployees = (inputValue: string, callback: Function) => {
    fetchEmployees(inputValue).then(employees => {
      callback(_.map(employees, employee => ({ label: `${employee.first_name} ${employee.last_name}`, value: employee.id, data: employee })));
    });
  }

  private _handleEmployeeChange = (e: any) => {
    if (!e) {
      return;
    }

    const employee: IFurnitureSpecialist = e.data as IFurnitureSpecialist;

    this.setState({ actualEmployee: {
      data: employee,
      label: `${employee.first_name} ${employee.last_name}`,
      value: employee.id,
    } as ISelectValue }, () => {
      this.props.updateQuote('specialist', employee);
    });
  }

  private _handleLanguageChange = (e: any) => {
    if (!e) {
      return;
    }

    const language: string = e.value as string;

    this.props.updateQuote('language', language);
  }

  private _getActualLanguageValue = () => {
    const code = this.props.quote.get().language;
    const lang = _.find(languages, language => language.value === code);

    return lang;
  }

  private _getActualSpecialist = () => {
    this._loadEmployees('%', (employees: Array<ISelectValue>) => {
      const quoteTemp = this.props.quote.get();

      if (quoteTemp.specialist !== undefined) {
        this.setState({ actualEmployee: {
          value: quoteTemp.specialist.id,
          label: `${quoteTemp.specialist.first_name} ${quoteTemp.specialist.last_name}`,
          data: quoteTemp.specialist,
        } as ISelectValue });

        return;
      }

      if (!window.location.href.split("specialist=")[1]) {
        this.setState({ actualEmployee: {} });
        return;
      }

      this.setState({ actualEmployee: _.find(employees, selectValue => {
        const employee = selectValue.data as IFurnitureSpecialist;

        return employee.id === window.location.href.split("specialist=")[1].split('&')[0];
      }) }, () => {
        if (this.state.actualEmployee) {
          this.props.updateQuote('specialist', this.state.actualEmployee.data);
        }
      })
    });
  }

  public render() {
    const { quote } = this.props;
    const style = { display: 'flex', flexDirection: 'row', overflow: 'visible' } as React.CSSProperties;
    const specialist = quote.get().specialist !== undefined ? quote.get().specialist : false;

    return (
      <div>
        <div className="header-container" style={style}>
          <div className="left-align">
            <img src="https://d1li5jetmb5ffl.cloudfront.net/skin/frontend/default/uber/images/logo.png" alt="UgoBuro"/>
            <div className="inline-form-container">
              <div>
                <address>123 rue des 123</address>
                <address>Montréal, QC, H1J 2K3</address>
                <address>Tel: 514.223.4435</address>
              </div>
            </div>
          </div>
          <div className="right-align">
            <h1>Soumission #{quote.get().number}</h1>
            {specialist &&
              <h6>{specialist.email}</h6>
            }
            <div className="inline-form-container" style={{ width: '100%' }}>
              <InlineTextInput
                value={quote.get().title}
                id="quote-top-title"
                title="Titre "
                placeholder=""
                onChange={value => this.props.updateQuote('title', value)}
                style={{ container: { justifyContent: 'space-between' } as React.CSSProperties}}
                inputWidth={300}
              />
              {/* <DateInput
                date={moment(quote.get().date).toDate()}
                id="date-date-input"
                min={moment().toDate()}
                max={false}
              /> */}
              <InlineTextInput
                value={quote.get().date.format('DD/MM/YYYY')}
                title="Date "
                placeholder=""
                onChange={value => {
                  if (!moment(value).isValid()) {
                    this.props.confirmationWithCallback(() => {}, 'Cette date n\'est pas valide.');
                    return;
                  }

                  this.props.updateQuote('date', moment(value));

                  if (moment(value).diff(moment(quote.get().validUntil), 'day') > -30) {
                    this.props.updateQuote('validUntil', moment(value).add(30, 'day'));
                  }
                }}
                style={{ container: { justifyContent: 'space-between' } as React.CSSProperties}}
                inputWidth={300}
                overrideType="date"
                id="date-date"
              />
              <InlineTextInput
                value={quote.get().validUntil.format('DD/MM/YYYY')}
                title="Valide jusqu'à "
                placeholder=""
                onChange={value => {
                  if (!moment(value).isValid()) {
                    this.props.confirmationWithCallback(() => {}, 'Cette date n\'est pas valide.');
                    return;
                  }

                  this.props.updateQuote('validUntil', moment(value));
                }}
                style={{ container: { justifyContent: 'space-between' } as React.CSSProperties}}
                inputWidth={300}
                overrideType="date"
                id="date-validUntil"
              />
              <div className="mb-2" style={{ width: '100%' }}>
                <SelectAsync
                  isClearable={false}
                  isSearchable={true}
                  value={this.state.actualEmployee}
                  label="Spécialiste : "
                  loadFunction={this._loadEmployees}
                  onChange={this._handleEmployeeChange}
                  inputWidth={300}
                  style={{ container: { justifyContent: 'space-between' } as React.CSSProperties}}
                />
              </div>
              <div className="mb-2" style={{ width: '100%', display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <label style={{ marginRight: 10 }}>Langue : </label>
                <div style={{ width: 300 }}>
                  <Select
                    value={this._getActualLanguageValue()}
                    onChange={this._handleLanguageChange}
                    options={languages}
                    style={{ container: { justifyContent: 'space-between' } as React.CSSProperties}}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
