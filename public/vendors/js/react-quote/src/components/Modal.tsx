import React from 'react';

interface IModalProps {
  readonly title: string;
  readonly doneBtnText: string;
  readonly doneBtnAction: () => {};
  readonly close: () => {};
}

export default class Modal extends React.Component<IModalProps> {
  public render() {
    return (
      <div>
        <div className="modal fade show" tabIndex={-1} role="dialog" style={{ display: 'block' }}>
          <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content form-horizontal">
                  <div className="modal-header p-4">
                      <h3 className="modal-title">{this.props.title}</h3>
                      <button className="close" type="button" data-dismiss="modal" onClick={this.props.close} aria-label="Fermer">
                          <span aria-hidden="true">×</span>
                      </button>
                  </div>
                  <div className="modal-body p-4"
                  style={{
                    height: '500px',
                    overflow: 'auto'
                  }}
                  >
                    {this.props.children}
                  </div>
                  <div className="modal-body p-4">
                      <div className="modal-footer text-right">
                          <input type="hidden" name="id" id="contact-id" value="" />
                          <button className="btn btn-rounded" data-dismiss="modal" id="cancel-button" onClick={this.props.close} type="button">Annuler</button>
                          {this.props.doneBtnText !== '' &&
                            <button className="btn btn-primary btn-rounded submit-btn" onClick={this.props.doneBtnAction}>{this.props.doneBtnText}</button>
                          }
                      </div>
                  </div>
              </div>
          </div>
      </div>
      </div>
    );
  }
}
