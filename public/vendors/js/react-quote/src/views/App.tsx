import React from 'react';

import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import PaperLike from '../components/PaperLike';
import Header from '../components/Header';
import Addresses from '../components/Addresses';
import Suppliers from '../components/Suppliers';
import OtherCosts from '../components/OtherCosts';
import Overview from '../components/Overview';
import Section from '../components/Section';
import Total from '../components/Total';
import { CSVImportModal } from '../components/CSVImportModal';
import UBQuote from '../models/UBQuote';
import * as Scroll from 'react-scroll';

import { createEmptySection, createEmptyItem, createEmptySectionFunctions, createEmptyItemFunctions, saveQuote, getBaseUrl, moneyFormat, decomposeAddressFromGoogle } from '../services';
import _ from 'lodash';

import { IQuote, IAddress, AddressTypeEnum, ISection, IItem, IConfirmation, ISelectValue, IClient, IDiscount, IContact, IAddressInQuote, IMessage } from '../entities';
import Actions from '../components/Actions';
import { fetchItem } from '../services/ItemServices';
import ConfirmationModal from '../components/ConfirmationModal';
import moment from 'moment';
import styled from 'styled-components';
import SelectAsync from '../components/SelectAsync';
import GetEnv from '../components/GetEnv';
import ReactGoogleMapsLoader from 'react-google-maps-loader';
import ReactGooglePlacesSuggest from 'react-google-places-suggest';
import InlineTextInput from '../components/Inputs/InlineTextInput';
import Modal from '../components/Modal';

//@ts-ignore
import { CSSProperties } from 'styled-components';
import UBContact from '../models/UBContact';
import UBClient from '../models/UBClient';
import { ProfitMethod } from '../constants/enums';

const Container = styled.div`
  .ibox-title {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;

    .la {
      font-size: 20pt;
    }
  }
`

const Body = styled.div`
  transition: all 0.3s ease-in-out;
  height: 100%;

  &.closed {
    height: 0px;
    overflow: hidden;
    padding-top: 0px;
    padding-bottom: 0px;
  }
`

interface IAppProps {};

interface IAppState {
  readonly quote: UBQuote;
  readonly move: boolean;
  readonly movingIndex: number;
  readonly isEditing: boolean;
  readonly isCSVImportOpen: boolean;
  readonly isInLotUpdating: boolean;
  readonly sectionsSelected: Array<number>;
  readonly itemsSelected: Array<number>;
  readonly confirmation: IConfirmation | null;
  readonly selectingClient: boolean;
  readonly newContact: IContact;
  readonly newClient: IClient;
  readonly organisationsAddress: Array<IAddress>;
  readonly contactAddress?: IAddress;
  readonly selectedAddress?: IAddress;
  readonly search: string;
  readonly value: string;
  readonly slider: Array<string>;
  readonly errors: Array<{readonly key: string, readonly msg: string}>
  readonly addressInQuote?: IAddressInQuote;
  readonly defaultLogisticCostMethod: 'global' | 'unit' | 'cost' | 'list';
  readonly openned: boolean;
};

export default class App extends React.Component<IAppProps> {
  public state: IAppState = {
    quote: new UBQuote(),
    movingIndex: -1,
    search: '',
    value: '',
    move: false,
    isEditing: false,
    isInLotUpdating: false,
    isCSVImportOpen: false,
    itemsSelected: [],
    sectionsSelected: [],
    confirmation: null,
    selectingClient: true,
    organisationsAddress: [],
    slider: [],
    errors: [],
    newContact: UBContact.createEmptyContact(),
    newClient: UBClient.createEmptyClient(),
    defaultLogisticCostMethod: 'global',
    openned: true
  }

  private timer = 0;

  public componentDidMount() {
    if (process.env.NODE_ENV !== 'development') {
      // @ts-ignore
      window.jQuery.fn.datepicker.defaults.format = "dd/mm/yyyy";
    }

    window.addEventListener('click', this._listenToCloseItem);

    const quoteIdFromUrl = window.location.href.split('id=')[1] ? window.location.href.split('id=')[1].split('&')[0] : false;
    const jsonQuote = localStorage.getItem(quoteIdFromUrl ? quoteIdFromUrl : 'quote');

    this.timer = window.setInterval(() => this.state.quote.get().id !== -1 && this.state.quote.saveQuote(), 1000 * 60 * 10);

    if (!jsonQuote) {
      return false;
    }

    const parsedQuote: IQuote = JSON.parse(jsonQuote) as IQuote;

    if (quoteIdFromUrl && quoteIdFromUrl !== parsedQuote.number) {

    } else {
      const quote = { ...parsedQuote, depot: Number(parsedQuote.depot), installationAndShippingParam: Number(parsedQuote.installationAndShippingParam), discountParam: Number(parsedQuote.discountParam), discountMethod: parsedQuote.discountMethod, installationAndShippingMethod: parsedQuote.installationAndShippingMethod, date: moment(parsedQuote.date).isValid() ? moment(parsedQuote.date) : moment(), validUntil: moment(parsedQuote.validUntil).isValid() ? moment(parsedQuote.validUntil) : moment(), sections: _.map(parsedQuote.sections, section => {
        return { ...createEmptySectionFunctions(), ...section, items: _.map(section.items, item => {
          //@ts-ignore
          return { ...createEmptyItemFunctions(), ...item, quantity: parseFloat(item.quantity), price: parseFloat(item.price), priceParameterValue: parseFloat(item.priceParameterValue), expeditionPrice: parseFloat(item.expeditionPrice), expeditionPercent: parseFloat(item.expeditionPercent) };
        }) };
      }) };

      this.setState({ quote: new UBQuote(quote) }, () => {
        this.state.quote.fetchTaxesByProvince().then(taxes => {
          this.state.quote.update('billTo', { ...this.state.quote.get().billTo, taxes });
        });
      });
    }
  }

  private _listenToCloseItem = (e: MouseEvent) => {
    const current = e.target as HTMLElement;

    if (current.classList.contains('quote-paper-container') || current.classList.contains('file')) {
      this._foldAllItem();
    }
  }

  public componentWillUnmount() {
    clearInterval(this.timer);
    window.removeEventListener('click', this._listenToCloseItem)
  }

  private _showCustomConfirmation = (confirmation: IConfirmation) => this.setState({ confirmation: { ...confirmation, onOk: () => {
    Promise.all([confirmation.onOk()]).then(() => {
      this._resetConfirmation();
    });
  }, onCancel: () => this._resetConfirmation()} }, () => {
      document.getElementsByTagName('body')[0].classList.add('stop-scrolling');
  });

  private _selectSection = (sectionId: number, selected: boolean) => {
    if (selected && _.filter(this.state.sectionsSelected, section => section === sectionId).length > 0) {
      return;
    }

    if (!selected && _.filter(this.state.sectionsSelected, section => section === sectionId).length === 0) {
      return;
    }

    if (selected) {
      return this.setState({ sectionsSelected: [ ...this.state.sectionsSelected, sectionId ] });
    }

    if (!selected) {
      return this.setState({ sectionsSelected: _.filter(this.state.sectionsSelected, id => id !== sectionId) });
    }
  }

  private _selectItem = (itemId: number, selected: boolean) => {
    if (selected && _.filter(this.state.itemsSelected, item => item === itemId).length > 0) {
      return;
    }

    if (!selected && _.filter(this.state.itemsSelected, item => item === itemId).length === 0) {
      return;
    }

    if (selected) {
      return this.setState({ itemsSelected: [ ...this.state.itemsSelected, itemId ] });
    }

    if (!selected) {
      return this.setState({ itemsSelected: _.filter(this.state.itemsSelected, id => id !== itemId) });
    }
  }

  private _openImportModal = () => {
    this.setState({ isCSVImportOpen : true });
  };

  private _closeImportModal = () => {
    this.setState({ isCSVImportOpen : false });
  };

  private _updateQuote = (param: string, value: any) => {
    return this.state.quote.update(param, value).then(quote => {
      this.setState({ quote }, () => {
        switch (param) {
          case 'availableDiscounts':
            const discounts = value as Array<IDiscount>;
            this._updateQuote('sections', _.map(this.state.quote.getSections(), (section, sectionIndex) => {
              return { ...section, items: _.map(section.items, item => {
                const dis = _.filter(discounts, discount => discount.name === item.discount.name);

                if (dis.length > 0) {
                  return { ...item, discount: dis[0] };
                }

                return item;
              }) }
            }))
            return;
          default:
            return;
        }
      });
    });
  }

  private _closeAllSections = (actualValue: boolean, callback: (value: boolean) => void) => {
    if (!actualValue) {
      this._updateQuote('sections', _.map(this.state.quote.getSections(), section => ({ ...section, folded: true }))).then(() => {
        callback(true);
      });
    } else {
      this._updateQuote('sections', _.map(this.state.quote.getSections(), section => ({ ...section, folded: false }))).then(() => {
        callback(false);
      });
    }
  }

  private _getSlidingIndex = (position: number, sectionsPositions: Array<number>, checkAt: number = 0): number => {
    if (position <= sectionsPositions[checkAt]) {
      return checkAt;
    }

    return this._getSlidingIndex(position, sectionsPositions, checkAt++);
  }

  private _handleEditing = (isEditing: boolean) => {
    this.setState({ isEditing }, () => {
      if (isEditing) {
        document.getElementsByTagName('body')[0].classList.add('stop-scrolling');
      } else {
        document.getElementsByTagName('body')[0].classList.remove('stop-scrolling');
      }
    });
  };

  private _saveContact = (newContact: IClient) => {
    const formData = new FormData();

    _.forEach(newContact, (info, key) => {
      switch(key) {
        case 'phone':
          formData.append('tel', String(info));
          break;
        case 'cell':
          formData.append('mobile', String(info));
          break;
        default:
          formData.append(key, info ? String(info) : '');
          break;
      }
    });

    return fetch(getBaseUrl() + 'webservices/contact/add', {
      method: 'POST',
      body: formData,
    }).then(res => ({ ok: res.ok, newId: res.json()}));
  };

  private _setAddress = (address: IAddress | null, param: AddressTypeEnum) => {
    return this.state.quote.update(param, address).then(quote => this.setState({ quote }));
  }

  private _updateAddress = async (address: IAddress, param: AddressTypeEnum) => {
    let quote = await this.state.quote.update(param, address);

    if (param === AddressTypeEnum.BILL_TO) {
      try {
        const taxes = await this.state.quote.fetchTaxesByProvince();
        quote = await this.state.quote.update('billTo', { ...this.state.quote.get().billTo, taxes });
      } catch (e) {
        console.log(e);
      }
    }

    if (param === AddressTypeEnum.BILL_TO && !this.state.quote.get().shipTo) {
      quote = await quote.update(AddressTypeEnum.SHIP_TO, address);
    }

    return this.setState({ isEditing: false, quote });
  }

  private _updateAddressInQuote = async (newAddressInQuote: IAddressInQuote, param: AddressTypeEnum) => {
    let quote = await this.state.quote.update(param, newAddressInQuote);

    if (param === AddressTypeEnum.BILL_TO) {
      try {
        const taxes = await this.state.quote.fetchTaxesByProvince();
        quote = await this.state.quote.update(param === AddressTypeEnum.BILL_TO ? 'billTo' : 'shipTo', { ...this.state.quote.get()[param === AddressTypeEnum.BILL_TO ? 'billTo' : 'shipTo'], taxes });
      } catch (e) {
        console.log(e);
      }
    }

    if (param === AddressTypeEnum.BILL_TO && !this.state.quote.get().shipTo.client) {
      quote = await quote.update(AddressTypeEnum.SHIP_TO, newAddressInQuote);
    }

    await this.state.quote.saveQuote()

    return this.setState({ isEditing: false, quote });
  }

  private _loadPossibleDiscount = (inputValue: string, callback: Function) => {
    fetch(getBaseUrl() + 'webservices/manufacturier/discounts.php?all=1&q=' + inputValue)
      .then(res => res.json())
      .then(json => {
        callback([..._.map(json, discount => {
          // let found = _.find(this.state.quote.getAllDiscounts(), (discountInState: IDiscount) => discountInState.name === discount.title);
          // if (found) {
          //   return {
          //     data: found,
          //     label: found.name,
          //     value: found.code,
          //   } as ISelectValue
          // }

          return {
            data: discount,
            label: discount.title,
            value: discount.id,
        } as ISelectValue
      }), ..._.map(_.filter(this.state.quote.getAllDiscounts(), dis => dis.name === inputValue), discount => ({ value: discount.code, label: discount.name + ' - (Déjà dans la soumission)', data: discount } as ISelectValue))]);
    });
  }

  private _loadItemOptions = (inputValue: string, callback: Function) => {
    fetchItem(inputValue).then(items => {
      callback(_.map(items, item => {
        const selectValue = { label: `${item.SKU} - ${item.title} - ${moneyFormat(item.price)}`, value: item.id, data: item } as ISelectValue;
        return selectValue;
      }));
    });
  }

  private _onDragEnd = (result: any) => {
    if(!result.destination) {
       return;
    }

    if (result.type === 'item') {
      const fromSection = Number(result.source.droppableId.split('section-')[1]);
      const toSection = Number(result.destination.droppableId.split('section-')[1]);
      const fromPosition = Number(result.source.index);
      const toPosition = Number(result.destination.index);

      if (fromSection === toSection) {
        if (fromPosition === toPosition) {
          return;
        }

        const sections = _.map(this.state.quote.getSections(), (section, sectionIndex) => {
          if (sectionIndex !== fromSection) {
            return section;
          }

          return { ...section, items: this._reorderItem(
            section.items,
            fromPosition,
            toPosition
          )};
        });

        this.state.quote.update('sections', sections).then(quote => this.setState({ quote }));
      } else {
        const itemToChange = this.state.quote.getSections()[fromSection].items[fromPosition];

        const sections = _.map(this.state.quote.getSections(), (section, sectionIndex) => {
          if (sectionIndex === toSection) {
            return { ...section, items: [ ...section.items.slice(0, toPosition), itemToChange, ...section.items.slice(toPosition) ] };
          }

          if (sectionIndex == fromSection) {
            return { ...section, items: [ ...section.items.slice(0, fromPosition), ...section.items.slice(fromPosition + 1) ] }
          }

          return section;
        });

        this.state.quote.update('sections', sections).then(quote => this.setState({ quote }));
      }
    }

    if (result.type === 'section') {
      const sections = this._reorder(
        this.state.quote.getSections(),
        result.source.index,
        result.destination.index
      );
      this.state.quote.update('sections', sections).then(quote => this.setState({ quote }));
      return;
    }
  }

  private _foldAll = () => {
    this.state.quote.update('sections', _.map(this.state.quote.getSections(), (section, sId) => {
      return { ...section, items: _.map(section.items, (item, iId) => {
        return { ...item, isFocused: false };
      }) }
    })).then(quote => this.setState({ quote }));
  }

  private _foldAllItem = () => {
    this.state.quote.update('sections', _.map(this.state.quote.getSections(), (section, sId) => {
      return { ...section, items: _.map(section.items, (item, iId) => {
        return { ...item, isFocused: false};
      }) }
    })).then(quote => this.setState({ quote }));
  }

  private _foldAllOtherItem = (itemToKeep: number, inSection: number) => {
    this.state.quote.update('sections', _.map(this.state.quote.getSections(), (section, sId) => {
      // if (sId !== inSection) {
      //   return section;
      // }

      return { ...section, items: _.map(section.items, (item, iId) => {
        return { ...item, isFocused: iId === itemToKeep && sId === inSection };
      }) }
    })).then(quote => this.setState({ quote }));
  }

  private _reorder = (list: Array<ISection>, startIndex: number, endIndex: number) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  private _reorderItem = (list: Array<IItem>, startIndex: number, endIndex: number) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  private _resetConfirmation = () => this.setState({ confirmation: null }, () => {
    document.getElementsByTagName('body')[0].classList.remove('stop-scrolling');
  });

  private _addNewSection = () => this._updateQuote('sections', [ ...this.state.quote.getSections(), createEmptySection(this.state.quote.getSections().length) ] );

  private _addNewSectionFirst = () => {
    const sections = [ ...this.state.quote.getSections(), createEmptySection(this.state.quote.getSections().length) ] ;
    const first_section = sections.pop();
    this._updateQuote('sections', [
      first_section,
      ...sections
    ] );
  }


  private _updateQuoteItem = (index: number, itemIndex: number, param: string, value: any) => {
    return this._updateQuote('sections', _.map(this.state.quote.getSections(), (section, id: number) => {
      if (id !== index) {
        return section;
      }

      return { ...section, items: _.map(section.items, (item, itemId: number) => {
        if (itemIndex !== itemId) {
          return item;
        }

        if (param.length === 0) {
          return { ...item, ...value, isFocused: true };
        }

        if (param === 'expeditionPriceMethod') {
          this.setState({ defaultLogisticCostMethod: value });
        }

        return { ...item, [param]: value, isFocused: true };
      }) };
    })).then(() => {
      return true;
    })
  }

  private _duplicateItemToSection = (index: number, itemIndex: number) => {
    this._updateQuote('sections', _.map(this.state.quote.getSections(), (section, sectionId) => {
      if (sectionId !== index) {
        return section;
      }

      return { ...section, items: [ ...section.items.slice(0, itemIndex), { ...section.items[itemIndex] }, ...section.items.slice(itemIndex) ] }
    }))
  }

  private _deleteSection = (index: number) => {
    this.setState({
      confirmation: {
        message: 'Voulez vous vraiment supprimer cette section?',
        title: 'Confirmation',
        onCancel: () => {
          this._resetConfirmation();
        },
        onOk: () => {
          this._updateQuote('sections', [...this.state.quote.getSections().slice(0, index), ...this.state.quote.getSections().slice(index + 1)]).then(() => this._resetConfirmation());
        },
      } as IConfirmation
    });
  }

  private _deleteDiscountNow = (index: number, discountName: string, callback: Function) => {
    this.setState({ quote: this.state.quote.deleteDiscountAtIndex(index, discountName) });
    this._updateQuote('availableDiscounts', [...this.state.quote.get().availableDiscounts.slice(0, index), ...this.state.quote.get().availableDiscounts.slice(index + 1)]).then(() => {
      this._resetConfirmation();
      callback();
    });
  }

  private _deleteDiscount = (index: number, discountName: string, callback: Function) => {
    this.setState({
      confirmation: {
        message: 'Voulez vous vraiment supprimer cet escompte?',
        title: 'Confirmation',
        onCancel: () => {
          this._resetConfirmation();
        },
        onOk: () => {
          this.setState({ quote: this.state.quote.deleteDiscountAtIndex(index, discountName) });
          this._updateQuote('availableDiscounts', [...this.state.quote.get().availableDiscounts.slice(0, index), ...this.state.quote.get().availableDiscounts.slice(index + 1)]).then(() => {
            this._resetConfirmation();
            callback();
          });
        },
      } as IConfirmation
    });
  }

  private _moveItemToSectionWithPosition = (prevItemIndex: number, prevSectionIndex: number, nextSectionIndex: number, position: 'top' | 'bottom') => {
    if (prevSectionIndex === nextSectionIndex) {
      const sections = _.map(this.state.quote.getSections(), (section, sectionIndex) => {
        if (sectionIndex !== prevSectionIndex) {
          return section;
        }

        if (position === 'top') {
          return { ...section, items: this._reorderItem(
            section.items,
            prevItemIndex,
            0
          )};
        } else {
          return { ...section, items: this._reorderItem(
            section.items,
            prevItemIndex,
            section.items.length
          )};
        }
      });

      this.state.quote.update('sections', sections).then(quote => this.setState({ quote }));
    } else {
      const itemToChange = this.state.quote.getSections()[prevSectionIndex].items[prevItemIndex];

        const sections = _.map(this.state.quote.getSections(), (section, sectionIndex) => {
          if (sectionIndex === nextSectionIndex) {
            return { ...section, items: [ ...section.items.slice(0, position === 'top' ? 0 : section.items.length), itemToChange, ...section.items.slice(position === 'top' ? 0 : section.items.length) ] };
          }

          if (sectionIndex == prevSectionIndex) {
            return { ...section, items: [ ...section.items.slice(0, prevItemIndex), ...section.items.slice(prevItemIndex + 1) ] }
          }

          return section;
        });

        this.state.quote.update('sections', sections).then(quote => this.setState({ quote }));
    }
  }

  private _toggleFolded = (index: number) => {
    this._updateQuote('sections', _.map(this.state.quote.getSections(), (section, id: number) => {
      if (index !== id) {
        return section;
      }
      return { ...section, folded: !section.folded };
    }));
  }

  private _deleteItemOfSection = (index: number, itemIndex: number) => {
    this._updateQuote('sections', _.map(this.state.quote.getSections(), (section, id: number) => {
      if (id !== index) {
        return section;
      }
      return { ...section, items: [...section.items.slice(0, itemIndex), ...section.items.slice(itemIndex + 1)] };
    }));
  }

  private _getValuesFromCSV = (): Promise<any> => {
    const input = document.getElementById('file_to_import') as HTMLInputElement;

    if (!input.files) {
      return new Promise(resolve => resolve(false));
    }

    const file = input.files[0];
    const formData = new FormData();

    formData.append('csv', file);

    return fetch(getBaseUrl() + 'webservices/item/import/csv.php', {
      method: 'POST',
      body: formData,
    })
    .then(res => res.json())
    .then(json => json)
    .catch(() => false);
  }

  private _loadSectionsForSelect = (keywords: string, callback: Function) => {
    const sections = this.state.quote.getSections();
    const rel = _.filter(sections, section => section.title.toLowerCase().indexOf(keywords.toLowerCase()) > -1);

    callback(_.map(rel, section => ({
      label: section.title,
      value: section.id,
      data: section,
    } as ISelectValue )));
  }

  private _insertDataIntoSection = async (data: Array<{}>, section: number | string) => {
    // console.log(data);
    // console.log(section);

    await this._updateQuote('sections',
      [
        ...this.state.quote.getSections(),
        createEmptySection(this.state.quote.getSections().length)
      ]
    );

    const section_index = ( this.state.quote.getSections().length - 1 );

    await this.state.quote.saveQuote();

    this._insertData(data, section_index);

    const jsonQuote = JSON.stringify({
      ...this.state.quote.get(),
      'csv_import': data,
      date: this.state.quote.get().date.isValid() ? this.state.quote.get().date.format('DD-MM-YYYY hh:mm:ss') : moment().format('DD-MM-YYYY hh:mm:ss'),
      validUntil: this.state.quote.get().validUntil.isValid() ? this.state.quote.get().validUntil.format('DD-MM-YYYY hh:mm:ss') : moment().format('DD-MM-YYYY hh:mm:ss'),
      total: this.state.quote.calculateQuoteTotal(),
      subTotal: this.state.quote.calculateSubTotal(),
      TPS: this.state.quote.calculateTPS(),
      TVQ: this.state.quote.calculateTVQ(),
    });

    const value = await fetch(getBaseUrl() + '/webservices/quote/update_json.php?id=' + this.state.quote.get().number, {
      method: 'POST',
      body: jsonQuote,
    }).then((res) => res.json()).then(json => {
        window.location.href=getBaseUrl()+'/ventes/soumission/load?id='+this.state.quote.get().number;
      }
    );
    return value;


  }

  private _insertData = (data: Array<{}>, sectionIndex: number): boolean => {
    this._updateQuote('sections', _.map(this.state.quote.getSections(), (section, sectionId: number) => {
      if (sectionId !== sectionIndex) {
        return section;
      }

      //@ts-ignore
      return {
        ...section,
        items: [
          ...section.items,
          ..._.map(data, itemFormCSV => ({
            ...createEmptyItem(),
            ...itemFormCSV,
            //@ts-ignore
            SKU: itemFormCSV['Part Number'],
            //@ts-ignore
            title: itemFormCSV['Part Description'],
            //@ts-ignore
            quantity: !isNaN(itemFormCSV['Qty']) ? Number(itemFormCSV['Qty']) : 0,
            profitMethod: ProfitMethod.FIXED_PRICE,
            //@ts-ignore
            price: !isNaN(parseFloat(String(itemFormCSV['List']).replace('$', '').replace(',', '.'))) ? parseFloat(String(itemFormCSV['List']).replace('$', '').replace(',', '.')) : 0,
            // priceParameterValue: !isNaN(parseFloat(String(itemFormCSV['List']).replace('$', '').replace(',', '.'))) ? parseFloat(String(itemFormCSV['List']).replace('$', '').replace(',', '.')) : 0
            priceParameterValue: 0
          } as IItem))
        ]
      };
    }));

    return true;
  }


  private _addItemToSection = (index: number) => {
    this._foldAll();
    return this._updateQuote('sections', _.map(this.state.quote.getSections(), (section, id: number) => {
      if (id === index) {
        return { ...section, items: [...section.items, createEmptyItem(section.items.length)] };
      }

      return section;
    }));
  }

  private _loadContacts = (keywords: string, callback: Function) => {
    if (keywords.length < 2) return [];

    this.state.quote.fetchAddresses(keywords).then(addresses => {
      callback(_.map(_.filter(addresses, address => address.address !== null), address => {
        const selectValue = {
          data: address,
          label: `${address.first_name} ${address.last_name} - ${address.email || 'Aucun courriel'} - ${(address.organisations[0] && address.organisations[0].entreprise) || 'Aucune organisation'} - ${address.postal_code}`,
          value: address.id
        } as ISelectValue;

        return selectValue;
      }));
    });
  }

  private _updateAvailableDiscounts = (discounts: Array<IDiscount>) => {
    this.state.quote.update('availableDiscounts', discounts).then(newQuote => this.setState({ quote: newQuote }));
  }

  private _handlerSlider = (images: Array<string>) => {
    this.setState({ slider: images });
  }

  private _getItemCountBeforeThisSection = (sectionIndex: number) => {
    let itemCount = 0;

    for (let i = 0; i < sectionIndex; i++) {
      itemCount = itemCount + this.state.quote.getSections()[i].items.length;
    }

    return itemCount;
  }

  private _confirmationWithCallback = (callback: Function, msg: string) => {
    this._showCustomConfirmation({
      message: msg,
      onCancel: () => {},
      onOk: () => callback(),
      title: 'Confirmation'
    } as IConfirmation);
  }

  private _addAddressToClientAndSelect = async (address: IAddress, client_id: number) => {
    const addAddressToClient = await fetch(`${getBaseUrl()}webservices/client/addresses/add.php`, {
      method: 'POST',
      body: JSON.stringify({ data: { address, client_id } })
    });

    const json = await addAddressToClient.json();

    return json;
  }

  private _addContactToClientAndSelect = async (contact: IContact, client_id: number) => {
    const addContactToClient = await fetch(`${getBaseUrl()}webservices/client/contacts/add.php`, {
      method: 'POST',
      body: JSON.stringify({ data: { contact, client_id } })
    });

    const json = await addContactToClient.json();

    return json;
  }

  private _goToSection = (id: string) => {
    let scroll = Scroll.animateScroll;
    scroll.scrollTo(this._retrieveY(id));
  }

  private _retrieveY = (id: string) => {
    //@ts-ignore
    let section = document.getElementById(id) as HTMLElement;
    let y = 0;

    if (!section) {
      return y;
    }

    while (section && !isNaN(section.offsetTop)) {
      y += section.offsetTop - section.scrollTop;
      section = section.offsetParent as HTMLElement;
    }

    return y > 140 ? y - 140 : y;
  }

  public render() {
    const clientElement = document.getElementById('quote-app') as HTMLElement;
    const sidebar = document.getElementsByClassName('page-sidebar')[0] as HTMLElement;
    const header = document.getElementsByClassName('header')[0] as HTMLElement;

    let sidebarWidth = 0
    let width = 400;
    let headerHeight = 40;

    if (sidebar) {
      sidebarWidth = sidebar.getBoundingClientRect().right;
    }

    if (clientElement) {
      width = clientElement.getBoundingClientRect().width + 30;
    }

    if (header) {
      headerHeight = header.getBoundingClientRect().height;
    }

    return (
      <div>
        {this.state.quote.clientIsSet() ?
          <PaperLike>
            {this.state.isEditing &&
              <div className="editing-bg">

              </div>
            }
            {this.state.confirmation &&
              <ConfirmationModal
                confirmation={this.state.confirmation}
              />
            }
            {this.state.slider.length > 0 &&
              <Modal
                title="Images"
                close={() => {
                  this.setState({ slider: [] });
                  return {};
                }}
                doneBtnAction={() => {
                  this.setState({ slider: [] });
                  return {};
                }}
                doneBtnText="Ok"
              >
                <div style={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: '100%',
                }}>
                  {_.map(this.state.slider, (img, imgIndex) => (
                    <img key={`img-${imgIndex}`} src={img} />
                  ))}
                </div>
              </Modal>
            }

            <div style={{ left: sidebarWidth, width }} id="the-sticky" className="nav-will-be fixed">
              <Overview
                closeAllSections={this._closeAllSections}
                saveQuote={() => saveQuote(this.state.quote.get()).then(res => {
                  if (res.id) {
                    this.state.quote.update('id', res.id);
                  }
                })}
                isCSVImportOpen={this.state.isCSVImportOpen}
                closeImportModal={this._closeImportModal}
                openImportModal={this._openImportModal}
                updateQuote={(param: string, value: any) => this.state.quote.update(param, value).then(quote =>this.setState({ quote }))}
                onInLotStateChange={(isInLotUpdating: boolean) => this.setState({ isInLotUpdating, sectionsSelected: [], itemsSelected: [] })}
                isInLotUpdating={this.state.isInLotUpdating}
                onModalNeeded={this._showCustomConfirmation}
                quote={this.state.quote}
                addNewSection={this._addNewSection}
                updateDiscounts={this._updateAvailableDiscounts}
                availableDiscounts={this.state.quote.get().availableDiscounts}
                loadPossibleDiscount={this._loadPossibleDiscount}
                deleteDiscount={this._deleteDiscount}
              />
            </div>

            <Header
              quote={this.state.quote}
              updateQuote={this._updateQuote}
              onModalNeeded={this._showCustomConfirmation}
              confirmationWithCallback={this._confirmationWithCallback}
            />
            <Addresses
              quote={this.state.quote}
              onEditing={this._handleEditing}
              handleSave={this._saveContact}
              updateAddress={this._updateAddress}
              setAddress={this._setAddress}
              updateAddressInQuote={this._updateAddressInQuote}
              addAddressToClientAndSelect={this._addAddressToClientAndSelect}
              addContactToClientAndSelect={this._addContactToClientAndSelect}
              confirmationWithCallback={this._confirmationWithCallback}
            />

              {this.state.isCSVImportOpen &&
                <CSVImportModal
                close={() => this._closeImportModal()}
                getValuesFromCSV={this._getValuesFromCSV}
                insertDataIntoSection={this._insertDataIntoSection}
                loadSectionsForSelect={this._loadSectionsForSelect}
                sections={_.map(this.state.quote.getSections(), (section, sectionIndex) => ({ sectionId: sectionIndex, sectionTitle: section.title }))}
                />
              }
                <div className="overview-container">
                  <div className="ibox">
                    <div className="ibox-head hide">
                      <div className="ibox-title"><h3 className="">Résumé</h3></div>
                    </div>
                    <div className="ibox-body">
                      <table className="table table-striped">
                        <thead>
                          <tr className="ft-row">
                            <th className="">&nbsp;</th>
                            <th className="text-right"><span>Vendant</span></th>
                            <th className="text-right"><span>Bénéfice brut</span></th>
                            <th className="text-right"><span>GP%</span></th>
                            <th className="text-right"><span>Coût total</span></th>
                            <th className="text-right"><span>Profit total</span></th>
                          </tr>
                        </thead>
                        <tbody>


                            {_.map(this.state.quote.getSections(), (section, index) => {
                              return (
                                <tr className="hide" onClick={() => this._goToSection(`section-${index}`)} key={`overview-${index}`}>
                                  <td className="">{section.title}</td>
                                  <td className="bold text-right"><span>{moneyFormat(section.calculateTotal())}</span></td>
                                  <td className="text-right">{moneyFormat(section.calculateProfit())}</td>
                                  <td className="text-right">{section.calculateGrossProfit().toFixed(2)} %</td>
                                  <td className="text-right">{moneyFormat(section.calculateCost())}</td>
                                  <td className="text-right">{moneyFormat(section.calculateProfit())}</td>
                                </tr>
                              );
                            })}

                          {this.state.quote.getSections().length > 3 &&
                            <tr>

                            </tr>
                          }
                          <tr className="" onClick={() => this._goToSection('totals')}>
                            <td className=""><span>Total</span></td>
                            <td className="bold text-right"><span>{moneyFormat(this.state.quote.calculateOverallTotal())}</span></td>
                            <td className="bold text-right"><span>{moneyFormat(this.state.quote.calculateOverallProfit())}</span></td>
                            <td className="bold text-right"><span>{this.state.quote.calculateOverallGrossProfit().toFixed(2)} %</span></td>
                            <td className="bold text-right"><span>{moneyFormat(this.state.quote.calculateOverallCost())}</span></td>
                            <td className="bold text-right"><span>{moneyFormat(this.state.quote.calculateOverallProfit())}</span></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              <Suppliers
                quote={this.state.quote}
                updateQuote={this._updateQuote}
                updateDiscounts={this._updateAvailableDiscounts}
                availableDiscounts={this.state.quote.get().availableDiscounts}
                loadPossibleDiscount={this._loadPossibleDiscount}
                deleteDiscount={this._deleteDiscountNow}
                confirmationWithCallback={this._confirmationWithCallback}
              />
              <OtherCosts
                quote={this.state.quote}
                updateQuote={this._updateQuote}
                confirmationWithCallback={this._confirmationWithCallback}
              />

            <Container>
              <div className="ibox">
                <div className="ibox-head">
                  <div className="ibox-title">
                    <h3>Sections</h3>
                    <div>
                      <i style={{ cursor: 'pointer', marginRight: 10 }} className={`la la-plus-circle`} onClick={() => this._addNewSection()}></i>
                      <i className={`la la-chevron-${this.state.openned ? 'up' : 'down'}`} onClick={() => this.setState({ openned: !this.state.openned })}></i>
                    </div>
                  </div>
                </div>
                <Body className={`ibox-body ${!this.state.openned ? 'closed' : ''}`}>

            <DragDropContext onDragEnd={this._onDragEnd}>
              <Droppable droppableId={`all-sections`} direction="vertical" type="section">
                {(droppableProvided, snapshot) => (
                  <div
                    ref={droppableProvided.innerRef}
                    {...droppableProvided.droppableProps}
                    style={{ backgroundColor: snapshot.isDraggingOver ? '#f04c2538': '' }}
                  >
                    {_.map(this.state.quote.getSections(), (section, index: number) =>
                      <Draggable draggableId={`section-${index}`} key={`section-${index}`} index={index}>
                        {draggableProvided => (
                          <div
                            ref={draggableProvided.innerRef}
                            { ...draggableProvided.draggableProps }
                          >
                            <Section
                              availableDiscounts={this.state.quote.get().availableDiscounts}
                              draggableProvided={draggableProvided}
                              key={`section-${index}`}
                              isItemChecked={(id) => _.indexOf(this.state.itemsSelected, id) > -1}
                              isSectionChecked={(id) => _.indexOf(this.state.sectionsSelected, id) > -1}
                              section={section}
                              index={index}
                              loadOptions={this._loadItemOptions}
                              isSelectingEnabled={this.state.isInLotUpdating}
                              onSelectSectionChange={this._selectSection}
                              onSelectItemChange={this._selectItem}
                              updateQuoteSection={(param: string, value: any) => this.state.quote.update('sections', _.map(this.state.quote.getSections(), (section, sectionIndex) =>
                                sectionIndex !== index ? section : { ...section, [param]: value }
                              )).then(quote => this.setState({ quote }))}
                              updateQuoteItems={(items: Array<IItem>) => this.state.quote.update('sections', _.map(this.state.quote.getSections(), (section, sectionIndex) =>
                                sectionIndex !== index ? section : { ...section, items }
                              )).then(quote => this.setState({ quote }))}
                              updateQuoteItem={this._updateQuoteItem}
                              addSection={() => {
                                this._addNewSectionFirst();
                              }}
                              duplicateItemToSection={this._duplicateItemToSection}
                              duplicateSection={(index: number) =>
                                this._updateQuote('sections', [ ...this.state.quote.getSections().slice(0, index), { ...this.state.quote.getSections()[index], folded: false }, ...this.state.quote.getSections().slice(index) ])}
                              deleteSection={this._deleteSection}
                              addItemToSection={this._addItemToSection}
                              deleteItemOfSection={this._deleteItemOfSection}
                              toggleFolded={this._toggleFolded}
                              foldAllOtherItem={this._foldAllOtherItem}
                              handleSlider={this._handlerSlider}
                              confirmationWithCallback={this._confirmationWithCallback}
                              sections={this.state.quote.getSections()}
                              moveItemToSectionWithPosition={this._moveItemToSectionWithPosition}
                              startIndex={this._getItemCountBeforeThisSection(index)}
                              defaultLogisticCostMethod={this.state.defaultLogisticCostMethod}
                            />
                            {draggableProvided.placeholder}
                          </div>
                        )}
                      </Draggable>
                    )}
                    {droppableProvided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
            <div className="section-row">
              <div className="row">
                <div className="col-md-12">
                  <button className="btn btn-outline-success btn-air btn-rounded btn-sm btn-float-right" onClick={this._addNewSection}>
                    <span className="btn-icon"><i className="la la-plus-circle"></i>Ajouter une section</span>
                  </button>
                </div>
              </div>
            </div><p/>
            </Body>
          </div>
        </Container>


            <Total
              quote={this.state.quote}
              updateQuote={this._updateQuote}
              onModalNeeded={this._showCustomConfirmation}
            />
          </PaperLike>
          :
          <PaperLike>
            <SectionHtml>
              <div>
                <SectionTitle>
                  {this.state.selectingClient ?
                    'Sélection du contact'
                  :
                    'Création d\'un nouveau contact'
                  }
                </SectionTitle>
                <NewClientRow>
                  {this.state.selectingClient ?
                    <Link className="add-client-btn" onClick={() => this._createNewClientForm()}><i className="fa fa-address-card-o"></i> Nouveau contact</Link>
                  :
                    <Link className="search-client-btn" onClick={() => this._selectClientWithSelect()}><i className="ti-search"></i> Rechercher un contact</Link>
                  }
                </NewClientRow>
                {this.state.selectingClient ?
                  <div className="form-group p-2 select-client multi-collapse-client collapse show">
                    <SelectAsync
                      isSearchable={true}
                      isClearable={true}
                      isCreatable={false}
                      value={this.state.quote.getBillToSelect()}
                      loadFunction={this._loadContacts}
                      onChange={this._handleClientChange}
                      placeholder="Rechercher un contact"
                    />
                    {this.state.quote.billToClientIsSetAndHasAddresses() &&
                      <div className="ibox" style={{ marginTop: 10 }}>
                        <div className="ibox-head">
                          <div className="ibox-title">Gestion du client - Adresse et contact</div>
                          <ul className="nav nav-tabs tabs-line">
                            <li className="nav-item">
                              <a className="nav-link active" href="#tab-1-1" data-toggle="tab">Adresse</a>
                            </li>
                            <li className="nav-item">
                              <a className="nav-link" href="#tab-1-2" data-toggle="tab">Contact</a>
                            </li>
                          </ul>
                        </div>
                        <div className="ibox-body">
                          <div className="tab-content">
                            <div className="tab-pane fade show active text-center" id="tab-1-1">
                              {_.map(this.state.quote.getBillToClientAddresses(), (address, index) => {
                                return address && (
                                  <div onClick={() => this.setState({ addressInQuote: { ...this.state.addressInQuote, address } })} className={`cursored ibox ${this.state.addressInQuote && this.state.addressInQuote.address === address ? 'selected' : ''}`} key={`organisation-address-${index}`}>
                                    <div className="ibox-head">
                                      <h3>Adresse de compte client - {this.state.newClient.title}</h3>
                                      <button className="close" type="button" onClick={() => this.setState({ addressInQuote: { ...this.state.addressInQuote, address } })} aria-label="Sélectionner">
                                        <span aria-hidden="true"><i className={`la la-${this.state.addressInQuote && this.state.addressInQuote.address === address ? 'check-' : ''}circle`} /></span>
                                      </button>
                                    </div>
                                    <div className="ibox-body">
                                      <p>
                                        {address.address}
                                      </p>
                                    </div>
                                  </div>
                                )
                              })}
                            </div>
                            <div className="tab-pane fade text-center" id="tab-1-2">
                              {_.map(this.state.quote.getBillToClientContacts(), (contact, index) => {
                                return contact && (
                                  <div onClick={() => this.setState({ addressInQuote: { ...this.state.addressInQuote, contact } })} className={`cursored ibox ${this.state.addressInQuote && this.state.addressInQuote.contact === contact ? 'selected' : ''}`} key={`organisation-address-${index}`}>
                                    <div className="ibox-head">
                                      <h3>Contact de compte client - {this.state.newClient.title}</h3>
                                      <button className="close" type="button" onClick={() => this.setState({ addressInQuote: { ...this.state.addressInQuote, contact } })} aria-label="Sélectionner">
                                        <span aria-hidden="true"><i className={`la la-${this.state.addressInQuote && this.state.addressInQuote.contact === contact ? 'check-' : ''}circle`} /></span>
                                      </button>
                                    </div>
                                    <div className="ibox-body">
                                      <p>
                                        {contact.first_name} {contact.last_name}
                                      </p>
                                    </div>
                                  </div>
                                )
                              })}
                            </div>
                          </div>
                        </div>
                      </div>
                    }
                  </div>
                :
                  <div className="form-group p-2 select-client multi-collapse-client collapse show">
                    <div className="container">
                      <div className="row">
                        <div className={`col-6 form-group mb-4${this._isInvalid('first_name') ? ' has-error' : ''}`}>
                          <input value={this.state.newContact.first_name} onChange={(e) => this._updateNewContact('first_name', e.currentTarget.value)} className="form-control form-control-line" id="contact-first-name" type="text" name="first_name" placeholder="Prénom*" />
                          {this._isInvalid('first_name') && <label className="help-block error">{this._getErrorMsg('first_name')}</label>}
                        </div>
                        <div className={`col-6 form-group mb-4${this._isInvalid('last_name') ? ' has-error' : ''}`}>
                          <input value={this.state.newContact.last_name} onChange={(e) => this._updateNewContact('last_name', e.currentTarget.value)} className="form-control form-control-line" id="contact-first-name" type="text" name="last_name" placeholder="Nom*" />
                          {this._isInvalid('last_name') && <label className="help-block error">{this._getErrorMsg('last_name')}</label>}
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 form-group mb-4">
                          <input onChange={(e) => this._updateNewContact('title', e.currentTarget.value)} className="form-control form-control-line" id="contact-title" type="text" name="title" placeholder="Titre" />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-6 form-group mb-4">
                          <div className="input-group-icon input-group-icon-left">
                            <span className="input-icon input-icon-left"><i className="ti-email"></i></span>
                            <input onChange={(e) => this._updateNewContact('email', e.currentTarget.value)} className="form-control form-control-line" id="contact-email" type="text" name="email" placeholder="Courriel" />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className={`col-6 form-group mb-4${this._isInvalid('phone') ? ' has-error' : ''}`}>
                          <div className="input-group-icon input-group-icon-left">
                            <span className="input-icon input-icon-left"><i className="fa fa-phone" aria-hidden="true"></i></span>
                            <input value={this.state.newContact.phone} onChange={(e) => this._updateNewContact('phone', e.currentTarget.value)} className="form-control form-control-line" id="contact-tel" type="text" name="tel" placeholder="555-555-5555*" />
                            {this._isInvalid('phone') && <label className="help-block error">{this._getErrorMsg('phone')}</label>}
                          </div>
                        </div>
                        <div className="col-6 form-group mb-4">
                          <div className="input-group-icon input-group-icon-left">
                            <span className="input-icon input-icon-left"><i className="ti-mobile"></i></span>
                            <input onChange={(e) => this._updateNewContact('cell', e.currentTarget.value)} className="form-control form-control-line" id="contact-mobile" type="text" name="mobile" placeholder="555-555-5555" />
                          </div>
                        </div>
                      </div>
                      <GetEnv>
                        {({ env }: any) => {
                          return env.GOOGLE_PUBLIC_API_KEY ?
                            <ReactGoogleMapsLoader
                              params={{
                                key: env.GOOGLE_PUBLIC_API_KEY,
                                libraries: "places,geocode",
                              }}
                              render={googleMaps =>
                                googleMaps && (
                                  <div style={{ width: '100%' }}>
                                    <ReactGooglePlacesSuggest
                                      autocompletionRequest={{input: this.state.search}}
                                      googleMaps={googleMaps}
                                      onSelectSuggest={this._handleSelectSuggest}
                                    >
                                      <InlineTextInput
                                        title=""
                                        noLabel={true}
                                        onChange={this._handleInputChange}
                                        placeholder="Adresse du contact"
                                        value={this.state.value}
                                        fullWidth={true}
                                      />
                                    </ReactGooglePlacesSuggest>
                                  </div>
                                )
                              }
                            />
                          :
                            <InlineTextInput
                              title=""
                              noLabel={true}
                              onChange={this._handleInputChange}
                              placeholder="Adresse du contact"
                              value={this.state.value}
                              fullWidth={true}
                            />
                        }}
                      </GetEnv>
                      <div className="row">
                        <div className="col-12 form-group mb-4">
                          <textarea onChange={(e) => this._updateNewContact('comments', e.currentTarget.value)} className="form-control form-control-line" name="comments" id="contact-comments" rows={3} placeholder="Commentaires"></textarea>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 form-group mb-4">
                          <SelectAsync
                            isSearchable={true}
                            isClearable={true}
                            isCreatable={false}
                            value={{ data: this.state.newClient, value: this.state.newClient.id, label: this.state.newClient.title } as ISelectValue}
                            loadFunction={this._loadClients}
                            onChange={this._handleClientChange}
                              //@ts-ignore
                            style={{ width: '100%' } as CSSProperties}
                            placeholder="Rechercher un compte client"
                          />
                        </div>
                      </div>
                    </div>
                    {/* <button className="btn btn-primary btn-rounded submit-btn" style={{ marginTop: 10 }} onClick={() => this._addNewClient()}>Ajouter</button> */}
                  </div>
                }
              </div>
            </SectionHtml>
            <div className="actions" style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: 30 }}>
              <button className="btn btn-primary btn-rounded submit-btn" id="submit-button" onClick={() => this._selectClientAddress(this.state.selectedAddress)} style={{ marginTop: 10 }}>Continuer</button>
            </div>
          </PaperLike>
        }
      </div>
    );
  }

  private _validateForm = (): boolean => {
    if (this.state.newContact.first_name === '') {
      this.setState({ errors: [ ...this.state.errors, { key: 'first_name', msg: 'Ce champ est obligatoire.'} ] })
      return false;
    }

    if (this.state.newContact.last_name === '') {
      this.setState({ errors: [ ...this.state.errors, { key: 'last_name', msg: 'Ce champ est obligatoire.'} ] })
      return false;
    }

    if (this.state.newContact.phone === '') {
      this.setState({ errors: [ ...this.state.errors, { key: 'phone', msg: 'Ce champ est obligatoire.'} ] })
      return false;
    }

    return true;
  }

  private _isInvalid = (field: string) => {
    let error = _.find(this.state.errors, error => error.key === field);
    if (error) {
      return true;
    }

    return false;
  }

  private _getErrorMsg = (field: string) => {
    let error = _.find(this.state.errors, error => error.key === field);
    if (error) {
      return error.msg;
    }

    return 'Champs invalide';
  }

  private _selectClientAddress = (address?: IAddress) => {
    if (address === undefined) {
      return;
    }

    if (!this.state.selectingClient) {
      if (this._validateForm()) {
        this._addNewClient();
      }
    } else {
      this.state.quote.update('billTo', address).then(() => this.state.quote.update('shipTo', address).then(quote => this.setState({ quote }, () => {
        this.state.quote.saveQuote().then(res => {
          if (res.id) {
            this.state.quote.update('id', res.id);
          }
        });
      })));
    }
  }

  private _handleContactsChange = (data: any) => {
    if (!data) {
      this.setState({ newContact: {
        id: -1,
        address: '',
        city: '',
        country: '',
        first_name: '',
        last_name: '',
        phone: '',
        postal_code: '',
        province: '',
        title: '',
      }, });
    } else {
      this.setState({ newContact: data.data });
    }
  }

  private _handleClientChange = (data: any) => {
    if (!data) {
      this.setState({ newClient: UBClient.createEmptyClient() });
    } else {
      this.setState({ newClient: data.data });
    }
  }

  private _loadClients = (keywords: string, callback: Function) => {
    if (keywords.length < 2) return [];

    this.state.quote.fetchClients(keywords).then(clients => {
      callback(_.map(clients, (client: IClient) => {
        const selectValue = {
          data: client,
          label: client.title,
          value: client.id
        } as ISelectValue;

        return selectValue;
      }));
    });
  }

  private _addNewClient = async () => new Promise<IMessage>(async (resolve, reject) => {
    try {
      const res = await fetch(getBaseUrl() + 'webservices/client/add', { method: 'POST', body: JSON.stringify(this.state.newClient) });
      const json = await res.json();

      if (json.code !== 200) {
        reject({
          done: false,
          message: 'Problem with server'
        });
      }

      const client = json.data as IClient;

      await this.state.quote.update(AddressTypeEnum.BILL_TO, { ...this.state.quote.get().billTo, client });
      await this.state.quote.update(AddressTypeEnum.SHIP_TO, { ...this.state.quote.get().shipTo, client });

      const saveQuoteResult = await this.state.quote.saveQuote();

      if (saveQuoteResult.id) {
        await this.state.quote.update('id', saveQuoteResult.id);
        resolve({
          done: true,
          message: 'Quote updated'
        });
      }

      resolve({
        done: true,
        message: 'Quote updated but no id returned'
      });
    } catch (e) {
      reject({
        done: false,
        message: 'Exception',
        error: e
      });
    }
  });

  private _selectClientWithSelect = (): void => {
    this.setState({ selectingClient: true }, () => {

    });
  }
  private _createNewClientForm = (): void => {
    this.setState({ selectingClient: false }, () => {

    });
  }

  private _updateNewContact = (param: string, value: any, callback?: Function) => {
    this.setState({ errors: _.filter(this.state.errors, error => error.key !== param), newContact: { ...this.state.newContact, [param]: value } }, () => callback && callback());
  }

  private _handleInputChange = (e: string) => {
    this.setState({search: e, value: e});
  }

  private _handleSelectSuggest = (suggest: any) => {
    const addressFromGoogle: IAddress = decomposeAddressFromGoogle(suggest.address_components);
    const { address, city, province, country, postal_code } = addressFromGoogle;

    this.setState({ newContact: {
      ...this.state.newContact,
      address,
      city,
      province,
      country,
      postal_code
    } });

    this.setState({search: "", value: suggest.formatted_address});
  }
}

const SectionHtml = styled.section`
  padding: 25px;
  border: 4px solid #e3e6e7;
  max-width: 900px;
  margin: 0 auto;
  margin-bottom: 30px;
`;

const SectionTitle = styled.h3`
  text-align: center;
  margin-bottom: 20px;
  font-weight: 400;
  font-size: 1.75rem;
`;

const NewClientRow = styled.div`
  text-align: right!important;
  padding: .5rem!important;
  margin-bottom: 1.5rem!important;
`;

const Link = styled.a`
  text-decoration: none;
  background-color: transparent;
`;
