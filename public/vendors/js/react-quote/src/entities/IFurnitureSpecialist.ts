export interface IFurnitureSpecialist {
  readonly address: string;
  readonly avatar: string;
  readonly city: string;
  readonly country: string;
  readonly creation: string;
  readonly email: string;
  readonly first_name: string;
  readonly fk_user_level_id: string;
  readonly id: string;
  readonly last_connection: string;
  readonly last_modification: string;
  readonly last_name: string;
  readonly mobile: string;
  readonly postal_code: string;
  readonly province: string;
  readonly user_level: string;
}
