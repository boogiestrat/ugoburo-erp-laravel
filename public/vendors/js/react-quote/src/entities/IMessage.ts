export interface IMessage {
  readonly done: boolean;
  readonly message: string;
  readonly error?: string;
  readonly data?: any;
}
