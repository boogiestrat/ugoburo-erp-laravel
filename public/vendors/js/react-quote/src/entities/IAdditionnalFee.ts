export interface IAdditionnalFee {
  readonly title: string,
  readonly value: number,
}
