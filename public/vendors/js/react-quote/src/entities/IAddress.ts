export interface IAddress {
  readonly id: number;
  readonly is_default_facturation: number;
  readonly is_default_shipping: number;
  readonly address: string;
  readonly city: string;
  readonly province: string;
  readonly postal_code: string;
  readonly country: string;
}

export enum AddressTypeEnum {
  "BILL_TO" = "billTo",
  "SHIP_TO" = "shipTo",
}
