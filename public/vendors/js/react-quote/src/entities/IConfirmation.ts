export interface IConfirmation {
  readonly title: string;
  readonly message: string;
  readonly body?: () => React.ReactElement;
  readonly onOk: Function;
  readonly onCancel: Function;
}
