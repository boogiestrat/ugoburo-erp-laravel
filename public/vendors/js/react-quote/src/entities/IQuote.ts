import { IFurnitureSpecialist, ITax, IAddress, ISection, IClause, IAdditionnalFee, IDiscount, IClient, IContact } from './';
import { Moment } from 'moment';

export interface IQuote {
  readonly id: number;
  readonly number: string;
  readonly title: string;
  readonly date: Moment;
  readonly validUntil: Moment;
  readonly specialist?: IFurnitureSpecialist;
  readonly billTo: IAddressInQuote;
  readonly shipTo: IAddressInQuote;
  readonly sections: Array<ISection>;
  readonly discountMethod: MoneyMethodEnum;
  readonly discountParam: number;
  readonly installationAndShippingMethod: MoneyMethodEnum;
  readonly installationAndShippingParam: number;
  readonly depot: number;
  readonly depotPercent: number;
  readonly clauses: Array<IClause>;
  readonly additionalNote: string;
  readonly additionalNoteInterne: string;
  readonly language: string;
  readonly additionnalFees: Array<IAdditionnalFee>;
  readonly availableDiscounts: Array<IDiscount>;

  readonly status: {
    readonly id: number,
    readonly name: string,
    readonly color: string,
    readonly list_order: number
  };
}

export enum MoneyMethodEnum {
  PERCENT = 'percent',
  AMOUNT = 'amount',
}

export interface IAddressInQuote {
  readonly client?: IClient,
  readonly contact?: IContact,
  readonly address?: IAddress,
  readonly taxes?: Array<ITax>,
}
