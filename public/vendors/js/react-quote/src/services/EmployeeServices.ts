import { IFurnitureSpecialist } from '../entities';
import { getBaseUrl } from './Services';

export const fetchEmployees = (keyword: string): Promise<Array<IFurnitureSpecialist>> => {
  return fetch(getBaseUrl() + 'webservices/employee/search?q=' + keyword, {
    method: 'POST',
  })
    .then(body => body.json())
    .then(json => {
      if (keyword.length === 0) {
        return [];
      }

      return json as Array<IFurnitureSpecialist>;
    });
}

export const getCurrentEmployee = (): Promise<IFurnitureSpecialist> => {
  return fetch(getBaseUrl() + 'webservices/employee/current', {
    method: 'POST',
  })
    .then(body => body.json())
    .then(json => {
      return json as IFurnitureSpecialist;
    });
}
