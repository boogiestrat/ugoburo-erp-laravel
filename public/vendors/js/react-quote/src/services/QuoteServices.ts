import _ from 'lodash';

import { IQuote, ISection, IItem, IClause } from '../entities';

import { ProfitMethod } from '../constants/enums';
import { getBaseUrl } from './Services';

export const getDefaultQuote = (): Array<IClause> => {
  return [
    {
      clause: 'A',
      value: false,
      percentValue: 0,
      priceValue: 100,
      description: 'Ajoute 100$ de frais supplémentaire',
    },
    {
      clause: 'B',
      value: false,
      percentValue: 0,
      priceValue: 0
    },
    {
      clause: 'C',
      value: false,
      percentValue: 0,
      priceValue: 0
    },
  ];
}

export const createEmptySection = (actualLength: number = 0): ISection => ({
  id: actualLength,
  title: 'Nouvelle section',
  items: [createEmptyItem()],
  folded: false,
  calculateCost,
  calculateGrossProfit,
  calculateProfit,
  calculateTotal,
});

export const createEmptySectionFunctions = () => ({
  calculateCost,
  calculateGrossProfit,
  calculateProfit,
  calculateTotal,
});

export const createEmptyItem = (actualLength: number = 0): IItem => ({
  id: actualLength,
  description: '',
  discount: {
    code: '',
    name: '',
    parts: [0, 0, 0, 0, 0],
  },
  expeditionPercent: 0,
  expeditionPrice: 0,
  expeditionPriceMethod: null,
  price: 0,
  title: '',
  priceParameterValue: 0,
  profitMethod: ProfitMethod.FIXED_PRICE,
  quantity: 0,
  SKU: '',
  isFocused: true,
  description_open: true,
  calculateUnitPrice,
  calculateCostPrice,
  calculateLineTotal,
  calculateItemProfit,
  calculateGrossProfit: calculateItemGrossProfit,
  calculateLineShipping,
});

export const createEmptyItemFunctions = () => ({
  calculateUnitPrice,
  calculateCostPrice,
  calculateLineTotal,
  calculateItemProfit,
  calculateGrossProfit: calculateItemGrossProfit,
  calculateLineShipping,
});

export function calculateUnitPrice(this: IItem) {
  let price = 0;

  switch (Number(this.profitMethod)) {
    case ProfitMethod.COST_PLUS_PERCENT:
      price = this.calculateCostPrice() * (1 + (this.priceParameterValue / 100));
      break;
    case ProfitMethod.GROSS_PROFIT:
      price = (this.calculateCostPrice() / (1 - (this.priceParameterValue / 100))) + this.calculateLineShipping();
      break;
    case ProfitMethod.NET_PROFIT:
      price = this.calculateCostPrice() + this.priceParameterValue;
      break;
    case ProfitMethod.LIST_MINUS:
      price = this.price - ((this.priceParameterValue / 100) * this.price);
      break;
    case ProfitMethod.FIXED_PRICE:
    default:
      price = this.priceParameterValue;
      break;
  }

  return price;
}

export function calculateCostPrice(this: IItem) {
  let price = this.price;

  if (this.discount.name === '') {
    return price;
  }

  _.forEach(this.discount.parts, discount => {
    if (discount !== '' && discount !== 0 && discount) {
      price = price - (price * ((typeof discount === 'string' ? (isNaN(parseFloat(discount)) ? 0 : parseFloat(discount)) : discount) / 100));
    }
  });

  // const expeditionPercent = this.expeditionPercent === 0 ? 0 : ((this.expeditionPercent) / 100) * price;

  // return Number(price) + Number(this.expeditionPrice) + Number(expeditionPercent);
  if (this.quantity === 0) return 0;
  // return Number(price) + (Number(this.calculateLineShipping()) / this.quantity);
  return Number(price);
}

export function calculateLineTotal(this: IItem) {
  return this.calculateUnitPrice() * this.quantity;
}

export function calculateItemProfit(this: IItem, bySupplier: string | boolean = false, allSupplier: string | boolean = false) {
  // if (bySupplier !== false) {
  //   if (this.discount.name === bySupplier) {
  //     return this.calculateLineTotal() - (this.calculateCostPrice() * this.quantity);
  //   }
  //
  //   return 0;
  // }
  //
  // if (allSupplier !== false) {
  //   return this.calculateLineTotal() - (this.calculateUnitPrice() * this.quantity);
  // }

  return ((Number(this.calculateUnitPrice() * this.quantity)) - Number((this.calculateCostPrice() * this.quantity) + (this.calculateLineShipping())));
}

export function calculateItemGrossProfit(this: IItem) {
  const gross = (this.calculateItemProfit() / this.calculateLineTotal()) * 100;

  if (isNaN(gross)) {
    return 0;
  }

  if (gross === Infinity) {
    return 100;
  }

  if (gross === Infinity * -1) {
    return -100;
  }

  return gross;
}

export function calculateLineShipping(this: IItem): number {
  switch (this.expeditionPriceMethod) {
    case null:
    case 'global':
      return this.expeditionPrice;
    case 'unit':
      return this.expeditionPrice * this.quantity;
    case 'cost':
      return (this.expeditionPrice / 100) * this.calculateCostPrice();
    case 'list':
      return (this.expeditionPrice / 100) * this.price;
  }
}

export function calculateCost(this : ISection) {
  return _.sumBy(this.items, item => item.calculateCostPrice() * item.quantity);
}

export function calculateTotal(this: ISection) {
  return _.sumBy(this.items, item => item.calculateLineTotal());
}

export function calculateGrossProfit(this : ISection) {
  const gross = (this.calculateProfit() / this.calculateTotal()) * 100;

  if (isNaN(gross)) {
    return 0;
  }

  if (gross === Infinity) {
    return 100;
  }

  if (gross === Infinity * -1) {
    return -100;
  }

  return gross;
}

export function calculateProfit(this : ISection) {
  return _.sumBy(this.items, item => item.calculateItemProfit());
}

export function calculateOverallCost(this: IQuote) {
  return _.sumBy(this.sections, section => section.calculateCost());
}

export const getPriceMethodStringBasedOnValue = (method: ProfitMethod) => {
  switch (Number(method)) {
    case ProfitMethod.COST_PLUS_PERCENT:
      return 'Cost + (%)';
    case ProfitMethod.FIXED_PRICE:
      return 'Fixed Price ($)';
    case ProfitMethod.GROSS_PROFIT:
      return 'Gross Profit (%)';
    case ProfitMethod.LIST_MINUS:
      return 'List Minus (%)';
    case ProfitMethod.NET_PROFIT:
      return 'Net Profit ($)';
    default:
      return 'Non défini';
  }
}

export function calculateOverallTotal(this: IQuote) {
  return _.sumBy(this.sections, section => section.calculateTotal());
}

export const saveQuote = (quote: IQuote) => {
  const jsonQuote = JSON.stringify(quote);

  return fetch(getBaseUrl() + '/webservices/quote/update_json.php?id=' + quote.id, {
    method: 'POST',
    body: jsonQuote,
  }).then((res) => res.json()).then(json => json);
}

export const getGeneralId = (date: string) => {
  const jsonId = JSON.stringify({ date });

  return fetch(getBaseUrl() + '/webservices/general/get_next_id.php', {
    method: 'POST',
    body: jsonId,
  }).then(res => res.json()).then(json => json).catch(() => `${date}00`);
}

export const loadQuoteFromId = (id: string): Promise<IQuote> => {
  return fetch(getBaseUrl() + '/webservices/quote/get.php', {
    method: 'POST',
    body: JSON.stringify({ id }),
  }).then(res => res.json()).then(res => res as IQuote);
}
