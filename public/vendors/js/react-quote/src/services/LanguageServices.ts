import _ from 'lodash';

export const fetchLanguages = (keyword: string): Promise<Array<string>> => {
  return new Promise((resolve) => resolve(_.filter(_languages, language => {
    return language.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
  })));
}

export const getActualAppLanguage = () => {

}

export const moneyFormat = (number: Number, locale: string = 'fr-CA', currency: string = 'CAD') => {
  return number.toLocaleString('fr-CA', { currency: 'CAD', maximumFractionDigits: 2, minimumFractionDigits: 2 }) + ' $';
}

export const percentFormat = (number: any) => {
  if (isNaN(parseFloat(number))) {
    return '0 %';
  }

  return `${parseFloat(number).toFixed(2)} %`;
}

export const numberParse = (number: any): number => {
  const value = number.toString() as String;

  const parsed = parseFloat(value.replace(',', '.'));

  if (isNaN(parsed)) {
    return 0;
  }

  return parsed;
}

const _languages: Array<string> = [
  'fr',
  'en',
];
