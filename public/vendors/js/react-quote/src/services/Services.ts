export const getBaseUrl = () => process.env.NODE_ENV === 'production' ? '../../' : 'http://dev.local/ugoburo-application-de-soumission/web/';
