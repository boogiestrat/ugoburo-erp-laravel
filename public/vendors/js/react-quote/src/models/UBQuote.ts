import _ from 'lodash';
import Cookies from 'js-cookie';

import { IQuote, IAddress, IDiscount, ISelectValue, ISection, IItem, ITax, MoneyMethodEnum } from '../entities';
import { ProfitMethod } from '../constants/enums';
import { getBaseUrl, createEmptySection, createEmptyItem, getFullDate } from '../services';
import moment from 'moment';

export default class UBQuote {
  public static createEmptyQuote(): IQuote {
    return {
      additionalNote: '',
      additionalNoteInterne: '',
      additionnalFees: [],
      availableDiscounts: [],
      billTo: {},
      clauses: [],
      date: moment(),
      depot: 0,
      depotPercent: 0,
      discountMethod: MoneyMethodEnum.PERCENT,
      discountParam: 0,
      installationAndShippingMethod: MoneyMethodEnum.PERCENT,
      installationAndShippingParam: 0,
      id: -1,
      language: Cookies.get('LANGUAGE') || "fr",
      number: window.location.href.split("id=")[1]
        ? window.location.href
            .split("id=")[1]
            .split("&")[0]
        : getFullDate() + "00",
      sections: [],
      shipTo: {},
      status: {
        color: 'primary',
        id: 1,
        name: 'En soumission',
        list_order: 1,
      },
      title: '',
      validUntil: moment().add(30, 'days')
    }
  }

  private _quote: IQuote;

  public constructor(public quote?: IQuote | null) {
    if (quote === undefined || quote === null) {
      this._quote = UBQuote.createEmptyQuote();
    } else {
      this._quote = quote;
    }
  }

  public get() {
    return this._quote;
  }

  public getSections() {
    return this._quote.sections;
  }

  public update(
    param: string,
    value: any
  ): Promise<UBQuote> {
    return new Promise(resolve => {
      this._quote = {
        ...this._quote,
        [param]: value
      };
      this.saveToLocalStorage();

      resolve(this);
    });
  }

  private saveToLocalStorage = () => {
    localStorage.setItem(
      this._quote.number,
      JSON.stringify(this._quote)
    );
  };

  public specialistIsNotSet() {
    return this._quote.specialist === undefined;
  }



  public calculateOverallGrossProfit(sectionId: number = -1) {
    const gross =
      (this.calculateOverallProfit(sectionId) /
        this.calculateOverallTotal(sectionId)) *
      100;

    if (isNaN(gross)) {
      return 0;
    }

    if (gross === Infinity) {
      return 100;
    }

    if (gross === Infinity * -1) {
      return -100;
    }

    return gross;
  }

  public calculateOverallCost() {
    const value = _.sumBy(
      this._quote.sections,
      section => section.calculateCost()
    ) + _.sumBy(this._quote.additionnalFees, fee => fee.value);

    if (isNaN(value)) {
      return 0;
    }

    return value;
  }

  public calculateProfitBySupplier(supplierName?: string) {
    if (supplierName === undefined) {
      return _.sumBy(this._quote.sections, section =>
        section.calculateProfit()
      )
    }

    return _.sumBy(this._quote.sections, section => {
      return _.sumBy(section.items, item => {
        if (item.discount.name === supplierName) {
          return item.calculateItemProfit();
        }
        else return 0;
      })
    })
  }

  public calculateTotalCostBySupplier(supplierName?: string) {
    if (supplierName === undefined) {
      return _.sumBy(this._quote.sections, section =>
        section.calculateCost()
      )
    }

    return _.sumBy(this._quote.sections, section => {
      return _.sumBy(section.items, item => {
        if (item.discount.name === supplierName) {
          return item.calculateCostPrice() * item.quantity;
        }
        else return 0;
      })
    })
  }

  public calculateListPriceBySupplier(supplierName?: string) {
    if (supplierName === undefined) {
      return _.sumBy(this._quote.sections, section =>
        _.sumBy(section.items, item => item.calculateUnitPrice())
      )
    }

    return _.sumBy(this._quote.sections, section => {
      return _.sumBy(section.items, item => {
        if (item.discount.name === supplierName) {
          return item.calculateUnitPrice() * item.quantity;
        }
        else return 0;
      })
    })
  }

  public calculateTotalListPriceBySupplier(supplierName?: string) {
    if (supplierName === undefined) {
      return _.sumBy(this._quote.sections, section =>
        _.sumBy(section.items, item => item.price * item.quantity)
      )
    }

    return _.sumBy(this._quote.sections, section => {
      return _.sumBy(section.items, item => {
        if (item.discount.name === supplierName) {
          return item.price * item.quantity;
        }
        else return 0;
      })
    })
  }

  public calculateOverallProfit(sectionId: number = -1) {
    let value = 0;

    if (sectionId === -1) {
      value = (
        _.sumBy(this._quote.sections, section =>
          section.calculateProfit()
        ) +
        this.calculateShippingAndInstallationTotal() -
        this.calculateDiscountTotal()
      ) - _.sumBy(this._quote.additionnalFees, fee => fee.value);
    } else {
      value = (this._quote.sections[sectionId].calculateProfit() +
        this.calculateShippingAndInstallationTotal() -
        this.calculateDiscountTotal()) -
        _.sumBy(this._quote.additionnalFees, fee => fee.value)
    }

    if (isNaN(value)) {
      return 0;
    }

    return value;
  }

  public getPriceMethodStringBasedOnValue = (
    method: ProfitMethod
  ) => {
    switch (Number(method)) {
      case ProfitMethod.COST_PLUS_PERCENT:
        return "Cost + (%)";
      case ProfitMethod.FIXED_PRICE:
        return "Fixed Price ($)";
      case ProfitMethod.GROSS_PROFIT:
        return "Gross Profit (%)";
      case ProfitMethod.LIST_MINUS:
        return "List Minus (%)";
      case ProfitMethod.NET_PROFIT:
        return "Net Profit ($)";
      default:
        return "Non défini";
    }
  };

  public getAllDiscounts(): Array<IDiscount> {
  let discounts = [] as Array<IDiscount>;

  _.forEach(this._quote.sections, section => {
    _.forEach(section.items, item => {
      discounts.push(item.discount);
    })
  })

  return discounts;
  }

  public calculateOverallTotal(sectionId: number = -1) {
    if (sectionId === -1) {
      return _.sumBy(
        this._quote.sections,
        section => section.calculateTotal()
      );
    }

    return this._quote.sections[sectionId].calculateTotal();
  }

  public calculateSubTotal = () => {
    return (
      // this.calculateOverallTotal() -
      // this.calculateDiscountTotal() +
      // _.sumBy(this._quote.clauses, clause =>
      //   clause.value ? clause.priceValue : 0
      // )
      this.calculateOverallTotal()
    );
  };

  public getDiscountParam = (): number => {
    if (!this._quote.discountParam) {
      return 0
    }

    return this._quote.discountParam;
  }

  public calculateDiscountTotal = () => {
    if (this._quote.discountMethod === MoneyMethodEnum.AMOUNT) {
      return this.getDiscountParam();
    }

    return (this.getDiscountParam() / 100) * this.calculateOverallTotal();
  }

  public calculateShippingAndInstallationTotal = () => {
    if (this._quote.installationAndShippingMethod === MoneyMethodEnum.AMOUNT) {
      return this._quote.installationAndShippingParam;
    }

    return (this._quote.installationAndShippingParam / 100) * this.calculateOverallTotal();
  }

  public calculateTotalFees = () => {
    return _.sumBy(this._quote.additionnalFees, fee => fee.value);
  }

  public calculateSolde() {
    return (
      this.calculateQuoteTotal() -
      (this._quote.depot ? this._quote.depot : 0)
    );
  }
  public calculateTPS(tps: number = 0.05) {
    return (
      this.calculateSubTotal() * tps +
      this.calculateShippingAndInstallationTotal() * tps
    );
  }
  public calculateTVQ(tvq: number = 0.0975) {
    return (
      this.calculateSubTotal() * tvq +
      this.calculateShippingAndInstallationTotal() * tvq
    );
  }
  public calculateQuoteTotal(
    tps: number = 0.05,
    tvq: number = 0.0975
  ) {
    return (
      this.calculateSubTotal() -
      (this.calculateDiscountTotal() +
        _.sumBy(this._quote.clauses, clause =>
          clause.value ? clause.priceValue : 0
        )
      ) +
      this.calculateShippingAndInstallationTotal() +
      _.sumBy(this.getTaxes(), tax => this.calculateTax(tax.value))
    );
  }

  public saveQuote = () => {
  const jsonQuote = JSON.stringify({
    ...this._quote,
    date: this._quote.date.isValid() ? this._quote.date.format('DD-MM-YYYY hh:mm:ss') : moment().format('DD-MM-YYYY hh:mm:ss'),
    validUntil:  this._quote.validUntil.isValid() ? this._quote.validUntil.format('DD-MM-YYYY hh:mm:ss') : moment().format('DD-MM-YYYY hh:mm:ss'),
    total: this.calculateQuoteTotal(),
    subTotal: this.calculateSubTotal(),
    TPS: this.calculateTPS(),
    TVQ: this.calculateTVQ(),
  });

  return fetch(getBaseUrl() + '/webservices/quote/update_json.php?id=' + (this._quote.id === -1 ? '-1' : this._quote.number), {
    method: 'POST',
    body: jsonQuote,
  }).then((res) => res.json()).then(json => json);
  };

  public getGeneralId = (date: string) => {
    const jsonId = JSON.stringify({ date });

    return fetch(
      getBaseUrl() +
        "/webservices/general/get_next_id.php",
      {
        method: "POST",
        body: jsonId
      }
    )
      .then(res => res.json())
      .then(json => json)
      .catch(() => `${date}00`);
  };

  public billToClientIsSetAndHasAddresses() {
    if (this._quote.billTo.client === undefined) {
      return false;
    }

    if (this._quote.billTo.client.addresses.length === 0) {
      return false;
    }

    return true;
  }

  public getBillToClientAddresses() {
    if (this._quote.billTo.client === undefined) {
      return [];
    }

    if (this._quote.billTo.client.addresses.length === 0) {
      return [];
    }

    return this._quote.billTo.client.addresses;
  }

  public getBillToClientContacts() {
    if (this._quote.billTo.client === undefined) {
      return [];
    }

    if (this._quote.billTo.client.contacts.length === 0) {
      return [];
    }

    return this._quote.billTo.client.contacts;
  }

  public shipToClientIsSetAndHasAddresses() {
    if (this._quote.shipTo.client === undefined) {
      return false;
    }

    if (this._quote.shipTo.client.addresses.length === 0) {
      return false;
    }

    return true;
  }

  public fetchAddresses = (input: string) => {
    return fetch(
      getBaseUrl() +
      "/webservices/address/search.php?q=" + input,
      {
        method: 'POST'
      }
    )
    .then(res => res.json())
    .then(json => json);
  }

  public fetchClients = (input: string) => {
    return fetch(
      getBaseUrl() +
      "/webservices/client/search.php?q=" + input,
      {
        method: 'POST'
      }
    )
    .then(res => res.json())
    .then(json => json);
  }

  public loadQuoteFromId = (
    id: string
  ): Promise<IQuote> => {
    return fetch(
      getBaseUrl() + "/webservices/quote/get.php",
      {
        method: "POST",
        body: JSON.stringify({ id })
      }
    )
      .then(res => res.json())
      .then(res => res as IQuote);
  };

  public addNewAddressToOrganisation = (
    clientId: number,
    newAddress: IAddress
  ) => {
    const formData = new FormData();

    formData.append("id", String(clientId));

    _.map(
      newAddress,
      (data: any, key: string) => {
        formData.append(
          `address_${key}_0`,
          String(data)
        );
      }
    );

    return fetch(
      getBaseUrl() +
        "/webservices/client/addresses/add",
      {
        method: "POST",
        body: formData
      }
    )
      .then(body => body.json())
      .then(json => {
        return json;
      });
  };

  public clientIsSet(): boolean {
  const helperNeeded = window.location.href.split('helper=')[1] && window.location.href.split('helper=')[1] === 'false' ? false : true;

  if (!helperNeeded) {
    return true;
  }

  return this._quote.billTo !== null;
  }

  public getBillToSelect(): ISelectValue | undefined {
    if (!this._quote.billTo) {
      return undefined;
    }

    return {
      label: `${this._quote.billTo.contact ? this._quote.billTo.contact.first_name : ''} ${this._quote.billTo.contact ? this._quote.billTo.contact.last_name : ''}`,
      data: this._quote.billTo,
      value: this._quote.billTo && this._quote.billTo.contact ? this._quote.billTo.contact.id : -1,
    } as ISelectValue;
  }

  public addSection(sectionTitle: string, data: Array<{}>): Array<ISection> {
    this._quote = { ...this._quote, sections: [...this._quote.sections, this.populateSectionWithDataFromCSV(data, {...createEmptySection(), title: sectionTitle })] };

    return this.getSections();
  }

  public populateSectionWithDataFromCSV(data: Array<{}>, section: ISection): ISection {
    return {
      ...section,
      items: [
        ...section.items,
        ..._.map(data, (itemFormCSV: any) => ({
          ...createEmptyItem(),
          ...itemFormCSV,
          SKU: itemFormCSV['Part Number'],
          title: itemFormCSV['Part Description'],
          quantity: !isNaN(itemFormCSV['Qty']) ? Number(itemFormCSV['Qty']) : 0,
          profitMethod: ProfitMethod.FIXED_PRICE,
          price: !isNaN(parseFloat(String(itemFormCSV['List']).replace('$', '').replace(',', '.'))) ? parseFloat(String(itemFormCSV['List']).replace('$', '').replace(',', '.')) : 0,
          // priceParameterValue: !isNaN(parseFloat(String(itemFormCSV['List']).replace('$', '').replace(',', '.'))) ? parseFloat(String(itemFormCSV['List']).replace('$', '').replace(',', '.')) : 0
          priceParameterValue: 0
        } as IItem))
      ]
    };
  }

  public async fetchTaxesByProvince() {
    return new Promise(async (resolve, reject) => {
      if (!this._quote.billTo) {
        reject({msg: 'Missing address'});
        return;
      }

      try {
        const formData = new FormData();
        formData.append('province', this._quote.billTo.address ? this._quote.billTo.address.province : '');

        const taxRep = await fetch(getBaseUrl() + 'webservices/province/taxes.php', { method: 'POST', body: formData });
        const taxJson = await taxRep.json();

        if (taxJson.done !== true) {
          reject({error: 'uncomplete'});
          return;
        }

        resolve(taxJson.data);
        return;
      } catch (error) {
        reject({error});
      }
    });
  }

  public getTaxes(): Array<ITax> {
    if (!this._quote.billTo) {
      return [];
    }

    if (!this._quote.billTo.address) {
      return [];
    }

    if (!this._quote.billTo.taxes) {
      return [];
    }

    return this._quote.billTo.taxes;
  }

  public calculateTax(taxValue: number): number {
    return (
      (this.calculateSubTotal() -
         this.calculateDiscountTotal() +
        _.sumBy(this._quote.clauses, clause =>
          clause.value ? clause.priceValue : 0
        )
      ) * (taxValue) +
      this.calculateShippingAndInstallationTotal() * (taxValue)
    );
  }

  public deleteDiscountAtIndex(index: number, discountName: string) {
    this.assignDiscountToItemsWithDiscount({ name: '', code: '', parts: [0,0,0,0,0] } as IDiscount, discountName);
    this._quote = { ...this._quote, availableDiscounts: [...this._quote.availableDiscounts.slice(0, index), ...this._quote.availableDiscounts.slice(index + 1)] };

    return this;
  }

  public assignDiscountToItemsWithDiscount(assignDiscount: IDiscount, existingDiscount: string) {
    this._quote = { ...this._quote, sections: _.map(this.getSections(), section => ({
      ...section,
      items: _.map(section.items, item => {
        if (item.discount.name !== existingDiscount) {
          return item;
        }

        return { ...item, discount: assignDiscount };
      })
    })) }
  }

  public async emailModal(){
    var quote_id = this._quote.id;
    var quote_number = this._quote.number;
    var quote_total = this.calculateQuoteTotal();
    var quote_language = this._quote.language;
    var quote_email = "";
    var formHtml;

    if(this.billToClientIsSetAndHasAddresses()){
      quote_email = this._quote.billTo && this._quote.billTo.contact ? this._quote.billTo.contact.email : ""
    }

    const formResponse = await fetch(getBaseUrl() + '/webservices/quote/email/modal.php?ajax=1',{
      method: 'GET',
        headers: {
          Accept: 'application/json',
        },
      });

    const formJson = formResponse.json();

    formJson.then( data=>{
      formHtml = data.html;

      window["openQuoteEmailModal"](quote_id,quote_number,quote_total,quote_email,quote_language,formHtml);
    });

  }

}
