<?php
    ## Init App ##
    require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

    // Auth middleware
    //$authMiddleware = new AuthMiddleware();
    //$authMiddleware->validateAuthLevel();

    $page_title = "Soumissions";
    $page_subtitle = "";
    $page_icon = "fa fa-file-text-o";
    $page_desc = "";
    $newQuote = false;

    ## DATA ##
?>

<?php  include_once APP_ROOT . 'includes/html/head.php';  ?>

<script>
          const id = location.href.split('id=')[1].split('&')[0];
          const formData = new FormData();
          formData.append('id', id);

          fetch('../../webservices/quote/get_json.php', {
            method: 'POST',
            body: formData,
          }).then(res => res.json())
            .then(json => {
              json = JSON.parse(json);
              json = { ...json, number: id };

              localStorage.setItem(
                id,
                JSON.stringify(json),
              );
              location.href = `/soumissions/${id}/edit/?id=${id}`
            });
        </script>
    </head>
    <body>
 
Loading...
    </body>
</html>
