<?php

$login_page = true;

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

$payment = new Ekosys\Payment\PaypalPayment();

$referer = $_SERVER['HTTP_REFERER'];
$amount = $_POST["amount"];
$nonce = $_POST["payment_method_nonce"];

$quoteId = $_POST["quote_id"];
$firstname = $_POST["firstname"];
$lastname = $_POST["lastname"];
$isDeposit = $_POST["is_deposit"];
$recipients = $_POST['recipients'];
$language = $_POST['language'];

switchLocale($language);

$result = $payment->charge($amount,$nonce);
$msg = new \Plasticbrain\FlashMessages\FlashMessages();


if ($result->success || !is_null($result->transaction)) {
    /** @var \Braintree\Transaction $transaction */
    $transaction = $result->transaction;

    $quotePayment = new \Ekosys\Entities\QuotePayment();
    $quotePayment->setAmount($transaction->amount);
    $quotePayment->setCardNumber($transaction->creditCardDetails->last4);
    $quotePayment->setDateCreated(new DateTime());
    $quotePayment->setClientFirstName($firstname);
    $quotePayment->setClientLastName($lastname);
    $quotePayment->setPaymentMethod(\Ekosys\Payment\PaypalPayment::PAYMENT_TYPE);
    $quotePayment->setTransactionId($transaction->id);
    $quotePayment->setQuoteId($quoteId);
    $quotePayment->setIsDeposit($isDeposit);
    $quotePayment->save();

    //update quote if deposit

    $quote = get_quote($quoteId);
    $quote_data = json_decode($quote['data'],JSON_OBJECT_AS_ARRAY);

    if($isDeposit){
        $quote_data['depot'] = $amount;
        $quote_data['status'] = Quote::STATUS_DEPOSIT;
    }else{
        $quote_data['status'] = Quote::STATUS_PAID;
    }

    $quote = new Quote($quote_data);
    $quote->update();

    update_quote_status($quoteId,$quote_data['status']);

    send_quote_payment_confirmation_email(get_quote($quoteId),$isDeposit,$amount,$recipients);

    log_data('client', '/' . LoggerSection::payment . '/' . LoggerType::add . '/{' . $quotePayment->getId() . '}', '{"data": ' . $quotePayment->toJson() . '}', LoggerSection::payment, LoggerType::add);

    $msg->success(__('payment_success'),$referer,true);

    //header("Location: " . $referer);
} else {
    $errorString = "";
    foreach($result->errors->deepAll() as $error) {
        $errorString .= 'Error: ' . $error->code . ": " . $error->message . "\n";
    }


    $msg->error($errorString,$referer,true);


    header("Location: " . $referer);

}
