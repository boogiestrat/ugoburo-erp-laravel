<?php

$login_page = true;

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

$payment = new Ekosys\Payment\PoPayment();

$referer = $_SERVER['HTTP_REFERER'];
$amount = $_POST["amount"];
$amount = str_replace(',','.',$amount);

$quoteId = $_POST["quote_id"];
$firstname = $_POST["firstname"];
$lastname = $_POST["lastname"];
$isDeposit = $_POST["is_deposit"];
$recipients = $_POST['recipients'];
$language = $_POST['language'];
$poNumber = $_POST['po_number'];

switchLocale($language);

$result = $payment->validate($amount,$poNumber);
$msg = new \Plasticbrain\FlashMessages\FlashMessages();


if ($result===true) {
    /** @var \Braintree\Transaction $transaction */
    //$transactionId = uniqid(\Ekosys\Payment\PoPayment::PAYMENT_TYPE.'_') ;

    $quotePayment = new \Ekosys\Entities\QuotePayment();
    $quotePayment->setAmount($amount);
    $quotePayment->setCardNumber('xxxx');
    $quotePayment->setDateCreated(new DateTime());
    $quotePayment->setClientFirstName($firstname);
    $quotePayment->setClientLastName($lastname);
    $quotePayment->setPaymentMethod(\Ekosys\Payment\PoPayment::PAYMENT_TYPE);
    $quotePayment->setTransactionId($poNumber);
    $quotePayment->setQuoteId($quoteId);
    $quotePayment->setIsDeposit($isDeposit);
    $quotePayment->save();

    //update quote if deposit
    $quote = get_quote($quoteId);
    $quote_data = json_decode($quote['data'],JSON_OBJECT_AS_ARRAY);

    if($isDeposit){
        $quote_data['depot'] = $amount;
        $quote_data['status'] = Quote::STATUS_DEPOSIT;
    }else{
        $quote_data['status'] = Quote::STATUS_PAID;
    }

    $quote = new Quote($quote_data);
    $quote->update();

    $order = $quote->create_order();
    $order["po_number"] = $poNumber;

    require("../qbwc/api.php");
    $quickbooks_api = new QBWCApi();
    $quickbooks_api->send_to_quickbooks_from_erp("purchase_order", $order);

    update_quote_status($quoteId,$quote_data['status']);

    send_quote_payment_confirmation_email(get_quote($quoteId),$isDeposit,$amount,$recipients);

    log_data('client', '/' . LoggerSection::payment . '/' . LoggerType::add . '/{' . $quotePayment->getId() . '}', '{"data": ' . $quotePayment->toJson() . '}', LoggerSection::payment, LoggerType::add);

    $msg->success(__('payment_success'),$referer,true);

    //header("Location: " . $referer);
} else {

    $msg->error($result,$referer,true);

    header("Location: " . $referer);

}
