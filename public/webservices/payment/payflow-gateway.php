<?php
$login_page = true;

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';
$payment = new Ekosys\Payment\PayflowPayment();

$referer = $_SERVER['HTTP_REFERER'];
$amount = $_POST["amount"];
$amount = str_replace(',','.',$amount);

$quoteId = $_POST["quote_id"];
$firstname = $_POST["firstname"];
$lastname = $_POST["lastname"];
$isDeposit = $_POST["is_deposit"];
$recipients = $_POST['recipients'];
$language = $_POST['language'];
$poNumber = $_POST['po_number'];


$cardName = $_POST['cc-name'];
$cardNumber = $_POST['cc-number'];
$cvv = $_POST['x_card_code'];
$cardExpiration = $_POST['cc-exp'];

switchLocale($language);

$result = $payment->charge($cardNumber,$cvv,$cardExpiration,$cardName,get_quote($quoteId),$amount,$firstname,$lastname);

$msg = new \Plasticbrain\FlashMessages\FlashMessages();

// debug($result);die();

if ($result['result']) {
    /** @var \Braintree\Transaction $transaction */
    //$transactionId = uniqid(\Ekosys\Payment\PoPayment::PAYMENT_TYPE.'_') ;

    $quotePayment = new \Ekosys\Entities\QuotePayment();
    $quotePayment->setAmount($amount);
    $quotePayment->setCardNumber(substr($cardName,-4,4));
    $quotePayment->setDateCreated(new DateTime());
    $quotePayment->setClientFirstName($firstname);
    $quotePayment->setClientLastName($lastname);
    $quotePayment->setPaymentMethod(\Ekosys\Payment\PayflowPayment::PAYMENT_TYPE);
    $quotePayment->setTransactionId($result['response']['PPREF']);
    $quotePayment->setQuoteId($quoteId);
    $quotePayment->setIsDeposit($isDeposit);
    $quotePayment->save();

    //update quote if deposit
    $quote = get_quote($quoteId);
    $quote_data = json_decode($quote['data'],JSON_OBJECT_AS_ARRAY);

    if($isDeposit){
        $quote_data['depot'] = $amount;
        $quote_data['status'] = Quote::STATUS_DEPOSIT;
    }else{
        $quote_data['status'] = Quote::STATUS_PAID;
    }

    $quote = new Quote($quote_data);
    $quote->update();

    $order = $quote->create_order();
    $quickbooks_api = new QBWCApi();
    $quickbooks_api->send_to_quickbooks_from_erp("credit_card", $order);

    update_quote_status($quoteId,$quote_data['status']);

    send_quote_payment_confirmation_email(get_quote($quoteId),$isDeposit,$amount,$recipients);

    log_data('client', '/' . LoggerSection::payment . '/' . LoggerType::add . '/{' . $quotePayment->getId() . '}', '{"data": ' . $quotePayment->toJson() . '}', LoggerSection::payment, LoggerType::add);

    $msg->success(__('payment_success'),$referer,true);

    //header("Location: " . $referer);
} else {

    $error= $result['response']['RESPMSG'];

    $msg->error($error,$referer,true);

    header("Location: " . $referer);

}
