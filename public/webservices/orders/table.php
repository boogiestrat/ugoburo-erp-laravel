<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

$table = 'orders';
$iTable = 0;
$primaryKey = 'id';
$columns = array(
  // From
  [
    'db' => 'q.order_number',
    'dt' => $iTable++,
    'field' => 'order_number'
  ],

  // From
  [
    'db' => 'q.id',
    'dt' => $iTable++,
    'formatter' => function () { return 'Soumission'; }
  ],

  // Purchased on (date)
  [
    'db' => 'q.purchased_on',
    'dt' => $iTable++,
    'field' => 'purchased_on',
    'as' => 'purchased_on'
  ],

  // Billed to
  [
    'db' => 'q.id',
    'dt' => $iTable++,
    'field' => 'bill_to',
    'formatter' => function($d, $row) {
      $address = get_order_address($row['id'], 'bill_to');
      $entreprise = $address['entreprise'];
      $firstname = $address['firstname'];
      $lastname = $address['lastname'];

      $ret = "$firstname $lastname";
      if (!empty($entreprise)) {
        $ret .= " ($entreprise)";
      }
      return $ret;
    }
  ],

  // Shipped to
  [
    'db' => 'q.id',
    'dt' => $iTable++,
    'field' => 'ship_to',
    'formatter' => function($d, $row) {
      $address = get_order_address($row['id'], 'ship_to');
      $entreprise = $address['entreprise'];
      $firstname = $address['firstname'];
      $lastname = $address['lastname'];

      $ret = "$firstname $lastname";
      if (!empty($entreprise)) {
        $ret .= " ($entreprise)";
      }
      return $ret;
    }
  ],

  /* G.T (Base)
  [
    'db' => 'q.gt_base',
    'dt' => $iTable++,
    'field' => 'gt_base',
    'as' => 'gt_base',
    'formatter' => function($d, $row) {
      return currency_format($row['gt_base']);
    }
  ],*/

  // G.T (Purchased) => Total
  [
    'db' => 'q.gt_base',
    'dt' => $iTable++,
    'field' => 'gt_base',
    'as' => 'gt_base',
    'formatter' => function($d, $row) {
      return currency_format($row['gt_base']);
    }
  ],

  // Status
  [
    'db' => 'os.desc_fr',
    'dt' => $iTable++,
    'field' => 'status_id',
    'as' => 'status_id'
  ],

  // Actions
  [
    'db' => 'q.id',
    'dt' => $iTable++,
    'formatter' => function($d, $row) {
      $html = '<div class="text-center">';
      $html .= "<a class=\"font-20 mx-2\" href=\"".PATH."webservices/orders/pdf/generate.php?id={$row['id']}\" target=\"_blank\"><i class=\"la la-file-pdf-o\"></i></a>";
      $html .= "<a style=\"display:none\" class=\"font-20 modify-btn mx-2\" href=\"javascript:;\" data-id=\"{$row['id']}\" data-toggle=\"modal\" data-target=\"#order-modal\" data-backdrop=\"static\"><i class=\"ti-truck\"></i></a>";
      $html .= "</div>";
      return $html;
    },
    'field' => 'options'
  ]
);

$joinQuery = "  FROM `orders` AS q

                LEFT JOIN `order_status` as `os` ON os.id = q.fk_status_id
              ";

  echo json_encode(
	SSP::simple($_GET, SSP::mysql_connection(), $table, $primaryKey, $columns, $joinQuery)
);
