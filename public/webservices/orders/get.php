<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

if (!$_GET['id']) {
  echo json_encode([
    'done' => false,
    'msg' => 'La commande n\'est pas valide',
    'error' => 'bad_id'
  ]);

  return;
}

if (!$result = get_order($_GET['id'])) {
  echo json_encode([
    'done' => false,
    'msg' => 'La commande n\'est pas valide',
    'error' => 'bad_order'
  ]);
}

echo json_encode([
  'done' => true,
  'data' => $result,
]);

return;
