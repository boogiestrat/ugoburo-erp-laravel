<?php

    require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';

    if (!isset($_GET['id'])) {
      echo '<strong>Erreur:</strong> Commande introuvable';
      exit;
    }

    if ($_GET['id'] <= 0) {
      echo '<strong>Erreur:</strong> Commande introuvable';
      exit;
    }

    include('order.tpl.pdf.php');

    if(@$_REQUEST['debug']){
        echo $quote_html;
        exit;
    }

    $mpdf = new \Mpdf\Mpdf([
        'mode' => 'utf-8',
        'format' => 'Letter',
        'orientation' => 'P',
        'tempDir' => sys_get_temp_dir()
    ]);

    $mpdf->SetProtection(array('copy','print'), '', PDF_PASSWORD, 128);

    $mpdf->SetHTMLFooter('<div class="quote-footer"><table class="quote-footer-table"><tr><td><b>'.__('quote-pdf_footer-customer-service').' :</b> '.__('quote-pdf_footer-business-hours').'<span class="quote-footer-separator">|</span> <b>'.__('quote-pdf_footer-email').'</b> : '.getenv('DEFAULT_CONTACT_EMAIL').' </td><td><b>Page {PAGENO}/{nb}</b></td></tr></table></div>');

    // send the captured HTML from the output buffer to the mPDF class for processing
    $mpdf->WriteHTML($quote_html);
    $mpdf->Output();
