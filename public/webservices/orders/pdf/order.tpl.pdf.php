<?php

if(!@$quote_id) {

    if (empty($_GET['id']) || !is_numeric($_GET['id'])) {
        header('Location: ' . PATH);
        exit();
    }

    $order_id = $_GET['id'];

    if (!$order = get_order($order_id)) {
        print "Erreur - Numéro de commande inexistant.";
        exit();
    }
}
// echo '<pre>';print_r($order);

// Total vars
$discount = $order['discount'];
$subTotal = 0;
$deposit = $order['deposit'];
$total = 0;

$page_title = __('order-pdf_page-title', $order['order_number'], format_locale_date($order['created_at']));

$showItemImage = false;
// echo '<pre>';print_r($data->billTo->client->title);die();

// Buffer the following html with PHP so we can store it to a variable later
ob_start();

?>
<!DOCTYPE html>
<html lang='fr-ca'>
<head>
    <title><?=$page_title?></title>
    <meta charset="UTF-8">
    <meta name="author" content="Ugoburo">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?=PATH?>assets/css/print-pdf.css">
    <style>
    body {
      font-family: 'Poppins', sans-serif;
      margin:0px;
    }

    .bg-dark-grey {
      background: #737373;
      color: #ffffff;
    }

    .bg-light-grey {
      font-weight: bold;
      background: #eeebeb;
    }

    .invoice-table {
      border-spacing: 0px;
      border-collapse: collapse;
    }

    .invoice-table td {
      padding: 15px;
      border: 1px solid black;
    }
    </style>
</head>

<body leftmargin="0" rightmargin="0">
    <table class="quote-head">
        <tr>
            <td width="50%" class="text-left">
                <img class="logo" src="<?=getenv('BASE_URL') ?>assets/img/ugoburo-logo-pdf.png" />
                <br>&nbsp;
            </td>
            <td width="50%" align="right">
                <p>
                    Ugoburo Inc.
                    3141 Blvd/Boul. Taschereau, #101<br>
                    Greenfield Park (Qc)<br>
                    Canada J4V 2H2<br>
                    GST/TPS 839049913 <br>
                </p>
                <br>&nbsp;
            </td>
        </tr>
    </table>

    <table class="invoice-table" border="1" cellspacing="0" cellpadding="0">
        <tr>
            <td width="100%" class="bg-dark-grey" colspan="2">
                <p>
                    <?php /*__('order_invoice-number') 20000003261 <br> */ ?>
                    <?=__('order_number')?> #<?=$order["order_number"];?> <br>
                    <?=__('order_date')?> <?=format_locale_date($order['purchased_on'])?> <br>
                    <?php
                    echo $order["bill_to"]["firstname"] . ' ' . $order["bill_to"]["lastname"] . ' ( ' . $order["bill_to"]["email"] . ' ) ';
                    ?>
                </p>
            </td>
        </tr>
        <tr>
            <td  width="50%" class="bg-light-grey"><?=__('address_billing')?></td>
            <td  width="50%" class="bg-light-grey"><?=__('address_shipping')?></td>
        </tr>
        <tr>
            <td width="50%" style="border-right: none;">

                <?php

                $formatted = str_replace("\n", "<br/>", $order["bill_to"]["formatted"]);
                echo $formatted;

                ?>

                <?php  if (!empty($order["bill_to"]["address"]["phone"])) {?>
                    <?=__('address_phone-abrv')?>: <?=$order["bill_to"]["address"]["phone"]?>
                <?php } ?>

                <?php  if (!empty($order["bill_to"]["address"]["cell"])) {?>
                    <?=__('address_cell-abrv')?>.: <?=$order["bill_to"]["address"]["cell"]?>
                <?php } ?>
            </td>
            <td width="50%" style="border-left: none;">

              <?php

              $formatted = str_replace("\n", "<br/>", $order["ship_to"]["formatted"]);
              echo $formatted;

              ?>

              <?php  if (!empty($order["ship_to"]["address"]["phone"])) {?>
                  <?=__('address_phone-abrv')?>: <?=$order["ship_to"]["address"]["phone"]?>
              <?php } ?>

              <?php  if (!empty($order["ship_to"]["address"]["cell"])) {?>
                  <?=__('address_cell-abrv')?>.: <?=$order["ship_to"]["address"]["cell"]?>
              <?php } ?>
            </td>
        </tr>
    </table>
    <br/>
    <table class="invoice-table" border="1" cellspacing="0" cellpadding="0">
        <tr>
            <td  width="50%" class="bg-light-grey"><?=__('order_payment-method')?></td>
            <td  width="50%" class="bg-light-grey"><?=__('order_shipping-method')?></td>
        </tr>
        <tr>
            <td  width="50%" style="border-right: none;">
              <?php if (!empty($order["payments"])) { ?>
                <?php foreach($order["payments"] as $payment) { ?>

                  <?php

                    if ($payment["payment_method"] == "purchase_order") {

                      echo __('order-pdf_payment-method-po') . '<br/>';
                      echo __('order-pdf_paidon') . date("Y-m-d", strtotime($payment["date_created"])).'<br/>';
                      echo __('order-pdf_paidby') . $payment["client_first_name"] . ' ' . $payment["client_last_name"].'<br/>';
                      echo __('order-pdf_po') . $payment["transaction_id"].'<br/>';

                    }

                    if ($payment["payment_method"] == "paypal_payflow") {

                      echo __('order-pdf_payment-method-cc') . '<br/>';
                      echo __('order-pdf_paidon') . date("Y-m-d", strtotime($payment["date_created"])).'<br/>';
                      echo __('order-pdf_paidby') . $payment["client_first_name"] . ' ' . $payment["client_last_name"].'<br/>';
                      echo __('order-pdf_cc') . $payment["card_number"].'<br/>';

                    }

                  ?>



                <?php } ?>
              <?php } ?>
            </td>
            <td  width="50%" style="border-left: none;">
              <?php
                  $total_shipping = 0;

                  foreach ($order["items"] as $item_key => $order_item) {

                      $quantity = (float)str_replace(',', '.', $order_item["quantity"]);
                      $unit_price = $order_item["price"];
                      $unit_total = $unit_price * $quantity;
                      $unit_taxes = 0;
                      foreach($order["taxes"] as $tax) {
                        $unit_taxes += $unit_total * $tax["rate"];
                      }
                      // print_r($order_item);
                      if ($order_item["expeditionPriceMethod"] == "global") {
                        if (!empty($order_item["expeditionPrice"])) {
                          $total_shipping += $order_item["expeditionPrice"];
                        }
                      }
                      if ($order_item["expeditionPriceMethod"] == "unit") {
                        if (!empty($order_item["expeditionPrice"])) {
                          $total_shipping += $order_item["expeditionPrice"] * $order_item["quantity"];
                        }
                      }
                      // $subTotal += $unit_total;
                  }

                  if (!empty($total_shipping)) {
                    echo __('order_total-shipping', currency_format($total_shipping));
                  } else {
                    echo __('order_free-shipping');
                  }
              ?>
            </td>
        </tr>
    </table>

    <br/>

    <table class="invoice-table" border="1" cellspacing="0" cellpadding="0">

            <tr>
              <td  class="bg-light-grey" width="400" align="left" style="border-right: none;"><?=__('order-item_products')?></td>
              <td  class="bg-light-grey" width="12" align="left" style="border-left: none;border-right: none;"><?=__('order-item_sku')?></td>
              <td  class="bg-light-grey" width="12" align="right" style="border-left: none;border-right: none;"><?=__('order_price')?></td>
              <td  class="bg-light-grey" width="5" align="right" style="border-left: none;border-right: none;"><?=__('order-item_qty')?></td>
              <!-- <td  class="bg-light-grey" width="40" align="right" style="border-left: none;border-right: none;">Taxes</th> -->
              <td  class="bg-light-grey" width="40" align="right" style="border-left: none;"><?=__('order_subtotal')?></td>
            </tr>
        <?php
            $total_shipping = 0;

            foreach ($order["items"] as $item_key => $order_item) {

                $id = $order_item["id"];
                $code = $order_item["SKU"];
                $description = str_replace("\n", "<br>", $order_item["description"]);
                $description = "<strong>{$order_item["title"]}</strong><br>{$description}";
                $quantity = (float)str_replace(',', '.', $order_item["quantity"]);
                $unit_price = $order_item["price"];
                $unit_total = $unit_price * $quantity;
                $unit_taxes = 0;
                foreach($order["taxes"] as $tax) {
                  $unit_taxes += $unit_total * $tax["rate"];
                }
                // print_r($order_item);
                if ($order_item["expeditionPriceMethod"] == "global") {
                  if (!empty($order_item["expeditionPrice"])) {
                    $total_shipping += $order_item["expeditionPrice"];
                  }
                }
                if ($order_item["expeditionPriceMethod"] == "unit") {
                  if (!empty($order_item["expeditionPrice"])) {
                    $total_shipping += $order_item["expeditionPrice"] * $order_item["quantity"];
                  }
                }
                $subTotal += $unit_total;


        ?>
            <tr>
                <td <?=$isAdmin?'class="no-border"':""?> align="left" style="border-right: none;"><?=$description?></td>
                <td <?=$isAdmin?'class="no-border"':""?> align="left" style="border-left: none;border-right: none;"><?=$order_item["SKU"]?></td>
                <td <?=$isAdmin?'class="no-border"':""?> align="right" style="border-left: none;border-right: none;"><?=currency_format($unit_price);?></td>
                <td <?=$isAdmin?'class="no-border"':""?> align="right" style="border-left: none;border-right: none;"><?=$quantity?></td>
                <!-- <td <?=$isAdmin?'class="no-border"':""?> align="right" style="border-left: none;border-right: none;"><?=currency_format($unit_taxes);?></td> -->
                <td <?=$isAdmin?'class="no-border"':""?> align="right" style="border-left: none;"><?=currency_format($unit_total);?></td>
            </tr>
        <?php } ?>

    </table>
    <br>
    <table>
        <tr>
            <td class="no-border">
                <table class="order-total" >
                    <tr>
                        <td width="85%" align="right"><b><?=__('order_subtotal-items')?></b></td>
                        <td width="15%" align="right"> <?= currency_format($subTotal); ?><br>&nbsp;</td>
                    </tr>

                    <?php if (!empty($discount) && $discount > 0) : ?>
                        <tr>
                            <td align="right"><?=__('order_discount')?></td>
                            <th width="100" align="right"><?= currency_format($discount); ?><br>&nbsp;</th>
                        </tr>
                    <?php endif ?>

                    <?php $taxes_total=0;

                    foreach ($order["taxes"] as $tax): ?>

                      <tr>
                          <td align="right"><b><?=$tax["value_description"]?></b></td>
                          <td width="100" align="right"> <?= currency_format($tax["value"] * $subTotal); ?><br>&nbsp;</td>
                      </tr>

                    <?php

                        $taxes_total += $tax["value"] * $subTotal;
                        endforeach
                    ?>

                    <tr>
                          <td align="right"><b><?=__('order_total-tax')?></b></td>
                          <td width="100" align="right"> <?= currency_format($taxes_total); ?><br>&nbsp;</td>
                    </tr>

                    <?php
                    if (!empty($total_shipping)) {
                      echo '
                      <tr>
                            <td align="right"><b>'.__('order_shipping').'</b></td>
                            <td width="100" align="right"> '.currency_format($total_shipping).'<br>&nbsp;</td>
                      </tr>';
                    }

                    $total = $subTotal + $taxes_total + $total_shipping;
                    if (!empty($deposit) && $deposit > 0) : ?>
                        <tr>
                            <td align="right"><b><?=__('quote_deposit')?>:</b></td>
                            <td width="100" align="right"> <?= currency_format($deposit); ?><br>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right"><b><?=__('quote_total-to-pay')?></b></td>
                            <td width="100" align="right"> <?= currency_format($total); ?><br>&nbsp;</td>
                        </tr>
                    <?php else : ?>
                        <tr>
                            <td align="right"><b>Total</b></td>
                            <td width="100" align="right"> <?= currency_format($total); ?><br>&nbsp;</td>
                        </tr>
                    <?php endif; ?>

                    <tr>
                        <td colspan="2" style="font-size:10px" align="right"><?=__('order_price-shown-cad')?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


</body>

</html>

<?php

    // Now collect the output buffer into a variable
    $quote_html = ob_get_contents();
    ob_end_clean();

    //exit($quote_html);
