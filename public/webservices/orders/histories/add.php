<?php

require dirname(dirname(dirname(dirname(__DIR__)))) . '/old/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!$order_id = $_POST['order_id']) {
  echo json_encode([
    'done' => false,
    'err' => 'bad_id',
    'msg' => 'Commande invalide'
  ]);

  return;
}

$data = add_history($_POST, $_FILES);


// emails
if (isset($_POST["action"])) {

if ($_POST["action"] == "shipping_form_save" && !empty($_POST["tracking_no"]) && $_POST["notify_tracking"] == "on" ) {
    $customer_lang = "fr";
    if ($data["quote"]["language"] == "en") {
      $customer_lang = "en";
    }

    $emails = [];
    // $emails[] = $data["bill_to"]["email"];
    // $emails[] = $data["ship_to"]["email"];
    //$emails[] = "maxime_labelle@hotmail.com"; // for testing purposes
    $emails[] = "boogiestrat@gmail.com";
    $customer_lang = "en"; 

    $mail = new \SendGrid\Mail\Mail();
    $from = getenv('DEFAULT_CONTACT_EMAIL');
    $fromName = getenv('CLIENT_NAME');
    $phone = getenv('CLIENT_PHONE');

    $order_number = $data["order_number"];
    $estimated_delivery_date = $data["estimated_delivery_date"];
    $comment = $_POST["comment"];

    $subject = "Ugoburo.ca : Livraison pour la commande n°$order_number";
    if ($customer_lang == "en") {
      $subject = "Ugoburo : Shipping for Order # $order_number";
    }

    ob_start();
    include('email-send-tracking-number_'.$customer_lang.'.php');
    $template = ob_get_clean();

    $mail->setFrom($from, $fromName);
    $mail->setSubject($subject);
    $mail->addTo($emails[0]);
    foreach ($emails as $emailKey=> $email){
      if($emailKey && $email != $emails[0]) $mail->addCc($email);
    }

    $mail->addContent("text/plain", strip_tags($template));
    $mail->addContent("text/html", $template);

    if (isset($data["filepath"])) {
      if ($data["filepath"]) {
        $mail->addAttachment(
          $data["filepath"],
          $data["filetype"],
          $data["filename"],
          "attachment"
        );
      }

    }

    $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

    try {
      $response = $sendgrid->send($mail);
    } catch (Exception $e) {
      $json = array('code' => 500, 'msg' => 'Caught exception: '.  $e->getMessage(). "\n");
      echo json_encode($json);
      exit();
    }

  }

  if ($_POST["action"] == "new_comment_form_save" && $_POST["notify"] == "on") {

    $customer_lang = "fr";
    if ($data["quote"]["language"] == "en") {
      $customer_lang = "en";
    }

    $emails = [];
    $emails[] = $data["bill_to"]["email"];
    // $emails[] = $data["ship_to"]["email"];
    // $emails[] = "maxime_labelle@hotmail.com"; // for testing purposes

    $mail = new \SendGrid\Mail\Mail();
    $from = getenv('DEFAULT_CONTACT_EMAIL');
    $fromName = getenv('CLIENT_NAME');
    $phone = getenv('CLIENT_PHONE');

    $order_number = $data["order_number"];
    $estimated_delivery_date = $data["estimated_delivery_date"];
    $comment = $_POST["comment"];

    $subject = "Ugoburo.ca : Mise à jour de la commande n°$order_number";
    if ($customer_lang == "en") {
      $subject = "Ugoburo : Ugoburo.ca: Order # $order_number update";
    }

    ob_start();
    include('email-send-new-comment_'.$customer_lang.'.php');
    $template = ob_get_clean();

    $mail->setFrom($from, $fromName);
    $mail->setSubject($subject);
    $mail->addTo($emails[0]);
    foreach ($emails as $emailKey=> $email){
      if($emailKey && $email != $emails[0]) $mail->addCc($email);
    }

    $mail->addContent("text/plain", strip_tags($template));
    $mail->addContent("text/html", $template);

    if (isset($data["filepath"])) {
      if ($data["filepath"]) {
        $mail->addAttachment(
          $data["filepath"],
          $data["filetype"],
          $data["filename"],
          "attachment"
        );
      }

    }

    $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

    try {
      $response = $sendgrid->send($mail);
    } catch (Exception $e) {
      $json = array('code' => 500, 'msg' => 'Caught exception: '.  $e->getMessage(). "\n");
      echo json_encode($json);
      exit();
    }

  }

  if ($_POST["action"] == "eta_form_save_and_notify_with_meeting") {

    $customer_lang = "fr";
    if ($data["quote"]["language"] == "en") {
      $customer_lang = "en";
    }

    $emails = [];
    $emails[] = $data["bill_to"]["email"];
    // $emails[] = $data["ship_to"]["email"];
    // $emails[] = "maxime_labelle@hotmail.com"; // for testing purposes

    $mail = new \SendGrid\Mail\Mail();
    $from = getenv('DEFAULT_CONTACT_EMAIL');
    $fromName = getenv('CLIENT_NAME');
    $phone = getenv('CLIENT_PHONE');

    $subject = "Ugoburo : Rendez-vous pour date de livraison planifiée";
    if ($customer_lang == "en") {
      $subject = "Ugoburo : Appointment for a planned delivery date";
    }

    $order_number = $data["order_number"];
    $estimated_delivery_date = $data["estimated_delivery_date"];
    ob_start();
    include('email-send-eta-with-meeting_'.$customer_lang.'.php');
    $template = ob_get_clean();

    $mail->setFrom($from, $fromName);
    $mail->setSubject($subject);
    $mail->addTo($emails[0]);
    foreach ($emails as $emailKey=> $email){
      if($emailKey && $email != $emails[0]) $mail->addCc($email);
    }

    $mail->addContent("text/plain", strip_tags($template));
    $mail->addContent("text/html", $template);

    $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

    try {
      $response = $sendgrid->send($mail);
    } catch (Exception $e) {
      $json = array('code' => 500, 'msg' => 'Caught exception: '.  $e->getMessage(). "\n");
      echo json_encode($json);
      exit();
    }

  }

  if ($_POST["action"] == "eta_form_save_and_notify_without_meeting") {

    $customer_lang = "fr";
    if ($data["quote"]["language"] == "en") {
      $customer_lang = "en";
    }

    $emails = [];
    $emails[] = $data["bill_to"]["email"];
    // $emails[] = $data["ship_to"]["email"];
    // $emails[] = "maxime_labelle@hotmail.com"; // for testing purposes

    $mail = new \SendGrid\Mail\Mail();
    $from = getenv('DEFAULT_CONTACT_EMAIL');
    $fromName = getenv('CLIENT_NAME');
    $phone = getenv('CLIENT_PHONE');

    $subject = "Ugoburo : Date de livraison estimée";
    if ($customer_lang == "en") {
      $subject = "Ugoburo : Estimated delivery date";
    }

    $order_number = $data["order_number"];
    $estimated_delivery_date = $data["estimated_delivery_date"];
    ob_start();
    include('email-send-eta-no-meeting_'.$customer_lang.'.php');
    $template = ob_get_clean();

    $mail->setFrom($from, $fromName);
    $mail->setSubject($subject);
    $mail->addTo($emails[0]);
    foreach ($emails as $emailKey=> $email){
      if($emailKey && $email != $emails[0]) $mail->addCc($email);
    }

    $mail->addContent("text/plain", strip_tags($template));
    $mail->addContent("text/html", $template);

    $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

    try {
      $response = $sendgrid->send($mail);
    } catch (Exception $e) {
      $json = array('code' => 500, 'msg' => 'Caught exception: '.  $e->getMessage(). "\n");
      echo json_encode($json);
      exit();
    }

  }


}

echo json_encode([
  'done' => true,
  'data' => $data
]);

return;
