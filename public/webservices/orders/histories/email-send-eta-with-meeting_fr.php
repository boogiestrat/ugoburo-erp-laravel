<?php

$month = array("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");
$month = $month[(date("n", strtotime($estimated_delivery_date)) - 1 )];
$date_str = date("d ", strtotime($estimated_delivery_date));
$date_str .= $month;
$date_str .= date(" Y", strtotime($estimated_delivery_date));
$estimated_delivery_date = $date_str;

$email_content = '
<h2>Date de livraison estimée</h2>

<p style="margin-top: 0; margin-bottom: 10px;">La présente est pour vous aviser qu\'un membre de notre équipe de livraison vous contactera le ou vers le '.$estimated_delivery_date.' pour prendre rendez-vous et planifier la livraison de votre commande (#'.$order_number.').</p>

<p style="margin-top: 0; margin-bottom: 10px;">Vous pouvez suivre votre commande en ligne en <a href="https://www.ugoburo.ca/en/customer/account/login">vous connectant à votre compte</a> et en visitant l\'onglet <a href="https://www.ugoburo.ca/en/sales/order/history/">"Mes commandes"</a>.</p>

<p style="margin-top: 0; margin-bottom: 10px;">Pour toute question relative à la livraison, veuillez communiquer avec le servive à la clientèle à l\'adresse <a href="mailto:service@ugoburo.ca">service@ugoburo.ca</a> ou par téléphone au 1 855 846-2876.</p>

<p style="margin-top: 0; margin-bottom: 10px;">Merci, Ugoburo.</p>
';

include(APP_ROOT . 'includes/html/email/email-body_en.php');

/*
    <p style="margin-top: 0; margin-bottom: 10px;">
        TPS 839049913<br/>
        TVQ 1217187512
    </p>
*/;
