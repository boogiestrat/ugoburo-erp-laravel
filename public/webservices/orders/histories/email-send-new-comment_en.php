<?php

$estimated_delivery_date = date("F jS, Y", strtotime($estimated_delivery_date));

$email_content = '
<h2>Estimated delivery date</h2>

<p style="margin-top: 0; margin-bottom: 10px;">Your order # '.$order_number.' status has been updated. Here are the details:</p>

<p style="margin-top: 0; margin-bottom: 10px;">'.$comment.'</p>

<p style="margin-top: 0; margin-bottom: 10px;">You can <a href="https://www.ugoburo.ca/en/customer/account/login">login to your account</a> to follow the status of your order.</p>

<p style="margin-top: 0; margin-bottom: 10px;">Please contact us with any further inquiries by e-mail at <a href="mailto:service@ugoburo.ca">service@ugoburo.ca</a> or by phone at 1 855 846-2876.M</p>

<p style="margin-top: 0; margin-bottom: 10px;">Thank you, Ugoburo.</p>
';

include(APP_ROOT . 'includes/html/email/email-body_en.php');

/*
    <p style="margin-top: 0; margin-bottom: 10px;">
        TPS 839049913<br/>
        TVQ 1217187512
    </p>
*/;
