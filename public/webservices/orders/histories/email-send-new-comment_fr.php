<?php

$month = array("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");
$month = $month[(date("n", strtotime($estimated_delivery_date)) - 1 )];
$date_str = date("d ", strtotime($estimated_delivery_date));
$date_str .= $month;
$date_str .= date(" Y", strtotime($estimated_delivery_date));
$estimated_delivery_date = $date_str;

$email_content = '
<h2>Date de livraison estimée</h2>

<p style="margin-top: 0; margin-bottom: 10px;">Le statut de votre commande n°'.$order_number.' a été mis à jour.</p>

<p style="margin-top: 0; margin-bottom: 10px;">'.$comment.'</p>

<p style="margin-top: 0; margin-bottom: 10px;">Vous pouvez suivre l\'historique de cette commande en ligne en <a href="https://www.ugoburo.ca/en/customer/account/login">vous connectant à votre compte</a>.</p>

<p style="margin-top: 0; margin-bottom: 10px;">Communiquez avec nous pour toute question, par courriel à l\'adresse <a href="mailto:service@ugoburo.ca">service@ugoburo.ca</a> ou par téléphone au 1 855 846-2876.</p>

<p style="margin-top: 0; margin-bottom: 10px;">Merci, Ugoburo.</p>
';

include(APP_ROOT . 'includes/html/email/email-body_en.php');

/*
    <p style="margin-top: 0; margin-bottom: 10px;">
        TPS 839049913<br/>
        TVQ 1217187512
    </p>
*/;
