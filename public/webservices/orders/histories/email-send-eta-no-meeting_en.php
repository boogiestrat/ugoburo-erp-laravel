<?php

$estimated_delivery_date = date("F jS, Y", strtotime($estimated_delivery_date));

$email_content = '
<h2>Estimated delivery date</h2>

<p style="margin-top: 0; margin-bottom: 10px;">This is to confirm your order (#'.$order_number.') will be delivered on or towards '.$estimated_delivery_date.'.</p>

<p style="margin-top: 0; margin-bottom: 10px;">You can <a href="https://www.ugoburo.ca/en/customer/account/login">login to your account</a> and follow the link <a href="https://www.ugoburo.ca/en/sales/order/history/">"My orders"</a> to view the status of your order.</p>

<p style="margin-top: 0; margin-bottom: 10px;">If you have any questions, please feel free to contact us at <a href="mailto:service@ugoburo.ca">service@ugoburo.ca</a> or by phone at 1 855 846-2876.</p>

<p style="margin-top: 0; margin-bottom: 10px;">Thank you, Ugoburo.</p>
';

include(APP_ROOT . 'includes/html/email/email-body_en.php');

/*
    <p style="margin-top: 0; margin-bottom: 10px;">
        TPS 839049913<br/>
        TVQ 1217187512
    </p>
*/;
