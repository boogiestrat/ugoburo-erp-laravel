<?php

$estimated_delivery_date = date("F jS, Y", strtotime($estimated_delivery_date));

$email_content = '

<p style="margin-top: 0; margin-bottom: 10px;">We wish to inform you your order #'.$order_number.' was shipped. You can click on the "Tracking number" link below to follow your order. Your shipment confirmation:</p>



<table style="width:100%;">

  <tr>

    <td style="width:50%;">

      <table style="width:100%;">
        <tr style="background:#d9e5ee;">
          <td style="padding:15px;"><strong>Billing Information:</strong></td>
        </tr>
        <tr style="background:#eeeeee;height: 150px;">
          <td>
            ';

            $email_content .= str_replace("\n","<br>", $data["bill_to"]["formatted"]).'<br/>';

            $email_content .= '
          </td>
        </tr>
      </table>

    </td>

    <td style="width:50%;">

      <table style="width:100%;">
        <tr style="background:#d9e5ee;">
          <td style="padding:15px;"><strong>Payment Method:</strong></td>
        </tr>
        <tr style="background:#eeeeee;height: 150px;">
          <td>
          ';

           if (!empty($data["payments"])) {
             foreach($data["payments"] as $payment) {



                if ($payment["payment_method"] == "purchase_order") {

                  $email_content .= __('order-pdf_payment-method-po') . '<br/>';
                  $email_content .= __('order-pdf_paidon') . date("Y-m-d", strtotime($payment["date_created"])).'<br/>';
                  $email_content .= __('order-pdf_paidby') . $payment["client_first_name"] . ' ' . $payment["client_last_name"].'<br/>';
                  $email_content .= __('order-pdf_po') . $payment["transaction_id"].'<br/>';

                }

                if ($payment["payment_method"] == "paypal_payflow") {

                  $email_content .= __('order-pdf_payment-method-cc') . '<br/>';
                  $email_content .= __('order-pdf_paidon') . date("Y-m-d", strtotime($payment["date_created"])).'<br/>';
                  $email_content .= __('order-pdf_paidby') . $payment["client_first_name"] . ' ' . $payment["client_last_name"].'<br/>';
                  $email_content .= __('order-pdf_cc') . $payment["card_number"].'<br/>';

                }





             }
           }
          $email_content .= '
          </td>
        </tr>
      </table>

    </td>

  </tr>

  <tr>

    <td style="width:50%;">

      <table style="width:100%;">
        <tr style="background:#d9e5ee;">
          <td style="padding:15px;"><strong>Shipping Information:</strong></td>
        </tr>
        <tr style="background:#eeeeee;height: 150px;">
          <td>
            ';

            $email_content .= str_replace("\n","<br>", $data["bill_to"]["formatted"]).'<br/>';

            $email_content .= '
          </td>
        </tr>
      </table>

    </td>

    <td style="width:50%;">

      <table style="width:100%;">
        <tr style="background:#d9e5ee;">
          <td style="padding:15px;"><strong>Shipping Method:</strong></td>
        </tr>
        <tr style="background:#eeeeee;height: 150px;">
          <td>
          ';
          $total_shipping = 0;

          foreach ($data["items"] as $item_key => $order_item) {

              if ($order_item["expeditionPriceMethod"] == "global") {
                if (!empty($order_item["expeditionPrice"])) {
                  $total_shipping += $order_item["expeditionPrice"];
                }
              }
              if ($order_item["expeditionPriceMethod"] == "unit") {
                if (!empty($order_item["expeditionPrice"])) {
                  $total_shipping += $order_item["expeditionPrice"] * $order_item["quantity"];
                }
              }

          }

          if (!empty($total_shipping)) {
            $email_content .= __('order_total-shipping', currency_format($total_shipping));
          } else {
            $email_content .= __('order_free-shipping');
          }


          $email_content .= '
          </td>
        </tr>
      </table>

    </td>

  </tr>

</table>


<table style="width:100%;">

  <tr style="background:#eeeeee;">
    <td style="padding:5px;">
      <strong>
        Item
      </strong>
    </td>
    <td style="padding:5px;">
      <strong>
        SKU
      </strong>
    </td>
    <td style="padding:5px;">
      <strong>
        Qty
      </strong>
    </td>
  </tr>
  ';
    foreach ($data["items"] as $item_key => $order_item) {
        $email_content .= '
        <tr>
        <td>'.str_replace("\n","<br/>", $order_item["description"]).'</td>
        <td>'.$order_item["SKU"].'</td>
        <td>'.$order_item["quantity"].'</td>
        </tr>
        ';
    }
    $email_content .= '
</table>

<br/><p/>

<p style="margin-top: 0; margin-bottom: 10px;">Tracking number: '.$data["tracking_no"].'</p>

<p style="margin-top: 0; margin-bottom: 10px;">If you have any questions, please feel free to contact us at <a href="mailto:service@ugoburo.ca">service@ugoburo.ca</a> or by phone at 1 855 846-2876.</p>

<p style="margin-top: 0; margin-bottom: 10px;">Thank you, Ugoburo.</p>
';

include(APP_ROOT . 'includes/html/email/email-body_en.php');

/*
    <p style="margin-top: 0; margin-bottom: 10px;">
        TPS 839049913<br/>
        TVQ 1217187512
    </p>
*/;
