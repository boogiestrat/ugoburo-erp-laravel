<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

/* CSRF protection */
if (!user_level(3)) { die(print_r(json_encode(array('code' => 500, 'msg' => "Error - Access denied.")))); }

$employees = get_all_employees();

if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
    $employees = json_encode($employees, JSON_PRETTY_PRINT);
else
    $employees = json_encode($employees);

print($employees);
