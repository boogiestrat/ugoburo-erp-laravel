<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

## TODO : Need global security validation ##
if (!empty($_REQUEST['q'])) {

    $employees = search_employees($_REQUEST['q']);

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $employees = json_encode($employees, JSON_PRETTY_PRINT);
    else
        $employees = json_encode($employees);

    print($employees);

} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
