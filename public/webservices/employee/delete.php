<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

/* CSRF protection */
if (!user_level(3)) { die(print_r(json_encode(array('code' => 500, 'msg' => "Error - Access denied.")))); }

if (!empty($_POST['id']) && is_numeric($_POST['id'])) {
    if ($result = delete_employee($_POST['id'])) {
        $json = array('code' => 200, 'msg' => "Employee deleted.");
        print(json_encode($json));
    }
} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
