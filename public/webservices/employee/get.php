<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##

if(!empty($_POST['id']) && is_numeric($_POST['id'])) {

    $employee = get_employee($_POST['id']);

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $employee = json_encode($employee, JSON_PRETTY_PRINT);
    else    
        $employee = json_encode($employee);

    print($employee);

} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json)); 
}