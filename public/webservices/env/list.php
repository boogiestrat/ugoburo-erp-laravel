<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

print json_encode(['GOOGLE_PUBLIC_API_KEY' => $_ENV['GOOGLE_PUBLIC_API_KEY']]);
exit;
