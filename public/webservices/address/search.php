<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

## TODO : Need global security validation ##
if (!empty($_REQUEST['q'])) {

    $clients = search_addresses($_REQUEST['q']);

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $clients = json_encode($clients, JSON_PRETTY_PRINT);
    else
        $clients = json_encode($clients);

    print($clients);

} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
