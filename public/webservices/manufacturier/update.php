<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

/* CSRF protection */
if (!user_level(3)) { die(print_r(json_encode(array('code' => 500, 'msg' => "Error - Access denied.")))); }

if(!empty($_POST['id']) && is_numeric($_POST['id'])) {
    if ($result = update_manufacturier($_POST)) {
        $json = array('code' => 200, 'msg' => "Manufacturier updated!");
        print_r(json_encode($json));
    } else {
        $json = array('code' => 500, 'msg' => "Error - Could not update the manufacturier!");
        print_r(json_encode($json));
    }
} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
