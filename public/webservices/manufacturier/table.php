<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

/* CSRF protection */
if (!user_level(3)) { die(print_r(json_encode(array('code' => 500, 'msg' => "Error - Access denied.")))); }

$table = 'manufacturier';
$primaryKey = 'id';
$rowIndex = 0;
$columns = array(
    array( 'db' => 'name', 'dt' => $rowIndex++, 'field' => 'name', 'as' => 'name' ),
    array( 'db' => 'pays', 'dt' => $rowIndex++, 'field' => 'pays', 'as' => 'pays' ),
    array( 'db' => 'updated_at', 'dt' => $rowIndex++, 'field' => 'updated_at', 'as' => 'updated_at' ),
    array( 'db' => 'id', 'dt' => $rowIndex++, 'formatter' => function($d, $row) {
        $html = '<div align="right">';
        $html .= '<a class="font-20 modify-btn" href="javascript:;" data-id="' . $row['id'] . '" data-toggle="modal" data-target="#manufacturier-modal" data-backdrop="static"><i class="ti-pencil"></i></a>';
        $html .= ' ';
        $html .= '<a class="font-20 delete-btn" href="javascript:;" data-id="' . $row['id'] . '" data-label="' . $row['name'] . '"><i class="ti-trash"></i></a>';
        $html .= ' </div>';

        return $html;
    }, 'field' => 'id', 'as' => 'id' ),
);

echo json_encode(
	SSP::simple($_GET, SSP::mysql_connection(), $table, $primaryKey, $columns)
);
