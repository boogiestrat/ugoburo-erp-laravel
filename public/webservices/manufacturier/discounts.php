<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

## TODO : Need global security validation ##

if(!empty($_GET['all']) && $_GET['all'] == '1') {
  print (json_encode(get_all_manufacturier_discounts($_GET['q'])));
  exit;
}

if(!empty($_GET['id']) && is_numeric($_GET['id'])) {

    $discounts = get_manufacturier_discounts($_GET['id']);

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $discounts = json_encode($discounts, JSON_PRETTY_PRINT);
    else
        $discounts = json_encode($discounts);

    print($discounts);
    exit;

} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
