<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##
if (!empty($_REQUEST['q'])) {

    $manufacturiers = search_manufacturiers($_REQUEST['q']);

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $manufacturiers = json_encode($manufacturiers, JSON_PRETTY_PRINT);
    else
        $manufacturiers = json_encode($manufacturiers);

    print($manufacturiers);

} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
