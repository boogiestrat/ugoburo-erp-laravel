<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';


/* CSRF protection */
if (!user_level(3)) { die(print_r(json_encode(array('code' => 500, 'msg' => "Error - Access denied.")))); }

if (!empty($_POST['name']) && !empty($_POST['pays'])) {

    if ($id = create_manufacturier($_POST)) {
        $json = array('code' => 200, 'msg' => "Manufacturier created!");
        print_r(json_encode($json));
    } else {
        $json = array('code' => 500, 'msg' => "Error - Could not create manufacturier!");
        print_r(json_encode($json));
    }
} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
