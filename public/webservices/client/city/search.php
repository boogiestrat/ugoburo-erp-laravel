<?php
$debug = false;
require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';

## TODO : Need global security validation ##
if (!empty($_GET['q'])) {
    $cities = search_city($_GET['q']);

    foreach ($cities as $key => $city) {
        $city_array[] = $city['city'];
    }

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
    $cities = json_encode($city_array/*, JSON_PRETTY_PRINT*/);
    else    
        $cities = json_encode($city_array);

    print($cities);
}