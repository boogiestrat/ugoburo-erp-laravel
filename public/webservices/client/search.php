<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

## TODO : Need global security validation ##

$request_body = file_get_contents('php://input');
$data = json_decode($request_body);

if (!empty($data->q)) {
    $clients = Client::search($data->q, 25, false);

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $clients = json_encode($clients, JSON_PRETTY_PRINT);
    else
        $clients = json_encode($clients);

    print($clients);

} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
