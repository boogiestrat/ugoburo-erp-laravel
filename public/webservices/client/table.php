<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

$results = search_clients_contacts_addresses($_GET);

foreach($results["data"] as $k=>$row) {
  $results["data"][$k]["modified"] = format_locale_datetime($row['modified']);
  $results["data"][$k]["options"] = '
  <a style="display:none;" class="font-20 modify-btn" href="javascript:;" data-id="' . $row['id'] . '" data-toggle="modal" data-target="#client-modal" data-backdrop="static"><i class="ti-pencil"></i></a>
  <a style="display:none;" class="font-20 create-quote-btn mx-2" href="javascript:;" data-id="' . $row['id'] . '"><i class="fa fa-file-text-o"></i></a>
  <a style="float: right;" class="font-20 delete-btn" href="javascript:;" data-id="' . $row['id'] . '" data-label="' . $row['entreprise'] . '"><i class="ti-trash"></i></a>
  ';
}

echo json_encode($results);
