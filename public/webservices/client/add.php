<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##

if (!empty($_POST['entreprise']) || (!empty($_POST['last_name']) && !empty($_POST['first_name']))) {
    if ($id = create_client($_POST)) {
        $client = $_POST;
        $client['id'] = $id;

        $json = array('code' => 200, 'msg' => "Client created!", 'data' => json_encode($client));
        print_r(json_encode($json));
    } else {
        $json = array('code' => 500, 'msg' => "Error - Could not create client!");
        print_r(json_encode($json));
    }
} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
