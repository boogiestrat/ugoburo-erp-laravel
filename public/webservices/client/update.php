<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##

// Update only status
if (isset($_GET['params']) && $_GET['params'] == 'fk_client_type_id') {
    if (!isset($_POST['id']) || !isset($_POST['fk_client_type_id'])) {
        $json = array('code' => 500, 'msg' => "Missing parameter.");
        print(json_encode($json));
        exit();
    }

    if (!update_client_type($_POST['id'], $_POST['fk_client_type_id'])) {
        $json = array('code' => 500, 'msg' => "Can't update selected client.");
        print(json_encode($json));
        exit();
    }

    $json = array('code' => 200, 'msg' => "Quote status updated!");
    print_r(json_encode($json));
    exit();
}

if(!empty($_POST['id']) && is_numeric($_POST['id'])) {
    if ($result = update_client($_POST)) {
        $json = array('code' => 200, 'msg' => "Client updated!");
        print_r(json_encode($json));
    } else {
        $json = array('code' => 500, 'msg' => "Error - Could not update the client!");
        print_r(json_encode($json));
    }
} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
