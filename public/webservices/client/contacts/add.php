<?php
$debug = false;
require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

## TODO : Need global security validation ##

if(!empty($_POST['id'])) {
    print('');

} else {
    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if (!empty($data->data)) {
      $res = add_one_contact_to_client($data->data->contact, $data->data->client_id);

      if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $client = json_encode(['id' => $res], JSON_PRETTY_PRINT);
      else
        $client = json_encode(['id' => $res]);

      print($client);
    } else {
      $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
      print_r(json_encode($json));
    }
}
