<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

$table = 'client';
$primaryKey = 'id';
$iTable = 0;
$columns = array(
    array( 'db' => 'client.entreprise', 'dt' => $iTable++, 'field' => 'entreprise', 'as' => 'entreprise', 'formatter' => function($d, $row) {
      return $row['entreprise'];
    }),
    array( 'db' => 'client.fk_client_type_id', 'dt' => $iTable++, 'formatter' => function($d, $row) {
        return "
            <div class=\"badge-selector-js\" style=\"padding:10px;background-color:#fff;display:none;box-shadow:1px 2px 10px 2px rgba(0, 0, 0, 0.2);z-index:9999;transform:translateY(-50px) translateX(-72px);position:absolute\">
                <span class=\"badge badge-success\" onclick=\"badgeSelectTypeFor('success', 'Particulier', {$row['options']}, this)\" style=\"cursor:pointer;display:inline-block;position:relative;margin:0px 5px\">Particulier</span>
                <span class=\"badge badge-warning\" onclick=\"badgeSelectTypeFor('warning', 'Entreprise', {$row['options']}, this)\" style=\"cursor:pointer;display:inline-block;position:relative;margin:0px 5px\">Entreprise</span>
                <span class=\"badge badge-primary\" onclick=\"badgeSelectTypeFor('primary', 'Gouvernement', {$row['options']}, this)\" style=\"cursor:pointer;display:inline-block;position:relative;margin:0px 5px\">Gouvernement</span>
            </div>
            <span class=\"badge badge-" . get_client_type_badge($row['fk_client_type_id'])['badge'] . " badge-chooser-js\">" . get_client_type_badge($row['fk_client_type_id'])['name'] . "</span>
        ";
    }, 'field' => 'fk_client_type_id', 'as' => 'fk_client_type_id' ),
    /*array( 'db' => 'client.city', 'dt' => $iTable++, 'formatter' => function($d, $row) {
        $contacts = get_client_contacts($row['options'], 3);

        return array_map(function($contact) {
            return "
                {$contact['first_name']} {$contact['last_name']}
            ";
        }, $contacts);
    }, 'field' => 'contact', 'as' => 'contact' ),*/
    /*array( 'db' => 'address.city', 'dt' => $iTable++, 'field' => 'city', 'as' => 'city' ),*/
    /*array( 'db' => 'CONCAT(address.province, ", ", address.country)', 'dt' => $iTable++, 'formatter' => function($d, $row) {
        return "{$row['province']}";
    }, 'field' => 'province', 'as' => 'province' ),*/
    /*array( 'db' => 'client.creation', 'dt' => $iTable++, 'field' => 'creation', 'as' => 'creation' ),*/
    array( 'db' => 'client.modified', 'dt' => $iTable++, 'field' => 'modified', 'as' => 'modified' ),
    array( 'db' => 'client.id', 'dt' => $iTable++, 'formatter' => function($d, $row) {
        $html = '<div class="text-right">';
        $html .= '<a class="font-20 modify-btn" href="javascript:;" data-id="' . $row['options'] . '" data-toggle="modal" data-target="#client-modal" data-backdrop="static"><i class="ti-pencil"></i></a>';
        $html .= ' ';
        $html .= '<a class="font-20 create-quote-btn mx-2" href="javascript:;" data-id="' . $row['options'] . '"><i class="fa fa-file-text-o"></i></a>';
        $html .= ' ';
        $html .= '<a class="font-20 delete-btn" href="javascript:;" data-id="' . $row['options'] . '" data-label="' . $row['entreprise'] . '"><i class="ti-trash"></i></a>';
        $html .= ' </div> ';

        return $html;
    }, 'field' => 'options', 'as' => 'options' ),
    /*[
        'db' => 'employee_clients.fk_employee_id', 'dt' => $iTable++, 'formatter' => function($d, $row) {
            return $row['fk_employee_id'] ? get_employee($row['fk_employee_id'])['first_name'] . ' ' . get_employee($row['fk_employee_id'])['last_name'] : 'Non défini';
        }, 'field' => 'fk_employee_id', 'as' => 'fk_employee_id'
    ],*/
);

$employeeLevel = get_current_employee()['fk_user_level_id'];
$employeeId = get_current_employee()['id'];
// $showMine = $employeeLevel === EmployeeLevel::RepresentantExterne;

if (user_level(1)) {
    $joinQuery = "
    FROM client
    LEFT JOIN employee_client_permissions ON employee_client_permissions.fk_client_id = client.id
    WHERE (client.fk_created_by_id = $employeeId)
    OR ( employee_client_permissions.fk_employee_id = '$employeeId'  ) ";
} else {
    $joinQuery = " FROM client ";
}

$clients = search_addresses_dt($_GET['search']['value']);
echo '<pre>';print_r($clients);die();

echo json_encode(
	SSP::simple($_GET, SSP::mysql_connection(), $table, $primaryKey, $columns, $joinQuery)
);
