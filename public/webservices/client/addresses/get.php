<?php
$debug = false;
require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

## TODO : Need global security validation ##

if(!empty($_POST['id']) && is_numeric($_POST['id'])) {

    $client = get_client_and_self_addresses($_POST['id']);

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $client = json_encode($client, JSON_PRETTY_PRINT);
    else
        $client = json_encode($client);

    print($client);

} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
