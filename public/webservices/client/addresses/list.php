<?php
  $debug = false;
  require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';

  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  ## TODO : Need global security validation ##

  if (!isset($_POST['id'])) {
    print(json_encode(['done' => false, 'error' => 'invalid params']));
    exit;
  }

  $client = new Client((int)$_POST['id']);
  $addresses = $client->getAddresses();

  if ($addresses) {
    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On') {
      $addresses = json_encode($addresses, JSON_PRETTY_PRINT);
    } else {
      $addresses = json_encode($addresses);
    }

    print($addresses);
  } else {
    print(json_encode([]));
    exit;
  }

