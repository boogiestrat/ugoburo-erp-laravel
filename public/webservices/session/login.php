<?php
    $login_page = true;
    require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

    // Ensure that this is no request forgery going on, and that the user
    // sending us this connect request is the user that was supposed to.
    if ($_REQUEST['state'] != $_SESSION['state']) {
        $json = array('code' => 401, 'msg' => "Error - 'Invalid state parameter'");
        print(json_encode($json));
        exit();
    }

    // state is a one-time use token
    $_SESSION['state'] = '';

    if (empty($_REQUEST['token'])) {
        $json = array('code' => 401, 'msg' => "Error - 'Empty token parameter'");
        print(json_encode($json));
        exit();
    }

    $code = $_REQUEST['token'];

    // Exchange the OAuth 2.0 authorization code for user credentials.
    /** @var Google_Client $googleClient */
    $googleClient = new Google_Client();
    $googleClient->setApplicationName($_ENV['GOOGLE_APP_NAME']);
    $googleClient->setClientId($_ENV['GOOGLE_CLIENT_ID']);
    $googleClient->setClientSecret($_ENV['GOOGLE_CLIENT_SECRETE']);
    $googleClient->setRedirectUri('postmessage');

    // $googleClient->authenticate($code);
    // $token = json_decode($googleClient->getAccessToken());

    // keep the google token in session in case we need it
    $_SESSION['token'] = $code;

    // You can read the Google user ID in the ID token.
    // "sub" represents the ID token subscriber which in our case
    // is the user ID. This sample does not use the user ID.
    $attributes = [];
    try {
      $attributes = $googleClient->verifyIdToken($code, $_ENV['GOOGLE_CLIENT_ID']);
    } catch (Execption $e) {
      unset($_SESSION["csrf_token"]);
      unset($_SESSION["employee"]);
      unset($_SESSION["token"]);
      session_destroy();
    }
    //->getAttributes();

    if (!isset($attributes['email'])) {
        $json = array('code' => 401, 'msg' => "Error - 'Cannot find user email on Google+.'");
        print(json_encode($json));
        exit();
    }

    //load user data
    if(!$employee = get_employee_by_email($attributes['email'])) {
        $json = array('code' => 401, 'msg' => "Error - 'Cannot find registered user in system.'");
        print(json_encode($json));
        exit();
    }

    // if (empty($employee['avatar'])) {
    //     /** @var Google_Service_Plus $gPlusService */
    //     $gPlusService = new Google_Service_Plus($googleClient);
    //     $employee_picture_url = $gPlusService->people->get("me")->getImage()->getUrl();

    //     if (!empty($employee_picture_url)) {
    //         //change image size to 300px
    //         $employee['avatar'] = str_replace('?sz=50', '?sz=200', $employee_picture_url);
    //         update_employee($employee);
    //     }
    // }
    // Store the token in the session for later use.
    $_SESSION['employee'] = serialize($employee);
    $_SESSION['csrf_token'] = csrf_token();
    // SUCCESS
    $json = array('code' => 200, 'msg' => 'Successfully connected with token: ' . print_r($token, true));
    print(json_encode($json));
    exit();
