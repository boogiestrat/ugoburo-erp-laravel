<?php
  $debug = false;
  require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  $newId = get_next_general_id();

  reserve_general_id($newId);

  print json_encode(['id' => $newId, 'employee' => get_current_employee()['id']]);
