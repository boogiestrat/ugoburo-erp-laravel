<?php
  $debug = false;
  require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  if (!isset($_POST['id'])) {
    print(json_encode(['done' => false, 'error' => 'bad id']));
    exit;
  }

  $res = reserve_general_id($_POST['id']);

  if ($res) {
    print(json_encode(['done' => true, 'res' => $res]));
    exit;
  }

  print(json_encode(['done' => false, 'error' => 'error while saving']));
  exit;
