<?php

require_once dirname(dirname(dirname(__DIR__))) . '/app/init.php';
/**
 * Remote API access key
 */
define('API_KEY', '97FHW87EGH78WB76237R6G276SDG76FGAIUH');

$action = @$_GET["action"];
$args = @$_GET["args"];

if (!empty($args)) {
  $args = @json_decode(@base64_decode(@urldecode($args)), true);
  if (!is_array($args)) { $args = []; }
}

$results = array(
  "status" => "success",
  "data" => []
);

if ($action == "get_order") {
  $order_id = @$_GET["id"];
  $order = @get_order($order_id);
  $results["data"] = $order;
}

if ($action == "get_orders") {
  $orders = @get_all_orders_magento2($args["email"]);
  $results["data"] = $orders;
}

echo json_encode($results);
