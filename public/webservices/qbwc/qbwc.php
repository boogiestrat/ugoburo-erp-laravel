<?php

error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
ini_set('display_errors', 1);

require_once dirname(__FILE__) . '/../../../vendor/autoload.php';

require_once("api.php");

$api = new QBWCApi();

$map = array(
	QUICKBOOKS_ADD_VENDOR => array( array($api, 'quickbooks_add_vendor_request'), array($api, 'quickbooks_add_vendor_response') ),
	QUICKBOOKS_ADD_SERVICEITEM => array( array($api, 'quickbooks_add_serviceitem_request'), array($api, 'quickbooks_add_serviceitem_response') ),
	QUICKBOOKS_ADD_INVENTORYITEM => array( array($api, 'quickbooks_add_inventoryitem_request'), array($api, 'quickbooks_add_inventoryitem_response') ),
	QUICKBOOKS_ADD_CUSTOMER => array( array($api, 'quickbooks_add_customer_request'), array($api, 'quickbooks_add_customer_response') ),
	QUICKBOOKS_ADD_INVOICE => array( array($api, 'quickbooks_add_invoice_request'), array($api, 'quickbooks_add_invoice_response') ),
	QUICKBOOKS_ADD_SALESRECEIPT => array( array($api, 'quickbooks_add_salesreceipt_request'), array($api, 'quickbooks_add_salesreceipt_response') ),
	QUICKBOOKS_IMPORT_CUSTOMER => array( array($api, 'quickbooks_customer_import_request'), array($api, 'quickbooks_customer_import_response') ),
	QUICKBOOKS_IMPORT_ACCOUNT => array( array($api, 'quickbooks_account_import_request'), array($api, 'quickbooks_account_import_response') ),
	QUICKBOOKS_IMPORT_VENDOR => array( array($api, 'quickbooks_vendor_import_request'), array($api, 'quickbooks_vendor_import_response') ),
);

$errmap = array(
	'*' => array($api, 'quickbooks_error_catchall'),
);

$log_level = QUICKBOOKS_LOG_DEVELOP;
$soapserver = QUICKBOOKS_SOAPSERVER_BUILTIN;

$hooks = array();
$soap_options = array();
$driver_options = array();
$callback_options = array();
$handler_options = array(
	'deny_concurrent_logins' => false,
	'deny_reallyfast_logins' => false,
);

$server = new QuickBooks_WebConnector_Server($api->dsn, $map, $errmap, $hooks, $log_level, $soapserver, QUICKBOOKS_WSDL, $soap_options, $handler_options, $driver_options, $callback_options);
$response = $server->handle(true, true);
