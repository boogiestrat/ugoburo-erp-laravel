<?php

require_once dirname(dirname(dirname(__DIR__))) . '/app/init.php';

require_once dirname(__FILE__) . '/../../../vendor/autoload.php';

/**
 * Remote API access key
 */
define('API_KEY', '9082UR98WJDF92WDJ076RWJ89S746DPK');

/**
 * Tax Code Mapping
 */
define('CODE_TAX_QC_TPS','G');
define('CODE_TAX_QC_TVQ','S');
define('CODE_TAX_ON_HST','HON');
define('CODE_TAX_BC_HST','HST');
define('CODE_TAX_NB_HST','HNB');
define('CODE_TAX_NL_HST','HNL');
define('CODE_TAX_NS_HST','HNS');
define('CODE_TAX_PE_HST','HPE');

/**
 * Configuration parameter for the quickbooks_config table, used to keep track of the last time the QuickBooks sync ran
 */
define('QB_QUICKBOOKS_CONFIG_LAST', 'last');

/**
 * Configuration parameter for the quickbooks_config table, used to keep track of the timestamp for the current iterator
 */
define('QB_QUICKBOOKS_CONFIG_CURR', 'curr');

/**
 * Maximum number of customers/invoices returned at a time when doing the import
 */
define('QB_QUICKBOOKS_MAX_RETURNED', 10);

/**
 *
 */
define('QB_PRIORITY_PURCHASEORDER', 4);

/**
 * Request priorities, items sync first
 */
define('QB_PRIORITY_ITEM', 3);

/**
 * Request priorities, customers
 */
define('QB_PRIORITY_CUSTOMER', 2);

/**
 * Request priorities, salesorders
 */
define('QB_PRIORITY_SALESORDER', 1);

/**
 * Request priorities, invoices last...
 */
define('QB_PRIORITY_INVOICE', 0);

class QBWCApi {

  function __construct() {

    $this->dotenv = new Dotenv\Dotenv(dirname(__FILE__) . '/../../../');
    $this->dotenv->overload();
    $this->dotenv->required(['MYSQL_HOST', 'MYSQL_NAME', 'MYSQL_USER', 'MYSQL_PASS', 'MYSQL_CHARSET']);
    define('MYSQL_HOST', $_ENV['MYSQL_HOST']);
    define('MYSQL_NAME', $_ENV['MYSQL_NAME']);
    define('MYSQL_USER', $_ENV['MYSQL_USER']);
    define('MYSQL_PASS', $_ENV['MYSQL_PASS']);

    $this->dsn = "mysqli://".MYSQL_USER.":".urlencode(MYSQL_PASS)."@".MYSQL_HOST."/".MYSQL_NAME;
    $this->link = mysqli_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_NAME);

    if (!QuickBooks_Utilities::initialized($this->dsn)) {
    	QuickBooks_Utilities::initialize($this->dsn);

      $qbwc_user = 'quickbooks';
      $qbwc_pass = 'wef789h87heADYUGHADU8G';

      QuickBooks_Utilities::createUser($this->dsn, $qbwc_user, $qbwc_pass);

      mysqli_query($this->link, "CREATE TABLE quickbooks_payloads (
        id int(10) unsigned NOT NULL AUTO_INCREMENT,
        payload text,
        tx_ident text,
        quickbooks_listid varchar(255) DEFAULT NULL,
        quickbooks_editsequence varchar(255) DEFAULT NULL,
        quickbooks_errnum varchar(255) DEFAULT NULL,
        quickbooks_errmsg varchar(255) DEFAULT NULL,
        PRIMARY KEY (id)
      ) ENGINE=InnoDB");
    }
  }

  function _quickbooks_set_current_run($user, $action, $force = null)
  {
  	$value = date('Y-m-d') . 'T' . date('H:i:s');

  	if ($force)
  	{
  		$value = date('Y-m-d', strtotime($force)) . 'T' . date('H:i:s', strtotime($force));
  	}

  	return QuickBooks_Utilities::configWrite($this->dsn, $user, md5(__FILE__), QB_QUICKBOOKS_CONFIG_CURR . '-' . $action, $value);
  }

  /**
   * Get the last date/time the QuickBooks sync ran
   *
   * @param string $user		The web connector username
   * @return string			A date/time in this format: "yyyy-mm-dd hh:ii:ss"
   */
  function _quickbooks_get_last_run($user, $action)
  {
  	$type = null;
  	$opts = null;
  	return QuickBooks_Utilities::configRead($this->dsn, $user, md5(__FILE__), QB_QUICKBOOKS_CONFIG_LAST . '-' . $action, $type, $opts);
  }

  /**
   * Set the last date/time the QuickBooks sync ran to NOW
   *
   * @param string $user
   * @return boolean
   */
  function _quickbooks_set_last_run($user, $action, $force = null)
  {
  	$value = date('Y-m-d') . 'T' . date('H:i:s');

  	if ($force)
  	{
  		$value = date('Y-m-d', strtotime($force)) . 'T' . date('H:i:s', strtotime($force));
  	}

  	return QuickBooks_Utilities::configWrite($this->dsn, $user, md5(__FILE__), QB_QUICKBOOKS_CONFIG_LAST . '-' . $action, $value);
  }

  function quickbooks_error_catchall($requestID, $user, $action, $ID, $extra, &$err, $xml, $errnum, $errmsg) {
  	mysqli_query($this->link, "
  		UPDATE
  			quickbooks_payloads
  		SET
  			quickbooks_errnum = '" . mysqli_real_escape_string($this->link, $errnum) . "',
  			quickbooks_errmsg = '" . mysqli_real_escape_string($this->link, $errmsg) . "'
  		WHERE
  			id = " . (int) $ID);

    $payload = $this->get_payload((int) $ID);

    if ($errnum == 500) {
      if ($payload["operation"] == "FAILED_OPERATION") {
        if ($payload["on_failure"] == "ON_FAILURE") {
          // ...
        }
      }
    }
    // return true;     // return TRUE if you want the Web Connector to continue to process requests
    // return false;    // return FALSE if you want the Web Connector to stop processing requests and report the error
    return true;
  }

  function quickbooks_payload_executed($id, $idents) {
    mysqli_query($this->link, "
  		UPDATE
  			quickbooks_payloads
  		SET
  			quickbooks_listid = '" . mysqli_real_escape_string($this->link, $idents['ListID']) . "',
  			quickbooks_editsequence = '" . mysqli_real_escape_string($this->link, $idents['EditSequence']) . "'
  		WHERE
  			id = " . (int) $id);

  }

  function quickbooks_add_vendor_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale) {
    $payload = $this->get_payload((int) $ID);

    $Vendor = new QuickBooks_QBXML_Object_Vendor();

    if (isset($payload["data"]["vendor"]["variables"])) {
      foreach($payload["data"]["vendor"]["variables"] as $key => $value) {
        $method="set$key";
        $Vendor->$method($value);
      }
    }

    $qbxml = $Vendor->asQBXML(QUICKBOOKS_ADD_VENDOR);

    return $this->qbxml_output($qbxml);
  }

  function quickbooks_add_vendor_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents) {
    $payload = $this->get_payload((int) $ID);

    $ListID = $idents['ListID'];

    $payload["data"]["vendor"]["variables"]["ListID"] = $ListID;
    $this->propagate_payload_data($payload["tx_ident"], $payload["data"]);

    if ($payload["on_success"] == "PUT_LISTID_INTO_SUPPLIER_TABLE") {
      // update TABLE set ListID='$ListID' WHERE id=...
    }

    $this->quickbooks_payload_executed($ID, $idents);
  }

  function quickbooks_add_serviceitem_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale) {
    $payload = $this->get_payload((int) $ID);

    $Item = new QuickBooks_QBXML_Object_ServiceItem();

    foreach($payload["data"]["service"]["variables"] as $key => $value) {
      $method="set$key";
      $Item->$method($value);
    }

    $Item->setIsActive(true);
    $Item->isSalesOrPurchase(true);

    $qbxml = $Item->asQBXML(QUICKBOOKS_ADD_SERVICEITEM);

    return $this->qbxml_output($qbxml);
  }

  function quickbooks_add_serviceitem_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents) {
    $payload = $this->get_payload((int) $ID);

    $ListID = $idents['ListID'];

    $payload["data"]["service"]["variables"]["ListID"] = $ListID;
    $this->propagate_payload_data($payload["tx_ident"], $payload["data"]);

    if ($payload["on_success"] == "PUT_LISTID_INTO_ITEM_TABLE") {
      // update TABLE set ListID='$CustomerListID' WHERE id=...
    }

    $this->quickbooks_payload_executed($ID, $idents);
  }

  function quickbooks_add_inventoryitem_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale) {
    $payload = $this->get_payload((int) $ID);

    $Item = new QuickBooks_QBXML_Object_InventoryItem();

    if (isset($payload["data"]["product"]["variables"])) {
      foreach($payload["data"]["product"]["variables"] as $key => $value) {
        $method="set$key";
        $Item->$method($value);
      }
    }

    $qbxml = $Item->asQBXML(QUICKBOOKS_ADD_INVENTORYITEM);

    return $this->qbxml_output($qbxml);
  }

  function quickbooks_add_inventoryitem_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents) {
    $payload = $this->get_payload((int) $ID);

    $ListID = $idents['ListID'];

    $payload["data"]["product"]["variables"]["ListID"] = $ListID;
    $this->propagate_payload_data($payload["tx_ident"], $payload["data"]);

    if ($payload["on_success"] == "PUT_LISTID_INTO_ITEM_TABLE") {
      // update TABLE set ListID='$CustomerListID' WHERE id=...
    }

    $this->quickbooks_payload_executed($ID, $idents);
  }

  function quickbooks_add_customer_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale) {
    $payload = $this->get_payload((int) $ID);

    $Customer = new QuickBooks_QBXML_Object_Customer();

    if ($payload["operation"] == "CREATE_CUSTOMER") {
      foreach($payload["data"]["customer"]["variables"] as $key => $value) {
        $method="set$key";
        $Customer->$method($value);
      }
    }
    if ($payload["operation"] == "CREATE_CONTACT") {
      foreach($payload["data"]["contact"]["variables"] as $key => $value) {
        $method="set$key";
        $Customer->$method($value);
      }
    }

    $qbxml = $Customer->asQBXML(QUICKBOOKS_ADD_CUSTOMER);

    return $this->qbxml_output($qbxml);
  }

  function quickbooks_add_customer_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents) {
    $payload = $this->get_payload((int) $ID);

    $CustomerListID = $idents['ListID'];

    if ($payload["operation"] == "CREATE_CUSTOMER") {
      $payload["data"]["customer"]["variables"]["CustomerListID"] = $CustomerListID;
      $this->propagate_payload_data($payload["tx_ident"], $payload["data"]);
    }

    if ($payload["operation"] == "CREATE_CONTACT") {
      $payload["data"]["contact"]["variables"]["CustomerListID"] = $CustomerListID;
      $this->propagate_payload_data($payload["tx_ident"], $payload["data"]);
    }

    $this->quickbooks_payload_executed($ID, $idents);
  }

  function quickbooks_account_import_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale) {
    $payload = $this->get_payload((int) $ID);

  	// Build the request
  	$qbxml = '<AccountQueryRq requestID="'.$requestID.'">
      ';

    if ($payload["operation"] == "GET_INCOME_ACCOUNT") {
      $qbxml .= '<FullName>'.$payload["data"]["income_account"]["variables"]["FullName"].'</FullName>';
    }

    if ($payload["operation"] == "GET_ASSET_ACCOUNT") {
      $qbxml .= '<FullName>'.$payload["data"]["asset_account"]["variables"]["FullName"].'</FullName>';
    }

    $qbxml .= '
      <OwnerID>0</OwnerID>
    </AccountQueryRq>
    ';

    return $this->qbxml_output($qbxml);
  }

  function quickbooks_account_import_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents) {

    $payload = $this->get_payload((int) $ID);

    $accounts = [];

  	$errnum = 0;
  	$errmsg = '';
  	$Parser = new QuickBooks_XML_Parser($xml);
  	if ($Doc = $Parser->parse($errnum, $errmsg)) {
  		$Root = $Doc->getRoot();
  		$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/AccountQueryRs');

  		foreach ($List->children() as $Account) {
  			$arr = array(
  				'ListID' => $Account->getChildDataAt('AccountRet ListID'),
  				'Name' => $Account->getChildDataAt('AccountRet Name'),
				);

  			foreach ($arr as $key => $value) {
  				$arr[$key] = mysqli_real_escape_string($this->link, $value);
  			}

        $accounts[] = $arr;
  		}
    }

    $account_exists = false;
    $AccountListID = false;
    if (count($accounts) === 1) {
      $account_exists = true;
      $AccountListID = $accounts[0]["ListID"];
    }

    if ($payload["operation"] == "GET_INCOME_ACCOUNT") {
      if ($account_exists) {
        $payload["data"]["income_account"]["variables"]["ListID"] = $AccountListID;
        $this->propagate_payload_data($payload["tx_ident"], $payload["data"]);
      }
    }

    if ($payload["operation"] == "GET_ASSET_ACCOUNT") {
      if ($account_exists) {
        $payload["data"]["asset_account"]["variables"]["ListID"] = $AccountListID;
        $this->propagate_payload_data($payload["tx_ident"], $payload["data"]);
      }
    }

    $this->quickbooks_payload_executed($ID, $idents);
  }

  function quickbooks_vendor_import_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale) {
    $payload = $this->get_payload((int) $ID);

  	// Build the request
  	$qbxml = '<VendorQueryRq requestID="'.$requestID.'">
      ';

    if ($payload["operation"] == "GET_VENDOR") {
      $qbxml .= '<FullName>'.$payload["data"]["vendor"]["variables"]["FullName"].'</FullName>';
    }

    $qbxml .= '
      <OwnerID>0</OwnerID>
    </VendorQueryRq>
    ';

    return $this->qbxml_output($qbxml);
  }

  function quickbooks_vendor_import_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents) {

    $payload = $this->get_payload((int) $ID);

    $vendors = [];

  	$errnum = 0;
  	$errmsg = '';
  	$Parser = new QuickBooks_XML_Parser($xml);
  	if ($Doc = $Parser->parse($errnum, $errmsg)) {
  		$Root = $Doc->getRoot();
  		$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/VendorQueryRs');

  		foreach ($List->children() as $Vendor) {
  			$arr = array(
  				'ListID' => $Vendor->getChildDataAt('VendorRet ListID'),
  				'Name' => $Vendor->getChildDataAt('VendorRet Name'),
				);

  			foreach ($arr as $key => $value) {
  				$arr[$key] = mysqli_real_escape_string($this->link, $value);
  			}

        $vendors[] = $arr;
  		}
    }

    $vendor_exists = false;
    $VendorListID = false;
    if (count($vendors) === 1) {
      $vendor_exists = true;
      $VendorListID = $vendors[0]["ListID"];
    }

    if ($payload["operation"] == "GET_VENDOR") {
      if ($vendor_exists) {
        $payload["data"]["vendor"]["variables"]["ListID"] = $VendorListID;
        $this->propagate_payload_data($payload["tx_ident"], $payload["data"]);
      }
    }

    $this->quickbooks_payload_executed($ID, $idents);
  }

  function quickbooks_customer_import_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale) {
    $payload = $this->get_payload((int) $ID);

  	// Build the request
  	$qbxml = '<CustomerQueryRq requestID="'.$requestID.'">
      ';

    if ($payload["operation"] == "GET_CUSTOMER") {
      $qbxml .= '<FullName>'.$payload["data"]["customer"]["variables"]["FullName"].'</FullName>';
    }

    if ($payload["operation"] == "GET_CONTACT") {
      $qbxml .= '<FullName>'.$payload["data"]["customer"]["variables"]["FullName"].':'.$payload["data"]["contact"]["variables"]["FullName"].'</FullName>';
    }

    $qbxml .= '
      <OwnerID>0</OwnerID>
    </CustomerQueryRq>
    ';

    return $this->qbxml_output($qbxml);
  }

  function quickbooks_customer_import_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents) {

    $payload = $this->get_payload((int) $ID);

    $customers = [];

  	$errnum = 0;
  	$errmsg = '';
  	$Parser = new QuickBooks_XML_Parser($xml);
  	if ($Doc = $Parser->parse($errnum, $errmsg)) {
  		$Root = $Doc->getRoot();
  		$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/CustomerQueryRs');

  		foreach ($List->children() as $Customer) {
  			$arr = array(
  				'ListID' => $Customer->getChildDataAt('CustomerRet ListID'),
  				'TimeCreated' => $Customer->getChildDataAt('CustomerRet TimeCreated'),
  				'TimeModified' => $Customer->getChildDataAt('CustomerRet TimeModified'),
  				'Name' => $Customer->getChildDataAt('CustomerRet Name'),
  				'FullName' => $Customer->getChildDataAt('CustomerRet FullName'),
  				'FirstName' => $Customer->getChildDataAt('CustomerRet FirstName'),
  				'MiddleName' => $Customer->getChildDataAt('CustomerRet MiddleName'),
  				'LastName' => $Customer->getChildDataAt('CustomerRet LastName'),
  				'Contact' => $Customer->getChildDataAt('CustomerRet Contact'),
  				'ShipAddress_Addr1' => $Customer->getChildDataAt('CustomerRet ShipAddress Addr1'),
  				'ShipAddress_Addr2' => $Customer->getChildDataAt('CustomerRet ShipAddress Addr2'),
  				'ShipAddress_City' => $Customer->getChildDataAt('CustomerRet ShipAddress City'),
  				'ShipAddress_State' => $Customer->getChildDataAt('CustomerRet ShipAddress State'),
  				'ShipAddress_PostalCode' => $Customer->getChildDataAt('CustomerRet ShipAddress PostalCode'),
  				);

  			foreach ($arr as $key => $value) {
  				$arr[$key] = mysqli_real_escape_string($this->link, $value);
  			}

        $customers[] = $arr;
  		}
    }

    $customer_exists = false;
    $CustomerListID = false;
    if (count($customers) === 1) {
      $customer_exists = true;
      $CustomerListID = $customers[0]["ListID"];
    }

    if ($payload["operation"] == "GET_CUSTOMER") {
      if ($customer_exists) {
        $payload["data"]["customer"]["variables"]["CustomerListID"] = $CustomerListID;
        if (isset($payload["data"]["contact"])) {
          $payload["data"]["contact"]["variables"]["ParentListID"] = $payload["data"]["customer"]["variables"]["CustomerListID"];
        }
        $this->propagate_payload_data($payload["tx_ident"], $payload["data"]);
      }
    }

    if ($payload["operation"] == "GET_CONTACT") {
      if ($customer_exists) {
        $payload["data"]["contact"]["variables"]["CustomerListID"] = $CustomerListID;
        $this->propagate_payload_data($payload["tx_ident"], $payload["data"]);

        if ($payload["on_success"] == "CREATE_INVOICE") {
          $this->create_invoice_payload($payload);
        }
      }
    }

    $this->quickbooks_payload_executed($ID, $idents);
  }

  function quickbooks_add_invoice_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale) {

    $payload = $this->get_payload((int) $ID);

    $Invoice = new QuickBooks_QBXML_Object_Invoice();

    // $payload["data"]["invoice"]["variables"]["CustomerListID"] = $payload["data"]["contact"]["variables"]["CustomerListID"];
    if (isset($payload["data"]["invoice"]["variables"])) {
      foreach($payload["data"]["invoice"]["variables"] as $key => $value) {
        $method="set$key";
        $Invoice->$method($value);
      }
    }

    if (isset($payload["data"]["invoice"]["lines"])) {
      foreach($payload["data"]["invoice"]["lines"] as $index => $line) {
        $InvoiceLine = new QuickBooks_QBXML_Object_Invoice_InvoiceLine();
        foreach($line["variables"] as $key => $value) {
          $method="set$key";
          $InvoiceLine->$method($value);
        }
        $Invoice->addInvoiceLine($InvoiceLine);
      }
    }

    $qbxml = $Invoice->asQBXML(QUICKBOOKS_ADD_INVOICE);

    return $this->qbxml_output($qbxml);
  }

  function quickbooks_add_invoice_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents) {
    /*
      TODO: assign newly created Invoice Number to the ERP's quote payments table
    */
    $this->quickbooks_payload_executed($ID, $idents);
  }

  function quickbooks_add_salesreceipt_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale) {

    $payload = $this->get_payload((int) $ID);

    $SalesReceipt = new QuickBooks_QBXML_Object_SalesReceipt();

    // $payload["data"]["salesreceipt"]["variables"]["CustomerListID"] = $payload["data"]["contact"]["variables"]["CustomerListID"];

    foreach($payload["data"]["salesreceipt"]["variables"] as $key => $value) {
      $method="set$key";
      $SalesReceipt->$method($value);
    }

    foreach($payload["data"]["salesreceipt"]["lines"] as $index => $line) {
      $SalesReceiptLine = new QuickBooks_QBXML_Object_SalesReceipt_SalesReceiptLine();
      foreach($line["variables"] as $key => $value) {
        $method="set$key";
        $SalesReceiptLine->$method($value);
      }
      $SalesReceipt->addSalesReceiptLine($SalesReceiptLine);
    }

    $qbxml = $SalesReceipt->asQBXML(QUICKBOOKS_ADD_SALESRECEIPT);

    $payload["qbxml"] = $qbxml;
    $this->update_payload((int) $ID, $payload);

    return $this->qbxml_output($qbxml);
  }

  function quickbooks_add_salesreceipt_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents) {
    /*
      TODO: assign newly created Invoice Number to the ERP's quote payments table
    */
    $this->quickbooks_payload_executed($ID, $idents);
  }

  function output($data = true, $type = "success") {
    return json_encode(array($type=>$data));
  }

  function qbxml_output($qbxml) {
    $xml = '<?xml version="1.0" encoding="utf-8"?>
<?qbxml version="8.0"?>
<QBXML>
  <QBXMLMsgsRq onError="stopOnError">
  ';
    $xml .= $qbxml;
    $xml .= '</QBXMLMsgsRq>
</QBXML>';
    return $xml;
  }

  function create_supplier($data) {
    $tx_ident = $this->gen_txid();

    $payload = array(
      "operation" => "CREATE_SUPPLIER",
      "action" => QUICKBOOKS_ADD_VENDOR,
      "on_failure" => false,
      "on_success" => "PUT_LISTID_INTO_SUPPLIER_TABLE",
      "data" => $data,
    );
    $this->insert_payload($tx_ident, $payload, 15);
  }

  function create_service($data) {
    $tx_ident = $this->gen_txid();

    $payload = array(
      "operation" => "CREATE_SERVICE",
      "action" => QUICKBOOKS_ADD_SERVICEITEM,
      "on_failure" => false,
      "on_success" => "PUT_LISTID_INTO_ITEM_TABLE",
      "data" => $data,
    );
    $this->insert_payload($tx_ident, $payload, 10);
  }

  function create_product($data) {
    /*
      create ITEM in QB when creating PRODUCT in ERP
    */
    $tx_ident = $this->gen_txid();

    $payload = array(
      "operation" => "CREATE_PRODUCT",
      "action" => QUICKBOOKS_ADD_INVENTORYITEM,
      "on_failure" => false,
      "on_success" => "PUT_LISTID_INTO_ITEM_TABLE",
      "data" => $data,
    );
    $this->insert_payload($tx_ident, $payload, 10);

  }

  function create_contact($data) {
    /*
      create ITEM in QB when creating PRODUCT in ERP
    */
    $tx_ident = $this->gen_txid();

    $payload = array(
      "operation" => "CREATE_CONTACT",
      "action" => QUICKBOOKS_ADD_CUSTOMER,
      "on_failure" => false,
      "on_success" => "PUT_LISTID_INTO_ITEM_TABLE",
      "data" => $data,
    );
    $this->insert_payload($tx_ident, $payload, 10);

  }

  function create_customer($data) {
    /*
      create ITEM in QB when creating PRODUCT in ERP
    */
    $tx_ident = $this->gen_txid();

    $payload = array(
      "operation" => "CREATE_CUSTOMER",
      "action" => QUICKBOOKS_ADD_CUSTOMER,
      "on_failure" => false,
      "on_success" => "PUT_LISTID_INTO_ITEM_TABLE",
      "data" => $data,
    );
    $this->insert_payload($tx_ident, $payload, 10);

  }

  function create_invoice($data) {

    $tx_ident = $this->gen_txid();

    $payload = array(
      "operation" => "CREATE_INVOICE",
      "action" => QUICKBOOKS_ADD_INVOICE,
      "on_failure" => false,
      "on_success" => "PUT_LISTID_INTO_ITEM_TABLE",
      "data" => $data,
    );
    $this->insert_payload($tx_ident, $payload, 5);

  }

  function create_salesreceipt($data) {

    $tx_ident = $this->gen_txid();

    $payload = array(
      "operation" => "CREATE_SALESRECEIPT",
      "action" => QUICKBOOKS_ADD_SALESRECEIPT,
      "on_failure" => false,
      "on_success" => "PUT_LISTID_INTO_ITEM_TABLE",
      "data" => $data,
    );
    $this->insert_payload($tx_ident, $payload, 5);

  }

  function send_to_quickbooks_from_erp($payment_method, $order) {

    // -----------------------------------------------------------------
    // transfer into QuickBooks

    // parse data

    $order_data = $order;

    $tx_ident = $this->gen_txid();

    $internal_order_id = $order_data["order_number"];
    $reference_number = $internal_order_id;

    $customer_fullname = $order_data["bill_to"]["firstname"].' '.$order_data["bill_to"]["lastname"].' - '.$order_data["bill_to"]["entreprise"];

    $po_number = false;
    if ($payment_method == "purchase_order") {
      $po_number = $order_data["po_number"];
    }

    // create customer if not exists
    $data = array(
      "customer" => array(
        "variables" => array(
          'FullName' => $customer_fullname,
        ),
      ),
    );
    $this->create_customer($data);

    // create vendor if not exists
    $data = array(
      "vendor" => array(
        "variables" => array(
          'Name' => "Product From Website"
        ),
      ),
    );
    $this->create_supplier($data);

    // parse order items
    $lines = [];
    foreach($order_data["items"] as $order_item ) {
      $line=[];
      $line["ItemName"] = $order_item["SKU"];
      $line["Desc"] = htmlentities($order_item["title"]);
      $line["Quantity"] = $order_item["quantity"];
      $line["Amount"] = $order_item["price"] * $order_item["quantity"];
      $lines[] = array("variables"=>$line);

      // create product if not exists
      $data = array(
        "product" => array(
          "variables" => array(
            'FullName' => $order_item["SKU"],
            "AssetAccountName" => "Inventory Asset",
            "IncomeAccountName" => "Ventes",
            "COGSAccountName" => "Cost of Goods Sold",
            "PreferredVendorName" => "Product From Website",
          ),
        ),
      );
      $this->create_product($data);

    }

    // process order

    if ($payment_method == "purchase_order") {
      // invoice when PO
      $data = array(
        "invoice" => array(
          "variables" => array(
            'RefNumber' => $reference_number,
            'CustomerFullName' => $customer_fullname,
          ),
          "lines" => $lines,
        ),
      );

      $this->create_invoice($data);
    }

    if ($payment_method == "credit_card") {
      // sales receipt when Credit Card
      $data = array(
        "salesreceipt" => array(
          "variables" => array(
            'RefNumber' => $reference_number,
            'CustomerFullName' => $customer_fullname,
          ),
          "lines" => $lines,
        ),
      );

      $this->create_salesreceipt($data);
    }


  }

  function create_order_from_magento2($data) {

    // -----------------------------------------------------------------
    /*
      example order payload -- taken from magento2 order on staging
    */
    // $data_payload = '{"order_details":{"applied_rule_ids":"","base_currency_code":"CAD","base_discount_amount":0,"base_grand_total":1459.17,"base_discount_tax_compensation_amount":0,"base_shipping_amount":59,"base_shipping_discount_amount":0,"base_shipping_discount_tax_compensation_amnt":0,"base_shipping_incl_tax":68.14,"base_shipping_tax_amount":9.14,"base_subtotal":989,"base_subtotal_incl_tax":1142.03,"base_tax_amount":162.17,"base_total_due":1459.17,"base_to_global_rate":1,"base_to_order_rate":1,"customer_email":"maxime.labelle@ekosys.ca","customer_firstname":"Maxime","customer_group_id":1,"customer_id":"8834","customer_is_guest":0,"customer_lastname":"Labelle","customer_note_notify":1,"discount_amount":0,"discount_description":"","global_currency_code":"CAD","grand_total":1459.17,"discount_tax_compensation_amount":0,"increment_id":"000000024","is_virtual":0,"order_currency_code":"CAD","quote_id":"88","remote_ip":"184.161.75.61","shipping_amount":59,"shipping_description":"Shipping - Shipping urbain","shipping_discount_amount":0,"shipping_discount_tax_compensation_amount":0,"shipping_incl_tax":68.14,"shipping_tax_amount":9.14,"store_currency_code":"CAD","store_id":1,"store_to_base_rate":0,"store_to_order_rate":0,"subtotal":989,"subtotal_incl_tax":1142.03,"tax_amount":162.17,"total_due":1459.17,"total_qty_ordered":1,"weight":0,"x_forwarded_for":"184.161.75.61","items":[{}],"status_histories":[],"extension_attributes":{},"addresses":[{},{}],"shipping_method":"regular_shipping","payment":{},"customer_middlename":null,"mailchimp_abandonedcart_flag":"0","mailchimp_campaign_id":null,"mailchimp_landing_page":null,"mailchimp_flag":0,"fee":"0.00","gift_message_id":null,"state":"new","status":"pending","store_name":"Furniture\nFurniture store\nEn","total_item_count":1,"protect_code":"38dfdca857b946cf920909ab8f4ad597","entity_id":"71","id":"71","created_at":"2019-11-07 18:39:09","updated_at":"2019-11-07 18:39:09","billing_address_id":"136","shipping_address_id":"135","applied_tax_is_saved":true,"send_email":true},"shipping_address":{"vat_id":null,"vat_is_valid":null,"vat_request_id":null,"vat_request_date":null,"vat_request_success":null,"customer_address_id":"13844","prefix":null,"firstname":"Maxime","middlename":null,"lastname":"Labelle","suffix":null,"company":null,"street":"2165 Rue Desroches","city":"Quebec City","region":"Quebec","region_id":"76","postcode":"G1J 1T7","country_id":"CA","telephone":"4199993333","fax":null,"email":"maxime.labelle@ekosys2.ca","address_type":"shipping","quote_address_id":"294","parent_id":"71","entity_id":"135","id":"135"},"billing_address":{"vat_id":null,"vat_is_valid":null,"vat_request_id":null,"vat_request_date":null,"vat_request_success":null,"customer_address_id":13844,"prefix":null,"firstname":"Maxime","middlename":null,"lastname":"Labelle","suffix":null,"company":null,"street":"2165 Rue Desroches","city":"Quebec City","region":"Quebec","region_id":76,"postcode":"G1J 1T7","country_id":"CA","telephone":"4199993333","fax":null,"email":"maxime.labelle@ekosys2.ca","address_type":"billing","quote_address_id":"295","parent_id":"71","entity_id":"136","id":"136"},"order_items":[{"id":"000000024","name":"Concept 300 LC300-CFCH - Credenza table with lateral file and hutch with doors","sku":"LC300-CFCH-L-3060/2042-RBY","Price":989,"description":"test desc","Ordered Qty":1}],"order_id":"000000024","customer_id":"8834","payment_methodTitle":"Purchase Order","payment_method":"purchaseorder","payment_details":{"method":"purchaseorder","additional_data":null,"additional_information":{"method_title":"Purchase Order"},"po_number":"345345","cc_type":null,"cc_number_enc":null,"cc_last_4":null,"cc_owner":null,"cc_exp_month":null,"cc_exp_year":"0","cc_ss_issue":null,"cc_ss_start_month":"0","cc_ss_start_year":"0","cc_number":null,"cc_cid":null,"parent_id":"71","amount_ordered":1459.17,"base_amount_ordered":1459.17,"shipping_amount":59,"base_shipping_amount":59,"method_instance":{},"entity_id":"68","id":"68","extension_attributes":{}}}';
    // $data = json_decode(trim($data_payload), true);
    // echo '<pre>';print_r($data);die();
    // -----------------------------------------------------------------

    // -----------------------------------------------------------------
    // create order in ERP
    //
    // $date = date("Ym");
    //
    // $query = " SELECT project_id FROM general_id order by project_id desc limit 1;";
    //
    // $result = mysqli_query($this->link, $query);
    //
    // $project_id=0;
    // while ($row = mysqli_fetch_assoc($result)) {
    //   $project_id=substr($row['project_id'], -2);
    // }
    //
    // $next_general_id = $date . sprintf("%04d", ($project_id + 1));
    // $sql = "INSERT INTO general_id(project_id) VALUES('".$next_general_id."')";
    // mysqli_query($this->link, $sql);

    $quote_json_template = '{"additionalNote":"","additionalNoteInterne":"","additionnalFees":[],"availableDiscounts":[],"billTo":{"client":{"id":"455","title":"03Medias","active":"1","created_at":"2013-06-11 22:24:37","updated_at":"2013-06-12 04:47:51","client_type_enum":"2","contacts":[{"first_name":"Isabelle","last_name":"Lessard","email":"info@03medias.com","phone":"450-770-7057","cell":null,"id":"462","is_default_contact":null}],"addresses":[{"id":"473","address":"863, de la Roche","city":"Granby","province":"Quebec","postal_code":"J2J2W8","country":"CA","is_default_shipping":"1","is_default_facturation":"1"}]},"address":{"id":"473","address":"863, de la Roche","city":"Granby","province":"Quebec","postal_code":"J2J2W8","country":"CA","is_default_shipping":"1","is_default_facturation":"1"},"contact":{"first_name":"Isabelle","last_name":"Lessard","email":"info@03medias.com","phone":"450-770-7057","cell":null,"id":"462","is_default_contact":null},"taxes":[{"id":11,"province":2,"valueDescription":"5% TPS - 839049913","value":0.05,"year":"2019","priority":0,"order":0},{"id":14,"province":2,"valueDescription":"9,975% TVQ - 1217187512","value":0.09975,"year":"2019","priority":0,"order":1}]},"clauses":[],"date":"28-01-2020 09:05:57","depot":0,"depotPercent":0,"discountMethod":"percent","discountParam":0,"installationAndShippingMethod":"percent","installationAndShippingParam":0,"id":7,"language":"fr","number":"2020010034","sections":[{"id":0,"title":"Nouvelle section","items":[],"folded":false}],"shipTo":{"client":{"id":"455","title":"03Medias","active":"1","created_at":"2013-06-11 22:24:37","updated_at":"2013-06-12 04:47:51","client_type_enum":"2","contacts":[{"first_name":"Isabelle","last_name":"Lessard","email":"info@03medias.com","phone":"450-770-7057","cell":null,"id":"462","is_default_contact":null}],"addresses":[{"id":"473","address":"863, de la Roche","city":"Granby","province":"Quebec","postal_code":"J2J2W8","country":"CA","is_default_shipping":"1","is_default_facturation":"1"}]},"address":{"id":"473","address":"863, de la Roche","city":"Granby","province":"Quebec","postal_code":"J2J2W8","country":"CA","is_default_shipping":"1","is_default_facturation":"1"},"contact":{"first_name":"Isabelle","last_name":"Lessard","email":"info@03medias.com","phone":"450-770-7057","cell":null,"id":"462","is_default_contact":null}},"status":{"color":"primary","id":1,"name":"En soumission","list_order":1},"title":"","validUntil":"27-02-2020 09:05:57","specialist":{"id":"0","first_name":"","last_name":"","address":"","city":"","province":"qc","country":"ca","postal_code":"","email":"","mobile":"","avatar":"default.png","fk_user_level_id":"3","last_connection":null,"creation":"2019-10-26 10:57:24","last_modification":"2019-10-27 12:56:50"},"total":0,"subTotal":0,"TPS":0,"TVQ":0}';

    $quote_json = json_decode($quote_json_template, true);
    // $quote_json["number"] = $next_general_id;
    $quote_json["total"] = $data["order_details"]["base_grand_total"];

    $sql = "

      INSERT INTO orders(

        fk_status_id,
        order_source,
        magento2_order_id,
        magento2_customer_id,
        order_number,
        gt_base,
        created_at,
        updated_at,
        purchased_on,
        created_fk_employee_id,
        fk_order_id

        )

      VALUES(

        '1',
        'website',
        '".$data["order_id"]."',
        '".$data["customer_id"]."',
        '".$next_general_id."',
        '".$data["order_details"]["base_grand_total"]."',
        '".date("Y-m-d H:i:s")."',
        '".date("Y-m-d H:i:s")."',
        '".date("Y-m-d H:i:s")."',
        '0',
        '0'

        )

    "; mysqli_query($this->link, $sql);

    $order_id = mysqli_insert_id($this->link);

    $contact_email = mysqli_real_escape_string($this->link, $data["billing_address"]["email"]);
    $contact_id = 0;
    $client_id = 0;
    $sql = "SELECT * FROM contact WHERE email='$contact_email'";
    $result = mysqli_query($this->link, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
      $contact_id = $row["id"];
      $client_id = $row["fk_client_id"];
    }

    if ($contact_id == 0) {

      $sql = "
      INSERT INTO client(
        fk_client_type_id,
        active,
        fk_created_by_id,
        entreprise,
        creation,
        modified
      )
      VALUES(
        '1',
        '1',
        '0',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["firstname"]." ".$data["billing_address"]["lastname"])."',
        '".date("Y-m-d H:i:s")."',
        '".date("Y-m-d H:i:s")."'
      )"; mysqli_query($this->link, $sql);

      $client_id = mysqli_insert_id($this->link);

      $sql = "INSERT INTO contact(
        fk_client_id,
        is_default_contact,
        fk_created_by_id,
        first_name,
        last_name,
        email,
        created_at,
        updated_at
      )
      VALUES(
        '".$client_id."',
        '1',
        '0',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["firstname"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["lastname"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["email"])."',
        '".date("Y-m-d H:i:s")."',
        '".date("Y-m-d H:i:s")."'
      )"; mysqli_query($this->link, $sql);

      $contact_id = mysqli_insert_id($this->link);

      $sql = "INSERT INTO address(
        fk_client_id,
        is_default_facturation,
        is_default_shipping,
        address,
        city,
        province,
        postal_code,
        country,
        created_at,
        updated_at
      )
      VALUES(
        '".$client_id."',
        '1',
        '1',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["street"]). ', '
        .mysqli_real_escape_string($this->link, $data["billing_address"]["city"]).', '
        .mysqli_real_escape_string($this->link, $data["billing_address"]["region"]).', '
        .mysqli_real_escape_string($this->link, $data["billing_address"]["postcode"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["city"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["region"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["postcode"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["country_id"])."',
        '".date("Y-m-d H:i:s")."',
        '".date("Y-m-d H:i:s")."'
      )"; mysqli_query($this->link, $sql);

    }

    $quote_json["billTo"]["client"]["id"] = $client_id;
    $quote_json["billTo"]["client"]["title"] = mysqli_real_escape_string($this->link, $data["billing_address"]["firstname"]." ".$data["billing_address"]["lastname"]);
    $quote_json["billTo"]["client"]["client_type_enum"] = 1;

    $quote_json["billTo"]["address"]["address"] = mysqli_real_escape_string($this->link, $data["billing_address"]["street"]). ', '
    .mysqli_real_escape_string($this->link, $data["billing_address"]["city"]).', '
    .mysqli_real_escape_string($this->link, $data["billing_address"]["region"]).', '
    .mysqli_real_escape_string($this->link, $data["billing_address"]["postcode"]);
    $quote_json["billTo"]["address"]["city"] = $data["billing_address"]["city"];
    $quote_json["billTo"]["address"]["province"] = $data["billing_address"]["region"];
    $quote_json["billTo"]["address"]["postal_code"] = $data["billing_address"]["postcode"];
    $quote_json["billTo"]["address"]["country"] = $data["billing_address"]["country_id"];
    $quote_json["billTo"]["address"]["is_default_shipping"] = 1;
    $quote_json["billTo"]["address"]["is_default_facturation"] = 1;

    $quote_json["billTo"]["contact"]["first_name"] = $data["billing_address"]["firstname"];
    $quote_json["billTo"]["contact"]["last_name"] = $data["billing_address"]["lastname"];
    $quote_json["billTo"]["contact"]["email"] = $data["billing_address"]["email"];
    $quote_json["billTo"]["contact"]["id"] = $contact_id;

    $sql = "UPDATE orders SET fk_client_id='$client_id' WHERE id='$order_id' ";
    mysqli_query($this->link, $sql);

    $formatted = $data["billing_address"]["firstname"]." ".$data["billing_address"]["lastname"]."\n"
    .$data["billing_address"]["street"]."\n"
    .$data["billing_address"]["city"]."\n"
    .$data["billing_address"]["region"]."\n"
    .$data["billing_address"]["postcode"]."\n"
    .$data["billing_address"]["country_id"]
    ;

    $sql = "

      INSERT INTO order_addresses(

        fk_order_id,
        fk_contact_id,
        type,
        formatted,
        firstname,
        lastname,
        email,
        address,
        city,
        province,
        postal_code,
        country,
        entreprise

        )

      VALUES(

        '".$order_id."',
        '".$contact_id."',
        'bill_to',
        '".mysqli_real_escape_string($this->link, $formatted)."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["firstname"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["lastname"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["email"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["street"]). ', '
        .mysqli_real_escape_string($this->link, $data["billing_address"]["city"]).', '
        .mysqli_real_escape_string($this->link, $data["billing_address"]["region"]).', '
        .mysqli_real_escape_string($this->link, $data["billing_address"]["postcode"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["city"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["region"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["postcode"])."',
        '".mysqli_real_escape_string($this->link, $data["billing_address"]["country_id"])."',
        ''

        )

    "; mysqli_query($this->link, $sql);

    $contact_email = mysqli_real_escape_string($this->link, $data["shipping_address"]["email"]);
    $contact_id = 0;
    $sql = "SELECT * FROM contact WHERE email='$contact_email'";
    $result = mysqli_query($this->link, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
      $contact_id = $row["id"];
    }

    if ($contact_id == 0) {

      $sql = "
      INSERT INTO client(
        fk_client_type_id,
        active,
        fk_created_by_id,
        entreprise,
        creation,
        modified
      )
      VALUES(
        '1',
        '1',
        '0',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["firstname"]." ".$data["shipping_address"]["lastname"])."',
        '".date("Y-m-d H:i:s")."',
        '".date("Y-m-d H:i:s")."'
      )"; mysqli_query($this->link, $sql);

      $client_id = mysqli_insert_id($this->link);

      $sql = "INSERT INTO contact(
        fk_client_id,
        is_default_contact,
        fk_created_by_id,
        first_name,
        last_name,
        email,
        created_at,
        updated_at
      )
      VALUES(
        '".$client_id."',
        '1',
        '0',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["firstname"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["lastname"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["email"])."',
        '".date("Y-m-d H:i:s")."',
        '".date("Y-m-d H:i:s")."'
      )"; mysqli_query($this->link, $sql);

      $contact_id = mysqli_insert_id($this->link);

      $sql = "INSERT INTO address(
        fk_client_id,
        is_default_facturation,
        is_default_shipping,
        address,
        city,
        province,
        postal_code,
        country,
        created_at,
        updated_at
      )
      VALUES(
        '".$client_id."',
        '1',
        '1',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["street"]). ', '
        .mysqli_real_escape_string($this->link, $data["shipping_address"]["city"]).', '
        .mysqli_real_escape_string($this->link, $data["shipping_address"]["region"]).', '
        .mysqli_real_escape_string($this->link, $data["shipping_address"]["postcode"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["city"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["region"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["postcode"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["country_id"])."',
        '".date("Y-m-d H:i:s")."',
        '".date("Y-m-d H:i:s")."'
      )"; mysqli_query($this->link, $sql);
    }

    $quote_json["shipTo"]["client"]["id"] = $client_id;
    $quote_json["shipTo"]["client"]["title"] = mysqli_real_escape_string($this->link, $data["shipping_address"]["firstname"]." ".$data["shipping_address"]["lastname"]);
    $quote_json["shipTo"]["client"]["client_type_enum"] = 1;

    $quote_json["shipTo"]["address"]["address"] = mysqli_real_escape_string($this->link, $data["shipping_address"]["street"]). ', '
    .mysqli_real_escape_string($this->link, $data["shipping_address"]["city"]).', '
    .mysqli_real_escape_string($this->link, $data["shipping_address"]["region"]).', '
    .mysqli_real_escape_string($this->link, $data["shipping_address"]["postcode"]);
    $quote_json["shipTo"]["address"]["city"] = $data["shipping_address"]["city"];
    $quote_json["shipTo"]["address"]["province"] = $data["shipping_address"]["region"];
    $quote_json["shipTo"]["address"]["postal_code"] = $data["shipping_address"]["postcode"];
    $quote_json["shipTo"]["address"]["country"] = $data["shipping_address"]["country_id"];
    $quote_json["shipTo"]["address"]["is_default_shipping"] = 1;
    $quote_json["shipTo"]["address"]["is_default_facturation"] = 1;

    $quote_json["shipTo"]["contact"]["first_name"] = $data["shipping_address"]["firstname"];
    $quote_json["shipTo"]["contact"]["last_name"] = $data["shipping_address"]["lastname"];
    $quote_json["shipTo"]["contact"]["email"] = $data["shipping_address"]["email"];
    $quote_json["shipTo"]["contact"]["id"] = $contact_id;

    $formatted = $data["shipping_address"]["firstname"]." ".$data["shipping_address"]["lastname"]."\n"
    .$data["shipping_address"]["street"]."\n"
    .$data["shipping_address"]["city"]."\n"
    .$data["shipping_address"]["region"]."\n"
    .$data["shipping_address"]["postcode"]."\n"
    .$data["shipping_address"]["country_id"]
    ;

    $sql = "

      INSERT INTO order_addresses(

        fk_order_id,
        fk_contact_id,
        type,
        formatted,
        firstname,
        lastname,
        email,
        address,
        city,
        province,
        postal_code,
        country,
        entreprise

        )

      VALUES(

        '".$order_id."',
        '".$contact_id."',
        'ship_to',
        '".mysqli_real_escape_string($this->link, $formatted)."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["firstname"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["lastname"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["email"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["street"]). ', '
        .mysqli_real_escape_string($this->link, $data["shipping_address"]["city"]).', '
        .mysqli_real_escape_string($this->link, $data["shipping_address"]["region"]).', '
        .mysqli_real_escape_string($this->link, $data["shipping_address"]["postcode"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["city"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["region"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["postcode"])."',
        '".mysqli_real_escape_string($this->link, $data["shipping_address"]["country_id"])."',
        ''

        )

    "; mysqli_query($this->link, $sql);

    foreach($data["order_items"] as $order_item ) {
      $sql = "

        INSERT INTO order_item(

          priceParameterValue,
          expeditionPrice,
          expeditionPercent,
          profitMethod,
          expeditionPriceMethod,
          pictureURL,
          discountCode,
        	discountName,
        	discountParts,
          fk_order_id,
          SKU,
          title,
          price,
          quantity,
          description

          )

        VALUES(

          '0',
          '0',
          '0',
          '0',
          '',
          '',
          '',
          '',
          '',
          '".$order_id."',
          '".mysqli_real_escape_string($this->link, $order_item["SKU"])."',
          '".mysqli_real_escape_string($this->link, $order_item["name"])."',
          '".mysqli_real_escape_string($this->link, $order_item["Price"])."',
          '".mysqli_real_escape_string($this->link, $order_item["Ordered Qty"])."',
          '".mysqli_real_escape_string($this->link, $order_item["description"])."'

          )

      "; mysqli_query($this->link, $sql);

      $quote_json["sections"][0]["items"][] = array(

                  "id"=> 0,
                  "description"=> $order_item["description"],
                  "discount"=> [
                    "code"=> "",
                    "name"=> "",
                    "parts"=> [
                      0,
                      0,
                      0,
                      0,
                      0
                    ]
                  ],
                  "expeditionPercent"=> 0,
                  "expeditionPrice"=> 0,
                  "expeditionPriceMethod"=> null,
                  "price"=> $order_item["Price"],
                  "title"=> $order_item["name"],
                  "priceParameterValue"=> 0,
                  "profitMethod"=> 0,
                  "quantity"=> $order_item["Ordered Qty"],
                  "SKU"=> $order_item["SKU"],
                  "isFocused"=> true,
                  "description_open"=> true,
                  "_LineShipping"=> 0,
                  "_CostPrice"=> 0,
                  "_ItemProfit"=> 0,
                  "_ItemPrice"=> 0,
                  "_GrossProfit"=> 0

      );
    }

    // -----------------------------------------------------------------
    // transfer into QuickBooks

    // parse data

    $order_data = $data;

    $tx_ident = $this->gen_txid();

    $internal_order_id = $order_data["order_id"];
    $reference_number = $internal_order_id;

    $internal_customer_id = $order_data["customer_id"];
    $customer_fullname = $order_data["billing_address"]["firstname"].' '.$order_data["billing_address"]["lastname"]." ($internal_customer_id)";

    $po_number = false;
    if (isset($order_data["payment_details"]["po_number"])) {
      $po_number = $order_data["payment_details"]["po_number"];
    }

    $payment_method = $order_data["payment_methodTitle"];

    // create customer if not exists
    $data = array(
      "customer" => array(
        "variables" => array(
          'FullName' => $customer_fullname,
        ),
      ),
    );
    $this->create_customer($data);

    // create vendor if not exists
    $data = array(
      "vendor" => array(
        "variables" => array(
          'Name' => "Product From Website"
        ),
      ),
    );
    $this->create_supplier($data);

    // parse order items
    $lines = [];
    foreach($order_data["order_items"] as $order_item ) {
      $line=[];
      $line["ItemName"] = $order_item["SKU"];
      $line["Desc"] = $order_item["title"];
      $line["Quantity"] = $order_item["Ordered Qty"];
      $line["Amount"] = $order_item["Price"] * $order_item["Ordered Qty"];
      $lines[] = array("variables"=>$line);

      // create product if not exists
      $data = array(
        "product" => array(
          "variables" => array(
            'FullName' => $order_item["SKU"],
            "AssetAccountName" => "Inventory Asset",
            "IncomeAccountName" => "Ventes",
            "COGSAccountName" => "Cost of Goods Sold",
            "PreferredVendorName" => "Product From Website",
          ),
        ),
      );
      $this->create_product($data);

    }

    // process order

    if ($payment_method == "Purchase Order") {
      // invoice when PO
      $data = array(
        "invoice" => array(
          "variables" => array(
            'RefNumber' => $reference_number,
            'CustomerFullName' => $customer_fullname,
          ),
          "lines" => $lines,
        ),
      );

      $this->create_invoice($data);
    }

    if ($payment_method == "Check") {
      // invoice when Check
      $data = array(
        "invoice" => array(
          "variables" => array(
            'RefNumber' => $reference_number,
            'CustomerFullName' => $customer_fullname,
          ),
          "lines" => $lines,
        ),
      );

      $this->create_invoice($data);
    }

    if ($payment_method == "Credit card") {
      // sales receipt when Credit Card
      $data = array(
        "salesreceipt" => array(
          "variables" => array(
            'RefNumber' => $reference_number,
            'CustomerFullName' => $customer_fullname,
          ),
          "lines" => $lines,
        ),
      );

      $this->create_salesreceipt($data);
    }

    // $quoteObj = new Quote($quote_json);
    // $new_quote_id = $quoteObj->create();
    //
    // $sql = "UPDATE orders SET fk_order_id='$new_quote_id' WHERE id='$order_id'";
    // mysqli_query($this->link, $sql);

  }

  function gen_txid() {
    return md5(uniqid());
  }

  function propagate_payload_data($tx_ident, $data) {
    $result = mysqli_query($this->link, "SELECT * FROM quickbooks_payloads
      WHERE tx_ident = '" . mysqli_real_escape_string($this->link,$tx_ident) . "'");
    while ($row = mysqli_fetch_assoc($result)) {
      $payload = json_decode($row["payload"], true);
      $payload["data"] = $data;
      mysqli_query($this->link, "UPDATE quickbooks_payloads
        SET payload='".mysqli_real_escape_string($this->link, json_encode($payload))."'
        WHERE id='".mysqli_real_escape_string($this->link, $row["id"])."'
        ");
    }
  }

  function update_payload($id, $payload) {
    mysqli_query($this->link, "UPDATE quickbooks_payloads
      SET payload='".mysqli_real_escape_string($this->link, json_encode($payload))."'
      WHERE id='".mysqli_real_escape_string($this->link, $id)."'
    ");
  }

  function get_payload($id) {
    $arr = mysqli_fetch_assoc(mysqli_query($this->link, "SELECT * FROM quickbooks_payloads WHERE id = " . (int) $id));
    $payload = json_decode($arr["payload"], true);
    return $payload;
  }

  function insert_payload($tx_ident, $payload, $priority = false) {
    $payload["tx_ident"] = $tx_ident;
    mysqli_query($this->link, "INSERT INTO
      quickbooks_payloads(tx_ident, payload)
      VALUES(
        '".mysqli_real_escape_string($this->link, $tx_ident)."',
        '".mysqli_real_escape_string($this->link, json_encode($payload))."'
      )
    ");

    $id = mysqli_insert_id($this->link);

  	$queue = new QuickBooks_WebConnector_Queue($this->dsn);
    if ($priority) {
      $queue->enqueue($payload["action"], $id, $priority);
    } else {
      $queue->enqueue($payload["action"], $id);
    }
  }

  function test() {

    // $data = array(
    //   "vendor" => array(
    //     "variables" => array(
    //       'Name' => "Hyundai"
    //     ),
    //   ),
    // );
    // $this->create_supplier($data);

    // $data = array(
    //   "product" => array(
    //     "variables" => array(
    //       'FullName' => "Sonata",
    //       "AssetAccountName" => "Inventory Asset",
    //       "IncomeAccountName" => "Ventes",
    //       "COGSAccountName" => "Cost of Goods Sold",
    //       "PreferredVendorName" => "Hyundai",
    //     ),
    //   ),
    // );
    // $this->create_product($data);

    // $data = array(
    //   "service" => array(
    //     "variables" => array(
    //       'FullName' => "Sonatax",
    //       "Price" => "5.99",
    //       "AccountName" => "Sales",
    //       "PreferredVendorName" => "Hyundai",
    //     ),
    //   ),
    // );

    // $data = array(
    //   "contact" => array(
    //     "variables" => array(
    //       'ParentName' => "Pro Hockey School",
    //       'FullName' => "John Doe",
    //     ),
    //   ),
    // );
    // $this->create_contact($data);

    // $data = array(
    //   "customer" => array(
    //     "variables" => array(
    //       'FullName' => "Pro Hockey School",
    //     ),
    //   ),
    // );
    // $this->create_customer($data);

    // $data = array(
    //   "invoice" => array(
    //     "variables" => array(
    //       'RefNumber' => "ABC1234",
    //       'CustomerFullName' => "Pro Hockey School:John Doe",
    //     ),
    //     "lines" => array(
    //       array(
    //         "variables" => array(
    //           'ItemName' => "Sonata",
    //           'Desc' => "brand new Sonata",
    //           'Quantity' => 4,
    //           'Amount' => 9.99,
    //         ),
    //       ),
    //     ),
    //   ),
    // );
    //
    // $this->create_invoice($data);

    // $data = array(
    //   "salesreceipt" => array(
    //     "variables" => array(
    //       'RefNumber' => "ABC1234",
    //       'CustomerFullName' => "Pro Hockey School:John Doe",
    //     ),
    //     "lines" => array(
    //       array(
    //         "variables" => array(
    //           'ItemName' => "Sonata",
    //           'Desc' => "brand new Sonata",
    //           'Quantity' => 4,
    //           'Amount' => 9.99,
    //         ),
    //       ),
    //     ),
    //   ),
    // );
    //
    // $this->create_salesreceipt($data);

    // return $this->create_order_from_magento2([]);
    $order_id = 2;
    $order = get_order($order_id);
    $order["po_number"] = "234234";

    $this->send_to_quickbooks_from_erp("purchase_order", $order);
  }

  function router() {

    // $this->test();die();

    $action = false;
    $data = array();

    // --------------------------------------------------------
    // call authentication

    if (isset($_POST["API_KEY"])) {
      $api_key = $_POST["API_KEY"];
      if ($api_key != API_KEY) {
        return false;
      }
    } else { return false; }

    // --------------------------------------------------------
    // argument parsing

    if (isset($_POST["action"])) {
      $action = $_POST["action"];
    }

    if (isset($_POST["data"])) {
      try {
        $data = json_decode($_POST["data"], true);
      } catch (Exception $e) {
        return $this->output($e->getMessage(), "error");
      }
    }

    // --------------------------------------------------------
    // action routing

    if (
      $action == "CREATE_ORDER_FROM_MAGENTO2"
    ) {
      return $this->create_order_from_magento2($data);
    }

    return $this->output("action not found.", "error");
  }

}
