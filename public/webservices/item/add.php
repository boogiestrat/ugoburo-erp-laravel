<?php

    require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';


    /* CSRF protection */
    if (!user_level(3)) { die(print_r(json_encode(array('code' => 500, 'msg' => "Error - Access denied.")))); }


    if(!empty($_POST['name'])) {
        if ($id = create_item($_POST)) {
            $json = array('code' => 200, 'msg' => "Item created!");
            print_r(json_encode($json));
        } else {
            $json = array('code' => 500, 'msg' => "Error - Could not create item!");
            print_r(json_encode($json));
        }
    } else {
        $json = array('code' => 500, 'msg' => "Error - Missing argument!");
        print_r(json_encode($json));
    }
