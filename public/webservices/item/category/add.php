<?php

    require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';


    /* CSRF protection */
    if (!user_level(3)) { die(print_r(json_encode(array('code' => 500, 'msg' => "Error - Access denied.")))); }

    if(!empty($_POST['name'])) {
        if ($cat_id = create_item_category($_POST)) {
            $json = array('code' => 200, 'msg' => "Category created!", 'id' => $cat_id);
            print_r(json_encode($json));
        } else {
            $json = array('code' => 500, 'msg' => "Error - Could not create category!");
            print_r(json_encode($json));
        }
    } else {
        $json = array('code' => 500, 'msg' => "Error - Missing argument!");
        print_r(json_encode($json));
    }
