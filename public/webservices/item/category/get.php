<?php

    $debug = false;
    require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';

    ## TODO : Need global security validation ##

    if(!empty($_POST['id']) && is_numeric($_POST['id'])) {

        $item = get_item_category($_POST['id']);

        if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
            $item = json_encode($item, JSON_PRETTY_PRINT);
        else    
            $item = json_encode($item);

        print($item);

    } else {
        $json = array('code' => 500, 'msg' => "Missing parameter");
        print(json_encode($json)); 
    }