<?php   

    $debug = false;
    require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';

    ## TODO : Need global security validation ##

    $items = get_all_items_categories();

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $items = json_encode($items, JSON_PRETTY_PRINT);
    else    
        $items = json_encode($items);

    print($items);