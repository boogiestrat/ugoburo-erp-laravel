<?php

$debug = false;
require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';

global $db;

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_FILES['csv'])) {
  echo json_encode(['done' => false]);
  exit;
}

$csv = array_map('str_getcsv', file(realpath($_FILES["csv"]["tmp_name"])));

array_walk($csv, function(&$a) use ($csv) {
  $a = array_combine($csv[0], $a);
});

echo json_encode(['done' => true, 'csv' => $csv]);
exit;
