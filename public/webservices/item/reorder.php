<?php

    require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

    ## TODO : Need global security validation ##

    if(!empty($_POST['id']) && is_numeric($_POST['id']) 
        && !empty($_POST['order']) && is_numeric($_POST['order'])
        && !empty($_POST['new_order']) && is_numeric($_POST['new_order'])) {

        reorder_item($_POST['id'], $_POST['order'], $_POST['new_order']);
        $json = array('code' => 200, 'msg' => "Item déplacé");
        print(json_encode($json)); 
        
    }