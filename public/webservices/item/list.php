<?php   

    $debug = false;
    require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

    ## TODO : Need global security validation ##

    $items = get_all_items();

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $items = json_encode($items, JSON_PRETTY_PRINT);
    else    
        $items = json_encode($items);

    print($items);