<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

## TODO : Need global security validation ##
if (!empty($_REQUEST['q'])) {

    $items = search_items($_REQUEST['q']);

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $items = json_encode($items, JSON_PRETTY_PRINT);
    else
        $items = json_encode($items);

    print($items);

} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
