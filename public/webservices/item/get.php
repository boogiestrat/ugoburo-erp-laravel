<?php

    $debug = false;
    require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

    ## TODO : Need global security validation ##

    if(!empty($_POST['id']) && is_numeric($_POST['id'])) {

        $item = get_item($_POST['id']);

        if ($item['fk_field_type_id'] == 4) {
            // Load children
            $item["children"] = get_item_child($_POST['id']);
        }
        
        $item["tasks"] = get_item_tasks_by_item_id($_POST['id']);

        if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
            $item = json_encode($item, JSON_PRETTY_PRINT);
        else    
            $item = json_encode($item);

        print($item);

    } else {
        $json = array('code' => 500, 'msg' => "Missing parameter");
        print(json_encode($json)); 
    }