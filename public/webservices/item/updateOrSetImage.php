<?php
  require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  ### PICTURE ###
  if ($_FILES) {
    $image = new Bulletproof\Image($_FILES);
    $upload_folder = realpath(ROOT .'web/assets/upload/item/');
    $image->setLocation($upload_folder)
        ->setSize(10, 10000000);

    if($image['picture']){
      $upload = $image->upload();

      if($upload){
        $cropped_image = new \Gumlet\ImageResize($image->getFullPath());
        $cropped_image->crop(1024, 1024);
        $cropped_image->save($image->getFullPath());

        $picture = $image->getName() . '.' . $image->getMime();
      }  else{
        echo json_encode(['done' => false]);
        exit;
      }
    }
  }

  echo json_encode(['done' => true, 'path' => PATH .'assets/upload/item/' . $picture]);
  exit;
