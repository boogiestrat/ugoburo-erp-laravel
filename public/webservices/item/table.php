<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

/* CSRF protection */
if (!user_level(3)) { die(print_r(json_encode(array('code' => 500, 'msg' => "Error - Access denied.")))); }

$table = 'item';
$primaryKey = 'id';
$iTable = 0;
$columns = array(
    array( 'db' => 'sku', 'dt' => $iTable++, 'field' => 'sku', 'as' => 'sku' ),
    array( 'db' => 'title', 'dt' => $iTable++, 'formatter' => function($d, $row) {
        $output = '';

        if ($row['picture'] != '') {
            $output .= '<img class="img-circle mr-3" src="' . $_ENV['PATH_UPLOAD'] . 'item/' . $row['picture'] . '" alt="' . $d . '" width="50">';
        }

        return $output . ' ' . $d;
    }, 'field' => 'title, picture', 'as' => 'title' ),
    array( 'db' => 'price', 'dt' => $iTable++, 'formatter' => function($d) {
        return currency_format($d);
    }, 'field' => 'price', 'as' => 'price' ),
    array( 'db' => 'last_modification', 'dt' => $iTable++, 'field' => 'last_modification', 'as' => 'last_modification' ),
    array( 'db' => 'id', 'dt' => $iTable++, 'formatter' => function($d, $row) {
        $html = '<div align="right">';
        $html .= '<a class="font-20 modify-btn" href="javascript:;" data-id="' . $row['id'] . '" data-toggle="modal" data-target="#item-modal" data-backdrop="static"><i class="ti-pencil"></i></a>';
        $html .= ' ';
        $html .= '<a class="font-20 delete-btn" href="javascript:;" data-id="' . $row['id'] . '" data-label="' . $row['title'] . '"><i class="ti-trash"></i></a>';
        $html .= '</div>';

        return $html;
    }, 'field' => 'id', 'as' => 'id' ),
    array( 'db' => 'picture', 'field' => 'picture', 'as' => 'picture' )
);

echo json_encode(
	SSP::simple($_GET, SSP::mysql_connection(), $table, $primaryKey, $columns)
);
