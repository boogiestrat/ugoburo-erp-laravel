<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/pdf');

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

if (!isset($_POST['html'])) {
  echo json_encode(['error' => 'bad data']);
  exit;
}

$mpdf = new \Mpdf\Mpdf([
  'mode' => 'utf-8',
  'format' => 'Legal',
  'orientation' => 'P',
  'tempDir' => sys_get_temp_dir()
]);

$mpdf->SetHTMLFooter('<div><strong>Customer service :</strong> 1 855 846-2876 - Monday to Friday : 8am - 6pm | <strong>Email : </strong>service@ugoburo.ca <strong> Page : {PAGENO} / {nbpg}</strong></div>');

$mpdf->WriteHTML($_POST['html']);

$quote_pdf_data = $mpdf->Output('', 'S');

echo $quote_pdf_data;
exit;
