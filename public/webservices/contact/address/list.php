<?php
$debug = false;
require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

## TODO : Need global security validation ##

if (!isset($_POST['id'])) {
  print(json_encode(['done' => false, 'error' => 'invalid params']));
  exit;
}

$contacts = get_contact_addresses($_POST['id']);

if ($contacts) {
  if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On') {
    $contacts = json_encode($contacts, JSON_PRETTY_PRINT);
  } else {
    $contacts = json_encode($contacts);
  }

  print($contacts);
} else {
  print(json_encode([]));
  exit;
}

