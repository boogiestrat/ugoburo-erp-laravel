<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##

if(!empty($_POST['id']) && is_numeric($_POST['id'])) {

    $contact = get_contact($_POST['id']);

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $contact = json_encode($contact, JSON_PRETTY_PRINT);
    else
        $contact = json_encode($contact);

    print($contact);

} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
