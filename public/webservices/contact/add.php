<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

## TODO : Need global security validation ##

if (!empty($_POST['last_name']) && !empty($_POST['first_name'])) {
  $ret = create_contact($_POST);
// print_r($ret);die();
    if (is_numeric($ret)) {
        $json = array('code' => 200, 'msg' => "Contact created!", 'newId' => $ret);
        echo json_encode($json);
        exit;
      } else {
        $json = array('code' => 500, 'msg' => $ret);
        echo json_encode($json);
        exit;
      }
} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    echo json_encode($json);
}

exit();
