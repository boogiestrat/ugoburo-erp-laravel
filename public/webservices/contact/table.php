<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

$table = 'contact';
$primaryKey = 'id';
$iTable = 0;
$columns = array(
    array( 'db' => 'contact.first_name', 'dt' => $iTable++, 'field' => 'first_name', 'as' => 'first_name' ),
    array( 'db' => 'contact.last_name', 'dt' => $iTable++, 'field' => 'last_name', 'as' => 'last_name' ),
    array( 'db' => 'contact.email', 'dt' => $iTable++, 'formatter' => function($d, $row) {
        return "<a href=\"mailto:'{$row['email']}'\"><i class=\"ti-email\"></i></a>";
    }, 'field' => 'email', 'as' => 'email' ),
    array( 'db' => 'contact.phone', 'dt' => $iTable++, 'formatter' => function($d, $row) {
        return "<a href=\"phone:'{$row['phone']}'\">{$row['phone']}</a>";
    }, 'field' => 'phone', 'as' => 'phone' ),
    array( 'db' => 'contact.cell', 'dt' => $iTable++, 'formatter' => function($d, $row) {
        return "<a href=\"phone:'{$row['cell']}'\">{$row['cell']}</a>";
    }, 'field' => 'cell', 'as' => 'cell' ),
    array( 'db' => 'CONCAT(client.entreprise)', 'dt' => $iTable++, 'field' => 'client_last_name', 'as' => 'client_last_name' ),
    array( 'db' => 'contact.id', 'dt' => $iTable++, 'formatter' => function($d, $row) {
        $html = '<div class="text-right">';
        $html .= '<a class="font-20 modify-btn" href="javascript:;" data-id="' . $row['options'] . '" data-toggle="modal" data-target="#contact-modal" data-backdrop="static"><i class="ti-pencil"></i></a>';
        $html .= ' ';
        $html .= '<a class="font-20 delete-btn" href="javascript:;" data-id="' . $row['options'] . '" data-label="' . $row['first_name'] . ' ' . $row['last_name'] . '"><i class="ti-trash"></i></a>';
        $html .= ' </div> ';

        return $html;
    }, 'field' => 'options', 'as' => 'options' ),
);


$employeeLevel = get_current_employee()['fk_user_level_id'];
$employeeId = get_current_employee()['id'];
// $showMine = $employeeLevel !== EmployeeLevel::Administrateur || ($employeeLevel === EmployeeLevel::Administrateur && $_GET['employeesOnly'] != 'false');
// $showMine = $employeeLevel === EmployeeLevel::RepresentantExterne;
// $showMine = true;
// if ($employeeLevel > 1) {
//   $showMine = false;
// }
if (user_level(1)) {
  $joinQuery = " FROM contact
  LEFT JOIN client ON client.id = contact.fk_client_id
  LEFT JOIN employee_client_permissions ON employee_client_permissions.fk_client_id = client.id
  WHERE (contact.fk_created_by_id = $employeeId)
  OR (employee_client_permissions.fk_employee_id = '$employeeId') ";
  //$joinQuery = " FROM client LEFT JOIN address ON (address.fk_client_id = client.id AND (address.is_default_facturation = 1 OR address.is_default_shipping = 1)) LEFT JOIN employee_clients ON client.id = employee_clients.fk_client_id LEFT JOIN employee_client_permissions ON employee_client_permissions.fk_client_id = client.id WHERE (employee_clients.fk_employee_id = $employeeId OR employee_client_permissions.fk_employee_id = $employeeId)";
} else {
  $joinQuery = " FROM contact LEFT JOIN client ON client.id = contact.fk_client_id";
  //$joinQuery = " FROM client LEFT JOIN address ON (address.fk_client_id = client.id AND (address.is_default_facturation = 1 OR address.is_default_shipping = 1)) LEFT JOIN employee_clients ON employee_clients.fk_client_id = client.id";
}

echo json_encode(
	SSP::simple($_GET, SSP::mysql_connection(), $table, $primaryKey, $columns, $joinQuery)
);
