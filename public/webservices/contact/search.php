<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##
if (isset($_REQUEST['qu'])) {
  $contact = search_unassigned_contact($_REQUEST['qu']);

  if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
      $contact = json_encode($contact, JSON_PRETTY_PRINT);
  else
      $contact = json_encode($contact);

  print($contact);
} else {
  if (!empty($_REQUEST['q'])) {

      $contact = search_contact($_REQUEST['q']);

      if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
          $contact = json_encode($contact, JSON_PRETTY_PRINT);
      else
          $contact = json_encode($contact);

      print($contact);

  } else {
      $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
      print_r(json_encode($json));
  }
}
