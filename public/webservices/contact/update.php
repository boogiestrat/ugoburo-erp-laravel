<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##

if(!empty($_POST['id']) && is_numeric($_POST['id'])) {
  $result = update_contact($_POST);
  // print_r($result);die();
    if (is_bool($result)) {
        $json = array('code' => 200, 'msg' => "Contact updated!");
        print_r(json_encode($json));
    } else {
        $json = array('code' => 500, 'msg' => $result);
        print_r(json_encode($json));
    }
} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
