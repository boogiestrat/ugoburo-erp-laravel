<?php

error_reporting(E_ALL);
ini_set('display_errors',1);


require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

$quoteNumber = @$_REQUEST['quote_id'];
$quoteId = @$_REQUEST['quote-email-id'];
$emails = @$_REQUEST['quote-email-recipients'];
$isDeposit = @$_REQUEST['quote-email-deposit'];
$depositType = @$_REQUEST['quote-email-deposit-type'];
$depositValue = @$_REQUEST['quote-email-deposit-value'];
$includePdf = @$_REQUEST['quote-email-pdf'];
$includeWeblink = @$_REQUEST['quote-email-weblink'];
$language = @$_REQUEST['quote-email-language'];


if (!$quoteId || !$emails) {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    echo json_encode($json);
    exit();
}

$emails = explode(',',$emails);

foreach($emails as $email){
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $json = array('code' => 500, 'msg' => "Error - Invalid Email!");
        print_r(json_encode($json));
        exit();
    }
}


$quote = get_quote_by_number($quoteNumber);
// print_r($quote);
// $quote = get_quote($quoteId);
$quote_data = json_decode($quote['data']);
$config = (array)json_decode($quote['data_config']);
$data_items = $quote['data_items'];
$client = get_client($quote['fk_client_id']);

$employee = unserialize($_SESSION['employee']);

if($depositType=='%'){
    $depositValue = number_format($quote_data->total * ($depositValue/100),2,'.','');
}


$paymentUrl = get_quote_payment_url($quoteNumber,$depositValue,$emails,$language);

echo json_encode(['url'=> $paymentUrl]);
