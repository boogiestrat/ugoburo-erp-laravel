<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##

if(!empty($_POST['id']) && is_numeric($_POST['id']) && !empty($_POST['new_modified_employee_id']) && is_numeric($_POST['new_modified_employee_id'])) {
    if (!update_quote_modified_employee($_POST['id'], $_POST['new_modified_employee_id'])) {
        $json = array('code' => 500, 'msg' => "Can't update selected quote.");
        print(json_encode($json));
        exit();
    }

    $json = array('code' => 200, 'msg' => "Quote employee updated!");
    print_r(json_encode($json));
    exit();
} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
