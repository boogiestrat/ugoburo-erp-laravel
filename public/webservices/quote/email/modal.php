<?php

require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

echo json_encode(["html"=>get_html_template_part([],'modal/quote-email-form.php')]);
exit;