<?php

error_reporting(E_ALL);
ini_set('display_errors',1);


require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';

$quoteId = @$_REQUEST['quote-email-id'];
$emails = @$_REQUEST['quote-email-recipients'];
$isDeposit = @$_REQUEST['quote-email-deposit'];
$depositType = @$_REQUEST['quote-email-deposit-type'];
$depositValue = @$_REQUEST['quote-email-deposit-value'];
$includePdf = @$_REQUEST['quote-email-pdf'];
$includeWeblink = @$_REQUEST['quote-email-weblink'];
$language = @$_REQUEST['quote-email-language'];


switchLocale($language);


if (!$quoteId || !$emails) {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    echo json_encode($json);
    exit();
}

$emails = explode(',',$emails);

foreach($emails as $email){
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $json = array('code' => 500, 'msg' => "Error - Invalid Email!");
        print_r(json_encode($json));
        exit();
    }
}


$quote = get_quote($quoteId);
$quote_data = json_decode($quote['data']);
$config = (array)json_decode($quote['data_config']);
$data_items = $quote['data_items'];
$client = get_client($quote['fk_client_id']);

$employee = unserialize($_SESSION['employee']);

if($depositType=='%'){
    $depositValue = number_format($quote_data->total * ($depositValue/100),2,'.','');
}


$paymentUrl = get_quote_payment_url($quote["number"],$depositValue,$emails,$language);


$mail = new \SendGrid\Mail\Mail();
$from = getenv('DEFAULT_CONTACT_EMAIL');
$fromName = getenv('CLIENT_NAME');
$phone = getenv('CLIENT_PHONE');
$subject = __('quote-email_subject',getenv("CLIENT_NAME"),$quote_data->number);

ob_start();
include('send-template.php');
$template = ob_get_clean();


$mail->setFrom($from, $fromName);
$mail->setSubject($subject);
$mail->addTo($emails[0]);
foreach ($emails as $emailKey=> $email){
    if($emailKey) $mail->addCc($email);
}

$mail->addContent("text/plain", strip_tags($template));
$mail->addContent("text/html", $template);

if($includePdf) {

// Create the attachment with your data
    $pdfName = 'quote-' . $quote_data->number;
    $mail->addAttachment(
        get_quote_pdf($quoteId,\Mpdf\Output\Destination::STRING_RETURN,$quote_data->number.'.pdf'),
        "application/pdf",
        url_safe($pdfName) . '.pdf',
        "attachment"
    );
}

$sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

try {
    $response = $sendgrid->send($mail);
    update_quote_status($quoteId,Quote::STATUS_SENT);
    $json = array('code' => $response->statusCode(), 'msg' => $response->body(). "\n");
    echo json_encode($json);
    exit();
} catch (Exception $e) {
    $json = array('code' => 500, 'msg' => 'Caught exception: '.  $e->getMessage(). "\n");
    echo json_encode($json);
    exit();
}
