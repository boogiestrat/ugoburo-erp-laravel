<?php

$email_content = '
<p style="margin-top: 0; margin-bottom: 10px;">'.  __('quote-email_hello',$quote_data->billTo->first_name,$quote_data->billTo->last_name) . '</p>

<p style="margin-top: 0; margin-bottom: 10px;">'.  __('quote-email_quote',$quote_data->number) . '</p>



';
if ($includeWeblink) {
  $email_content .= '
  <p style="margin-top: 0; margin-bottom: 10px;">'.  __('quote-email_link') . '</p>
  <p style="margin-top: 0; margin-bottom: 10px;"><a href=\''.$paymentUrl.'\' style=\'font-size:16px;\'>#'.$quote_data->number.'</a>'.  __('quote-email_payment-link') . '</p>
  <p style="margin-top: 0; margin-bottom: 10px;"><small>'. $paymentUrl . '</small></p>
  ';
}


$email_content .= '

<p style="margin-top: 0; margin-bottom: 10px;">'.  __('quote-email_contact',$from,$from,getenv('CLIENT_PHONE')) . '</p>

<p style="margin-top: 0; margin-bottom: 10px;">'.  __('quote-email_thanks') . '</p>

';

include(APP_ROOT . 'includes/html/email/email-body_en.php');

/*
    <p style="margin-top: 0; margin-bottom: 10px;">
        TPS 839049913<br/>
        TVQ 1217187512
    </p>
*/;
