<?php

if(!$quote_id) {

    if (empty($_GET['id']) || !is_numeric($_GET['id'])) {
        header('Location: ' . PATH);
        exit();
    }

    $quote_id = $_GET['id'];

    if (!$quote = get_quote($quote_id)) {
        print "Erreur - Numéro de soumission inexistant.";
        exit();
    }
}

$isSoumission = $quote['fk_status_id'] == '1';

### Get Quote DATA ###
$config = (array)json_decode($quote['data_config']);
$data_items = $quote['data_items'];
$data = json_decode($quote['data']);
$client = get_client($quote['fk_client_id']);

$page_title = 'Soumission : ' . $quote['type'] . ' - '. $quote['last_name'] . ' - ' . format_locale_date($quote['creation']);

// Buffer the following html with PHP so we can store it to a variable later
ob_start();

?>
<!DOCTYPE html>
<html lang='fr-ca'>

<head>

    <title><?=$page_title?></title>
    <meta charset="UTF-8">
    <meta name="author" content="Complexe funéraire Desnoyers et Fils">
    <link rel="stylesheet" href="<?=PATH?>assets/css/print-pdf.css">

</head>

<body>

    <table class="quote-head">
        <tr>
            <td>
                <h1><?php if ($isSoumission):?>Soumission<?php else: ?>Contrat<?php endif ?></h1>
                <h4 class="text-muted">Date : <?=format_locale_date($quote['creation'])?></h4>
            </td>
            <td class="text-right">
                <img class="logo" src="<?=PATH?>assets/img/desnoyers-logo-pdf.png" />
            </td>
            <td class="address">
                <strong>Complexe Funéraire Desnoyers</strong> <br>
                1981, Périgny <br>
                Chambly, Québec <br>
                J3L 4C3 <br>
                Tél. 450.658.8551 <br>
                Fax. 450.658.1397 <br>
                <a href="mailto:sympathie@maison-desnoyers.com">sympathie@maison-desnoyers.com</a>

            </td>
        </tr>
    </table>

    <table class="quote-info">
        <tr>
            <th class="grey-border text-left">
                <h3 class="panel-title">Information client</h3>
            </th>
            <th class="grey-border text-left">
                <h3 class="panel-title">Information supplémentaire</h3>
            </th>
        </tr>
        <tr>
            <td class="grey-border" style="vertical-align:text-top;">
                <table class="client-info">
                    <tr>
                        <th>Nom</th>
                        <td><?=$client['first_name']?> <?=$client['last_name']?></td>
                    </tr>
                    <tr>
                        <th>Adresse</th>
                        <td>
                            <?=$client['address']?> <br>
                            <?=ucfirst($client['city'])?> <br>
                            <?=($client['province']=='Extérieur du Canada'?'':ucfirst($client['province']).', ')?>
                            <?=ucfirst($client['country'])?> <br>
                            <?=strtoupper($client['postal_code'])?>
                        </td>
                    </tr>
		<?php
                        if (!empty($client['tel'])) {
                ?>
                    <tr>
                        <th>Tél.</th>
                        <td><?=$client['tel']?></td>
                    </tr>
		<?php
                        }
                ?>
		<?php
                        if (!empty($client['mobile'])) {
                ?>
                    <tr>
                        <th>Cell</th>
                        <td><?=$client['mobile']?></td>
                    </tr>
		<?php
                        }
                ?>
		<?php
                        if (!empty($client['email'])) {
                ?>
                    <tr>
                        <th>Courriel</th>
                        <td><?=$client['email']?></td>
                    </tr>
		<?php
                        }
                ?>
                </table>
            </td>
            <td class="grey-border">
                <table class="client-info">
                <?php
                        if (!empty($quote['type'])) {
                ?>
                    <tr>
                        <th>Type</th>
                        <td><?=$quote['type']?></td>
                    </tr>
                <?php
                        }
                ?>
                    <tr>
                        <th>Succession de</th>
                        <td><?=$quote['beneficiary_first_name']?> <?=$quote['beneficiary_last_name']?></td>
                    </tr>
                <?php
                        if (!empty($quote['payment_type'])) {
                ?>
                    <tr>
                        <th>Mode de paiement</th>
                        <td><?=$quote['payment_type']?></td>
                    </tr>
                <?php
                        }
                ?>
                    <tr>
                        <th>Conseiller</th>
                        <td><?=$quote['created_by']?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="quote-body">
        <thead>
            <tr>
                <th colspan="4" class="text-left"><h3>Services / Produits</h3></th>
            </tr>
            <tr>
                <th class="text-left">Articles / Détails</th>
                <th class="text-right">Coût</th>
                <th class="text-right">Unité(s)</th>
                <th class="text-right">Total</th>
            </tr>
        </thead>
        <tbody>
        <?php
            //List Service prices
            foreach ($data as $key => $item) {

                $id = $item->id;

                //Initialize data
                $note = $item->description;
                $unit_price = (float)str_replace(',', '.', $item->price);
                $quantity = (float)str_replace(',', '.', $item->quantity);
                $total_price = (float)str_replace(',', '.', $item->total);

        ?>
            <tr>
                <td>
                    <?=$item->name?>
                <?php if (!empty($note)) { ?><br>
                    <span class="text-muted small"><?=nl2br($note)?></span>
                <?php } ?>
                </td>
                <td class="text-right"><?=currency_format($unit_price)?></td>
                <td class="text-right"><?=$quantity?></td>
                <td class="text-right"><?=currency_format($total_price)?></td>
            </tr>

        <?php

            }

        ?>

        </tbody>
    </table>

    <table class="quote-total">
        <thead>
            <tr>
                <th class="text-center col-xs-1">Sous-total</th>
            <?php if (!empty($quote['discount']) && $quote['discount']>0) { ?>
                <th class="text-center col-xs-1">Escompte</th>
            <?php } ?>
                <th class="text-center col-xs-1">TPS</th>
                <th class="text-center col-xs-1">TVQ</th>
            <?php if (!empty($config['deposit']) && $config['deposit']>0) { ?>
                <th class="text-center col-xs-1">Acompte</th>
                <th class="text-center col-xs-1">TOTAL À PAYER</th>
            <?php } else { ?>
                <th class="text-center col-xs-1">TOTAL</th>
            <?php } ?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center"><?=currency_format($quote['sub_total'])?></td>
            <?php if (!empty($quote['discount']) && $quote['discount']>0) { ?>
                <td class="text-center"><?=currency_format($quote['discount'])?></td>
            <?php } ?>
                <td class="text-center"><?=currency_format($quote['tps'])?></td>
                <td class="text-center"><?=currency_format($quote['tvq'])?></td>
            <?php if (!empty($config['deposit']) && $config['deposit']>0) { ?>
                <td class="text-center"><?=currency_format($config['deposit'])?></td>
                <td class="text-center"><?=currency_format($config['balance'])?></td>
            <?php } ?>
                <td class="text-center total"><strong><?=currency_format($quote['total'])?></strong></td>
            </tr>
        </tbody>
    </table>

    <?php if (!$isSoumission):?>

        <div class="quote-terms">

            <?=$config['contract_terms']?>

            <p>Fait lu, et signé à ____________________________________________, le ____________________________________________ <?=date('Y')?></p>

            <p>Nom : ____________________________________________</p>

            <p>Signature : ____________________________________________</p>

            <p>Signature du conseiller : ____________________________________________</p>

        </div>

    <?php endif ?>

    <div class="quote-footer">
        <strong>Complexe funéraire Desnoyers et Fils &copy;<?=date('Y')?></strong>
    </div>

</body>

</html>

<?php

    // Now collect the output buffer into a variable
    $quote_html = ob_get_contents();
    ob_end_clean();
