<?php

if(!@$quote_id) {

    if (empty($_GET['id']) || !is_numeric($_GET['id'])) {
        header('Location: ' . PATH);
        exit();
    }

    $quote_id = $_GET['id'];

    if (!$quote = get_quote_by_number($quote_id)) {
        print "Erreur - Numéro de soumission inexistant.";
        exit();
    }
}

$isSoumission = true;

### Get Quote DATA ###
$config = (array)json_decode($quote['data_config']);
$data_items = $quote['data_items'];
$data = json_decode($quote['data']);
$client = get_client($quote['fk_client_id']);

//$page_title = 'Soumission : ' . $quote['number'] . ' - ' . format_locale_date($quote['creation']);
$page_title = __('quote-pdf_page-title',$quote['number'],format_locale_date($quote['creation']));

// Total vars
$discount = $data->discountPrice;
$subTotal = $data->subTotal;
$taxes = $data->billTo->taxes;

$TPS = $data->TPS;
$TVQ = $data->TVQ;
$deposit = $data->depot;
$total = $data->total;

$showItemImage = false;
// echo '<pre>';print_r($data->billTo->client->title);die();

// Buffer the following html with PHP so we can store it to a variable later
ob_start();

?>
<!DOCTYPE html>
<html lang='fr-ca'>
<head>
    <title><?=$page_title?></title>
    <meta charset="UTF-8">
    <meta name="author" content="Ugoburo">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?=PATH?>assets/css/print-pdf.css">
    <style>
    body {
      font-family: 'Poppins', sans-serif;
      margin:0px;
    }
    </style>
    <?php /*<link rel="stylesheet" href="<?=PATH?>assets/css/main.css">*/ ?>
</head>

<body leftmargin="0" rightmargin="0">
    <table class="quote-head">
        <tr>
            <td class="text-left">
                <img class="logo" src="<?=getenv('BASE_URL') ?>assets/img/ugoburo-logo-pdf.png" />
                <br>&nbsp;
                <p>
                    3141 Taschereau Blvd #101<br>
                    Greenfield Park, QC. J4V 2H2<br>
                    Tel: 1 855-846-2876 <br>
                </p>
                <br>&nbsp;
            </td>
            <td align="right">
                <h1><?php if ($isSoumission):?><?=__('general_quote')?> <?=$data->number;?> <?php else: ?><?=__('general_contract')?><?php endif ?></h1>
                  <br/><h3><?php echo $quote["title"]; ?></h3>
                <div class="text-muted">Date: <?=format_locale_date($data->date)?></div>
                <div class="text-muted"><?= __('quote-pdf_valid-until',format_locale_date($data->validUntil))?></div>
                <div class="text-muted"><?= __('quote-pdf_specialist-advisor', $data->specialist->first_name.' '.$data->specialist->last_name)?></div>
                <?php if (!empty($data->specialist->email)) { ?><div class="text-muted"><?= __('quote-pdf_specialist-advisor-email', $data->specialist->email)?><br>&nbsp;</div> <?php } ?>
            </td>
        </tr>
    </table>
    <?php $address_height = "125"; ?>
    <table class="quote-info" height="<?php echo $address_height; ?>">
        <tr height="<?php echo $address_height; ?>">
            <td width="50%" height="<?php echo $address_height; ?>">
                <table class="client-info" height="<?php echo $address_height; ?>">
                    <tr height="<?php echo $address_height; ?>">
                        <td height="<?php echo $address_height; ?>">

                            <h3 class="panel-title"><?=__('address_billing')?></h3>
                            <?php if (isset($data->billTo->client->title) && $data->billTo->client->client_type_enum != 1) {
                              echo $data->billTo->client->title.'<br/>';
                            }?>
                            <?=$data->billTo->contact->first_name?> <?=$data->billTo->contact->last_name?><br>
                            <?=$data->billTo->address?$data->billTo->address->address."<br>":"" ?>
                            <?php  if (!empty($data->billTo->contact->phone)) {?>
                                <?=__('address_phone-abrv')?>: <?=$data->billTo->contact->phone?>
                            <?php } ?>

                            <?php  if (!empty($data->billTo->contact->cell)) {?>
                                <?=__('address_cell-abrv')?>.: <?=$data->billTo->contact->cell?>
                            <?php } ?>


                        </td>
                    </tr>
                </table>
            </td>
            <td width="50%" height="<?php echo $address_height; ?>">
                <table class="client-info" height="<?php echo $address_height; ?>">
                    <tr height="<?php echo $address_height; ?>">
                        <td height="<?php echo $address_height; ?>">
                            <h3 class="panel-title"><?=__('address_shipping')?></h3>
                            <?php if (isset($data->shipTo->client->title) && $data->shipTo->client->client_type_enum != 1) {
                              echo $data->shipTo->client->title.'<br/>';
                            }?>

                            <?=$data->shipTo->contact->first_name?> <?=$data->shipTo->contact->last_name?><br>
                            <?=$data->shipTo->address?$data->shipTo->address->address."<br>":"" ?>
                            <?php  if (!empty($data->shipTo->contact->phone)) {?>
                                <?=__('address_phone-abrv')?>: <?=$data->shipTo->contact->phone?>
                            <?php } ?>

                            <?php  if (!empty($data->shipTo->contact->cell)) {?>
                                <?=__('address_cell-abrv')?>.: <?=$data->shipTo->contact->cell?>
                            <?php } ?>


                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table cellpadding="5">
        <tr>
            <td width="50%" >
                <?php if($data->additionalNote): ?>
                    <div id="additionnal-info">
                        <h3><?=__('quote_additional-informations')?></h3><br>
                        <p>
                            <?=nl2br($data->additionalNote);?>
                        </p><br>
                    </div>
                <?php endif; ?>
                <?php if($data->livraisonNote): ?>
                <div id="installation-info">
                    <h3><?=__('quote_installation-informations')?></h3><br>
                    <p>
                        <?=nl2br($data->livraisonNote);?>
                    </p><br>
                </div>
                <?php endif; ?>
                <?php if($data->additionalNoteInterne && $isAdmin): ?>
                    <div id="installation-info">
                        <h3><?=__('quote_additional-informations')?></h3><br>
                        <p>
                            <?=nl2br($data->additionalNoteInterne);?>
                        </p><br>
                    </div>
                <?php endif; ?>
                <?php if($data->livraisonNoteInterne && $isAdmin): ?>
                    <div id="installation-info">
                        <h3><?=__('quote_service-shipping-admin')?></h3><br>
                        <p>
                            <?=nl2br($data->livraisonNoteInterne);?>
                        </p><br>
                    </div>
                <?php endif; ?>
            </td>


            <td width="50%">
                <?php if($summary = get_quote_sections($quote)) :?>
                <table class="quote-summary" cellspacing="0" cellpadding="5">
                    <tr>
                        <th colspan="2" align="left">
                            <h3><?=__('quote_summary')?></h3>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-left name"><?=__('quote_section-name')?></th>
                        <th align="right" class="name"><?=__('quote_price')?></th>
                    </tr>

                    <?php foreach ($summary as $section): ?>
                    <tr>
                        <td><?=mb_strtoupper($section->title)?></td>
                        <td align="right"><?=$section->total?></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
                <?php endif ?>
            </td>
        </tr>
    </table>

    <table class="quote-body" cellspacing="0" cellpadding="8">
        <tbody>
        <?php foreach ($data->sections as $sectionKey => $section):

            $section_subtotal = 0;

            ?>
            <tr>
                <th class="section-title" colspan="5" align="left" class="title"><br><?=mb_strtoupper($section->title)?><hr/></th>
            </tr>
            <tr>
               <?php if($showItemImage):?> <th width="10%" align="left">IMAGE</th> <?php endif; ?>
                <th align="left">CODE</th>
                <th width="60%" align="left">DESCRIPTION</th>
                <th width="8%" align="right"><?=__('quote_item-qty')?></th>
                <th width="12%" align="right"><?=__('quote_item-priceunit')?></th>
                <th width="12%" align="right">EXT</th>
            </tr>

           <?php foreach ($section->items as $quote_item):


                $id = $quote_item->id;
                $profitMethod = $quote_item->profitMethod;
                $code = $quote_item->SKU;
                $description = "<strong>{$quote_item->title}</strong><br>{$quote_item->description}";
                $quantity = (float)str_replace(',', '.', $quote_item->quantity);

                $cost = $quote_item->_CostPrice;
                $priceParameterValue = $quote_item->priceParameterValue;
                if ($profitMethod == 0) { // Fixed Price ($)
                  $cost = $priceParameterValue;
                }
                if ($profitMethod == 1) { // Cost + %
                  $cost = $quote_item->_CostPrice;
                  $cost = $cost + ( $cost * ( $priceParameterValue / 100 ) ) ;
                }
                if ($profitMethod == 2) { // Gross Profit (%)
                  $cost = $quote_item->_CostPrice;
                  $cost = $cost / ( 1 - ( $priceParameterValue / 100 ) ) ;
                }
                if ($profitMethod == 3) { // Net Profit ($)
                  $cost = $quote_item->_CostPrice + $priceParameterValue;
                }
                if ($profitMethod == 4) { // List Minus (%)
                  $cost = $quote_item->price; //Use List Price
                  $cost = $cost - ( $cost * ( $priceParameterValue / 100 ) ) ;
                }
                $cost_total = $cost * $quantity;
                // $unit_price = $quote_item->_ItemPrice;
                // $unit_total = $unit_price * $quantity;
                $unit_total = $cost * $quantity;
                $section_subtotal+=$unit_total;

                if($unit_total==0)
                    $gp = 'n/a';
                else
                    $gp = number_format($quote_item->_GrossProfit, 2);

                $ext = $quantity * $cost;
                $expedition_price = $quote_item->_LineShipping;
                $expedition_price = $quote_item->_LineShipping;
                $expeditionPriceMethod = $quote_item->expeditionPriceMethod;
                $expeditionPercent = number_format($quote_item->expeditionPercent, 2);
                $expeditionPrice = number_format($quote_item->expeditionPrice, 2);
// print_r($quote_item);
                ?>
                <tr>
                    <?php if($showItemImage):?>
                        <td class="no-border" align="left">
                            <?php if(@$quote_item->pictureURL):

                                $imagePath = str_replace(getenv('BASE_URL'),getenv('BASE_DIR'),$quote_item->pictureURL);
                                ?>

                                <img width="50px" src="<?=$imagePath?>" />
                            <?php endif; ?>
                        </td>
                    <?php endif; ?>
                    <td  class="no-border" align="left"><?=$quote_item->SKU?></td>
                    <td class="no-border" align="left"><?=$description?></td>
                    <td  class="no-border" align="right"><?=$quantity?></td>
                    <td  class="no-border" align="right"><?=currency_format($cost);?></td>
                    <td  class="no-border" align="right"><?=currency_format($unit_total);?></td>
                </tr>
                <?php if($isAdmin): ?>
                    <tr>
                        <td class="quote-item-admin" colspan="<?=$showItemImage?6:5?>">
                              <b><?=__('quote-item_list-price')?></b> : <?=currency_format($quote_item->price)?>
                            | <b><?=__('quote-item_discount')?></b> : [<?= number_format($quote_item->discount->parts[0],2) ?>] [<?= number_format($quote_item->discount->parts[1],2) ?>] [<?= number_format($quote_item->discount->parts[2],2) ?>] [<?= number_format($quote_item->discount->parts[3],2) ?>] [<?= number_format($quote_item->discount->parts[4],2) ?>]
                            | <b><?=__('quote-item_cost-per-unit')?></b> : <?= currency_format($cost) ?>
                            | <b>Ext</b> : <?= currency_format($ext) ?>
                            | <b>Exp</b> : <?=currency_format($expedition_price);?>
                            (<?php
                            if ($expeditionPriceMethod == "global") { echo $expeditionPrice.__('quote-pdf_exp-global'); }
                            if ($expeditionPriceMethod == "unit") { echo $expeditionPrice.__('quote-pdf_exp-unit'); }
                            if ($expeditionPriceMethod == "cost") { echo $expeditionPrice.__('quote-pdf_exp-cost'); }
                            if ($expeditionPriceMethod == "list") { echo $expeditionPrice.__('quote-pdf_exp-list'); }
                            ?>)
                            | <b>GP</b> : <?=$gp?>%
                          </td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
                <tr>
                    <td class="big-border" width="85%" colspan="<?=$showItemImage?5:4?>" align="right"><b><?=__('quote_subtotal')?>&nbsp;&nbsp;</b> </td>
                    <td class="big-border" width="15%" align="right"><?= currency_format($section_subtotal); ?><br>&nbsp;</td>
                </tr>

         <?php endforeach; ?>
        </tbody>
    </table>
    <br>
    <table>
        <tr>
            <td class="no-border">
                <table class="quote-total" >
                    <tr>
                        <td width="85%" align="right"><b><?=__('quote_subtotal-items')?></b></td>
                        <td width="15%" align="right"> <?= currency_format($subTotal); ?><br>&nbsp;</td>
                    </tr>

                    <?php if (!empty($discount) && $discount > 0) : ?>
                        <tr>
                            <td align="right"><?=__('quote_discount')?></td>
                            <th width="100" align="right"><?= currency_format($discount); ?><br>&nbsp;</th>
                        </tr>
                    <?php endif ?>

                    <?php foreach ($taxes as $tax): ?>

                      <tr>
                          <td align="right"><b><?=$tax->valueDescription?></b></td>
                          <td width="100" align="right"> <?= currency_format($tax->value * $subTotal); ?><br>&nbsp;</td>
                      </tr>

                    <?php endforeach ?>

                    <?php if (!empty($deposit) && $deposit > 0) : ?>
                        <tr>
                            <td align="right"><b><?=__('quote_deposit')?>:</b></td>
                            <td width="100" align="right"> <?= currency_format($deposit); ?><br>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right"><b><?=__('quote_total-to-pay')?></b></td>
                            <td width="100" align="right"> <?= currency_format($total); ?><br>&nbsp;</td>
                        </tr>
                    <?php else : ?>
                        <tr>
                            <td align="right"><b>Total</b></td>
                            <td width="100" align="right"> <?= currency_format($total); ?><br>&nbsp;</td>
                        </tr>
                    <?php endif; ?>

                    <tr>
                        <td colspan="2" style="font-size:10px" align="right"><?=__('quote_price-shown-cad')?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


    <?php if($isAdmin):

        $overview = calculate_quote_overview($quote);
        $discountOverviews = calculate_quote_discount_overview($quote);
        $sectionOverviews = calculate_quote_section_overview($quote);

        ?>

        <pagebreak />

        <div class="admin-overview">
            <table width="100%">
                <tr>
                    <th align="left" width="30%"><?=__('quote-pdf_admin-overview')?></th>
                    <?php if($discountOverviews): ?><th align="left" width="30%"><?=__('quote-pdf_price-lists')?></th><?php endif; ?>
                    <?php if($sectionOverviews): ?><th align="left" width="30%"><?=__('quote-pdf_tags')?></th><?php endif; ?>
                </tr>

                <tr>
                    <td>
                        <b><?=__('quote-pdf_overall-gp')?>:</b> <?= $overview->gp ?><br>
                        <b><?=__('quote-pdf_overall-cost')?>:</b> <?= $overview->cost ?><br>
                        <b><?=__('quote-pdf_overall-profit')?>:</b> <?= $overview->profit ?>
                    </td>

                    <td>
                        <?php if($discountOverviews): ?>
                            <?php foreach($discountOverviews as $discountOverview): ?>
                                <b><?=$discountOverview->name?></b><br>
                                <?=__('quote-pdf_gp')?>: <?= $discountOverview->gp ?><br>
                                <?=__('quote-pdf_cost')?>: <?= $discountOverview->cost ?><br>
                                <?=__('quote-pdf_profit')?>: <?= $discountOverview->profit ?>
                                <br><br>
                            <?php  endforeach; ?>
                        <?php endif; ?>
                    </td>

                    <td>
                        <?php if($sectionOverviews): ?>
                            <?php foreach($sectionOverviews as $sectionOverview): ?>
                                <b><?=$sectionOverview->title?></b><br>
                                <?=__('quote-pdf_gp')?>: <?= $sectionOverview->gp ?><br>
                                <?=__('quote-pdf_cost')?>: <?= $sectionOverview->cost ?><br>
                                <?=__('quote-pdf_profit')?>: <?= $sectionOverview->profit ?>
                                <br><br>
                            <?php  endforeach; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </div>

        <?php if($data->additionnalFees):?>
        <div class="admin-overview">
            <h3><?=__('quote-pdf_other-costs')?></h3>
            <table>
                <?php foreach($data->additionnalFees as $fee): ?>
                    <tr>
                        <td>
                            <b><?=$fee->title?>: </b><?=currency_format($fee->value)?><br>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>

    <?php endif; ?>


    <pagebreak />

    <div class="conditions">
        <?=__('quote-pdf_conditions')?>
    </div>

    <?php if (!$isSoumission):?>

        <div class="quote-terms">

            <?=$config['contract_terms']?>

            <p>Fait lu, et signé à ____________________________________________, le ____________________________________________ <?=date('Y')?></p>

            <p>Nom : ____________________________________________</p>

            <p>Signature : ____________________________________________</p>

            <p>Signature du conseiller : ____________________________________________</p>

        </div>

    <?php endif ?>

    <?php /*<div class="quote-footer">
        <strong>Ugoburo &copy;<?=date('Y')?></strong>
    </div> */ ?>

</body>

</html>

<?php

    // Now collect the output buffer into a variable
    $quote_html = ob_get_contents();
    ob_end_clean();

    //exit($quote_html);
