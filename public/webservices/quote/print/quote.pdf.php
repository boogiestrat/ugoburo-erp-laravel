<?php 
    
    require dirname(dirname(dirname(dirname(__DIR__)))) . '/app/init.php';

    include('quote.tpl.pdf.php');

    $mpdf = new \Mpdf\Mpdf([
        'mode' => 'utf-8', 
        'format' => 'Legal', 
        'orientation' => 'P',
        'tempDir' => sys_get_temp_dir()
    ]);

    // Encrypt the file and grant permissions to the user to copy and print
    // No password is required to open the document
    // Owner has full rights using the password
    $mpdf->SetProtection(array('copy','print'), '', PDF_PASSWORD, 128);

    // send the captured HTML from the output buffer to the mPDF class for processing
    $mpdf->WriteHTML($quote_html);
    $mpdf->Output();