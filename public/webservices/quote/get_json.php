<?php
$debug = true;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##

if(!empty($_POST['id'])) {
  if ($res = get_quote_json($_POST['id'])) {
    print(json_encode($res));
  } else {
    $json = array('code' => 500, 'msg' => "Error - Empty quote!");
    print_r(json_encode($json));
  }
} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
