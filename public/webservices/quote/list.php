<?php

    $debug = false;
    require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

    ## TODO : Need global security validation ##

    $quotes = get_all_quotes();

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $quotes = json_encode($quotes, JSON_PRETTY_PRINT);
    else
        $quotes = json_encode($quotes);

    print($quotes);
