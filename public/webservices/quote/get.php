<?php
$debug = false;
require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##

if(!empty($_POST['id']) && is_numeric($_POST['id'])) {

    $quote = get_quote($_POST['id']);

    if (isset($_ENV['DEBUG']) && $_ENV['DEBUG'] == 'On')
        $quote = json_encode($quote, JSON_PRETTY_PRINT);
    else
        $quote = json_encode($quote);

    print($quote);

} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
