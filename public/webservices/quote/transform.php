<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##

// Query the quote
if(!empty($_POST['id']) && is_numeric($_POST['id'])) {

    $quote = get_quote($_POST['id']);

} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}

$_POST['name'] = $quote['beneficiary_last_name'] . ', ' . $quote['beneficiary_first_name'];
$_POST['note'] = "Project créé à partir de la soumission #" . $quote['id'];
$_POST['budget'] = $quote['sub_total'];
$_POST['fk_type_id'] = 1;
$_POST['fk_client_id'] = $quote['fk_client_id'];
$_POST['fk_beneficiary_id'] = $quote['fk_beneficiary_id'];
$_POST['fk_employee_id'] = $quote['created_fk_employee_id'];
$_POST['fk_status_id'] = 2;


if(!empty($_POST['name'])) {
    if ($projectId = create_project($_POST)) {
        //add_project_quote($projectId, $quote['id']);
        add_project_items($projectId, $quote['id']);
        add_project_tasks($projectId, $quote['id']);
        $json = array('code' => 200, 'msg' => "Project created!", 'data' => $projectId);
        print_r(json_encode($json));
    } else {
        $json = array('code' => 500, 'msg' => "Error - Could not create project!");
        print_r(json_encode($json));
    }
} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}

