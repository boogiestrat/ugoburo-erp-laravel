<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##

if(!empty($_POST['client_id'])) {

    $quote_id = new_customer_quote($_POST['client_id']);

    $quote = get_quote($quote_id);

    // https://dev.ugo.com/ventes/soumission/?id=2019120001

    $json = array('code' => 200, 'msg' => "Quote created!", "quote_number" => $quote["number"]);

    print_r(json_encode($json));

} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
