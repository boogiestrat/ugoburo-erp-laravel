<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

## TODO : Need global security validation ##

// Update only status
if (isset($_GET['params']) && $_GET['params'] == 'status') {
    if (!isset($_POST['id']) || !isset($_POST['fk_status_id'])) {
        $json = array('code' => 500, 'msg' => "Missing parameter.");
        print(json_encode($json));
        exit();
    }

    if (!$res = update_quote_status($_POST['id'], $_POST['fk_status_id'])) {
        $json = array('code' => 500, 'msg' => "Can't update selected quote.");
        print(json_encode($json));
        exit();
    }

    $json = array('code' => 200, 'msg' => "Quote status updated!", 'data' => $res);
    print_r(json_encode($json));
    exit();
}

if (isset($_POST['fk_client_id'])) { // Existing client
    $fk_client_id = $_POST['fk_client_id'];
} elseif (isset($_POST['client_first_name']) && isset($_POST['client_last_name'])) { // Create Client
    $data = extract_prefixed_data($_POST, 'client_');
    if (!$fk_client_id = create_client($data)) {
        $json = array('code' => 500, 'msg' => "Could not create client.");
        print(json_encode($json));
        exit();
    }
} else {
    $json = array('code' => 500, 'msg' => "Missing parameter.");
    print(json_encode($json));
    exit();
}

// Beneficiary  fk_beneficiary_id
$data = array();

if (isset($_POST['fk_beneficiary_id'])) { // Existing beneficiary
    $fk_beneficiary_id = $_POST['fk_beneficiary_id'];
} elseif (isset($_POST['beneficiary_first_name']) && isset($_POST['beneficiary_last_name'])) { // New beneficiary
    $data = extract_prefixed_data($_POST, 'beneficiary_');
    if (!$fk_beneficiary_id = create_beneficiary($data)) {
        $json = array('code' => 500, 'msg' => "Could not create beneficiary.");
        print(json_encode($json));
        exit();
    }
} else {
    $json = array('code' => 500, 'msg' => "Missing parameter.");
    print(json_encode($json));
    exit();
}

// Death Certificate  TODO
//create_death_certificate

// QUOTE
$_POST['fk_client_id'] = $fk_client_id;
$_POST['fk_beneficiary_id'] = $fk_beneficiary_id;
$_POST['sub_total'] = parseCADCurrency($_POST['sub_total']);
$_POST['discount'] = parseCADCurrency($_POST['discount']);
$_POST['tps'] = parseCADCurrency($_POST['tps']);
$_POST['tvq'] = parseCADCurrency($_POST['tvq']);
$_POST['total'] = parseCADCurrency($_POST['total']);
$_POST['deposit'] = parseCADCurrency($_POST['deposit']);
$_POST['balance'] = parseCADCurrency($_POST['balance']);

if(!empty($_POST['id']) && is_numeric($_POST['id'])) {
    if ($result = update_quote($_POST)) {
        $json = array('code' => 200, 'msg' => "Quote updated!");
        print_r(json_encode($json));
    } else {
        $json = array('code' => 500, 'msg' => "Error - Could not update the quote!");
        print_r(json_encode($json));
    }
} else {
    $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
    print_r(json_encode($json));
}
