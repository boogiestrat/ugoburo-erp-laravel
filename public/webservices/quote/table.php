<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

$table = 'quote';
$iTable = 0;
$primaryKey = 'id';
$columns = array(
  array( 'db' => 'qs.name', 'dt' => $iTable++, 'formatter' => function($d, $row) {
      return $row['name'];
  }, 'field' => 'status_name' ),
    array( 'sortBy' => 'q.number', 'db' => 'q.number', 'dt' => $iTable++, 'field' => 'number', 'as' => 'number' ),

    array( 'db' => 'q.data_bill_to', 'dt' => $iTable++, 'formatter' => function($d, $row) {
      $client = json_decode($row['data_bill_to']);
      return $client->client->title . ' - ' . $client->contact->first_name . ' ' . $client->contact->last_name;
    }, 'field' => 'client' ),
    array( 'db' => 'q.title', 'dt' => $iTable++, 'formatter' => function($d, $row) {
      return $row['title'];
  }, 'field' => 'title' ),
    array( 'db' => 'q.fk_status_id', 'dt' => $iTable++, 'formatter' => function($d, $row) {
      return "
        <select class=\"form-control form-control-line\" onchange=\"updateQuoteStatusWithStatusId({$row['id']}, this.value)\">
          ". implode('', array_map(function ($s) use ($row) {
            $color = $s['color'] ? $s['color'] : 'default';

            if (get_quote_status_badge($row['fk_status_id'])['id'] === $s['id']) {
              return "<option value=\"{$s['id']}\" selected>{$s['name']}</option>";
            }
            return "<option value=\"{$s['id']}\">{$s['name']}</option>";
            //return "<span class=\"badge badge-$color\" onclick=\"badgeSelectTypeFor('{$s['id']}', '{$s['name']}', '{$s['color']}', '{$row['id']}', this)\" style=\"cursor:pointer;display:inline-block;position:relative;margin:0px 5px\">{$s['name']}</span>";
          }, get_all_quote_status())) ."
        <select>
      ";
    }, 'field' => 'fk_status_id' ),
    array( 'db' => 'q.total', 'dt' => $iTable++, 'formatter' => function($d, $row) {
        return currency_format($row['total']);
    }, 'field' => 'total' ),
    array( 'sortBy' => 'ec.last_name', 'db' => 'concat(ec.last_name, ", ", ec.first_name) as ec_name, ec.email as ec_email', 'dt' => $iTable++, 'formatter' => function($d, $row) {
        return "<a href=\"mailto:'{$row['ec_email']}'\">{$row['ec_name']}</a>";
    }, 'field' => 'created_by' ),
    array( 'db' => 'q.creation', 'dt' => $iTable++, 'field' => 'creation', 'formatter' => function($d, $row) {
      return format_locale_datetime($row['creation']);
    }),
    array( 'db' => 'q.id', 'dt' => $iTable++, 'formatter' => function($d, $row) {

        $html = '<div class="text-center">';
        // $html .= "<a class=\"font-20 mx-2\" href=\"{$_ENV['PATH']}webservices/quote/print/quote.pdf?id={$row['id']}\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i></a>";
        // $html .= "<a class=\"font-20 mx-2 email-btn\" href=\"javascript:;\" data-id=\"{$row['id']}\" data-email=\"{$row['client_email']}\"><i class=\"ti-email\"></i></a>";
        // if (!data.projects) {
            // $html .= "<a class=\"font-20 add-project-quote-btn mx-2\" href=\"javascript:;\" data-id=\"{$row['id']}\"><i class=\"fa fa-folder-open-o\"></i></a>";
        // }

        // $html .= "<a class=\"font-20 copy-project-quote-btn mx-2\" href=\"javascript:;\" data-id=\"{$row['id']}\"><i class=\"fa fa-copy\"></i></a>";
        $clientEmail = json_decode($row["data_bill_to"])->contact->email;
        $html .= "<a class=\"font-20 modify-quote-btn mx-2\" style=\"display:none;\" href=\"../ventes/soumission/load?id={$row['number']}\"><i class=\"ti-pencil\"></i></a>";
        // $html .= "<a class=\"font-20 ordercreate-quote-btn mx-2\" href=\"javascript:;\" data-id=\"{$row['id']}\"><i class=\"fa fa-edit\"></i></a>";
        $html .= "<a class=\"font-20 copy-quote-btn mx-2\" href=\"javascript:;\" data-id=\"{$row['id']}\"><i class=\"fa fa-copy\"></i></a>";

        if (isset($_ENV['DEBUG'])) {
          if ($_ENV['DEBUG'] == "On") {
            $html .= "<a class=\"font-20 promote-quote-btn mx-2\" href=\"javascript:;\" data-id=\"{$row['id']}\"><i class=\"ti-credit-card\"></i></a>";
          }
        }


        $html .= "<a class=\"font-20 send-quote-btn mx-2\" href=\"javascript:;\" data-lang=\"{$row['language']}\" data-email=\"{$clientEmail}\" data-label=\"#{$row['number']}\"  data-total=\"{$row['total']}\" data-id=\"{$row['number']}\" ><i class=\"ti-email\"></i></a>";
        $html .= "<a class=\"font-20 delete-btn mx-2\" href=\"javascript:;\" data-id=\"{$row['id']}\" data-label=\"#{$row['id']}\"><i class=\"ti-trash\"></i></a>";
        $html .= "</div>";
        return $html;
    }, 'field' => 'options' ),

    ['db'=>'language']
);

$joinQuery = "  FROM quote AS q
                LEFT JOIN employee AS ec ON ec.id = q.created_fk_employee_id
                LEFT JOIN quote_status AS qs ON qs.id = q.fk_status_id
                ";

if (user_level(1)) {
  $employeeId = get_current_employee()['id'];
  $joinQuery = "  FROM quote AS q
                  LEFT JOIN employee AS ec ON ec.id = q.created_fk_employee_id
                  LEFT JOIN quote_status AS qs ON qs.id = q.fk_status_id
                  WHERE ( q.created_fk_employee_id = '$employeeId' ) ";
} else {
  $joinQuery = "  FROM quote AS q
  LEFT JOIN employee AS ec ON ec.id = q.created_fk_employee_id
  LEFT JOIN quote_status AS qs ON qs.id = q.fk_status_id
  ";
}
// $joinQuery .= " ORDER BY q.creation DESC ";
// print_r($columns);die();
  echo json_encode(
	SSP::simple($_GET, SSP::mysql_connection(), $table, $primaryKey, $columns, $joinQuery)
);
