<?php

    require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';;

    ## TODO : Need global security validation ##

    if(!empty($_POST['id']) && is_numeric($_POST['id'])) {
        $result = delete_quote($_POST['id']);
        $json = array('code' => 200, 'msg' => "Quote deleted.");
        print(json_encode($json));
    } else {
        $json = array('code' => 500, 'msg' => "Error - Missing parameters!");
        print_r(json_encode($json));
    }
