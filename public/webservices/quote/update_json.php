<?php

require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_GET['id'])) {
  print(json_encode(['valid' => false]));
  exit();
}

$quote = json_decode(file_get_contents('php://input'), true);

if ($_GET['id'] != '-1') {
  $update = update_quote_json($quote);

  if ($update) {
    print(json_encode(['valid' => true, 'data' => $update]));
  } else {
    print(json_encode(['valid' => false]));
  }
} else {
  $newId = create_quote_json($quote);

  print(json_encode(['valid' => true, 'id' => $newId]));
}
