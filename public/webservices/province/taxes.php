<?php

  require dirname(dirname(dirname(__DIR__))) . '/old/app/init.php';

  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  if (!isset($_REQUEST['province'])) {
    echo json_encode(['error' => 'missing parameters `province`', 'done' => true]);
    exit;
  }

  $taxes = fetchTaxByProvinceString($_REQUEST['province']);

  echo json_encode(['done' => true, 'data' => $taxes]);
  exit;
