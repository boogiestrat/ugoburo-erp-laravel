<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model
{
    public function order()
    {
        return $this->belongsTo('App\Order', 'fk_order_id');
    }
}
