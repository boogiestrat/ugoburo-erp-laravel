<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';

    protected $fillable = [
        'fk_client_id',
        'titre',
        'address',
        'city',
        'province',
        'country',
        'postal_code',
        'is_default_facturation',
        'is_default_shipping'
    ];

    public function client() {
        return $this->belongsTo('App\Client');
    }
}
