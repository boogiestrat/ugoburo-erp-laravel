<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralId extends Model
{
    protected $table = 'general_id';
}
