<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $table = 'discount';

    public function manufacturier() {
        return $this->belongsTo('App\Manufacturier', 'fk_manufacturier_id');
    }
}
