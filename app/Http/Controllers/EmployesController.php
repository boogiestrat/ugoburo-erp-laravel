<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class EmployesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employes = User::all();
  
        return view('pages.employes.index',compact('employes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.employes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
        ]);

        $employe = User::create($request->all());
        
        toastr()->success('Employee created successfully');

        return redirect()->route('employes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $employee = auth()->user();

        return response()->json([
            'data' => $employee
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employe = User::where('id', $id)->first();

        return view('pages.employes.edit',compact('employe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $employe)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required'
        ]);
  
        $employe->update($request->all());

        toastr()->success('Employee updated successfully');

        return redirect()->route('employes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $employe)
    {
        $employe->delete();
  
        toastr()->success('Employee deleted successfully');

        return redirect()->route('employes.index');
    }

    /**
     * API endpoint to retrieve a user
     *
     * @param  int  $kw
     * @return \Illuminate\Http\Response
     */
    public function search($searchTerm) 
    {
        if( $searchTerm == 'all' ) {
            $employes = User::all();
    
            return response()->json([
                'data' => $employes
            ]);
        }

        $employe = User::query()
                        ->where('first_name', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('last_name', 'LIKE', "%{$searchTerm}%") 
                        ->orWhere('email', 'LIKE', "%{$searchTerm}%") 
                        ->get();

        return response()->json([
            'data' => $employe
        ]);
    }
}
