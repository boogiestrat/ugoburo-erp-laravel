<?php

namespace App\Http\Controllers;

use Auth;

use App\Quote;
use Carbon\Carbon;
use App\GeneralId;
use Illuminate\Http\Request;

class QuotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // List quotes
        $quotes = Quote::All();

        return response()->json([
            'data' => $quotes
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        // List quotes
        $quotes = Quote::take(10)->get();

        return view('pages.soumissions.index',compact('quotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getNow = Carbon::now();
        $getNowYear = Carbon::now()->format('Y');
        $getMonth = Carbon::now()->format('mm');
        $date = Carbon::now()->format('Ymm');

        $quoteId = $this->nextId($date);
        $employeeId = Auth()->user()->id;

        return view('pages.soumissions.edit',compact('quoteId', 'employeeId'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quote = Quote::where('number', $id)->first();

        return $quote->data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.soumissions.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $quote = Quote::where('id', $id)->first();

        $quote->update($request->all());
    }

    public function updatejson($id)
    {
        $quote = json_decode(file_get_contents('php://input'), true);

        if ($id != '-1') {
            $quoteResult = Quote::where('id', $id)->first();
            $update = $quoteResult->update($quote);

            if ($update) {
                return response()->json([
                    'data' => $update
                ]);
            } 

            return response()->json([
                'valid' => false
            ]);
        } else {
            $newId = $this->create_quote_json($quote);
            return response()->json([
                'did' => $newId
            ]);
        }
    }

    public function create_quote_json($quote) 
    {
        $quoteResult = Quote::where('number', $quote['number'])->first();

        if (!empty($quoteResult)) {
            $quoteResult->update($quote);
        } else {
          $quoteObj = new Quote();
          $quoteObj->create($quote);
          return $quoteObj;
        }
    }

    public function nextId()
    {
        $newId = $this->get_next_general_id();
        $this->reserve_general_id($newId);

        return response()->json([
            'id' => $newId,
            'employee' => Auth::id()
        ]);
    }

    function get_next_general_id() 
    {
        $date = date("Ym");
        $result = GeneralId::orderBy('project_id', 'DESC')->first()->toArray();
      
        if (empty($result)) {
          $result['project_id'] = 0001;
        } else {
          $result['project_id'] = substr($result['project_id'], -4);
        }
      
        return $date . sprintf("%04d", ($result['project_id'] + 1));
      }
      
    function reserve_general_id($id) 
    {
        $generalId = new GeneralId;
        $generalId->project_id = $id;
        $generalId->save();
      
        return $generalId;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
