<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Client;
use App\Contact;
use App\Address;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::take('20')->get();
  
        return view('pages.clients.index', compact('clients'));
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employes = User::all();

        return view('pages.clients.create')->with('employes', $employes);
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'entreprise' => 'required',
            'client_type_id' => 'required',
        ]);

        $request->fk_created_by_id = Auth::id();
  
        $client = Client::create($request->all());

        if($request->contacts) {
            foreach($request->contacts as $contact) {
                $client->contacts()->create($contact);
            }
        }

        if($request->addresses) {
            foreach($request->addresses as $address) {
                $client->addresses()->create($address);
            }
        }

        if($request->permissions) {
            foreach($request->permissions as $permission) {
                $user = User::where('id', $permission)->first();
                $client->permissions()->attach($user);
            }
        }
   
        toastr()->success('Client created successfully');

        return redirect()->route('clients.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeClient(Request $request)
    {
        $request->validate([
            'entreprise' => 'required',
            'client_type_id' => 'required',
        ]);
  
        $client = Client::create($request->all());

        return response()->json([
            'code' => 200, 
            'msg' => "Client created!",
            'data' => $client
        ]);
    }
   
    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $client = Client::where('id', $request->id)
                        ->with('addresses')
                        ->with('contacts')
                        ->first();

        return response()->json([
            'data' => $client
        ]);
    }

    /**
     * Display the searched resource(s).
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $client = Client::query()
                        ->where('entreprise', 'LIKE', "%{$request->q}%")
                        ->with('addresses')
                        ->with('contacts')
                        ->get();

        return response()->json([
            'data' => $client
        ]);
    }

    /**
     * Display the searched resource(s).
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchAddress(Request $request)
    {
        $client = Address::query()
                        ->where('address', 'LIKE', "%{$request->q}%")
                        ->get();

        return response()->json([
            'data' => $client
        ]);
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        $client->load('contacts');
        $client->load('addresses');

        $employes = User::all();

        return view('pages.clients.edit',compact('client'))->with('employes', $employes);
    }
  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $request->validate([
            'entreprise' => 'required',
            'client_type_id' => 'required',
        ]);
  
        $client->update($request->all());

        if($request->contacts) {
            foreach($request->contacts as $contact) {
                if(isset($contact['id'])) {
                    $client->contacts()->where('id', $contact['id'])->update($contact);
                } else {
                    $client->employee_id = Auth::id();
                    $client->contacts()->create($contact);
                }
            }
        }

        if($request->address) {
            foreach($request->addresses as $address) {
                if(isset($address['id'])) {
                    $client->addresses()->where('id', $address['id'])->update($address);
                } else {
                    $client->employee_id = Auth::id();
                    $client->addresses()->create($address);
                }
            }
        }

        if($request->permissions) {
            foreach($request->permissions as $permission) {
                $user = User::where('id', $permission)->first();
                $client->permissions()->attach($user);
            }
        }

        toastr()->success('Client updated successfully');

        return redirect()->route('clients.index');
    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $Client)
    {
        $Client->delete();
  
        toastr()->success('Client deleted successfully');

        return redirect()->route('clients.index');
    }

    public function storeContact(Request $request)
    {
        if( $request->fk_client_id ) {
            $client = Client::where('id', $request->fk_client_id);
            $contact = $client->contacts()->create($request);
        } else {
            $lastClientId = Client::orderBy('id', 'DESC')->first()->pluck('id');
            $request->fk_client_id = $lastClientId ++;
            $contact = Contact::create($request->all());
        }

        $contactId = $contact->id;

        return response()->json([
            'data' => $contactId,
        ]);
    }
}
