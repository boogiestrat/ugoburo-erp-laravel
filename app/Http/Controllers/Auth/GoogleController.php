<?php
  
namespace App\Http\Controllers\Auth;
  
use App\Http\Controllers\Controller;
use Socialite;
use Auth;
use Exception;
use App\User;
  
class GoogleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
      
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback()
    {
        try {
    
            $user = Socialite::driver('google')->user();
     
            $finduser = User::where('email', $user->getEmail())->first();
     
            if($finduser){
     
                Auth::login($finduser);

                $finduser->last_connection = now();
                $finduser->save();
    
                return redirect('/');
     
            }else{

                return redirect('/login');
                
            }
    
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}