<?php

namespace App\Http\Controllers;

use App\Tax;
use App\Province;
use Illuminate\Http\Request;

class ProvincesController extends Controller
{
    public function taxes(Request $request)
    {
        if($request->province != '') {
            $province = Province::where('province_name', $request->province)->first();
            $taxes = Tax::where('fk_province_id', $province->id)->get();

            return response()->json([
                'done' => true,
                'data' => $taxes
            ]);
        } else {
            return response()->json([
                'done' => false,
                'data' => []
            ]);
        }
    }
}
