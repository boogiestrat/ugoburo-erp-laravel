<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturier extends Model
{
    protected $table = 'manufacturier';

    public function discounts()
    {
        return $this->hasMany('App\Discount', 'fk_manufacturier_id');
    }
}
