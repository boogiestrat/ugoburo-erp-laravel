<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'client_id',
        'employee_id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'cell'
    ];

    public function client() {
        return $this->belongsTo('App\Client');
    }
}
