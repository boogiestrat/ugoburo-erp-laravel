<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    public function order()
    {
        return belongsToMany('App\Order', 'fk_status_id');
    }
}
