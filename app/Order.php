<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function billingAddress()
    {
        return $this->hasOne('App\OrderAddress', 'fk_order_id')->where('type', 'bill_to');
    }

    public function shippingAddress()
    {
        return $this->hasOne('App\OrderAddress', 'fk_order_id')->where('type', 'ship_to');
    }

    public function status()
    {
        return $this->belongsTo('App\OrderStatus', 'fk_status_id');
    }
}
