<?php

namespace App;

use App\Processors\AvatarProcessor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $appends = [
        'created',
        'title'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'file_id',
        'file'
    ];

    protected $fillable = [
        'entreprise',
        'is_active',
        'client_type_id',
        'employe_id',
        'notes',
        'file_id'
    ];

    public function contacts() {
        return $this->hasMany('App\Contact');
    }

    public function defaultContact() {
        return $this->contacts()->where('is_default', '0')->first();
    }

    public function addresses() {
        return $this->hasMany('App\Address', 'fk_client_id');
    }

    public function defaultAddress() {
        return $this->addresses()->where('is_default_facturation', '1')->first();
    }

    public function permissions() {
        return $this->belongsToMany('App\User', 'employee_clients', 'fk_client_id', 'fk_employee_id');
    }

    public function file() {
        return $this->belongsTo(File::class);
    }

    public function getAvatarFilenameAttribute() {
        if (!empty($this->file)) {
            return $this->file->name;
        }

        return null;
    }

    public function getAvatarAttribute() {
        return AvatarProcessor::get($this);
    }

    public function getCreatedAttribute() {
        if (empty($this->created_at)) {
            return null;
        }

        return $this->created_at->toFormattedDateString();
    }

    public function getTitleAttribute() {
        return $this->entreprise;
    }

    public function getCreatedMmDdYyyyAttribute() {
        if (empty($this->created_at)) {
            return null;
        }

        return $this->created_at->format('m-d-Y');
    }

    public function setCreatedDateAttribute( $value ) {
        try {
            $this->attributes['created_at'] = new Carbon($value);
        } catch (\Exception $exception) {
            $this->attributes['created_at'] = now();
        }
    }
}
